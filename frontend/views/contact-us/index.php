<?php
/* @var $this yii\web\View */

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = Yii::$app->name . ": " . Yii::$app->controller->id;
?>
<?php
if ( ! empty($contactUsDatas)) {
    foreach ($contactUsDatas as $contactUsData) {
        ?>
        <!-- subheader begin -->
        <section id="subheader" data-speed="8" data-type="background" style="background-image:url('<?= Url::to(Yii::$app->urlManagerBackend->baseUrl . $contactUsData['contact_us_feature_image']['full'], TRUE); ?>')">
            <div class="layer"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?= $contactUsData['contact_us_heading']; ?></h1>
                        <ul class="crumb">
                            <li><?= Html::a('Home', Url::base(true)); ?></li>
                            <li class="sep">/</li>
                            <li><?= $contactUsData['contact_us_heading']; ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="search-box">
                <input class="search-input" type="text" name="" placeholder="Search Product">
                <a href="javascript:void(0);" class="search-btn"><i class="fa fa-search"></i></a>   
            </div>
        </section>
        <!-- subheader close -->

        <section class="no-top no-bottom" data-bgcolor="#333" style="position: sticky;top: 0px; z-index: 99999;" id="contact-menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 pull-right">
                        <div class="contact-menu" style="background-size: cover;">
                            <ul>
                                <li><?= Html::a('Contact Us', Url::current(['#' => 'contact'])); ?></li>
                                <li><?= Html::a('Terms &amp; Policies', Url::current(['#' => 'terms'])); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
        </section>

        <!-- content begin -->
        <div id="content" class="no-top no-bottom">
            <?php if ( ! empty($contactUsData['contact_us_detail'])) { ?>
                <section data-bgcolor="#f0eae5" class="custom-padding-50">
                    <div class="container">
                        <div class="row">
                            <?php foreach ($contactUsData['contact_us_detail'] as $contact_us_detail) {
                                ?>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                                    <div class="general-contact">
                                        <?= Html::img(Url::to(Yii::$app->urlManagerBackend->baseUrl . $contact_us_detail['icon']['full']), ['class' => 'icon']); ?>
                                        <h4><?= $contact_us_detail['name']; ?></h4>
                                        <?= $contact_us_detail['description']; ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </section>
            <?php } ?>

            <section id="de-map" aria-label="map-container" data-bgcolor="#f0eae5" class="no-top no-bottom">
                <div class="container">
                    <div class="touch-content d-flex" id="contact">
                        <div class="touch-form">
                            <div class="inquiry-form">
                                <?php
                                Pjax::begin(['id' => 'pjax-inquiry-form-submit']);
                                $form = ActiveForm::begin(['id' => 'inquiry-form-submit', 'options' => ['autocomplete' => 'off', 'data-pjax' => true]]);
                                ?>
                                <?php if (Yii::$app->session->hasFlash('success')): ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <h4><i class="icon fa fa-check"></i>Saved!</h4>
                                        <?= Yii::$app->session->getFlash('success') ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (Yii::$app->session->hasFlash('error')): ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <h4><i class="icon fa fa-check"></i>Saved!</h4>
                                        <?= Yii::$app->session->getFlash('error') ?>
                                    </div>
                                <?php endif; ?>
                                <h3>Send Us An Email</h3>
                                <p class="des">Our beloved team will answer you as soon as possible</p>
                                <div class="inputGroup">    
                                    <label>Name <span class="required">*</span></label>
                                    <?= $form->field($inquiryModel, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Enter your name'])->label(false); ?>
                                    <span class="inputBar"></span>

                                </div>
                                <div class="inputGroup">    
                                    <label>Email <span class="required">*</span></label>  
                                    <?= $form->field($inquiryModel, 'email')->textInput(['maxlength' => true, 'placeholder' => "Enter your email addess"])->label(false); ?>
                                    <span class="inputBar"></span>

                                </div>
                                <div class="inputGroup">     
                                    <label>Phone</label> 
                                    <?= $form->field($inquiryModel, 'phone')->textInput(['maxlength' => true, 'placeholder' => 'Enter your phone'])->label(false); ?>
                                    <span class="inputBar"></span>

                                </div>
                                <div class="inputGroup">   
                                    <label>Message <span class="required">*</span></label> 
                                    <?= $form->field($inquiryModel, 'message')->textarea(['placeholder' => 'Your message here. . .'])->label(false); ?>
                                    <span class="inputBar"></span>

                                </div>
                                <div class="submit" style="text-align: center;">
                                    <?= Html::submitButton('Submit', ['class' => 'color-black btn-custom text-purple font-weight-bold']) ?>
                                </div>
                                <?php
                                ActiveForm::end();
                                Pjax::end();
                                ?>
                            </div>
                        </div>
                        <div class="touch-image">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7531.4921170172875!2d-122.22373756979772!3d37.4701094804544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fa3753a9ffb49%3A0x7263bb6559cbd881!2s123WeddingCards!5e0!3m2!1sen!2sin!4v1604464252696!5m2!1sen!2sin" width="800" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            <!--                            <a href="https://www.google.com/maps?ll=37.469982,-122.21797&z=12&t=m&hl=en&gl=US&mapclient=embed&saddr&daddr=Redwood+Oaks,+Redwood+City,+CA+94061&dirflg=d" target="_blank">
                                                            <img src="https://www.123weddingcards.com/blog/wp-content/uploads/2020/09/map-1.jpg" class="touch-img">
                                                        </a>-->
                        </div>
                    </div>
                </div>
            </section>

            <?php if ( ! empty($contactUsData['contact_us_tc'])) { ?>
                <section data-bgcolor="#f0eae5" class="custom-padding-50" id="terms">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 offset-md-3 text-center">
                                <h1><?= $contactUsData['contact_us_tc']['heading']; ?></h1>
                                <div class="separator"><span><i class="fa fa-square"></i></span></div>
                                <div class="spacer-single"></div>
                            </div>
                            <div class="col-md-12 welcome-text-holder">
                                <?= $contactUsData['contact_us_tc']['description']; ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php } ?>

            <section class="call-to-action text-light padding20" aria-label="cta" data-bgcolor="#ceba84">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-8 col-lg-6 col-md-4 col-sm-4">
                            <h3 class="size-2 no-margin">Stay in the Know</h3>
                        </div>

                        <div class="col-xl-4 col-lg-6 col-md-8 col-sm-8 wc123-1 text-right">
                            <form class="subscribe">
                                <p class="email">
                                    <input name="email" type="text" class="feedback-input" placeholder="Email ID" id="email" />
                                    <a href="contact.html" class="btn-line-white font-weight-bold subscribe-btn">Subscribe</a>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <?php
    }
}
?>
<?php
$this->registerJsFile(
        '@web/js/contact-us.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
