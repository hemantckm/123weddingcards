<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\widgets\Menu;

$navClass = array ();
if (in_array(Yii::$app->controller->action->controller->id, ['site'])) {
    $navClass['nav_class'] = 'menu_data';
} else if (in_array(Yii::$app->controller->action->controller->id, ['faq', 'contact-us', 'error'])) {
    $navClass['nav_class'] = '';
}
?>
<nav class="<?= $navClass['nav_class']; ?>">
    <?php if (Yii::$app->controller->action->controller->id != 'error') { ?>
        <?=
        Menu::widget([
            'options' => ['id' => 'mainmenu'],
            'items' => [
                ['label' => 'Home', 'url' => ['/']],
                ['label' => 'Invitations', 'url' => ['/']],
                ['label' => 'Stationery', 'url' => ['/']],
                ['label' => 'Blog', 'url' => ['/']],
                ['label' => 'Get In Touch', 'url' => ['/contact-us']],
                ['label' => '<i class="fa fa-shopping-cart"></i> Cart', 'url' => ['cart.html']],
                ['label' => '<i class="fa fa-heart"></i> Wishlist', 'url' => 'javascript:void(0);', 'options' => ['id' => 'de-selector']],
                ['label' => '<i class="fa fa-sign-in"></i>', 'url' => 'javascript:void(0);', 'options' => ['title' => 'Login', 'class' => 'login', 'data-toggle' => 'modal', 'data-target' => '#exampleModal']],
            ],
            'encodeLabels' => false, //allows you to use html in labels
            'activateParents' => true
        ]);
        ?>
    <?php } else { ?>
        <?=
        Menu::widget([
            'options' => ['id' => 'mainmenu'],
            'items' => [
                ['label' => 'Home', 'url' => ['/']],
                ['label' => 'Invitations', 'url' => ['/']],
                ['label' => 'Stationery', 'url' => ['/']],
                ['label' => 'Blog', 'url' => ['/']],
                ['label' => 'Get In Touch', 'url' => ['/contact-us']],
            ],
            'encodeLabels' => false, //allows you to use html in labels
            'activateParents' => true
        ]);
        ?>
    <?php } ?>
</nav>
