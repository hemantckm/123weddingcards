<div id="theme-select-wrapper" class="custom-123-4">
    <div id="theme-select">
        <div class="row">
            <div class="col-md-6 offset-md-3 text-center">
                <h1>Wishlist</h1>
                <div class="separator"><span><i class="fa fa-square"></i></span></div>
                <div class="spacer-single"></div>
            </div>
        </div>

        <!--item 1 start -->
        <div class="row align-items-center wishlist-corner">
            <div class="col-md-5">
                <img src="https://www.123weddingcards.com/blog/wp-content/uploads/2020/10/Card-2.png" class="img-responsive" alt="">
            </div>

            <div class="col-md-7">
                <div class="text">
                    <p class="title font-weight-bold text-white">Atomic Tangerine</p>
                    <p class="price font-weight-bold"> 
                        <i class="fa fa-usd"></i>1.82 
                    </p>
                </div>

                <div class="wishlist-button">
                    <a href="javascript:void(0)" class="btn-custom text-white font-weight-bold text-purple">Add to Cart</a>
                    <a href="javascript:void(0)" class="btn-custom text-white font-weight-bold bg-grey">Remove</a>
                </div>
            </div>
        </div>
        <!--item 1 close -->


        <!--item 1 start -->
        <div class="row align-items-center wishlist-corner">
            <div class="col-md-5">
                <img src="https://www.123weddingcards.com/blog/wp-content/uploads/2020/10/Card-4.png" class="img-responsive" alt="">
            </div>

            <div class="col-md-7">
                <div class="text">
                    <p class="title font-weight-bold text-white">Atomic Tangerine</p>
                    <p class="price font-weight-bold"> 
                        <i class="fa fa-usd"></i>1.82 
                    </p>
                </div>

                <div class="wishlist-button">
                    <a href="javascript:void(0)" class="btn-custom text-white font-weight-bold text-purple">Add to Cart</a>
                    <a href="javascript:void(0)" class="btn-custom text-white font-weight-bold bg-grey">Remove</a>
                </div>
            </div>
        </div>
        <!--item 1 close -->


        <!--item 1 start -->
        <div class="row align-items-center  wishlist-corner">
            <div class="col-md-5">
                <img src="https://www.123weddingcards.com/blog/wp-content/uploads/2020/10/Card-7.png" class="img-responsive" alt="">
            </div>

            <div class="col-md-7">
                <div class="text">
                    <p class="title font-weight-bold text-white">Atomic Tangerine</p>
                    <p class="price font-weight-bold"> 
                        <i class="fa fa-usd"></i>1.82 
                    </p>
                </div>

                <div class="wishlist-button">
                    <a href="javascript:void(0)" class="btn-custom text-white font-weight-bold text-purple">Add to Cart</a>
                    <a href="javascript:void(0)" class="btn-custom text-white font-weight-bold bg-grey">Remove</a>
                </div>
            </div>
        </div>
        <!--item 1 close -->
    </div>
</div>