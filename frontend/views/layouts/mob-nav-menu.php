<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\widgets\Menu;
?>
<div class="mobile-nav">
    <ul class="nav-mobile">
        <li class="small-logo">
            <a href="https://www.123weddingcards.com/"> 
                <img src="https://ik.imagekit.io/weddingcards/123_mobilelogo_8xRVM4Vtx.png" alt="123WeddingCards"> 
            </a>
        </li> 

        <li class="mobile-icon">
            <a href="cart.html" title="Cart"><i class="fa fa-shopping-cart"></i></a>
            <a href="javascript:void(0);" title="Wishlist"><i class="fa fa-heart"></i></a>
            <a href="Javascript:void(0);" title="Login" class="login" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-sign-in"></i> </a>
        </li>
        <li class="menu-container"> 
            <input id="menu-toggle" type="checkbox">
            <label for="menu-toggle" class="menu-button">  
                <svg class="icon-open" viewBox="0 0 24 24"><path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path></svg>
                <svg class="icon-close" viewBox="0 0 100 100">
                <path d="M83.288 88.13c-2.114 2.112-5.575 2.112-7.69 0L53.66 66.188c-2.113-2.112-5.572-2.112-7.686 0l-21.72 21.72c-2.114 2.113-5.572 2.113-7.687 0l-4.693-4.692c-2.114-2.114-2.114-5.573 0-7.688l21.72-21.72c2.112-2.115 2.112-5.574 0-7.687L11.87 24.4c-2.114-2.113-2.114-5.57 0-7.686l4.842-4.842c2.113-2.114 5.57-2.114 7.686 0l21.72 21.72c2.114 2.113 5.572 2.113 7.688 0l21.72-21.72c2.115-2.114 5.574-2.114 7.688 0l4.695 4.695c2.112 2.113 2.112 5.57-.002 7.686l-21.72 21.72c-2.112 2.114-2.112 5.573 0 7.686L88.13 75.6c2.112 2.11 2.112 5.572 0 7.687l-4.842 4.84z"/>
                </svg> 
            </label> 

            <?=
            Menu::widget([
                'options' => ['class' => 'menu-sidebar'],
                'items' => [
                    ['label' => '<i class="fa fa-home"></i> Home', 'url' => ['/']],
                    ['label' => 'Invitations', 'url' => ['product-category']],
                    ['label' => 'Stationery', 'url' => ['stationery']],
                    ['label' => 'Blog', 'url' => ['http://123weddingcards.com/blog']],
                    ['label' => 'Get In Touch', 'url' => ['contact-us.html']],
                ],
                'encodeLabels' => false, //allows you to use html in labels
                'activateParents' => true
            ]);
            ?>
        </li>
    </ul>
</div>
