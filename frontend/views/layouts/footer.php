<?php

use yii\helpers\Html;
use common\models\SocialMedia;
use yii\helpers\Url;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!-- footer begin -->
<footer>
    <div class="subfooter">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-7 col-md-12">
                    <p>&copy; 2003-<?= date('Y'); ?> <?= Yii::$app->name; ?> an ISO 9001 certified company. All rights reserved.</p>
                    <!-- Getting Social Media Data -->
                    <?php
                    $socialMediaModels = SocialMedia::findAll(['status' => SocialMedia::STATUS_ACTIVE]);
                    if ( ! empty($socialMediaModels)) {
                        ?>
                        <div class="social-icons">
                            <?php
                            foreach ($socialMediaModels as $socialMediaModel) {
                                echo Html::a('<i class="fa fa-' . $socialMediaModel['data'] . ' fa-lg"></i>', $socialMediaModel['link'], ['target' => '_blank', 'title' => $socialMediaModel['name']]);
                            }
                            ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-xl-6 col-lg-5 col-md-12 text-right">
                    <div class="footer-menu">
                        <ul>
                            <li><?= Html::a('FAQ', Url::toRoute('faq/')); ?></li>
                            <li><?= Html::a('Terms & Policy', Url::toRoute(['/contact-us', '#' => 'terms'])); ?></li>
                            <li><a href="announcement.html">Announcements</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    <a href="#" id="back-to-top"></a>-->
</footer>
<!-- footer close -->

<!--Login Modal Start -->
<?=
$this->render('/site/login');
?>
<!--Login Modal End -->