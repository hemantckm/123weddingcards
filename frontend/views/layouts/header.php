<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use backend\helpers\Html;
use yii\helpers\Url;
use common\models\Logo;

$headerClass = array ();
if (in_array(Yii::$app->controller->action->controller->id, ['site'])) {
    $headerClass['header_class']     = 'menu';
    $headerClass['header_container'] = 'test';
} else if (in_array(Yii::$app->controller->action->controller->id, ['faq', 'contact-us', 'error'])) {
    $headerClass['header_class']     = 'header-bg';
    $headerClass['header_container'] = 'container';
}
?>
<!-- header begin Desktop -->
<header class="<?= $headerClass['header_class']; ?>">
    <div class="<?= $headerClass['header_container']; ?>">
        <div class="row">
            <div class="col-md-12">
                <!-- logo begin -->
                <?php
                $logoModel = Logo::find()->andWhere(['default' => Logo::DEFAULT_ACTIVE, 'status' => Logo::STATUS_ACTIVE])->one();
                if ( ! empty($logoModel)) {
                    ?>
                    <div id="logo">
                        <?= Html::a(Html::img(Url::to(Yii::$app->urlManagerBackend->baseUrl . '/' . Logo::MAIN_DIRECTORY . Logo::ORIGINAL_FILEPATH . $logoModel->image, TRUE), ['alt' => $logoModel->heading]), Url::to(['/'])); ?>
                    </div>
                <?php } ?>
                <!-- logo close -->

                <!-- small button begin -->
                <span id="menu-btn"></span>
                <!-- small button close -->

                <!-- mainmenu begin -->
                <?= $this->render('main-menu'); ?>
                <!-- mainmenu close -->

                <!-- Wishlist begin -->
                <?= $this->render('wishlist'); ?>
                <!-- Wishlist close -->

                <!-- click button begin -->
                <a href="javascript:void(0);" class="opner">
                    <span></span><span></span><span></span>
                </a>
                <!-- click button close -->
            </div>
        </div>
    </div>
</header>
<!-- header close Desktop -->

<!-- header Start Ipad, Mobile -->
<?= $this->render('mob-nav-menu'); ?>
<!-- header Close Ipad, Mobile -->
