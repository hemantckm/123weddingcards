<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title                   = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal fade text-dark text-dark modal-box" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content clearfix">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-body">
                <?php $form                          = ActiveForm::begin(['id' => 'login-form']); ?>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
                <h3 class="title"><?= Yii::$app->name; ?></h3>
                <p class="description">Sign in with social media</p>
                <ul class="social">
                    <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                    <li><a href="#"><i class="fa fa-google"></i></a></li>
                </ul>
                <p class="or">OR</p>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Enter your email here...">
                    <span class="input-group-btn">
                        <button class="btn btn-default subscribe" type="button">Continue</button>
                    </span>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php /* <div class="site-login">
  <h1><?= Html::encode($this->title) ?></h1>

  <p>Please fill out the following fields to login:</p>

  <div class="row">
  <div class="col-lg-5">
  <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

  <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

  <?= $form->field($model, 'password')->passwordInput() ?>

  <?= $form->field($model, 'rememberMe')->checkbox() ?>

  <div style="color:#999;margin:1em 0">
  If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
  <br>
  Need new verification email? <?= Html::a('Resend', ['site/resend-verification-email']) ?>
  </div>

  <div class="form-group">
  <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
  </div>

  <?php ActiveForm::end(); ?>
  </div>
  </div>
  </div> */ ?>
