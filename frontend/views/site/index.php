<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Welcome To ' . (Yii::$app->name);
?>
<!-- content begin -->
<div id="content" class="no-bottom no-top">
    <!-- section begin -->
    <?php
    if ( ! empty($bannerModel)) {
        ?>
        <section id="section-welcome-7" class="bg-loop bg-fit-height full-height custom-123-1" style="background-image:url('<?= Url::to(Yii::$app->urlManagerBackend->baseUrl . '/' . common\models\Banner::MAIN_DIRECTORY . common\models\Banner::ORIGINAL_FILEPATH . $bannerModel->image, TRUE); ?>');">
            <div class="center-y text-center">
                <div class="spacer-double"></div>
                <div id="text-carousel" class="owl-carousel owl-theme text-slider style-2 border-deco">
                    <?= $bannerModel['description']; ?>
                </div>
            </div>
        </section>
    <?php } ?>
    <!-- section close -->

    <!--about section begin DESKTOP -->
    <?php
    if ( ! empty($aboutModel)) {
        ?>
        <section id="section-no-bg" class="custom-padding-30 responsive-0">
            <div class="container">
                <div class="row align-items-center about-us">
                    <div class="col-md-6">
                        <?= Html::img(Url::to(Yii::$app->urlManagerBackend->baseUrl . '/' . \common\models\About::MAIN_DIRECTORY . \common\models\About::ORIGINAL_FILEPATH . $aboutModel->image, TRUE), ['alt' => $aboutModel->heading1, 'class' => 'img-responsive', 'style' => 'width: 85%;']); ?>
                    </div>
                    <div class="col-md-6">
                        <h2><?= $aboutModel->heading1; ?></h2>
                        <?= $aboutModel->description1; ?>
                        <h2><?= $aboutModel->heading2; ?></h2>
                        <?= $aboutModel->description2; ?>                    
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <!--about section close DESKTOP-->

        <!--about section begin IPAD, MOBILE -->
        <section id="section-no-bg" class="custom-padding-30 responsive-1 ">
            <div class="container-fluid">
                <div class="row align-items-center about-us">
                    <div class="col-md-6 col-sm-6">
                        <?= Html::img(Url::to(Yii::$app->urlManagerBackend->baseUrl . '/' . \common\models\About::MAIN_DIRECTORY . \common\models\About::ORIGINAL_FILEPATH . $aboutModel->image, TRUE), ['alt' => $aboutModel->heading1, 'class' => 'img-responsive', 'style' => 'width: 90%;']); ?>
                        <h2><?= $aboutModel->heading1; ?></h2>
                        <?= $aboutModel->description1; ?>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <h2><?= $aboutModel->heading2; ?></h2>
                        <?= $aboutModel->description2; ?>
                        <?= Html::img(Url::to(Yii::$app->urlManagerBackend->baseUrl . '/' . \common\models\About::MAIN_DIRECTORY . \common\models\About::ORIGINAL_FILEPATH . $aboutModel->image, TRUE), ['alt' => $aboutModel->heading2, 'class' => 'img-responsive', 'style' => 'width: 90%;']); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    <?php } ?>
    <!--about section close IPAD, MOBILE-->

    <!--catalouge section begin -->
    <section data-bgcolor="#f0eae5" class="custom-padding-30">
        <div class="container-fluid custom-123-3">
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center">
                    <h1>Featured Invitations</h1>
                    <div class="separator"><span><i class="fa fa-square"></i></span></div>
                    <div class="spacer-single"></div>
                </div>

                <div class="clearfix"></div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 s2">
                    <div class="picframe mb-15">
                        <a class="" href="javascript:void(0)">
                            <span class="overlay-v">
                                <span class="pf_text">
                                    <span class="project-name">New Arrivals</span>
                                </span>
                            </span>
                            <img src="https://www.123weddingcards.com/blog/wp-content/uploads/2020/09/4.png" alt="" />
                        </a>                        
                    </div>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 s2">
                    <div class="picframe mb-15">
                        <a class="" href="javascript:void(0)">
                            <span class="overlay-v">
                                <span class="pf_text">
                                    <span class="project-name">Wedding Invitations</span>
                                </span>
                            </span>
                            <img src="https://www.123weddingcards.com/blog/wp-content/uploads/2020/09/3.png" alt="" />
                        </a>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 s2">
                    <div class="picframe mb-15">
                        <a class="" href="javascript:void(0)">
                            <span class="overlay-v">
                                <span class="pf_text">
                                    <span class="project-name">Party Invitations</span>
                                </span>
                            </span>
                            <img src="https://www.123weddingcards.com/blog/wp-content/uploads/2020/09/2.png" alt="" />
                        </a>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 s2">
                    <div class="picframe mb-15">
                        <a class="" href="javascript:void(0)">
                            <span class="overlay-v">
                                <span class="pf_text">
                                    <span class="project-name">Elegant Invitations</span>
                                </span>
                            </span>
                            <img src="https://www.123weddingcards.com/blog/wp-content/uploads/2020/09/1.png" alt="" />
                        </a>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 s2">
                    <div class="picframe mb-15">
                        <a class="" href="javascript:void(0)">
                            <span class="overlay-v">
                                <span class="pf_text">
                                    <span class="project-name">Modern Invitations</span>
                                </span>
                            </span>
                            <img src="https://www.123weddingcards.com/blog/wp-content/uploads/2020/09/4.png" alt="" />
                        </a>                        
                    </div>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 s2">
                    <div class="picframe mb-15">
                        <a class="" href="javascript:void(0)">
                            <span class="overlay-v">
                                <span class="pf_text">
                                    <span class="project-name">Digital Printing</span>
                                </span>
                            </span>
                            <img src="https://www.123weddingcards.com/blog/wp-content/uploads/2020/09/5.png" alt="" />
                        </a>                        
                    </div>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 s2">
                    <div class="picframe mb-15">
                        <a class="" href="javascript:void(0)">
                            <span class="overlay-v">
                                <span class="pf_text">
                                    <span class="project-name">Silk Printing</span>
                                </span>
                            </span>
                            <img src="https://www.123weddingcards.com/blog/wp-content/uploads/2020/09/4.png" alt="" />
                        </a>                        
                    </div>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 s2">
                    <div class="picframe mb-15">
                        <a class="" href="javascript:void(0)">
                            <span class="overlay-v">
                                <span class="pf_text">
                                    <span class="project-name">Top Rated</span>
                                </span>
                            </span>
                            <img src="https://www.123weddingcards.com/blog/wp-content/uploads/2020/09/4.png" alt="" />
                        </a>                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--catalouge section close -->

    <!-- section begin -->
    <section class="custom-padding-30 home-stationery" data-bgcolor="#f0eae5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center">
                    <h1>Wedding Stationery</h1>
                    <div class="separator"><span><i class="fa fa-circle"></i></span></div>
                    <div class="spacer-single"></div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 item residential">
                    <div class="picframe">
                        <a class="" href="javascript:void(0)">
                            <span class="overlay">
                                <span class="pf_text">
                                    <span class="project-name">RSVP Cards</span>
                                </span>
                            </span>
                            <img alt="Weddings" src="https://www.indianweddingcards.com/blog/wp-content/uploads/2019/11/rsvp-iwc.jpg">
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 item residential">
                    <div class="picframe">
                        <a class="" href="javascript:void(0)">
                            <span class="overlay">
                                <span class="pf_text">
                                    <span class="project-name">Save The Date</span>
                                </span>
                            </span>
                            <img alt="Weddings" src="https://www.indianweddingcards.com/blog/wp-content/uploads/2019/11/save-the-date-iwc.jpg">
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 item residential">
                    <div class="picframe">
                        <a class="" href="javascript:void(0)">
                            <span class="overlay">
                                <span class="pf_text">
                                    <span class="project-name">Thank You</span>
                                </span>
                            </span>
                            <img alt="Weddings" src="https://www.indianweddingcards.com/blog/wp-content/uploads/2019/11/place_card.jpg">
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 item residential">
                    <div class="picframe">
                        <a class="" href="stationery.html">
                            <span class="overlay">
                                <span class="pf_text">
                                    <span class="project-name">All Stationery</span>
                                </span>
                            </span>
                            <img alt="Weddings" src="https://www.indianweddingcards.com/blog/wp-content/uploads/2019/11/menu-card-iwc.jpg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <!--product section begin -->
    <?php
    if ( ! empty($inviteModel)) {
        ?>
        <section id="section-intro" data-bgcolor="#f0eae5" class="custom-padding-30">
            <div class="container">
                <div class="row align-items-center bg-white">
                    <div class="col-lg-6 col-sm-6">
                        <?= Html::img(Url::to(Yii::$app->urlManagerBackend->baseUrl . '/' . \common\models\Invite::MAIN_DIRECTORY . \common\models\Invite::ORIGINAL_FILEPATH . $inviteModel->image, TRUE), ['alt' => $inviteModel->heading, 'class' => 'img-responsive']); ?>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="padding20">
                            <h2><?= $inviteModel->heading; ?></h2>
                            <p style="font-size: 15px;"><?= $inviteModel->description; ?></p>
                            <?= Html::a($inviteModel->button_name, [$inviteModel->link], ['class' => 'btn-custom font-weight-bold text-white']); ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    <?php } ?>
    <!--product section close -->

    <!-- section begin -->
    <section id="section-testimonial" class="text-light custom-padding-30 custom-123-4">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center">
                    <h1>Customer Says</h1>
                    <div class="separator"><span><i class="fa fa-circle"></i></span></div>
                    <div class="spacer-single"></div>
                </div>
            </div>
            <div id="testimonial-carousel" class="owl-carousel owl-theme de_carousel">
                <div class="item">
                    <div class="de_testi">
                        <blockquote>
                            <p>123WeddingCards and the team did phenomenal work. From the moment we inquired online, we were quickly contacted to discuss our requirements and then provided design, price and production options to make a quick decision.</p>
                            <div class="de_testi_by">
                                Una Morrison, Sri Lanka
                            </div>
                        </blockquote>

                    </div>
                </div>

                <div class="item">
                    <div class="de_testi">
                        <blockquote>
                            <p>I received my order last Friday, as promised. I am so impressed with the quality and design. Thank you so much for delivering this beautiful product. I am so excited to send out my announcements.</p>
                            <div class="de_testi_by">
                                Katie, United States
                            </div>
                        </blockquote>
                    </div>
                </div>

                <div class="item">
                    <div class="de_testi">
                        <blockquote>
                            <p>I really loved the scroll wedding invitations I received my wedding invites 5 day after ordering. Everyone praised our wedding invites, the best part of invitation was its design, pattern and printing. You guys did an absolutely stunning job The quality is very impressive Thank you.</p>
                            <div class="de_testi_by">
                                Timothy Anderson, United States
                            </div>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <!-- section begin -->
    <?php
    if ( ! empty($vendorModels)) {
        ?>
        <section class="custom-padding-30 list-vendor" data-bgcolor="#f0eae5">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3 text-center">
                        <h1>Listed as Verified Vendor</h1>
                        <div class="separator"><span><i class="fa fa-circle"></i></span></div>
                        <div class="spacer-single"></div>
                    </div>
                    <div class="col-md-12 d-flex justify-content-center">
                        <?php
                        foreach ($vendorModels as $vendorModel) {
                            echo Html::img(Url::to(Yii::$app->urlManagerBackend->baseUrl . '/' . common\models\Vendor::MAIN_DIRECTORY . common\models\Vendor::ORIGINAL_FILEPATH . $vendorModel->image, TRUE), ['alt' => $vendorModel->name]);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
    <!-- section close -->
</div>
