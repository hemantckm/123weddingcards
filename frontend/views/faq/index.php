<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::$app->name . ": " . strtoupper(Yii::$app->controller->id);
?>

<?php
if ( ! empty($faqModelDatas)) {
    foreach ($faqModelDatas as $faqModelData) {
        ?>
        <!-- subheader begin-->
        <section id="subheader" data-speed="8" data-type="background" style="background-image:url('<?= $faqModelData['feature_image']; ?>')">
            <div class="layer"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?= $faqModelData['heading']; ?></h1>
                        <ul class="crumb">
                            <li><?= Html::a('Home', Url::base(true)); ?></li>
                            <li class="sep">/</li>
                            <li>Faq</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="search-box">
                <input class="search-input" type="text" name="" placeholder="Search Product">
                <a href="javascript:void(0);" class="search-btn"><i class="fa fa-search"></i></a>     
            </div>
        </section>
        <!-- subheader close -->

        <!-- content begin -->
        <div id="content" class="no-top no-bottom">
            <?php if ( ! empty($faqModelData['faq_type'])) { ?>
                <section data-bgcolor="#f0eae5" class="custom-padding-35" id="faq">
                    <div class="container">
                        <div class="row faq">
                            <div class="col-lg-12 nav-pill-main-div">
                                <div class="customize_solution pt-1">
                                    <ul class="tabs nav nav-justified">
                                        <?php
                                        $counts = 0;
                                        foreach ($faqModelData['faq_type'] as $faqTypeKey => $faqTypeDatas) {
                                            ?>
                                            <li class="tab-link <?php if ($counts == 0) { ?> current <?php } ?> nav-pill mt-2" href="tab-<?= $faqTypeKey; ?>">
                                                <span class="ease-effect">
                                                    <?= Html::img($faqTypeDatas['fq_type_icon'], ['style' => 'margin-bottom: 10px']); ?>
                                                    <br/>
                                                    <?= $faqTypeDatas['faq_type_name']; ?>
                                                </span>
                                            </li>
                                            <?php
                                            $counts ++;
                                        }
                                        ?>
                                    </ul>

                                    <?php
                                    $questionCounts = 0;
                                    foreach ($faqModelData['faq_type'] as $faqTypeKey => $faqTypeDatas) {
                                        ?>
                                        <div class="tab-content <?php if ($questionCounts == 0) { ?> current <?php } ?>" id="tab-<?= $faqTypeKey; ?>">
                                            <?php if ( ! empty($faqTypeDatas['question_answer'])) { ?>
                                                <div class="panel-group accordion" id="accordion">
                                                    <?php foreach ($faqTypeDatas['question_answer'] as $questionIndex => $questionAnswerDatas) { ?>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle collapsed" href="#question<?= $questionCounts . $questionIndex; ?>" data-toggle="collapse"><?= $questionAnswerDatas['question']; ?></a>
                                                                </h4> 
                                                            </div>
                                                            <div id="question<?= $questionCounts . $questionIndex; ?>" class="panel-collapse collapse">
                                                                <div class="panel-body"><p><?= $questionAnswerDatas['answer']; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <?php
                                        $questionCounts ++;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php } ?>
            <!-- section begin -->
            <section class="call-to-action text-light padding40" aria-label="cta" data-bgcolor="#ceba84">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-7">
                            <h3 class="size-2 no-margin">Still Have a Question</h3>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5 text-right wc123-1">
                            <?= Html::a('Contact Us', Url::to(['/contact-us', '#' => 'contact']), ['class' => 'btn-line-white wow fadeInUp font-weight-bold']); ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- section close -->
        </div>
        <?php
    }
}
?>