<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = Yii::$app->name . ": " . ucwords('404 ' . Yii::$app->controller->id);
?>
<!-- content begin -->
<div id="content" class="no-top">
    <!-- section begin -->
    <section class="error-404" data-bgcolor="#f0eae5">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-6">
                    <?= Html::img(Url::to(Yii::getAlias('@web/images') . '/404.png')); ?>
                    <h1>Maybe this page moved? Got deleted?<br>Never existed in the first place?</h1>
                    <p>Let's go <?= Html::a('Home', Url::base(true)); ?> and try from there.</p>
                    <h3>OR</h3>
                    <div class="widget widget_search d-flex justify-content-center">
                        <input type="text" name="search" id="search" class="form-control" placeholder="Search Product">
                        <button id="btn-search" type="submit"></button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->
</div>