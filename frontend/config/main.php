<?php

use \yii\web\Request;

$params = array_merge(
        require __DIR__ . '/../../common/config/params.php', require __DIR__ . '/../../common/config/params-local.php', require __DIR__ . '/params.php', require __DIR__ . '/params-local.php'
);

//$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
$baseUrl = (new Request)->getBaseUrl();
return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'name' => '123WeddingCards', //Site Name
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'enableCsrfValidation' => false,
            'baseUrl' => $baseUrl,
        //'baseUrl' => '/123weddingcards',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'authTimeout' => 60 * 60 * 12, // 12 Hours
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'PHPFRONTSESSID',
            'timeout' => 60 * 60 * 12
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'error',
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,
            'rules' => [
                '' => 'site/index',
                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
            /* '<controller:\w+>/<id:\d+>' => '<controller>/view',
              '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
              '<controller:\w+>/<action:\w+>' => '<controller>/<action>', */
            ],
        ],
        'urlManagerBackend' => [
            'enablePrettyUrl' => true,
            'class' => 'yii\web\UrlManager',
            'hostInfo' => 'http://local.123weddingcardsadmin.com', //Backend Host Url
            'baseUrl' => 'http://local.123weddingcardsadmin.com', //Backend Base Url
        ],
    /* 'assetManager' => [
      'class' => 'yii\web\AssetManager',
      'bundles' => [
      'yii\bootstrap\BootstrapAsset' => [
      //'sourcePath' => null, 'css' => ['/css/bootstrap.min.css'],
      ],
      'yii\web\JqueryAsset' => [
      //'sourcePath' => null, 'js' => ['/js/jquery-2.2.4.min.js'],
      ],
      'yii\bootstrap\BootstrapPluginAsset' => [
      //'sourcePath' => null, 'js' => ['/js/bootstrap.min.js'],
      ],
      ],
      ], */
    ],
    'params' => $params,
];
