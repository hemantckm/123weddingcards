/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $("#pjax-inquiry-form-submit").on("pjax:end", function () {
        $("#inquiry-form-submit")[0].reset();
        $("#inquiry-name").val('');
        $("#inquiry-email").val('');
        $("#inquiry-phone").val('');
        $("#inquiry-message").val('');
        $(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");
    });
});

