<?php

namespace frontend\controllers;

use yii\web\Controller;
use common\models\Faq;

class FaqController extends Controller
{

    public function actionIndex ()
    {
        $faqModelDatas = Faq::find()->getFrontendData();
        return $this->render('index', [
                    'faqModelDatas' => $faqModelDatas,
        ]);
    }

}
