<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\ContactUs;

class ContactUsController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex ()
    {
        $contactUsDatas = ContactUs::find()->getContactUsData(); //Getting All Data For Contact-Us Page.
        $inquiryModel   = new \common\models\Inquiry(); //Create New Inquiry Model.

        if ($inquiryModel->load(Yii::$app->request->post()) && $inquiryModel->validate()) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $inquiryModel->name = ucwords(strtolower($inquiryModel->name));
                if ($inquiryModel->sendEmail(Yii::$app->params['adminEmail'])) {
                    $inquiryModel->save();
                    Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
                } else {
                    Yii::$app->session->setFlash('error', 'There was an error sending your message.');
                }
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollBack();
            }
            //return $this->refresh();
        }
        return $this->render('index', [
                    'contactUsDatas' => $contactUsDatas,
                    'inquiryModel' => $inquiryModel,
        ]);
    }

}
