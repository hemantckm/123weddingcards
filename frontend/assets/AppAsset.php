<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl  = '@web';
    public $css      = [
        //'css/bootstrap.css',
        'css/jpreloader.css',
        'css/animate.css',
        'css/plugin.css',
        'css/owl.carousel.css',
        'css/owl.theme.css',
        'css/owl.transitions.css',
        'css/magnific-popup.css',
        'css/style.css',
        'css/bg.css', //custom background
        'css/color.css', //color scheme
        'fonts/font-awesome/css/font-awesome.css', //load fonts
        'css/rev-settings.css', //revolution slider
        'css/font-style-2.css', //custom font
        'css/responsive.css', //custom font
    ];
    public $js       = [
        'js/jquery.min.js',
        'js/jpreLoader.js',
        //'js/bootstrap.min.js',
        'js/easing.js',
        'js/owl.carousel.js',
        'js/wow.min.js',
        'js/jquery.magnific-popup.min.js',
        'js/enquire.min.js',
        'js/designesia.js',
    ];
    public $depends  = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

}
