<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%insert_die}}`.
 */
class m200924_084905_create_insert_die_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%insert_die}}', [
            'insert_id' => $this->integer(11)->unsigned(),
            'die_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (insert_id, die_id)',
        ]);
        $this->addForeignKey('FK_insert_insert_die', 'insert_die', 'insert_id', 'insert', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_die_insert_die', 'insert_die', 'die_id', 'die_cut', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%insert_die}}');
    }

}
