<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%logo}}`.
 */
class m201104_062947_create_logo_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%logo}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'heading' => $this->string(100)->notNull()->unique(),
            'image' => $this->string(255),
            'default' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%logo}}');
    }

}
