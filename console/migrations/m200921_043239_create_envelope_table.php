<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%envelope}}`.
 */
class m200921_043239_create_envelope_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%envelope}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'card_id' => $this->integer(11)->unsigned(),
            'status' => $this->smallInteger(1)->unsigned()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_card_envelope', 'envelope', 'card_id', 'card', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%envelope}}');
    }

}
