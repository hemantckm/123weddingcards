<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%card_theme}}`.
 */
class m200908_095251_create_card_theme_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%card_theme}}', [
            'card_id' => $this->integer(11)->unsigned(),
            'theme_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (card_id, theme_id)',
        ]);
        $this->addForeignKey('FK_card_card_theme', 'card_theme', 'card_id', 'card', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_theme_card_theme', 'card_theme', 'theme_id', 'theme', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%card_theme}}');
    }

}
