<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon_envelope_paper}}`.
 */
class m201015_043820_create_addon_envelope_paper_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%addon_envelope_paper}}', [
            'addon_envelope_id' => $this->integer(11)->unsigned(),
            'paper_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (addon_envelope_id, paper_id)',
        ]);
        $this->addForeignKey('FK_addon_envelope_addon_envelope_paper', 'addon_envelope_paper', 'addon_envelope_id', 'addon_envelope', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_paper_addon_envelope_paper', 'addon_envelope_paper', 'paper_id', 'paper', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%addon_envelope_paper}}');
    }

}
