<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%block}}`.
 */
class m200918_113858_create_block_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%block}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'code' => $this->string(50)->notNull()->unique(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%block}}');
    }

}
