<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%website}}`.
 */
class m200908_090025_create_website_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%website}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'name' => $this->string(50)->notNull()->unique(),
            'url' => $this->string(255)->notNull()->unique(),
            'code' => $this->string(50),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%website}}');
    }

}
