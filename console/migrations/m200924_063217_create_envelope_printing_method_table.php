<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%envelope_printing_method}}`.
 */
class m200924_063217_create_envelope_printing_method_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%envelope_printing_method}}', [
            'envelope_id' => $this->integer(11)->unsigned(),
            'printing_method_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (envelope_id, printing_method_id)',
        ]);
        $this->addForeignKey('FK_envelope_envelope_printing_method', 'envelope_printing_method', 'envelope_id', 'envelope', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_printing_method_envelope_printing_method', 'envelope_printing_method', 'printing_method_id', 'printing_method', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%envelope_printing_method}}');
    }

}
