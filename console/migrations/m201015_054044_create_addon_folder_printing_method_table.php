<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon_folder_printing_method}}`.
 */
class m201015_054044_create_addon_folder_printing_method_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%addon_folder_printing_method}}', [
            'addon_folder_id' => $this->integer(11)->unsigned(),
            'printing_method_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (addon_folder_id, printing_method_id)',
        ]);
        $this->addForeignKey('FK_addon_folder_addon_folder_printing_method', 'addon_folder_printing_method', 'addon_folder_id', 'addon_folder', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_printing_method_addon_folder_printing_method', 'addon_folder_printing_method', 'printing_method_id', 'printing_method', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%addon_folder_printing_method}}');
    }

}
