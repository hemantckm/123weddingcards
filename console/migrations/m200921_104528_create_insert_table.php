<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%insert}}`.
 */
class m200921_104528_create_insert_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%insert}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'card_id' => $this->integer(11)->unsigned(),
            'status' => $this->smallInteger(1)->unsigned()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_card_insert', 'insert', 'card_id', 'card', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%insert}}');
    }

}
