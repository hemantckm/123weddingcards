<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%about}}`.
 */
class m201104_100741_create_about_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%about}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'heading1' => $this->string(100)->notNull()->unique(),
            'description1' => $this->text(),
            'heading2' => $this->string(100)->notNull()->unique(),
            'description2' => $this->text(),
            'image' => $this->string(255),
            'default' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%about}}');
    }

}
