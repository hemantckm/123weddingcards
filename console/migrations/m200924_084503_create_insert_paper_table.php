<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%insert_paper}}`.
 */
class m200924_084503_create_insert_paper_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%insert_paper}}', [
            'insert_id' => $this->integer(11)->unsigned(),
            'paper_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (insert_id, paper_id)',
        ]);
        $this->addForeignKey('FK_insert_insert_paper', 'insert_paper', 'insert_id', 'insert', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_paper_insert_paper', 'insert_paper', 'paper_id', 'paper', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%insert_paper}}');
    }

}
