<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%folder_printing_method}}`.
 */
class m200924_075640_create_folder_printing_method_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%folder_printing_method}}', [
            'folder_id' => $this->integer(11)->unsigned(),
            'printing_method_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (folder_id, printing_method_id)',
        ]);
        $this->addForeignKey('FK_folder_folder_printing_method', 'folder_printing_method', 'folder_id', 'folder', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_printing_method_folder_printing_method', 'folder_printing_method', 'printing_method_id', 'printing_method', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%folder_printing_method}}');
    }

}
