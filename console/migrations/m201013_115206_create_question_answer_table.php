<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%question_answer}}`.
 */
class m201013_115206_create_question_answer_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%question_answer}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'question' => $this->string(255)->notNull()->unique(),
            'answer' => $this->text(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%question_answer}}');
    }

}
