<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%insert_printing_method}}`.
 */
class m200924_085134_create_insert_printing_method_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%insert_printing_method}}', [
            'insert_id' => $this->integer(11)->unsigned(),
            'printing_method_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (insert_id, printing_method_id)',
        ]);
        $this->addForeignKey('FK_insert_insert_printing_method', 'insert_printing_method', 'insert_id', 'insert', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_printing_method_insert_printing_method', 'insert_printing_method', 'printing_method_id', 'printing_method', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%insert_printing_method}}');
    }

}
