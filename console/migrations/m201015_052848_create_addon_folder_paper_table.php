<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon_folder_paper}}`.
 */
class m201015_052848_create_addon_folder_paper_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%addon_folder_paper}}', [
            'addon_folder_id' => $this->integer(11)->unsigned(),
            'paper_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (addon_folder_id, paper_id)',
        ]);
        $this->addForeignKey('FK_addon_folder_addon_folder_paper', 'addon_folder_paper', 'addon_folder_id', 'addon_folder', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_paper_addon_folder_paper', 'addon_folder_paper', 'paper_id', 'paper', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%addon_folder_paper}}');
    }

}
