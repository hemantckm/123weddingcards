<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tube}}`.
 */
class m201005_092736_create_tube_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%tube}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'scroll_id' => $this->integer(11)->unsigned(),
            'length' => $this->float(10)->notNull()->defaultValue(0),
            'weight' => $this->float(10)->notNull()->defaultValue(0),
            'diameter' => $this->float(10)->notNull()->defaultValue(0),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_scroll_tube', 'tube', 'scroll_id', 'scroll', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%tube}}');
    }

}
