<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon_folder_die}}`.
 */
class m201015_053157_create_addon_folder_die_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%addon_folder_die}}', [
            'addon_folder_id' => $this->integer(11)->unsigned(),
            'die_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (addon_folder_id, die_id)',
        ]);
        $this->addForeignKey('FK_addon_folder_addon_folder_die', 'addon_folder_die', 'addon_folder_id', 'addon_folder', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_die_addon_folder_die', 'addon_folder_die', 'die_id', 'die_cut', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%addon_folder_die}}');
    }

}
