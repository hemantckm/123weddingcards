<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%contact_us_tc}}`.
 */
class m201030_082410_create_contact_us_tc_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%contact_us_tc}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'contact_us_id' => $this->integer(11)->unsigned(),
            'heading' => $this->string(50)->notNull()->unique(),
            'description' => $this->text(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_contactus_contact_us_tc', 'contact_us_tc', 'contact_us_id', 'contact_us', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%contact_us_tc}}');
    }

}
