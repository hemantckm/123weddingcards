<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%color}}`.
 */
class m200917_083230_create_color_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%color}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'name' => $this->string(50)->notNull()->unique(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%color}}');
    }

}
