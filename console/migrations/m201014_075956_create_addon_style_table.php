<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon_style}}`.
 */
class m201014_075956_create_addon_style_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%addon_style}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'addon_type_id' => $this->integer(11)->unsigned()->notNull(),
            'name' => $this->string(50)->notNull()->unique(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_addon_type_addon_style', 'addon_style', 'addon_type_id', 'addon_type', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%addon_style}}');
    }

}
