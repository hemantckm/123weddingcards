<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon_envelope}}`.
 */
class m201015_042808_create_addon_envelope_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%addon_envelope}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'addon_id' => $this->integer(11)->unsigned(),
            'status' => $this->smallInteger(1)->unsigned()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_addon_addon_envelope', 'addon_envelope', 'addon_id', 'addon', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%addon_envelope}}');
    }

}
