<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%box_paper}}`.
 */
class m201005_085316_create_box_paper_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%box_paper}}', [
            'box_id' => $this->integer(11)->unsigned(),
            'paper_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (box_id, paper_id)',
        ]);
        $this->addForeignKey('FK_box_box_paper', 'box_paper', 'box_id', 'box', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_paper_box_paper', 'box_paper', 'paper_id', 'paper', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%box_paper}}');
    }

}
