<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%card}}`.
 */
class m200908_093641_create_card_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%card}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'name' => $this->string(100)->notNull()->unique(),
            'slug' => $this->string(100)->notNull()->unique(),
            'sample_price' => $this->double(10)->notNull(),
            'bulk_price' => $this->double(10)->notNull(),
            'print_charge' => $this->double(10)->notNull(),
            'length' => $this->float(10)->notNull()->defaultValue(0),
            'width' => $this->float(10)->notNull()->defaultValue(0),
            'thickness' => $this->float(10)->notNull()->defaultValue(0),
            'weight' => $this->float(10)->notNull()->defaultValue(0),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%card}}');
    }

}
