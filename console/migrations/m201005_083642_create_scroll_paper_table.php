<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%scroll_paper}}`.
 */
class m201005_083642_create_scroll_paper_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%scroll_paper}}', [
            'scroll_id' => $this->integer(11)->unsigned(),
            'paper_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (scroll_id, paper_id)',
        ]);
        $this->addForeignKey('FK_scroll_scroll_paper', 'scroll_paper', 'scroll_id', 'scroll', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_paper_scroll_paper', 'scroll_paper', 'paper_id', 'paper', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%scroll_paper}}');
    }

}
