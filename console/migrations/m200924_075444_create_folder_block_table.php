<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%folder_block}}`.
 */
class m200924_075444_create_folder_block_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%folder_block}}', [
            'folder_id' => $this->integer(11)->unsigned(),
            'block_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (folder_id, block_id)',
        ]);
        $this->addForeignKey('FK_folder_folder_block', 'folder_block', 'folder_id', 'folder', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_block_folder_block', 'folder_block', 'block_id', 'block', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%folder_block}}');
    }

}
