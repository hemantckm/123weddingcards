<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tassle}}`.
 */
class m200923_082800_create_tassle_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%tassle}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'code' => $this->string(50)->notNull()->unique(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%tassle}}');
    }

}
