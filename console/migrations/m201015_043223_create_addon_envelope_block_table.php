<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon_envelope_block}}`.
 */
class m201015_043223_create_addon_envelope_block_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%addon_envelope_block}}', [
            'addon_envelope_id' => $this->integer(11)->unsigned(),
            'block_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (addon_envelope_id, block_id)',
        ]);
        $this->addForeignKey('FK_addon_envelope_addon_envelope_block', 'addon_envelope_block', 'addon_envelope_id', 'addon_envelope', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_block_addon_envelope_block', 'addon_envelope_block', 'block_id', 'block', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%addon_envelope_block}}');
    }

}
