<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%inquiry}}`.
 */
class m201102_085422_create_inquiry_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%inquiry}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'name' => $this->string(100)->notNull(),
            'email' => $this->string(255)->notNull(),
            'phone' => $this->string(200),
            'message' => $this->text(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%inquiry}}');
    }

}
