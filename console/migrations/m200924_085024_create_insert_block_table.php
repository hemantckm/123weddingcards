<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%insert_block}}`.
 */
class m200924_085024_create_insert_block_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%insert_block}}', [
            'insert_id' => $this->integer(11)->unsigned(),
            'block_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (insert_id, block_id)',
        ]);
        $this->addForeignKey('FK_insert_insert_block', 'insert_block', 'insert_id', 'insert', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_block_insert_block', 'insert_block', 'block_id', 'block', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%insert_block}}');
    }

}
