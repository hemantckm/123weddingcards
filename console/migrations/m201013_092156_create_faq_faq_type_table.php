<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%faq_faq_type}}`.
 */
class m201013_092156_create_faq_faq_type_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%faq_faq_type}}', [
            'faq_id' => $this->integer(11)->unsigned(),
            'faq_type_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (faq_id, faq_type_id)',
        ]);
        $this->addForeignKey('FK_faq_faq_faq_type', 'faq_faq_type', 'faq_id', 'faq', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_faq_type_faq_faq_type', 'faq_faq_type', 'faq_type_id', 'faq_type', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%faq_faq_type}}');
    }

}
