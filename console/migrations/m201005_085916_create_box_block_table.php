<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%box_block}}`.
 */
class m201005_085916_create_box_block_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%box_block}}', [
            'box_id' => $this->integer(11)->unsigned(),
            'block_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (box_id, block_id)',
        ]);
        $this->addForeignKey('FK_box_box_block', 'box_block', 'box_id', 'box', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_block_box_block', 'box_block', 'block_id', 'block', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%box_block}}');
    }

}
