<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%box}}`.
 */
class m201005_084504_create_box_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%box}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'card_id' => $this->integer(11)->unsigned(),
            'box_type_id' => $this->integer(11)->unsigned(),
            'length' => $this->float(10)->notNull()->defaultValue(0),
            'width' => $this->float(10)->notNull()->defaultValue(0),
            'thickness' => $this->float(10)->notNull()->defaultValue(0),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_card_box', 'box', 'card_id', 'card', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_box_type_box', 'box', 'box_type_id', 'box_type', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%box}}');
    }

}
