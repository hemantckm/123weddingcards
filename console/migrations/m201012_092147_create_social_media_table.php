<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%social_media}}`.
 */
class m201012_092147_create_social_media_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%social_media}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'name' => $this->string(50)->notNull()->unique(),
            'link' => $this->string(255)->notNull()->unique(),
            'data' => $this->string(50),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%social_media}}');
    }

}
