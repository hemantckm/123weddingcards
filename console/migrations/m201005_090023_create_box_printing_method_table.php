<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%box_printing_method}}`.
 */
class m201005_090023_create_box_printing_method_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%box_printing_method}}', [
            'box_id' => $this->integer(11)->unsigned(),
            'printing_method_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (box_id, printing_method_id)',
        ]);
        $this->addForeignKey('FK_box_box_printing_method', 'box_printing_method', 'box_id', 'box', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_printing_method_box_printing_method', 'box_printing_method', 'printing_method_id', 'printing_method', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%box_printing_method}}');
    }

}
