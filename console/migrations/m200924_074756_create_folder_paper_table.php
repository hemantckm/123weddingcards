<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%folder_paper}}`.
 */
class m200924_074756_create_folder_paper_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%folder_paper}}', [
            'folder_id' => $this->integer(11)->unsigned(),
            'paper_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (folder_id, paper_id)',
        ]);
        $this->addForeignKey('FK_folder_folder_paper', 'folder_paper', 'folder_id', 'folder', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_paper_folder_paper', 'folder_paper', 'paper_id', 'paper', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%folder_paper}}');
    }

}
