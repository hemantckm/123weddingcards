<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%scroll_printing_method}}`.
 */
class m201005_084036_create_scroll_printing_method_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%scroll_printing_method}}', [
            'scroll_id' => $this->integer(11)->unsigned(),
            'printing_method_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (scroll_id, printing_method_id)',
        ]);
        $this->addForeignKey('FK_scroll_scroll_printing_method', 'scroll_printing_method', 'scroll_id', 'scroll', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_printing_method_scroll_printing_method', 'scroll_printing_method', 'printing_method_id', 'printing_method', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%scroll_printing_method}}');
    }

}
