<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%die_cut}}`.
 */
class m200918_105912_create_die_cut_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%die_cut}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'code' => $this->string(50)->notNull()->unique(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%die_cut}}');
    }

}
