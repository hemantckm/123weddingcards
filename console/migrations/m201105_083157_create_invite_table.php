<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%invite}}`.
 */
class m201105_083157_create_invite_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%invite}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'heading' => $this->string(100)->notNull()->unique(),
            'description' => $this->text(),
            'image' => $this->string(255),
            'link' => $this->string(255),
            'button_name' => $this->string(50),
            'default' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%invite}}');
    }

}
