<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%question_answer_faq}}`.
 */
class m201013_123232_create_question_answer_faq_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%question_answer_faq}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'question_answer_id' => $this->integer(11)->unsigned(),
            'faq_id' => $this->integer(11)->unsigned(),
            'faq_type_id' => $this->integer(11)->unsigned(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_qa_question_answer_faq', 'question_answer_faq', 'question_answer_id', 'question_answer', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_f_question_answer_faq', 'question_answer_faq', 'faq_id', 'faq', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_ft_question_answer_faq', 'question_answer_faq', 'faq_type_id', 'faq_type', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%question_answer_faq}}');
    }

}
