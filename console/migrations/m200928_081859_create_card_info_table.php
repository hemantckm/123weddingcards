<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%card_info}}`.
 */
class m200928_081859_create_card_info_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%card_info}}', [
            'card_id' => $this->primaryKey(11)->unsigned(),
            'theme' => $this->text(),
            'envelope' => $this->text(),
            'folder' => $this->text(),
            'scroll' => $this->text(),
            'box' => $this->text(),
            'insert' => $this->text(),
            'additions' => $this->text(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
        ]);
        $this->addForeignKey('FK_card_card_info', 'card_info', 'card_id', 'card', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%card_info}}');
    }

}
