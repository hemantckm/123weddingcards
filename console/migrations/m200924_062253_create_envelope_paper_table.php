<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%envelope_paper}}`.
 */
class m200924_062253_create_envelope_paper_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%envelope_paper}}', [
            'envelope_id' => $this->integer(11)->unsigned(),
            'paper_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (envelope_id, paper_id)',
        ]);
        $this->addForeignKey('FK_envelope_envelope_paper', 'envelope_paper', 'envelope_id', 'envelope', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_paper_envelope_paper', 'envelope_paper', 'paper_id', 'paper', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%envelope_paper}}');
    }

}
