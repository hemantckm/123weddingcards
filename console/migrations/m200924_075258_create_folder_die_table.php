<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%folder_die}}`.
 */
class m200924_075258_create_folder_die_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%folder_die}}', [
            'folder_id' => $this->integer(11)->unsigned(),
            'die_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (folder_id, die_id)',
        ]);
        $this->addForeignKey('FK_folder_folder_die', 'folder_die', 'folder_id', 'folder', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_die_folder_die', 'folder_die', 'die_id', 'die_cut', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%folder_die}}');
    }

}
