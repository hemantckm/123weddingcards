<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%contact_us_details}}`.
 */
class m201029_122550_create_contact_us_details_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%contact_us_details}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'contact_us_id' => $this->integer(11)->unsigned(),
            'name' => $this->string(50)->notNull()->unique(),
            'icon' => $this->string(255),
            'description' => $this->text(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_contactus_contact_us_details', 'contact_us_details', 'contact_us_id', 'contact_us', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%contact_us_details}}');
    }

}
