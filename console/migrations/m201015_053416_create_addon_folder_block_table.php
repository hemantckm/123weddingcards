<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon_folder_block}}`.
 */
class m201015_053416_create_addon_folder_block_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%addon_folder_block}}', [
            'addon_folder_id' => $this->integer(11)->unsigned(),
            'block_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (addon_folder_id, block_id)',
        ]);
        $this->addForeignKey('FK_addon_folder_addon_folder_block', 'addon_folder_block', 'addon_folder_id', 'addon_folder', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_block_addon_folder_block', 'addon_folder_block', 'block_id', 'block', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%addon_folder_block}}');
    }

}
