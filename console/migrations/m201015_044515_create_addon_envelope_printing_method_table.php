<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon_envelope_printing_method}}`.
 */
class m201015_044515_create_addon_envelope_printing_method_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%addon_envelope_printing_method}}', [
            'addon_envelope_id' => $this->integer(11)->unsigned(),
            'printing_method_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (addon_envelope_id, printing_method_id)',
        ]);
        $this->addForeignKey('FK_addon_envelope_addon_envelope_printing_method', 'addon_envelope_printing_method', 'addon_envelope_id', 'addon_envelope', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_printing_method_addon_envelope_printing_method', 'addon_envelope_printing_method', 'printing_method_id', 'printing_method', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%addon_envelope_printing_method}}');
    }

}
