<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rhinestone}}`.
 */
class m200924_041743_create_rhinestone_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%rhinestone}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'code' => $this->string(50)->notNull()->unique(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%rhinestone}}');
    }

}
