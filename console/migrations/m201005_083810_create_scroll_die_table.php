<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%scroll_die}}`.
 */
class m201005_083810_create_scroll_die_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%scroll_die}}', [
            'scroll_id' => $this->integer(11)->unsigned(),
            'die_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (scroll_id, die_id)',
        ]);
        $this->addForeignKey('FK_scroll_scroll_die', 'scroll_die', 'scroll_id', 'scroll', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_die_scroll_die', 'scroll_die', 'die_id', 'die_cut', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%scroll_die}}');
    }

}
