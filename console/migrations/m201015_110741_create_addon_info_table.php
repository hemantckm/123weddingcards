<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon_info}}`.
 */
class m201015_110741_create_addon_info_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%addon_info}}', [
            'addon_id' => $this->primaryKey(11)->unsigned(),
            'card_id' => $this->integer(11)->unsigned(),
            'addon_type' => $this->text(),
            'addon_style' => $this->text(),
            'envelope' => $this->text(),
            'folder' => $this->text(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
        ]);
        $this->addForeignKey('FK_addon_addon_info', 'addon_info', 'addon_id', 'addon', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_card_addon_info', 'addon_info', 'card_id', 'card', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%addon_info}}');
    }

}
