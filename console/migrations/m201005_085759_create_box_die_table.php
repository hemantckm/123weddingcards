<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%box_die}}`.
 */
class m201005_085759_create_box_die_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%box_die}}', [
            'box_id' => $this->integer(11)->unsigned(),
            'die_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (box_id, die_id)',
        ]);
        $this->addForeignKey('FK_box_box_die', 'box_die', 'box_id', 'box', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_die_box_die', 'box_die', 'die_id', 'die_cut', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%box_die}}');
    }

}
