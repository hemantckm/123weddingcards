<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%paper_type}}`.
 */
class m200917_092148_create_paper_type_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%paper_type}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'name' => $this->string(50)->notNull()->unique(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%paper_type}}');
    }

}
