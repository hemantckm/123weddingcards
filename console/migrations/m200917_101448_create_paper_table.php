<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%paper}}`.
 */
class m200917_101448_create_paper_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%paper}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'code' => $this->string(50)->notNull()->unique(),
            'paper_type_id' => $this->integer(11)->unsigned(),
            'color_id' => $this->integer(11)->unsigned(),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_paper_type_paper', 'paper', 'paper_type_id', 'paper_type', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_color_paper', 'paper', 'color_id', 'color', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%paper}}');
    }

}
