<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%envelope_block}}`.
 */
class m200924_062945_create_envelope_block_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%envelope_block}}', [
            'envelope_id' => $this->integer(11)->unsigned(),
            'block_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (envelope_id, block_id)',
        ]);
        $this->addForeignKey('FK_envelope_envelope_block', 'envelope_block', 'envelope_id', 'envelope', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_block_envelope_block', 'envelope_block', 'block_id', 'block', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%envelope_block}}');
    }

}
