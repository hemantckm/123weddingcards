<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon_folder}}`.
 */
class m201015_052347_create_addon_folder_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%addon_folder}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'addon_id' => $this->integer(11)->unsigned(),
            'status' => $this->smallInteger(1)->unsigned()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_addon_addon_folder', 'addon_folder', 'addon_id', 'addon', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%addon_folder}}');
    }
}
