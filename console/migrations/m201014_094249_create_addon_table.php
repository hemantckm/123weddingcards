<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon}}`.
 */
class m201014_094249_create_addon_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%addon}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'code' => $this->string(150)->notNull()->unique(),
            'card_id' => $this->integer(11)->unsigned()->notNull(),
            'addon_type_id' => $this->integer(11)->unsigned()->notNull(),
            'addon_style_id' => $this->integer(11)->unsigned(),
            'price' => $this->double(10)->notNull(),
            'length' => $this->float(10)->notNull()->defaultValue(0),
            'width' => $this->float(10)->notNull()->defaultValue(0),
            'weight' => $this->float(10)->notNull()->defaultValue(0),
            'status' => $this->smallInteger(1)->unsigned()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_card_addon', 'addon', 'card_id', 'card', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_addon_type_addon', 'addon', 'addon_type_id', 'addon_type', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%addon}}');
    }

}
