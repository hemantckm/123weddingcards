<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%addon_envelope_die}}`.
 */
class m201015_044158_create_addon_envelope_die_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%addon_envelope_die}}', [
            'addon_envelope_id' => $this->integer(11)->unsigned(),
            'die_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (addon_envelope_id, die_id)',
        ]);
        $this->addForeignKey('FK_addon_envelope_addon_envelope_die', 'addon_envelope_die', 'addon_envelope_id', 'addon_envelope', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_die_addon_envelope_die', 'addon_envelope_die', 'die_id', 'die_cut', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%addon_envelope_die}}');
    }

}
