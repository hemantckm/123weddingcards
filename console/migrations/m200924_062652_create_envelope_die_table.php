<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%envelope_die}}`.
 */
class m200924_062652_create_envelope_die_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%envelope_die}}', [
            'envelope_id' => $this->integer(11)->unsigned(),
            'die_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (envelope_id, die_id)',
        ]);
        $this->addForeignKey('FK_envelope_envelope_die', 'envelope_die', 'envelope_id', 'envelope', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_die_envelope_die', 'envelope_die', 'die_id', 'die_cut', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%envelope_die}}');
    }

}
