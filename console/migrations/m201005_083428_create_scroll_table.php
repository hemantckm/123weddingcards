<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%scroll}}`.
 */
class m201005_083428_create_scroll_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%scroll}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'card_id' => $this->integer(11)->unsigned(),
            'status' => $this->smallInteger(1)->unsigned()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_card_scroll', 'scroll', 'card_id', 'card', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%scroll}}');
    }

}
