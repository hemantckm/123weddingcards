<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%banner}}`.
 */
class m201104_074917_create_banner_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%banner}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'heading' => $this->string(100)->notNull()->unique(),
            'image' => $this->string(255),
            'description' => $this->text(),
            'default' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%banner}}');
    }

}
