<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%scroll_block}}`.
 */
class m201005_083922_create_scroll_block_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%scroll_block}}', [
            'scroll_id' => $this->integer(11)->unsigned(),
            'block_id' => $this->integer(11)->unsigned(),
            'PRIMARY KEY (scroll_id, block_id)',
        ]);
        $this->addForeignKey('FK_scroll_scroll_block', 'scroll_block', 'scroll_id', 'scroll', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_block_scroll_block', 'scroll_block', 'block_id', 'block', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%scroll_block}}');
    }

}
