<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%additions}}`.
 */
class m200924_044745_create_additions_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%additions}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'card_id' => $this->integer(11)->unsigned(),
            'tassle_id' => $this->integer(11)->unsigned(),
            'ribbon_id' => $this->integer(11)->unsigned(),
            'rhinestone_id' => $this->integer(11)->unsigned(),
            'extra_insert_weight' => $this->float(10)->notNull()->defaultValue(0),
            'extra_insert_price' => $this->double(10)->notNull()->defaultValue(0),
            'photocard' => $this->smallInteger(1)->unsigned()->defaultValue(0),
            'linear' => $this->smallInteger(1)->unsigned()->defaultValue(0),
            'velvet' => $this->smallInteger(1)->unsigned()->defaultValue(0),
            'status' => $this->smallInteger(1)->unsigned()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
        $this->addForeignKey('FK_card_additions', 'additions', 'card_id', 'card', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_tassle_additions', 'additions', 'tassle_id', 'tassle', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_ribbon_additions', 'additions', 'ribbon_id', 'ribbon', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_rhinestone_additions', 'additions', 'rhinestone_id', 'rhinestone', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%additions}}');
    }

}
