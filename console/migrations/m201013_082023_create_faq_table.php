<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%faq}}`.
 */
class m201013_082023_create_faq_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp ()
    {
        $this->createTable('{{%faq}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'heading' => $this->string(100)->notNull()->unique(),
            'feature_image' => $this->string(255),
            'default' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'status' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->bigInteger(20)->unsigned(),
            'updated_at' => $this->bigInteger(20)->unsigned()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown ()
    {
        $this->dropTable('{{%faq}}');
    }

}
