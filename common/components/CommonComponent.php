<?php

namespace common\components;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\CommonModel;
use yii\web\UploadedFile;

class CommonComponent
{

    /**
     * This Function is used for Envelope Details save with cardId.
     * @param type $envelopeModel
     * @param type $envelopeDetails
     * @param type $data
     */
    public static function Envelope ($envelopeModel, $envelopeDetails, $data)
    {
        if ( ! empty($envelopeDetails)) {
            $envelopePapers          = ! empty($envelopeDetails['envelope_paper']) ? $envelopeDetails['envelope_paper'] : array ();
            $envelopeDies            = ! empty($envelopeDetails['envelope_die']) ? $envelopeDetails['envelope_die'] : array ();
            $envelopeBlocks          = ! empty($envelopeDetails['envelope_block']) ? $envelopeDetails['envelope_block'] : array ();
            $envelopePrintingMethods = ! empty($envelopeDetails['envelope_printing_method']) ? $envelopeDetails['envelope_printing_method'] : array ();

            //Create New Envelope of Card.
            $envelopeModel->card_id = $data['id'];
            $envelopeModel->save(false);

            //Create Envelope Papers With Envelope.
            if ( ! empty($envelopePapers)) {
                foreach ($envelopePapers as $envelopePaper) {
                    $envelopePaperModel              = new \common\models\EnvelopePaper();
                    $envelopePaperModel->envelope_id = $envelopeModel->id;
                    $envelopePaperModel->paper_id    = $envelopePaper;
                    $envelopePaperModel->save();
                }
            }

            //Create Envelope Dies With Envelope.
            if ( ! empty($envelopeDies)) {
                foreach ($envelopeDies as $envelopeDie) {
                    $envelopeDieModel              = new \common\models\EnvelopeDie();
                    $envelopeDieModel->envelope_id = $envelopeModel->id;
                    $envelopeDieModel->die_id      = $envelopeDie;
                    $envelopeDieModel->save();
                }
            }

            //Create Envelope Blocks With Envelope.
            if ( ! empty($envelopeBlocks)) {
                foreach ($envelopeBlocks as $envelopeBlock) {
                    $envelopeBlockModel              = new \common\models\EnvelopeBlock();
                    $envelopeBlockModel->envelope_id = $envelopeModel->id;
                    $envelopeBlockModel->block_id    = $envelopeBlock;
                    $envelopeBlockModel->save();
                }
            }

            //Create Envelope Printing Method With Envelope.
            if ( ! empty($envelopePrintingMethods)) {
                foreach ($envelopePrintingMethods as $envelopePrintingMethod) {
                    $envelopePrintingMethodModel                     = new \common\models\EnvelopePrintingMethod();
                    $envelopePrintingMethodModel->envelope_id        = $envelopeModel->id;
                    $envelopePrintingMethodModel->printing_method_id = $envelopePrintingMethod;
                    $envelopePrintingMethodModel->save();
                }
            }
        }
    }

    /**
     * This Function is used for Folder Details save with cardId.
     * @param type $folderModel
     * @param type $folderDetails
     * @param type $data
     */
    public static function Folder ($folderModel, $folderDetails, $data)
    {
        if ( ! empty($folderDetails)) {
            $folderPapers          = ! empty($folderDetails['folder_paper']) ? $folderDetails['folder_paper'] : array ();
            $folderDies            = ! empty($folderDetails['folder_die']) ? $folderDetails['folder_die'] : array ();
            $folderBlocks          = ! empty($folderDetails['folder_block']) ? $folderDetails['folder_block'] : array ();
            $folderPrintingMethods = ! empty($folderDetails['folder_printing_method']) ? $folderDetails['folder_printing_method'] : array ();

            //Create New Folder of Card.
            $folderModel->card_id = $data['id'];
            $folderModel->save(false);

            //Create Folder Papers With Folder.
            if ( ! empty($folderPapers)) {
                foreach ($folderPapers as $folderPaper) {
                    $folderPaperModel            = new \common\models\FolderPaper();
                    $folderPaperModel->folder_id = $folderModel->id;
                    $folderPaperModel->paper_id  = $folderPaper;
                    $folderPaperModel->save();
                }
            }

            //Create Folder Dies With Folder.
            if ( ! empty($folderDies)) {
                foreach ($folderDies as $folderDie) {
                    $folderDieModel            = new \common\models\FolderDie();
                    $folderDieModel->folder_id = $folderModel->id;
                    $folderDieModel->die_id    = $folderDie;
                    $folderDieModel->save();
                }
            }

            //Create Folder Blocks With Folder.
            if ( ! empty($folderBlocks)) {
                foreach ($folderBlocks as $folderBlock) {
                    $folderBlockModel            = new \common\models\FolderBlock();
                    $folderBlockModel->folder_id = $folderModel->id;
                    $folderBlockModel->block_id  = $folderBlock;
                    $folderBlockModel->save();
                }
            }

            //Create Folder Printing Method With Folder.
            if ( ! empty($folderPrintingMethods)) {
                foreach ($folderPrintingMethods as $folderPrintingMethod) {
                    $folderPrintingMethodModel                     = new \common\models\FolderPrintingMethod();
                    $folderPrintingMethodModel->folder_id          = $folderModel->id;
                    $folderPrintingMethodModel->printing_method_id = $folderPrintingMethod;
                    $folderPrintingMethodModel->save();
                }
            }
        }
    }

    /**
     * This Function is used for Scroll/Tube Details save with cardId.
     * @param type $scrollModel
     * @param type $scrollDetails
     * @param type $tubeModel
     * @param type $tubeDetails
     * @param type $data
     */
    public static function ScrollTube ($scrollModel, $scrollDetails, $tubeModel, $tubeDetails, $data)
    {
        if ( ! empty($scrollDetails)) {
            $scrollPapers          = ! empty($scrollDetails['scroll_paper']) ? $scrollDetails['scroll_paper'] : array ();
            $scrollDies            = ! empty($scrollDetails['scroll_die']) ? $scrollDetails['scroll_die'] : array ();
            $scrollBlocks          = ! empty($scrollDetails['scroll_block']) ? $scrollDetails['scroll_block'] : array ();
            $scrollPrintingMethods = ! empty($scrollDetails['scroll_printing_method']) ? $scrollDetails['scroll_printing_method'] : array ();

            //Create New Scroll of Card.
            $scrollModel->card_id = $data['id'];
            $scrollModel->save(false);

            //Create New Tube With Scroll.
            if ( ! empty($tubeDetails)) {
                $tubeModel->scroll_id = $scrollModel->id;
                $tubeModel->length    = ! empty($tubeDetails['length']) ? $tubeDetails['length'] : 0;
                $tubeModel->weight    = ! empty($tubeDetails['weight']) ? $tubeDetails['weight'] : 0;
                $tubeModel->diameter  = ! empty($tubeDetails['diameter']) ? $tubeDetails['diameter'] : 0;
                $tubeModel->save();
            }

            //Create Scroll Papers With Scroll.
            if ( ! empty($scrollPapers)) {
                foreach ($scrollPapers as $scrollPaper) {
                    $scrollPaperModel            = new \common\models\ScrollPaper();
                    $scrollPaperModel->scroll_id = $scrollModel->id;
                    $scrollPaperModel->paper_id  = $scrollPaper;
                    $scrollPaperModel->save();
                }
            }

            //Create Scroll Dies With Scroll.
            if ( ! empty($scrollDies)) {
                foreach ($scrollDies as $scrollDie) {
                    $scrollDieModel            = new \common\models\ScrollDie();
                    $scrollDieModel->scroll_id = $scrollModel->id;
                    $scrollDieModel->die_id    = $scrollDie;
                    $scrollDieModel->save();
                }
            }

            //Create Scroll Blocks With Scroll.
            if ( ! empty($scrollBlocks)) {
                foreach ($scrollBlocks as $scrollBlock) {
                    $scrollBlockModel            = new \common\models\ScrollBlock();
                    $scrollBlockModel->scroll_id = $scrollModel->id;
                    $scrollBlockModel->block_id  = $scrollBlock;
                    $scrollBlockModel->save();
                }
            }

            //Create Scroll Printing Method With Scroll.
            if ( ! empty($scrollPrintingMethods)) {
                foreach ($scrollPrintingMethods as $scrollPrintingMethod) {
                    $scrollPrintingMethodModel                     = new \common\models\ScrollPrintingMethod();
                    $scrollPrintingMethodModel->scroll_id          = $scrollModel->id;
                    $scrollPrintingMethodModel->printing_method_id = $scrollPrintingMethod;
                    $scrollPrintingMethodModel->save();
                }
            }
        }
    }

    /**
     * This Function is used for Box Details save with cardId.
     * @param type $boxModel
     * @param type $boxDetails
     * @param type $data
     */
    public static function Box ($boxModel, $boxDetails, $data)
    {
        if ( ! empty($boxDetails)) {
            $boxPapers          = ! empty($boxDetails['box_paper']) ? $boxDetails['box_paper'] : array ();
            $boxDies            = ! empty($boxDetails['box_die']) ? $boxDetails['box_die'] : array ();
            $boxBlocks          = ! empty($boxDetails['box_block']) ? $boxDetails['box_block'] : array ();
            $boxPrintingMethods = ! empty($boxDetails['box_printing_method']) ? $boxDetails['box_printing_method'] : array ();

            //Create New Box of Card.
            $boxModel->card_id     = $data['id'];
            $boxModel->box_type_id = ! empty($boxDetails['box_type_id']) ? $boxDetails['box_type_id'] : NULL;
            $boxModel->length      = ! empty($boxDetails['length']) ? $boxDetails['length'] : 0;
            $boxModel->width       = ! empty($boxDetails['width']) ? $boxDetails['width'] : 0;
            $boxModel->thickness   = ! empty($boxDetails['thickness']) ? $boxDetails['thickness'] : 0;
            $boxModel->save(false);

            //Create Box Papers With Box.
            if ( ! empty($boxPapers)) {
                foreach ($boxPapers as $boxPaper) {
                    $boxPaperModel           = new \common\models\BoxPaper();
                    $boxPaperModel->box_id   = $boxModel->id;
                    $boxPaperModel->paper_id = $boxPaper;
                    $boxPaperModel->save();
                }
            }

            //Create Box Dies With Box.
            if ( ! empty($boxDies)) {
                foreach ($boxDies as $boxDie) {
                    $boxDieModel         = new \common\models\BoxDie();
                    $boxDieModel->box_id = $boxModel->id;
                    $boxDieModel->die_id = $boxDie;
                    $boxDieModel->save();
                }
            }

            //Create Box Blocks With Box.
            if ( ! empty($boxBlocks)) {
                foreach ($boxBlocks as $boxBlock) {
                    $boxBlockModel           = new \common\models\BoxBlock();
                    $boxBlockModel->box_id   = $boxModel->id;
                    $boxBlockModel->block_id = $boxBlock;
                    $boxBlockModel->save();
                }
            }

            //Create Box Printing Method With Box.
            if ( ! empty($boxPrintingMethods)) {
                foreach ($boxPrintingMethods as $boxPrintingMethod) {
                    $boxPrintingMethodModel                     = new \common\models\BoxPrintingMethod();
                    $boxPrintingMethodModel->box_id             = $boxModel->id;
                    $boxPrintingMethodModel->printing_method_id = $boxPrintingMethod;
                    $boxPrintingMethodModel->save();
                }
            }
        }
    }

    /**
     * This Function is used for Insert Details save with cardId.
     * @param type $insertModels
     * @param type $insertDetails
     * @param type $data
     */
    public static function Insert ($insertModels, $insertDetails, $data)
    {
        if ( ! empty($insertDetails)) {
            //Insert Detail
            $insertoldIDs     = ArrayHelper::map($insertModels, 'id', 'id');
            $insertModels     = CommonModel::createMultiple(\common\models\Insert::classname(), $insertModels);
            CommonModel::loadMultiple($insertModels, Yii::$app->request->post());
            $insertDeletedIDs = array_diff($insertoldIDs, array_filter(ArrayHelper::map($insertModels, 'id', 'id')));

            if ( ! empty($insertDeletedIDs)) {
                \common\models\Insert::deleteAll(['id' => $insertDeletedIDs]);
            }

            //Create Multiple Insert Models.
            foreach ($insertModels as $key => $insertModel) {
                $insertPapers          = ! empty($insertDetails[$key]['insert_paper']) ? $insertDetails[$key]['insert_paper'] : array ();
                $insertDies            = ! empty($insertDetails[$key]['insert_die']) ? $insertDetails[$key]['insert_die'] : array ();
                $insertBlocks          = ! empty($insertDetails[$key]['insert_block']) ? $insertDetails[$key]['insert_block'] : array ();
                $insertPrintingMethods = ! empty($insertDetails[$key]['insert_printing_method']) ? $insertDetails[$key]['insert_printing_method'] : array ();

                //Create New Insert of Card.
                $insertModel->card_id = $data['id'];
                $insertModel->save(false);

                //Create Insert Papers With Insert.
                if ( ! empty($insertPapers)) {
                    foreach ($insertPapers as $insertPaper) {
                        $insertPaperModel            = new \common\models\InsertPaper();
                        $insertPaperModel->insert_id = $insertModel->id;
                        $insertPaperModel->paper_id  = $insertPaper;
                        $insertPaperModel->save();
                    }
                }

                //Create Insert Dies With Insert.
                if ( ! empty($insertDies)) {
                    foreach ($insertDies as $insertDie) {
                        $insertDieModel            = new \common\models\InsertDie();
                        $insertDieModel->insert_id = $insertModel->id;
                        $insertDieModel->die_id    = $insertDie;
                        $insertDieModel->save();
                    }
                }

                //Create Insert Blocks With Insert.
                if ( ! empty($insertBlocks)) {
                    foreach ($insertBlocks as $insertBlock) {
                        $insertBlockModel            = new \common\models\InsertBlock();
                        $insertBlockModel->insert_id = $insertModel->id;
                        $insertBlockModel->block_id  = $insertBlock;
                        $insertBlockModel->save();
                    }
                }

                //Create Insert Printing Method With Insert.
                if ( ! empty($insertPrintingMethods)) {
                    foreach ($insertPrintingMethods as $insertPrintingMethod) {
                        $insertPrintingMethodModel                     = new \common\models\InsertPrintingMethod();
                        $insertPrintingMethodModel->insert_id          = $insertModel->id;
                        $insertPrintingMethodModel->printing_method_id = $insertPrintingMethod;
                        $insertPrintingMethodModel->save();
                    }
                }
            }
        }
    }

    /**
     * This Function is used for Additions Details save with cardId.
     * @param type $additionsModel
     * @param type $additionsDetails
     * @param type $data
     */
    public static function Additions ($additionsModel, $additionsDetails, $data)
    {
        if ( ! empty($additionsDetails)) {
            //Create New Folder of Card.
            $additionsModel->card_id             = $data['id'];
            $additionsModel->tassle_id           = ! empty($additionsDetails['tassle_id']) ? $additionsDetails['tassle_id'] : NULL;
            $additionsModel->ribbon_id           = ! empty($additionsDetails['ribbon_id']) ? $additionsDetails['ribbon_id'] : NULL;
            $additionsModel->rhinestone_id       = ! empty($additionsDetails['rhinestone_id']) ? $additionsDetails['rhinestone_id'] : NULL;
            $additionsModel->extra_insert_weight = ! empty($additionsDetails['extra_insert_weight']) ? $additionsDetails['extra_insert_weight'] : 0;
            $additionsModel->extra_insert_price  = ! empty($additionsDetails['extra_insert_price']) ? $additionsDetails['extra_insert_price'] : 0;
            $additionsModel->photocard           = ! empty($additionsDetails['photocard']) ? $additionsDetails['photocard'] : 0;
            $additionsModel->linear              = ! empty($additionsDetails['linear']) ? $additionsDetails['linear'] : 0;
            $additionsModel->velvet              = ! empty($additionsDetails['velvet']) ? $additionsDetails['velvet'] : 0;
            $additionsModel->save();
        }
    }

    /**
     * This Function is used for Addon Envelope Details save with addonId.
     * @param type $addonEnvelopeModel
     * @param type $addonEnvelopeDetails
     * @param type $data
     */
    public static function AddonEnvelope ($addonEnvelopeModel, $addonEnvelopeDetails, $data)
    {
        if ( ! empty($addonEnvelopeDetails)) {
            $addonEnvelopePapers          = ! empty($addonEnvelopeDetails['addon_envelope_paper']) ? $addonEnvelopeDetails['addon_envelope_paper'] : array ();
            $addonEnvelopeDies            = ! empty($addonEnvelopeDetails['addon_envelope_die']) ? $addonEnvelopeDetails['addon_envelope_die'] : array ();
            $addonEnvelopeBlocks          = ! empty($addonEnvelopeDetails['addon_envelope_block']) ? $addonEnvelopeDetails['addon_envelope_block'] : array ();
            $addonEnvelopePrintingMethods = ! empty($addonEnvelopeDetails['addon_envelope_printing_method']) ? $addonEnvelopeDetails['addon_envelope_printing_method'] : array ();

            //Create New AddonEnvelope of Addon.
            $addonEnvelopeModel->addon_id = $data['id'];
            $addonEnvelopeModel->save(false);

            //Create AddonEnvelope Papers With Envelope.
            if ( ! empty($addonEnvelopePapers)) {
                foreach ($addonEnvelopePapers as $addonEnvelopePaper) {
                    $addonEnvelopePaperModel                    = new \common\models\AddonEnvelopePaper();
                    $addonEnvelopePaperModel->addon_envelope_id = $addonEnvelopeModel->id;
                    $addonEnvelopePaperModel->paper_id          = $addonEnvelopePaper;
                    $addonEnvelopePaperModel->save();
                }
            }

            //Create AddonEnvelope Dies With Envelope.
            if ( ! empty($addonEnvelopeDies)) {
                foreach ($addonEnvelopeDies as $addonEnvelopeDie) {
                    $addonEnvelopeDieModel                    = new \common\models\AddonEnvelopeDie();
                    $addonEnvelopeDieModel->addon_envelope_id = $addonEnvelopeModel->id;
                    $addonEnvelopeDieModel->die_id            = $addonEnvelopeDie;
                    $addonEnvelopeDieModel->save();
                }
            }

            //Create AddonEnvelope Blocks With Envelope.
            if ( ! empty($addonEnvelopeBlocks)) {
                foreach ($addonEnvelopeBlocks as $addonEnvelopeBlock) {
                    $addonEnvelopeBlockModel                    = new \common\models\AddonEnvelopeBlock();
                    $addonEnvelopeBlockModel->addon_envelope_id = $addonEnvelopeModel->id;
                    $addonEnvelopeBlockModel->block_id          = $addonEnvelopeBlock;
                    $addonEnvelopeBlockModel->save();
                }
            }

            //Create AddonEnvelope Printing Method With Envelope.
            if ( ! empty($addonEnvelopePrintingMethods)) {
                foreach ($addonEnvelopePrintingMethods as $addonEnvelopePrintingMethod) {
                    $addonEnvelopePrintingMethodModel                     = new \common\models\AddonEnvelopePrintingMethod();
                    $addonEnvelopePrintingMethodModel->addon_envelope_id  = $addonEnvelopeModel->id;
                    $addonEnvelopePrintingMethodModel->printing_method_id = $addonEnvelopePrintingMethod;
                    $addonEnvelopePrintingMethodModel->save();
                }
            }
        }
    }

    /**
     * This Function is used for Folder Details save with cardId.
     * @param type $addonFolderModel
     * @param type $addonFolderDetails
     * @param type $data
     */
    public static function AddonFolder ($addonFolderModel, $addonFolderDetails, $data)
    {
        if ( ! empty($addonFolderDetails)) {
            $addonFolderPapers          = ! empty($addonFolderDetails['addon_folder_paper']) ? $addonFolderDetails['addon_folder_paper'] : array ();
            $addonFolderDies            = ! empty($addonFolderDetails['addon_folder_die']) ? $addonFolderDetails['addon_folder_die'] : array ();
            $addonFolderBlocks          = ! empty($addonFolderDetails['addon_folder_block']) ? $addonFolderDetails['addon_folder_block'] : array ();
            $addonFolderPrintingMethods = ! empty($addonFolderDetails['addon_folder_printing_method']) ? $addonFolderDetails['addon_folder_printing_method'] : array ();

            //Create New AddonFolder of Card.
            $addonFolderModel->addon_id = $data['id'];
            $addonFolderModel->save(false);

            //Create AddonFolder Papers With Folder.
            if ( ! empty($addonFolderPapers)) {
                foreach ($addonFolderPapers as $addonFolderPaper) {
                    $addonFolderPaperModel                  = new \common\models\AddonFolderPaper();
                    $addonFolderPaperModel->addon_folder_id = $addonFolderModel->id;
                    $addonFolderPaperModel->paper_id        = $addonFolderPaper;
                    $addonFolderPaperModel->save();
                }
            }

            //Create AddonFolder Dies With Folder.
            if ( ! empty($addonFolderDies)) {
                foreach ($addonFolderDies as $addonFolderDie) {
                    $addonFolderDieModel                  = new \common\models\AddonFolderDie();
                    $addonFolderDieModel->addon_folder_id = $addonFolderModel->id;
                    $addonFolderDieModel->die_id          = $addonFolderDie;
                    $addonFolderDieModel->save();
                }
            }

            //Create AddonFolder Blocks With Folder.
            if ( ! empty($addonFolderBlocks)) {
                foreach ($addonFolderBlocks as $addonFolderBlock) {
                    $addonFolderBlockModel                  = new \common\models\AddonFolderBlock();
                    $addonFolderBlockModel->addon_folder_id = $addonFolderModel->id;
                    $addonFolderBlockModel->block_id        = $addonFolderBlock;
                    $addonFolderBlockModel->save();
                }
            }

            //Create Folder Printing Method With Folder.
            if ( ! empty($addonFolderPrintingMethods)) {
                foreach ($addonFolderPrintingMethods as $addonFolderPrintingMethod) {
                    $addonFolderPrintingMethodModel                     = new \common\models\AddonFolderPrintingMethod();
                    $addonFolderPrintingMethodModel->addon_folder_id    = $addonFolderModel->id;
                    $addonFolderPrintingMethodModel->printing_method_id = $addonFolderPrintingMethod;
                    $addonFolderPrintingMethodModel->save();
                }
            }
        }
    }

    /**
     * This Function is used for Contact-US Details save with Contact-US.
     * @param type $contactUsDetailModels
     * @param type $ContactUsDetails
     * @param type $data
     */
    public static function ContactUsDetails ($contactUsDetailModels, $ContactUsDetails, $data)
    {
        if ( ! empty($ContactUsDetails)) {
            //Contact-Us Detail
            $contactUsOldIDs            = ArrayHelper::map($contactUsDetailModels, 'id', 'id');
            $contactUsDetailModels      = CommonModel::createMultiple(\common\models\ContactUsDetails::classname(), $contactUsDetailModels);
            CommonModel::loadMultiple($contactUsDetailModels, Yii::$app->request->post());
            $contactUsDetailsDeletedIDs = array_diff($contactUsOldIDs, array_filter(ArrayHelper::map($contactUsDetailModels, 'id', 'id')));

            if ( ! empty($contactUsDetailsDeletedIDs)) {
                \common\models\ContactUsDetails::deleteAll(['id' => $contactUsDetailsDeletedIDs]);
            }

            //Create Multiple Contact-Us Details Models.
            foreach ($contactUsDetailModels as $key => $contactUsDetailModel) {

                $contactUsDetailModel->name        = ! empty($ContactUsDetails[$key]['name']) ? $ContactUsDetails[$key]['name'] : '';
                $contactUsDetailModel->description = ! empty($ContactUsDetails[$key]['description']) ? $ContactUsDetails[$key]['description'] : '';

                /**
                 * ContactUs Detail Icon Upload.
                 */
                $contactUsDetailModel->icon = UploadedFile::getInstance($contactUsDetailModel, "[$key]icon");
                $oldIconImage               = $contactUsDetailModel->getOldAttribute('icon');
                if (empty($contactUsDetailModel->icon)) {
                    $contactUsDetailModel->icon = $oldIconImage;
                } else {
                    if ( ! empty($oldIconImage)) {
                        $originalFilePath = '/' . \common\models\ContactUsDetails::MAIN_DIRECTORY . \common\models\ContactUsDetails::ORIGINAL_FILEPATH . $oldIconImage;
                        $thumbFilePath    = '/' . \common\models\ContactUsDetails::MAIN_DIRECTORY . \common\models\ContactUsDetails::THUMBNAIL_FILEPATH . $oldIconImage;
                        if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                            unlink(Yii::$app->basePath . '/web' . $originalFilePath);
                            unlink(Yii::$app->basePath . '/web' . $thumbFilePath);
                        }
                    }
                    $contactUsDetailModel->icon = $contactUsDetailModel->upload();
                }

                //Create New Contact-Us Details of Contact-Us.
                $contactUsDetailModel->contact_us_id = $data['id'];
                $contactUsDetailModel->save();
            }
        }
    }

}
