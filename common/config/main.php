<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'timeZone' => 'Asia/Calcutta',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
	// 'as beforeRequest' => [  //if guest user access site so, redirect to login page.
        // 'class' => 'yii\filters\AccessControl',
        // 'rules' => [
            // [
                // 'actions' => ['login', 'error'],
                // 'allow' => true,
            // ],
            // [
                // 'allow' => true,
                // 'roles' => ['@'],
            // ],
        // ],
    // ],
];
