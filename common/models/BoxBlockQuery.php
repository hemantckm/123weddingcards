<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[BoxBlock]].
 *
 * @see BoxBlock
 */
class BoxBlockQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return BoxBlock[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return BoxBlock|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
