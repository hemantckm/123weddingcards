<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "paper_type".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class PaperType extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'paper_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['name'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'filter', 'filter' => 'trim'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * {@inheritdoc}
     * @return PaperTypeQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new PaperTypeQuery(get_called_class());
    }

}
