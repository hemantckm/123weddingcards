<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property string $heading1
 * @property string|null $description1
 * @property string $heading2
 * @property string|null $description2
 * @property string|null $image
 * @property int $default
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class About extends ActiveRecord
{

    const STATUS_ACTIVE        = 1;
    const STATUS_INACTIVE      = 0;
    const STATUS_DELETE        = 2;
    const DEFAULT_ACTIVE       = 1;
    const DEFAULT_INACTIVE     = 0;
    const MAIN_DIRECTORY       = 'uploads/about/';
    const ORIGINAL_FILEPATH    = 'original/';
    const THUMBNAIL_FILEPATH   = 'thumbnail/';
    const ORIGINAL_IMAGE_WIDTH = 485;
    const THUMB_IMAGE_HEIGHT   = 75;
    const THUMB_IMAGE_WIDTH    = 75;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'about';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['heading1', 'heading2', 'description1', 'description2'], 'required'],
            [['description1', 'description2'], 'string'],
            [['default', 'status', 'created_at', 'updated_at'], 'integer'],
            [['heading1', 'heading2'], 'string', 'max' => 100],
            [['image'], 'safe'],
            [['image'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,jpeg'],
            [['heading1', 'heading2', 'description1', 'description2'], 'filter', 'filter' => 'trim'],
            [['heading1'], 'unique'],
            [['heading2'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'heading1' => 'Heading1',
            'description1' => 'Description1',
            'heading2' => 'Heading2',
            'description2' => 'Description2',
            'image' => 'Image',
            'default' => 'Default',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * About Upload.
     * @return type
     */
    public function upload ()
    {
        //File BaseName.
        $baseName = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $this->heading1 . '_' . $this->heading2));

        //File Extension.
        $imageExtension = $this->image->extension;

        //Main Directory Path.
        FileHelper::createDirectory(About::MAIN_DIRECTORY);

        //Original File Path.
        $originalFilePath = About::MAIN_DIRECTORY . About::ORIGINAL_FILEPATH;
        FileHelper::createDirectory($originalFilePath);

        //Thumbnail File Path.
        $thumbnailFilePath = About::MAIN_DIRECTORY . About::THUMBNAIL_FILEPATH;
        FileHelper::createDirectory($thumbnailFilePath);

        //Original Image Save.
        $response = $this->image->saveAs($originalFilePath . $baseName . '.' . $imageExtension);

        //Original Image Rotation.
        if (($response == 1) && file_exists($originalFilePath . $baseName . '.' . $imageExtension)) {
            $imagine = Image::getImagine()->open($originalFilePath . $baseName . '.' . $imageExtension);
            $exif    = @exif_read_data($originalFilePath . $baseName . '.' . $imageExtension);

            //Rotate Image By Orientation.
            if ( ! empty($exif['Orientation'])) {
                switch ($exif['Orientation'])
                {
                    case 3:
                        $imagine->rotate(180);
                        break;
                    case 6:
                        $imagine->rotate(90);
                        break;
                    case 8:
                        $imagine->rotate(-90);
                        break;
                }
            }

            $imagine->save($originalFilePath . $baseName . '.' . $imageExtension, ['quality' => 70]);

            //Original Image Compress Image and Save.
            //1280x800 
            $sizes  = getimagesize($originalFilePath . $baseName . '.' . $imageExtension);
            $width  = About::ORIGINAL_IMAGE_WIDTH;
            $height = round($sizes[1] * $width / $sizes[0]);
            $imagine->resize(new Box($width, $height))->save($originalFilePath . $baseName . '.' . $imageExtension, ['quality' => 70]);

            //Thumbnail Image Save.
            Image::getImagine()
                    ->open($originalFilePath . $baseName . '.' . $imageExtension)
                    ->thumbnail(new Box(About::THUMB_IMAGE_WIDTH, About::THUMB_IMAGE_HEIGHT))
                    ->save($thumbnailFilePath . $baseName . '.' . $imageExtension);
            return $baseName . '.' . $imageExtension;
        }
    }

    /**
     * {@inheritdoc}
     * @return AboutQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AboutQuery(get_called_class());
    }

}
