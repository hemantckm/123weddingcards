<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "faq_faq_type".
 *
 * @property int $faq_id
 * @property int $faq_type_id
 *
 * @property Faq $faq
 * @property FaqType $faqType
 */
class FaqFaqType extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'faq_faq_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['faq_id', 'faq_type_id'], 'required'],
            [['faq_id', 'faq_type_id'], 'integer'],
            [['faq_id', 'faq_type_id'], 'unique', 'targetAttribute' => ['faq_id', 'faq_type_id']],
            [['faq_id'], 'exist', 'skipOnError' => true, 'targetClass' => Faq::className(), 'targetAttribute' => ['faq_id' => 'id']],
            [['faq_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => FaqType::className(), 'targetAttribute' => ['faq_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'faq_id' => 'Faq ID',
            'faq_type_id' => 'Faq Type ID',
        ];
    }

    /**
     * Gets query for [[Faq]].
     *
     * @return \yii\db\ActiveQuery|FaqQuery
     */
    public function getFaq ()
    {
        return $this->hasOne(Faq::className(), ['id' => 'faq_id']);
    }

    /**
     * Gets query for [[FaqType]].
     *
     * @return \yii\db\ActiveQuery|FaqTypeQuery
     */
    public function getFaqType ()
    {
        return $this->hasOne(FaqType::className(), ['id' => 'faq_type_id']);
    }

    /**
     * {@inheritdoc}
     * @return FaqFaqTypeQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new FaqFaqTypeQuery(get_called_class());
    }

}
