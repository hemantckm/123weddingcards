<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "box_block".
 *
 * @property int $box_id
 * @property int $block_id
 *
 * @property Block $block
 * @property Box $box
 */
class BoxBlock extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'box_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['box_id', 'block_id'], 'required'],
            [['box_id', 'block_id'], 'integer'],
            [['box_id', 'block_id'], 'unique', 'targetAttribute' => ['box_id', 'block_id']],
            [['block_id'], 'exist', 'skipOnError' => true, 'targetClass' => Block::className(), 'targetAttribute' => ['block_id' => 'id']],
            [['box_id'], 'exist', 'skipOnError' => true, 'targetClass' => Box::className(), 'targetAttribute' => ['box_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'box_id' => 'Box ID',
            'block_id' => 'Block ID',
        ];
    }

    /**
     * Gets query for [[Block]].
     *
     * @return \yii\db\ActiveQuery|BlockQuery
     */
    public function getBlock ()
    {
        return $this->hasOne(Block::className(), ['id' => 'block_id']);
    }

    /**
     * Gets query for [[Box]].
     *
     * @return \yii\db\ActiveQuery|BoxQuery
     */
    public function getBox ()
    {
        return $this->hasOne(Box::className(), ['id' => 'box_id']);
    }

    /**
     * {@inheritdoc}
     * @return BoxBlockQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new BoxBlockQuery(get_called_class());
    }

}
