<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ScrollDie]].
 *
 * @see ScrollDie
 */
class ScrollDieQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ScrollDie[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ScrollDie|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
