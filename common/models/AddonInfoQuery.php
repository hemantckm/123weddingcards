<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[AddonInfo]].
 *
 * @see AddonInfo
 */
class AddonInfoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return AddonInfo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AddonInfo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
