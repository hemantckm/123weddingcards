<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "paper".
 *
 * @property int $id
 * @property string $code
 * @property int|null $paper_type_id
 * @property int|null $color_id
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Color $color
 * @property PaperType $paperType
 */
class Paper extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'paper';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['code', 'paper_type_id', 'color_id'], 'required'],
            [['paper_type_id', 'color_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['code'], 'string', 'max' => 50],
            [['code'], 'filter', 'filter' => 'trim'],
            [['code'], 'unique'],
            [['color_id'], 'exist', 'skipOnError' => true, 'targetClass' => Color::className(), 'targetAttribute' => ['color_id' => 'id']],
            [['paper_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaperType::className(), 'targetAttribute' => ['paper_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'paper_type_id' => 'Paper Type',
            'color_id' => 'Color',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[Color]].
     *
     * @return \yii\db\ActiveQuery|ColorQuery
     */
    public function getColor ()
    {
        return $this->hasOne(Color::className(), ['id' => 'color_id']);
    }

    /**
     * Gets query for [[PaperType]].
     *
     * @return \yii\db\ActiveQuery|PaperTypeQuery
     */
    public function getPaperType ()
    {
        return $this->hasOne(PaperType::className(), ['id' => 'paper_type_id']);
    }

    /**
     * {@inheritdoc}
     * @return PaperQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new PaperQuery(get_called_class());
    }

}
