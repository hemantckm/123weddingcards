<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Color]].
 *
 * @see Color
 */
class ColorQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Color[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Color|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get Color List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, name')
                ->asArray()
                ->where(['status' => Color::STATUS_ACTIVE])
                ->orderBy('name asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'name');
    }

}
