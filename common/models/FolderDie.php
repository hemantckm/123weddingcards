<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "folder_die".
 *
 * @property int $folder_id
 * @property int $die_id
 *
 * @property DieCut $die
 * @property Folder $folder
 */
class FolderDie extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'folder_die';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['folder_id', 'die_id'], 'required'],
            [['folder_id', 'die_id'], 'integer'],
            [['folder_id', 'die_id'], 'unique', 'targetAttribute' => ['folder_id', 'die_id']],
            [['die_id'], 'exist', 'skipOnError' => true, 'targetClass' => DieCut::className(), 'targetAttribute' => ['die_id' => 'id']],
            [['folder_id'], 'exist', 'skipOnError' => true, 'targetClass' => Folder::className(), 'targetAttribute' => ['folder_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'folder_id' => 'Folder ID',
            'die_id' => 'Die ID',
        ];
    }

    /**
     * Gets query for [[Die]].
     *
     * @return \yii\db\ActiveQuery|DieCutQuery
     */
    public function getDie ()
    {
        return $this->hasOne(DieCut::className(), ['id' => 'die_id']);
    }

    /**
     * Gets query for [[Folder]].
     *
     * @return \yii\db\ActiveQuery|FolderQuery
     */
    public function getFolder ()
    {
        return $this->hasOne(Folder::className(), ['id' => 'folder_id']);
    }

    /**
     * {@inheritdoc}
     * @return FolderDieQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new FolderDieQuery(get_called_class());
    }

}
