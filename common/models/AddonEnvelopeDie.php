<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "addon_envelope_die".
 *
 * @property int $addon_envelope_id
 * @property int $die_id
 *
 * @property AddonEnvelope $addonEnvelope
 * @property DieCut $die
 */
class AddonEnvelopeDie extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'addon_envelope_die';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['addon_envelope_id', 'die_id'], 'required'],
            [['addon_envelope_id', 'die_id'], 'integer'],
            [['addon_envelope_id', 'die_id'], 'unique', 'targetAttribute' => ['addon_envelope_id', 'die_id']],
            [['addon_envelope_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddonEnvelope::className(), 'targetAttribute' => ['addon_envelope_id' => 'id']],
            [['die_id'], 'exist', 'skipOnError' => true, 'targetClass' => DieCut::className(), 'targetAttribute' => ['die_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'addon_envelope_id' => 'Addon Envelope ID',
            'die_id' => 'Die ID',
        ];
    }

    /**
     * Gets query for [[AddonEnvelope]].
     *
     * @return \yii\db\ActiveQuery|AddonEnvelopeQuery
     */
    public function getAddonEnvelope ()
    {
        return $this->hasOne(AddonEnvelope::className(), ['id' => 'addon_envelope_id']);
    }

    /**
     * Gets query for [[Die]].
     *
     * @return \yii\db\ActiveQuery|DieCutQuery
     */
    public function getDie ()
    {
        return $this->hasOne(DieCut::className(), ['id' => 'die_id']);
    }

    /**
     * {@inheritdoc}
     * @return AddonEnvelopeDieQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AddonEnvelopeDieQuery(get_called_class());
    }

}
