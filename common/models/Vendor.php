<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * This is the model class for table "vendor".
 *
 * @property int $id
 * @property string $name
 * @property string|null $image
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Vendor extends ActiveRecord
{

    const STATUS_ACTIVE         = 1;
    const STATUS_INACTIVE       = 0;
    const STATUS_DELETE         = 2;
    const MAIN_DIRECTORY        = 'uploads/vendor/';
    const ORIGINAL_FILEPATH     = 'original/';
    const THUMBNAIL_FILEPATH    = 'thumbnail/';
    const ORIGINAL_IMAGE_WIDTH  = 152;
    const ORIGINAL_IMAGE_HEIGHT = 51;
    const THUMB_IMAGE_HEIGHT    = 45;
    const THUMB_IMAGE_WIDTH     = 45;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'vendor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['name'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 255],
            [['image'], 'safe'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, svg'],
            [['name'], 'filter', 'filter' => 'trim'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image' => 'Image',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Vendor Upload.
     * @return type
     */
    public function upload ()
    {
        //File BaseName.
        $baseName = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $this->name));

        //File Extension.
        $imageExtension = $this->image->extension;

        //Main Directory Path.
        FileHelper::createDirectory(Vendor::MAIN_DIRECTORY);

        //Original File Path.
        $originalFilePath = Vendor::MAIN_DIRECTORY . Vendor::ORIGINAL_FILEPATH;
        FileHelper::createDirectory($originalFilePath);

        //Thumbnail File Path.
        $thumbnailFilePath = Vendor::MAIN_DIRECTORY . Vendor::THUMBNAIL_FILEPATH;
        FileHelper::createDirectory($thumbnailFilePath);

        //Original Image Save.
        $response = $this->image->saveAs($originalFilePath . $baseName . '.' . $imageExtension);

        //Original Image Rotation.
        if (($response == 1) && file_exists($originalFilePath . $baseName . '.' . $imageExtension)) {
            $imagine = Image::getImagine()->open($originalFilePath . $baseName . '.' . $imageExtension);
            $exif    = @exif_read_data($originalFilePath . $baseName . '.' . $imageExtension);

            //Rotate Image By Orientation.
            if ( ! empty($exif['Orientation'])) {
                switch ($exif['Orientation'])
                {
                    case 3:
                        $imagine->rotate(180);
                        break;
                    case 6:
                        $imagine->rotate(90);
                        break;
                    case 8:
                        $imagine->rotate(-90);
                        break;
                }
            }

            $imagine->save($originalFilePath . $baseName . '.' . $imageExtension, ['quality' => 70]);

            //Original Image Compress Image and Save.
            //1280x800 
            $sizes  = getimagesize($originalFilePath . $baseName . '.' . $imageExtension);
            $width  = Vendor::ORIGINAL_IMAGE_WIDTH;
            //$height = round($sizes[1] * $width / $sizes[0]);
            $height = Vendor::ORIGINAL_IMAGE_HEIGHT;
            $imagine->resize(new Box($width, $height))->save($originalFilePath . $baseName . '.' . $imageExtension, ['quality' => 70]);

            //Thumbnail Image Save.
            Image::getImagine()
                    ->open($originalFilePath . $baseName . '.' . $imageExtension)
                    ->thumbnail(new Box(Vendor::THUMB_IMAGE_WIDTH, Vendor::THUMB_IMAGE_HEIGHT))
                    ->save($thumbnailFilePath . $baseName . '.' . $imageExtension);
            return $baseName . '.' . $imageExtension;
        }
    }

    /**
     * {@inheritdoc}
     * @return VendorQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new VendorQuery(get_called_class());
    }

}
