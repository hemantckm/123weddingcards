<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "envelope_printing_method".
 *
 * @property int $envelope_id
 * @property int $printing_method_id
 *
 * @property Envelope $envelope
 * @property PrintingMethod $printingMethod
 */
class EnvelopePrintingMethod extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'envelope_printing_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['envelope_id', 'printing_method_id'], 'required'],
            [['envelope_id', 'printing_method_id'], 'integer'],
            [['envelope_id', 'printing_method_id'], 'unique', 'targetAttribute' => ['envelope_id', 'printing_method_id']],
            [['envelope_id'], 'exist', 'skipOnError' => true, 'targetClass' => Envelope::className(), 'targetAttribute' => ['envelope_id' => 'id']],
            [['printing_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingMethod::className(), 'targetAttribute' => ['printing_method_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'envelope_id' => 'Envelope ID',
            'printing_method_id' => 'Printing Method ID',
        ];
    }

    /**
     * Gets query for [[Envelope]].
     *
     * @return \yii\db\ActiveQuery|EnvelopeQuery
     */
    public function getEnvelope ()
    {
        return $this->hasOne(Envelope::className(), ['id' => 'envelope_id']);
    }

    /**
     * Gets query for [[PrintingMethod]].
     *
     * @return \yii\db\ActiveQuery|PrintingMethodQuery
     */
    public function getPrintingMethod ()
    {
        return $this->hasOne(PrintingMethod::className(), ['id' => 'printing_method_id']);
    }

    /**
     * {@inheritdoc}
     * @return EnvelopePrintingMethodQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new EnvelopePrintingMethodQuery(get_called_class());
    }

}
