<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "addon_envelope_paper".
 *
 * @property int $addon_envelope_id
 * @property int $paper_id
 *
 * @property AddonEnvelope $addonEnvelope
 * @property Paper $paper
 */
class AddonEnvelopePaper extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'addon_envelope_paper';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['addon_envelope_id', 'paper_id'], 'required'],
            [['addon_envelope_id', 'paper_id'], 'integer'],
            [['addon_envelope_id', 'paper_id'], 'unique', 'targetAttribute' => ['addon_envelope_id', 'paper_id']],
            [['addon_envelope_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddonEnvelope::className(), 'targetAttribute' => ['addon_envelope_id' => 'id']],
            [['paper_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paper::className(), 'targetAttribute' => ['paper_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'addon_envelope_id' => 'Addon Envelope ID',
            'paper_id' => 'Paper ID',
        ];
    }

    /**
     * Gets query for [[AddonEnvelope]].
     *
     * @return \yii\db\ActiveQuery|AddonEnvelopeQuery
     */
    public function getAddonEnvelope()
    {
        return $this->hasOne(AddonEnvelope::className(), ['id' => 'addon_envelope_id']);
    }

    /**
     * Gets query for [[Paper]].
     *
     * @return \yii\db\ActiveQuery|PaperQuery
     */
    public function getPaper()
    {
        return $this->hasOne(Paper::className(), ['id' => 'paper_id']);
    }

    /**
     * {@inheritdoc}
     * @return AddonEnvelopePaperQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AddonEnvelopePaperQuery(get_called_class());
    }
}
