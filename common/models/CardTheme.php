<?php

namespace common\models;

use Yii;
use common\components\ActiveRecord;

/**
 * This is the model class for table "card_theme".
 *
 * @property int $card_id
 * @property int $theme_id
 *
 * @property Card $card
 * @property Theme $theme
 */
class CardTheme extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'card_theme';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['card_id', 'theme_id'], 'required'],
            [['card_id', 'theme_id'], 'integer'],
            [['card_id', 'theme_id'], 'unique', 'targetAttribute' => ['card_id', 'theme_id']],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['card_id' => 'id']],
            [['theme_id'], 'exist', 'skipOnError' => true, 'targetClass' => Theme::className(), 'targetAttribute' => ['theme_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'card_id' => 'Card ID',
            'theme_id' => 'Theme ID',
        ];
    }

    /**
     * Gets query for [[Card]].
     *
     * @return \yii\db\ActiveQuery|CardQuery
     */
    public function getCard ()
    {
        return $this->hasOne(Card::className(), ['id' => 'card_id']);
    }

    /**
     * Gets query for [[Theme]].
     *
     * @return \yii\db\ActiveQuery|ThemeQuery
     */
    public function getTheme ()
    {
        return $this->hasOne(Theme::className(), ['id' => 'theme_id']);
    }

    /**
     * {@inheritdoc}
     * @return CardThemeQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new CardThemeQuery(get_called_class());
    }

}
