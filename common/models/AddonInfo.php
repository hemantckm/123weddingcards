<?php

namespace common\models;

use Yii;
use common\components\ActiveRecord;

/**
 * This is the model class for table "addon_info".
 *
 * @property int $addon_id
 * @property int|null $card_id
 * @property string|null $addon_type
 * @property string|null $addon_style
 * @property string|null $envelope
 * @property string|null $folder
 * @property int $status
 *
 * @property Addon $addon
 * @property Card $card
 */
class AddonInfo extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'addon_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['card_id', 'status'], 'integer'],
            [['addon_type', 'addon_style', 'envelope', 'folder'], 'string'],
            [['addon_id'], 'exist', 'skipOnError' => true, 'targetClass' => Addon::className(), 'targetAttribute' => ['addon_id' => 'id']],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['card_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'addon_id' => 'Addon ID',
            'card_id' => 'Card ID',
            'addon_type' => 'Addon Type',
            'addon_style' => 'Addon Style',
            'envelope' => 'Envelope',
            'folder' => 'Folder',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[Addon]].
     *
     * @return \yii\db\ActiveQuery|AddonQuery
     */
    public function getAddon ()
    {
        return $this->hasOne(Addon::className(), ['id' => 'addon_id']);
    }

    /**
     * Gets query for [[Card]].
     *
     * @return \yii\db\ActiveQuery|CardQuery
     */
    public function getCard ()
    {
        return $this->hasOne(Card::className(), ['id' => 'card_id']);
    }

    /**
     * {@inheritdoc}
     * @return AddonInfoQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AddonInfoQuery(get_called_class());
    }

    /**
     * Update Addon Info by AddonId
     * @param type $addonId
     */
    public static function updateAddonInfo ($addonId)
    {
        // AddonEnvelopes (Multiple)
        $addonEnvelopeArray = array ();
        $addonEnvelopes     = \Yii::$app->db->createCommand("SELECT ae.id FROM addon_envelope ae INNER JOIN addon a ON FIND_IN_SET(ae.addon_id, a.id) > 0 Where ae.addon_id = " . $addonId . " GROUP BY ae.addon_id")->queryAll();
        if ( ! empty($addonEnvelopes)) {
            foreach ($addonEnvelopes as $addonEnvelope) {
                //Addon Envelope Papers (Multiple)
                $addonEnvelopePapers = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', p.code order by p.code) paper FROM addon_envelope_paper aep INNER JOIN paper p ON FIND_IN_SET(p.id, aep.paper_id) > 0 Where aep.addon_envelope_id = " . $addonEnvelope['id'] . " GROUP BY aep.addon_envelope_id")->queryAll();
                if ( ! empty($addonEnvelopePapers)) {
                    foreach ($addonEnvelopePapers as $addonEnvelopePaper) {
                        $addonEnvelopeArray[$addonEnvelope['id']]['paper'] = $addonEnvelopePaper['paper'];
                    }
                }

                //Addon Envelope Paper Colors (Multiple)
                $addonEnvelopePaperColors     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', c.name order by c.name) color FROM addon_envelope_paper aep INNER JOIN paper p ON FIND_IN_SET(p.id, aep.paper_id) > 0 INNER JOIN color c ON FIND_IN_SET(c.id,p.color_id) > 0 Where aep.addon_envelope_id = " . $addonEnvelope['id'] . " GROUP BY p.id")->queryAll();
                $addonEnvelopePaperColorArray = array ();
                foreach ($addonEnvelopePaperColors as $addonEnvelopePaperColor) {
                    $addonEnvelopePaperColorArray[] = $addonEnvelopePaperColor['color'];
                }
                $addonEnvelopeArray[$addonEnvelope['id']]['paper_color'] = implode(', ', $addonEnvelopePaperColorArray);

                //Addon Envelope Paper Type (Multiple)
                $addonEnvelopePaperTypes     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pt.name order by pt.name) paper_type FROM addon_envelope_paper aep INNER JOIN paper p ON FIND_IN_SET(p.id, aep.paper_id) > 0 INNER JOIN paper_type pt ON FIND_IN_SET(pt.id,p.paper_type_id) > 0 Where aep.addon_envelope_id = " . $addonEnvelope['id'] . " GROUP BY p.id")->queryAll();
                $addonEnvelopePaperTypeArray = array ();
                foreach ($addonEnvelopePaperTypes as $addonEnvelopePaperType) {
                    $addonEnvelopePaperTypeArray[] = $addonEnvelopePaperType['paper_type'];
                }
                $addonEnvelopeArray[$addonEnvelope['id']]['paper_type'] = implode(', ', $addonEnvelopePaperTypeArray);

                //Addon Envelope DieCut (Multiple)
                $addonEnvelopeDies = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', dc.code order by dc.code) diecut FROM addon_envelope_die aed INNER JOIN die_cut dc ON FIND_IN_SET(dc.id, aed.die_id) > 0 Where aed.addon_envelope_id = " . $addonEnvelope['id'] . " GROUP BY aed.addon_envelope_id")->queryAll();
                if ( ! empty($addonEnvelopeDies)) {
                    foreach ($addonEnvelopeDies as $addonEnvelopeDie) {
                        $addonEnvelopeArray[$addonEnvelope['id']]['diecut'] = $addonEnvelopeDie['diecut'];
                    }
                }

                //Addon Envelope Block (Multiple)
                $addonEnvelopeBlocks = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', b.code order by b.code) block FROM addon_envelope_block aeb INNER JOIN block b ON FIND_IN_SET(b.id, aeb.block_id) > 0 Where aeb.addon_envelope_id = " . $addonEnvelope['id'] . " GROUP BY aeb.addon_envelope_id")->queryAll();
                if ( ! empty($addonEnvelopeBlocks)) {
                    foreach ($addonEnvelopeBlocks as $addonEnvelopeBlock) {
                        $addonEnvelopeArray[$addonEnvelope['id']]['block'] = $addonEnvelopeBlock['block'];
                    }
                }

                //Addon Envelope PrintingMethod (Multiple)
                $addonEnvelopePrintingMethods = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pm.name order by pm.name) printing_method FROM addon_envelope_printing_method aepm INNER JOIN printing_method pm ON FIND_IN_SET(pm.id, aepm.printing_method_id) > 0 Where aepm.addon_envelope_id = " . $addonEnvelope['id'] . " GROUP BY aepm.addon_envelope_id")->queryAll();
                if ( ! empty($addonEnvelopePrintingMethods)) {
                    foreach ($addonEnvelopePrintingMethods as $addonEnvelopePrintingMethod) {
                        $addonEnvelopeArray[$addonEnvelope['id']]['printing_method'] = $addonEnvelopePrintingMethod['printing_method'];
                    }
                }
            }
        }

        // AddonFolders (Multiple)
        $addonFolderArray = array ();
        $addonFolders     = \Yii::$app->db->createCommand("SELECT af.id FROM addon_folder af INNER JOIN addon a ON FIND_IN_SET(af.addon_id, a.id) > 0 Where af.addon_id = " . $addonId . " GROUP BY af.addon_id")->queryAll();
        if ( ! empty($addonFolders)) {
            foreach ($addonFolders as $addonFolder) {
                //Addon Folder Papers (Multiple)
                $addonFolderPapers = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', p.code order by p.code) paper FROM addon_folder_paper afp INNER JOIN paper p ON FIND_IN_SET(p.id, afp.paper_id) > 0 Where afp.addon_folder_id = " . $addonFolder['id'] . " GROUP BY afp.addon_folder_id")->queryAll();
                if ( ! empty($addonFolderPapers)) {
                    foreach ($addonFolderPapers as $addonFolderPaper) {
                        $addonFolderArray[$addonFolder['id']]['paper'] = $addonFolderPaper['paper'];
                    }
                }

                //Addon Folder Paper Colors (Multiple)
                $addonFolderPaperColors     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', c.name order by c.name) color FROM addon_folder_paper afp INNER JOIN paper p ON FIND_IN_SET(p.id, afp.paper_id) > 0 INNER JOIN color c ON FIND_IN_SET(c.id,p.color_id) > 0 Where afp.addon_folder_id = " . $addonFolder['id'] . " GROUP BY p.id")->queryAll();
                $addonFolderPaperColorArray = array ();
                foreach ($addonFolderPaperColors as $addonFolderPaperColor) {
                    $addonFolderPaperColorArray[] = $addonFolderPaperColor['color'];
                }
                $addonFolderArray[$addonFolder['id']]['paper_color'] = implode(', ', $addonFolderPaperColorArray);

                //Addon Folder Paper Type (Multiple)
                $addonFolderPaperTypes     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pt.name order by pt.name) paper_type FROM addon_folder_paper afp INNER JOIN paper p ON FIND_IN_SET(p.id, afp.paper_id) > 0 INNER JOIN paper_type pt ON FIND_IN_SET(pt.id,p.paper_type_id) > 0 Where afp.addon_folder_id = " . $addonFolder['id'] . " GROUP BY p.id")->queryAll();
                $addonFolderPaperTypeArray = array ();
                foreach ($addonFolderPaperTypes as $addonFolderPaperType) {
                    $addonFolderPaperTypeArray[] = $addonFolderPaperType['paper_type'];
                }
                $addonFolderArray[$addonFolder['id']]['paper_type'] = implode(', ', $addonFolderPaperTypeArray);

                //Addon Folder DieCut (Multiple)
                $addonFolderDies = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', dc.code order by dc.code) diecut FROM addon_folder_die afd INNER JOIN die_cut dc ON FIND_IN_SET(dc.id, afd.die_id) > 0 Where afd.addon_folder_id = " . $addonFolder['id'] . " GROUP BY afd.addon_folder_id")->queryAll();
                if ( ! empty($addonFolderDies)) {
                    foreach ($addonFolderDies as $addonFolderDie) {
                        $addonFolderArray[$addonFolder['id']]['diecut'] = $addonFolderDie['diecut'];
                    }
                }

                //Addon Folder Block (Multiple)
                $addonFolderBlocks = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', b.code order by b.code) block FROM addon_folder_block afb INNER JOIN block b ON FIND_IN_SET(b.id, afb.block_id) > 0 Where afb.addon_folder_id = " . $addonFolder['id'] . " GROUP BY afb.addon_folder_id")->queryAll();
                if ( ! empty($addonFolderBlocks)) {
                    foreach ($addonFolderBlocks as $addonFolderBlock) {
                        $addonFolderArray[$addonFolder['id']]['block'] = $addonFolderBlock['block'];
                    }
                }

                //Addon Folder PrintingMethod (Multiple)
                $addonFolderPrintingMethods = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pm.name order by pm.name) printing_method FROM addon_folder_printing_method afpm INNER JOIN printing_method pm ON FIND_IN_SET(pm.id, afpm.printing_method_id) > 0 Where afpm.addon_folder_id = " . $addonFolder['id'] . " GROUP BY afpm.addon_folder_id")->queryAll();
                if ( ! empty($addonFolderPrintingMethods)) {
                    foreach ($addonFolderPrintingMethods as $addonFolderPrintingMethod) {
                        $addonFolderArray[$addonFolder['id']]['printing_method'] = $addonFolderPrintingMethod['printing_method'];
                    }
                }
            }
        }

        // Get AddonInfo.
        $fields = ['a.id', 'a.card_id', 'at.name addon_type_name', 'ast.name addon_style_name', 'a.status'];
        $addon  = (new \yii\db\Query())
                ->select($fields)
                ->from('addon a')
                ->innerJoin('card c', 'c.id = a.card_id')
                ->leftJoin('addon_type at', 'at.id = a.addon_type_id')
                ->leftJoin('addon_style ast', 'ast.id = a.addon_style_id')
                ->andWhere(['a.id' => $addonId])
                ->one();

        if (($addonInfo = AddonInfo::findOne($addonId)) == null) {
            $addonInfo = new AddonInfo();
        }
        $addonInfo->addon_id    = $addonId;
        $addonInfo->card_id     = $addon['card_id'];
        $addonInfo->addon_type  = $addon['addon_type_name'];
        $addonInfo->addon_style = $addon['addon_style_name'];
        $addonInfo->envelope    = ! empty($addonEnvelopeArray) ? trim(json_encode($addonEnvelopeArray, TRUE)) : '';
        $addonInfo->folder      = ! empty($addonFolderArray) ? trim(json_encode($addonFolderArray, TRUE)) : '';
        $addonInfo->status      = $addon['status'];

        if ( ! $addonInfo->save()) {
            print_r($addonInfo->getErrors());
        }
    }

}
