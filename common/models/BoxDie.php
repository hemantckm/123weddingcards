<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "box_die".
 *
 * @property int $box_id
 * @property int $die_id
 *
 * @property Box $box
 * @property DieCut $die
 */
class BoxDie extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'box_die';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['box_id', 'die_id'], 'required'],
            [['box_id', 'die_id'], 'integer'],
            [['box_id', 'die_id'], 'unique', 'targetAttribute' => ['box_id', 'die_id']],
            [['box_id'], 'exist', 'skipOnError' => true, 'targetClass' => Box::className(), 'targetAttribute' => ['box_id' => 'id']],
            [['die_id'], 'exist', 'skipOnError' => true, 'targetClass' => DieCut::className(), 'targetAttribute' => ['die_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'box_id' => 'Box ID',
            'die_id' => 'Die ID',
        ];
    }

    /**
     * Gets query for [[Box]].
     *
     * @return \yii\db\ActiveQuery|BoxQuery
     */
    public function getBox ()
    {
        return $this->hasOne(Box::className(), ['id' => 'box_id']);
    }

    /**
     * Gets query for [[Die]].
     *
     * @return \yii\db\ActiveQuery|DieCutQuery
     */
    public function getDie ()
    {
        return $this->hasOne(DieCut::className(), ['id' => 'die_id']);
    }

    /**
     * {@inheritdoc}
     * @return BoxDieQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new BoxDieQuery(get_called_class());
    }

}
