<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "website".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string|null $code
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Website extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;
    const CODE            = 'web';

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'website';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['name', 'url'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'code'], 'string', 'max' => 50],
            [['url'], 'string', 'max' => 255],
            [['url'], 'url'],
            [['name', 'url'], 'filter', 'filter' => 'trim'],
            [['name'], 'unique'],
            [['url'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'code' => 'Code',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * {@inheritdoc}
     * @return WebsiteQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new WebsiteQuery(get_called_class());
    }

}
