<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Tube]].
 *
 * @see Tube
 */
class TubeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tube[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tube|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
