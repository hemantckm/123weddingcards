<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[InsertBlock]].
 *
 * @see InsertBlock
 */
class InsertBlockQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return InsertBlock[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return InsertBlock|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
