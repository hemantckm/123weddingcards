<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "box".
 *
 * @property int $id
 * @property int|null $card_id
 * @property int|null $box_type_id
 * @property float $length
 * @property float $width
 * @property float $thickness
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property BoxType $boxType
 * @property Card $card
 * @property BoxBlock[] $boxBlocks
 * @property Block[] $blocks
 * @property BoxDie[] $boxDies
 * @property DieCut[] $dies
 * @property BoxPaper[] $boxPapers
 * @property Paper[] $papers
 * @property BoxPrintingMethod[] $boxPrintingMethods
 * @property PrintingMethod[] $printingMethods
 */
class Box extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * Box Paper Function Define.
     * @var type 
     */
    public $box_paper;

    /**
     * Box Die Function Define.
     * @var type 
     */
    public $box_die;

    /**
     * Box Block Function Define.
     * @var type 
     */
    public $box_block;

    /**
     * Box Printing Method Function Define.
     * @var type 
     */
    public $box_printing_method;

    /**
     * Paper Color Function Define.
     * @var type 
     */
    public $paper_color;

    /**
     * Paper Type Function Define.
     * @var type 
     */
    public $paper_type;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'box';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['card_id', /*'box_type_id' , 'box_paper', 'box_die', 'box_block', 'box_printing_method' */], 'required'],
            [['card_id', 'box_type_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['length', 'width', 'thickness'], 'number'],
            [['box_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BoxType::className(), 'targetAttribute' => ['box_type_id' => 'id']],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['card_id' => 'id']],
            //BoxType Field Not Empty.
            [['length', 'width', 'thickness'], 'required', 'when' => function($model) {
                    return ( ! empty($model->box_type_id) && ($model->box_type_id > 0));
                },
                'whenClient' => "function (attribute, value) { 
                    return $('#box-box_type_id').val() > '0'; 
                }"
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'card_id' => 'Card',
            'box_type_id' => 'Box Type',
            'length' => 'Length',
            'width' => 'Width',
            'thickness' => 'Thickness',
            'box_paper' => 'Paper',
            'box_die' => 'Die',
            'box_block' => 'Block',
            'box_printing_method' => 'Printing Method',
            'paper_color' => 'Paper Color',
            'paper_type' => 'Paper Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[BoxType]].
     *
     * @return \yii\db\ActiveQuery|BoxTypeQuery
     */
    public function getBoxType ()
    {
        return $this->hasOne(BoxType::className(), ['id' => 'box_type_id']);
    }

    /**
     * Gets query for [[Card]].
     *
     * @return \yii\db\ActiveQuery|CardQuery
     */
    public function getCard ()
    {
        return $this->hasOne(Card::className(), ['id' => 'card_id']);
    }

    /**
     * Gets query for [[BoxBlocks]].
     *
     * @return \yii\db\ActiveQuery|BoxBlockQuery
     */
    public function getBoxBlocks ()
    {
        return $this->hasMany(BoxBlock::className(), ['box_id' => 'id']);
    }

    /**
     * Gets query for [[Blocks]].
     *
     * @return \yii\db\ActiveQuery|BlockQuery
     */
    public function getBlocks ()
    {
        return $this->hasMany(Block::className(), ['id' => 'block_id'])->viaTable('box_block', ['box_id' => 'id']);
    }

    /**
     * Gets query for [[BoxDies]].
     *
     * @return \yii\db\ActiveQuery|BoxDieQuery
     */
    public function getBoxDies ()
    {
        return $this->hasMany(BoxDie::className(), ['box_id' => 'id']);
    }

    /**
     * Gets query for [[Dies]].
     *
     * @return \yii\db\ActiveQuery|DieCutQuery
     */
    public function getDies ()
    {
        return $this->hasMany(DieCut::className(), ['id' => 'die_id'])->viaTable('box_die', ['box_id' => 'id']);
    }

    /**
     * Gets query for [[BoxPapers]].
     *
     * @return \yii\db\ActiveQuery|BoxPaperQuery
     */
    public function getBoxPapers ()
    {
        return $this->hasMany(BoxPaper::className(), ['box_id' => 'id']);
    }

    /**
     * Gets query for [[Papers]].
     *
     * @return \yii\db\ActiveQuery|PaperQuery
     */
    public function getPapers ()
    {
        return $this->hasMany(Paper::className(), ['id' => 'paper_id'])->viaTable('box_paper', ['box_id' => 'id']);
    }

    /**
     * Gets query for [[BoxPrintingMethods]].
     *
     * @return \yii\db\ActiveQuery|BoxPrintingMethodQuery
     */
    public function getBoxPrintingMethods ()
    {
        return $this->hasMany(BoxPrintingMethod::className(), ['box_id' => 'id']);
    }

    /**
     * Gets query for [[PrintingMethods]].
     *
     * @return \yii\db\ActiveQuery|PrintingMethodQuery
     */
    public function getPrintingMethods ()
    {
        return $this->hasMany(PrintingMethod::className(), ['id' => 'printing_method_id'])->viaTable('box_printing_method', ['box_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return BoxQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new BoxQuery(get_called_class());
    }

}
