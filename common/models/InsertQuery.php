<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Insert]].
 *
 * @see Insert
 */
class InsertQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Insert[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Insert|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
