<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[FolderBlock]].
 *
 * @see FolderBlock
 */
class FolderBlockQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return FolderBlock[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FolderBlock|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
