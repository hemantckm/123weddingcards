<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "envelope_paper".
 *
 * @property int $envelope_id
 * @property int $paper_id
 *
 * @property Envelope $envelope
 * @property Paper $paper
 */
class EnvelopePaper extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'envelope_paper';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['envelope_id', 'paper_id'], 'required'],
            [['envelope_id', 'paper_id'], 'integer'],
            [['envelope_id', 'paper_id'], 'unique', 'targetAttribute' => ['envelope_id', 'paper_id']],
            [['envelope_id'], 'exist', 'skipOnError' => true, 'targetClass' => Envelope::className(), 'targetAttribute' => ['envelope_id' => 'id']],
            [['paper_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paper::className(), 'targetAttribute' => ['paper_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'envelope_id' => 'Envelope ID',
            'paper_id' => 'Paper ID',
        ];
    }

    /**
     * Gets query for [[Envelope]].
     *
     * @return \yii\db\ActiveQuery|EnvelopeQuery
     */
    public function getEnvelope ()
    {
        return $this->hasOne(Envelope::className(), ['id' => 'envelope_id']);
    }

    /**
     * Gets query for [[Paper]].
     *
     * @return \yii\db\ActiveQuery|PaperQuery
     */
    public function getPaper ()
    {
        return $this->hasOne(Paper::className(), ['id' => 'paper_id']);
    }

    /**
     * {@inheritdoc}
     * @return EnvelopePaperQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new EnvelopePaperQuery(get_called_class());
    }

}
