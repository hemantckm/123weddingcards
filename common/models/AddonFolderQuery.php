<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[AddonFolder]].
 *
 * @see AddonFolder
 */
class AddonFolderQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return AddonFolder[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AddonFolder|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
