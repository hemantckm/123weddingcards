<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\models\User;

/**
 * This is the model class for table "audit_trail".
 *
 * @property int $id
 * @property string $old_value
 * @property string $new_value
 * @property string $action
 * @property string $model
 * @property string $field
 * @property string $stamp
 * @property string $user_id
 * @property string $model_id
 */
class AuditTrail extends ActiveRecord
{

    const ACTION_DELETE = 'DELETE';
    const ACTION_CREATE = 'CREATE';
    const ACTION_SET    = 'SET';
    const ACTION_CHANGE = 'CHANGE';

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'audit_trail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['old_value', 'new_value'], 'string'],
            [['action', 'model', 'stamp', 'model_id'], 'required'],
            [['stamp'], 'safe'],
            [['action', 'model', 'field', 'user_id', 'model_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'old_value' => 'Old Value',
            'new_value' => 'New Value',
            'action' => 'Action',
            'model' => 'Model',
            'field' => 'Field',
            'stamp' => 'Stamp',
            'user_id' => 'User ID',
            'model_id' => 'Model ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return AuditTrailQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AuditTrailQuery(get_called_class());
    }

    public static function modelList ()
    {
        $tables      = [];
        $auditTrails = ArrayHelper::map(AuditTrail::find()->select(['model'])->asArray()->distinct()->all(), 'model', 'model');
        foreach ($auditTrails as $table) {
            $tables[$table] = substr($table, strrpos($table, '\\') + 1);
        }
        return $tables;
    }

    public static function userList ()
    {
        return ArrayHelper::map(User::find()->asArray()->all(), 'id', 'username');
    }

}
