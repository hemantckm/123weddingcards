<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ScrollPaper]].
 *
 * @see ScrollPaper
 */
class ScrollPaperQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ScrollPaper[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ScrollPaper|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
