<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "insert_paper".
 *
 * @property int $insert_id
 * @property int $paper_id
 *
 * @property Insert $insert
 * @property Paper $paper
 */
class InsertPaper extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'insert_paper';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['insert_id', 'paper_id'], 'required'],
            [['insert_id', 'paper_id'], 'integer'],
            [['insert_id', 'paper_id'], 'unique', 'targetAttribute' => ['insert_id', 'paper_id']],
            [['insert_id'], 'exist', 'skipOnError' => true, 'targetClass' => Insert::className(), 'targetAttribute' => ['insert_id' => 'id']],
            [['paper_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paper::className(), 'targetAttribute' => ['paper_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'insert_id' => 'Insert ID',
            'paper_id' => 'Paper ID',
        ];
    }

    /**
     * Gets query for [[Insert]].
     *
     * @return \yii\db\ActiveQuery|InsertQuery
     */
    public function getInsert ()
    {
        return $this->hasOne(Insert::className(), ['id' => 'insert_id']);
    }

    /**
     * Gets query for [[Paper]].
     *
     * @return \yii\db\ActiveQuery|PaperQuery
     */
    public function getPaper ()
    {
        return $this->hasOne(Paper::className(), ['id' => 'paper_id']);
    }

    /**
     * {@inheritdoc}
     * @return InsertPaperQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new InsertPaperQuery(get_called_class());
    }

}
