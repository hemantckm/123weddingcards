<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "theme".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property CardTheme[] $cardThemes
 * @property Card[] $cards
 */
class Theme extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'theme';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['name'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'filter', 'filter' => 'trim'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[CardThemes]].
     *
     * @return \yii\db\ActiveQuery|CardThemeQuery
     */
    public function getCardThemes ()
    {
        return $this->hasMany(CardTheme::className(), ['theme_id' => 'id']);
    }

    /**
     * Gets query for [[Cards]].
     *
     * @return \yii\db\ActiveQuery|CardQuery
     */
    public function getCards ()
    {
        return $this->hasMany(Card::className(), ['id' => 'card_id'])->viaTable('card_theme', ['theme_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ThemeQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new ThemeQuery(get_called_class());
    }

}
