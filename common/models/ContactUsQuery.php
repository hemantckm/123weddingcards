<?php

namespace common\models;

use Yii;
use backend\helpers\Html;

/**
 * This is the ActiveQuery class for [[ContactUs]].
 *
 * @see ContactUs
 */
class ContactUsQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return ContactUs[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ContactUs|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * Getting All Data of The Selected ContactUs
     * @param type $contactUsId
     */
    public static function getContactUsData ($contactUsId = NULL)
    {
        $contactUsArray = $condition      = array ();
        $time           = '?r=' . time();

        if ( ! empty($contactUsId)) {
            $condition['cus.id'] = $contactUsId;
        } else {
            $condition['cus.status']  = ContactUs::STATUS_ACTIVE;
            $condition['cus.default'] = ContactUs::DEFAULT_ACTIVE;
        }

        $contactUsModelDatas = (new \yii\db\Query())
                ->select(
                        [
                            'cus.id contact_us_id',
                            'cus.heading contact_us_heading',
                            'cus.feature_image contact_us_feature_image',
                            'cus.default contact_us_default',
                            'cus.status contact_us_status',
                            'cusd.name contact_us_detail_name',
                            'cusd.icon contact_us_detail_icon',
                            'cusd.description contact_us_detail_description',
                            'custc.heading contact_us_tc_heading',
                            'custc.description contact_us_tc_description',
                        ]
                )
                ->from('contact_us cus')
                ->leftJoin('contact_us_details cusd', 'cus.id = cusd.contact_us_id')
                ->leftJoin('contact_us_tc custc', 'cus.id = custc.contact_us_id')
                ->andWhere($condition)
                ->orderBy(['cus.id' => SORT_DESC])
                ->all();

        if ( ! empty($contactUsModelDatas)) {
            foreach ($contactUsModelDatas as $contactUsModelData) {
                $contactUsArray[$contactUsModelData['contact_us_id']]['contact_us_id']                       = $contactUsModelData['contact_us_id'];
                $contactUsArray[$contactUsModelData['contact_us_id']]['contact_us_heading']                  = $contactUsModelData['contact_us_heading'];
                $contactUsArray[$contactUsModelData['contact_us_id']]['contact_us_feature_image']['full']    = ! empty($contactUsModelData['contact_us_feature_image']) ? '/' . ContactUs::MAIN_DIRECTORY . ContactUs::ORIGINAL_FILEPATH . $contactUsModelData['contact_us_feature_image'] . $time : FaqType::NO_IMAGE_FULL . $time;
                $contactUsArray[$contactUsModelData['contact_us_id']]['contact_us_feature_image']['small']   = ! empty($contactUsModelData['contact_us_feature_image']) ? '/' . ContactUs::MAIN_DIRECTORY . ContactUs::THUMBNAIL_FILEPATH . $contactUsModelData['contact_us_feature_image'] . $time : FaqType::NO_IMAGE_SMALL . $time;
                $contactUsArray[$contactUsModelData['contact_us_id']]['contact_us_feature_image']['gallery'] = Html::a(Html::img(Yii::getAlias('@web') . $contactUsArray[$contactUsModelData['contact_us_id']]['contact_us_feature_image']['small'], ['class' => 'img-thumbnail', 'style' => 'margin-bottom:10px']), Yii::getAlias('@web') . $contactUsArray[$contactUsModelData['contact_us_id']]['contact_us_feature_image']['full'], ['data-fancybox' => 'gallery']);
                $contactUsArray[$contactUsModelData['contact_us_id']]['contact_us_default']                  = $contactUsModelData['contact_us_default'];
                $contactUsArray[$contactUsModelData['contact_us_id']]['contact_us_status']                   = $contactUsModelData['contact_us_status'];
                $contactUsArray[$contactUsModelData['contact_us_id']]['contact_us_detail'][]                 = array (
                    'name' => $contactUsModelData['contact_us_detail_name'],
                    'icon' => array (
                        'full' => ! empty($contactUsModelData['contact_us_detail_icon']) ? '/' . ContactUsDetails::MAIN_DIRECTORY . ContactUsDetails::ORIGINAL_FILEPATH . $contactUsModelData['contact_us_detail_icon'] . $time : \common\models\FaqType::NO_IMAGE_FULL . $time,
                        'small' => ! empty($contactUsModelData['contact_us_detail_icon']) ? '/' . ContactUsDetails::MAIN_DIRECTORY . ContactUsDetails::THUMBNAIL_FILEPATH . $contactUsModelData['contact_us_detail_icon'] . $time : \common\models\FaqType::NO_IMAGE_SMALL . $time,
                    ),
                    'description' => $contactUsModelData['contact_us_detail_description'],
                );
                $contactUsArray[$contactUsModelData['contact_us_id']]['contact_us_tc']['heading']            = $contactUsModelData['contact_us_tc_heading'];
                $contactUsArray[$contactUsModelData['contact_us_id']]['contact_us_tc']['description']        = $contactUsModelData['contact_us_tc_description'];
            }
        }
        return $contactUsArray;
    }

}
