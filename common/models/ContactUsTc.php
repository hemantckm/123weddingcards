<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "contact_us_tc".
 *
 * @property int $id
 * @property int|null $contact_us_id
 * @property string $$heading
 * @property string|null $description
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property ContactUs $contactUs
 */
class ContactUsTc extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'contact_us_tc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['description', 'heading'], 'required'],
            [['contact_us_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['heading'], 'string', 'max' => 50],
            [['heading'], 'filter', 'filter' => 'trim'],
            [['heading'], 'unique'],
            [['contact_us_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContactUs::className(), 'targetAttribute' => ['contact_us_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'contact_us_id' => 'Contact Us',
            'heading' => 'Heading',
            'description' => 'Description',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[ContactUs]].
     *
     * @return \yii\db\ActiveQuery|ContactUsQuery
     */
    public function getContactUs ()
    {
        return $this->hasOne(ContactUs::className(), ['id' => 'contact_us_id']);
    }

    /**
     * {@inheritdoc}
     * @return ContactUsTcQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new ContactUsTcQuery(get_called_class());
    }

}
