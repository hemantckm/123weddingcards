<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[InsertDie]].
 *
 * @see InsertDie
 */
class InsertDieQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return InsertDie[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return InsertDie|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
