<?php

namespace common\models;

use Yii;
use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "inquiry".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $phone
 * @property string|null $message
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Inquiry extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'inquiry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['name', 'email', 'message'], 'required'],
            [['message'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['email'], 'email'],
            [['name', 'email', 'phone'], 'filter', 'filter' => 'trim'],
            [['name'], 'string', 'max' => 100],
            [['email'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'message' => 'Message',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * {@inheritdoc}
     * @return InquiryQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new InquiryQuery(get_called_class());
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail ($email)
    {
        $message = "Name: " . $this->name;
        $message .= "Email: " . $this->email;
        $message .= "Phone: " . $this->phone;
        $message .= "Message: <p>" . $this->message . "</p>";
        return Yii::$app->mailer->compose()
                        ->setTo($email)
                        ->setFrom([$this->email => $this->name])
                        ->setSubject("Inquiry")
                        ->setTextBody($message)
                        ->send();
    }

}
