<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Ribbon]].
 *
 * @see Ribbon
 */
class RibbonQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Ribbon[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ribbon|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get Ribbon List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, code')
                ->asArray()
                ->where(['status' => Ribbon::STATUS_ACTIVE])
                ->orderBy('code asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'code');
    }

}
