<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "scroll_paper".
 *
 * @property int $scroll_id
 * @property int $paper_id
 *
 * @property Paper $paper
 * @property Scroll $scroll
 */
class ScrollPaper extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'scroll_paper';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['scroll_id', 'paper_id'], 'required'],
            [['scroll_id', 'paper_id'], 'integer'],
            [['scroll_id', 'paper_id'], 'unique', 'targetAttribute' => ['scroll_id', 'paper_id']],
            [['paper_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paper::className(), 'targetAttribute' => ['paper_id' => 'id']],
            [['scroll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Scroll::className(), 'targetAttribute' => ['scroll_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'scroll_id' => 'Scroll ID',
            'paper_id' => 'Paper ID',
        ];
    }

    /**
     * Gets query for [[Paper]].
     *
     * @return \yii\db\ActiveQuery|PaperQuery
     */
    public function getPaper ()
    {
        return $this->hasOne(Paper::className(), ['id' => 'paper_id']);
    }

    /**
     * Gets query for [[Scroll]].
     *
     * @return \yii\db\ActiveQuery|ScrollQuery
     */
    public function getScroll ()
    {
        return $this->hasOne(Scroll::className(), ['id' => 'scroll_id']);
    }

    /**
     * {@inheritdoc}
     * @return ScrollPaperQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new ScrollPaperQuery(get_called_class());
    }

}
