<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "folder".
 *
 * @property int $id
 * @property int|null $card_id
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Card $card
 * @property FolderBlock[] $folderBlocks
 * @property Block[] $blocks
 * @property FolderDie[] $folderDies
 * @property DieCut[] $dies
 * @property FolderPaper[] $folderPapers
 * @property Paper[] $papers
 * @property FolderPrintingMethod[] $folderPrintingMethods
 * @property PrintingMethod[] $printingMethods
 */
class Folder extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * Folder Paper Function Define.
     * @var type 
     */
    public $folder_paper;

    /**
     * Folder Die Function Define.
     * @var type 
     */
    public $folder_die;

    /**
     * Folder Block Function Define.
     * @var type 
     */
    public $folder_block;

    /**
     * Folder Printing Method Function Define.
     * @var type 
     */
    public $folder_printing_method;

    /**
     * Paper Color Function Define.
     * @var type 
     */
    public $paper_color;

    /**
     * Paper Type Function Define.
     * @var type 
     */
    public $paper_type;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'folder';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['card_id'/*, 'folder_paper', 'folder_die', 'folder_block', 'folder_printing_method'*/], 'required'],
            [['card_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['card_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'card_id' => 'Card',
            'folder_paper' => 'Paper',
            'folder_die' => 'Die',
            'folder_block' => 'Block',
            'folder_printing_method' => 'Printing Method',
            'paper_color' => 'Paper Color',
            'paper_type' => 'Paper Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[Card]].
     *
     * @return \yii\db\ActiveQuery|CardQuery
     */
    public function getCard ()
    {
        return $this->hasOne(Card::className(), ['id' => 'card_id']);
    }

    /**
     * Gets query for [[FolderBlocks]].
     *
     * @return \yii\db\ActiveQuery|FolderBlockQuery
     */
    public function getFolderBlocks ()
    {
        return $this->hasMany(FolderBlock::className(), ['folder_id' => 'id']);
    }

    /**
     * Gets query for [[Blocks]].
     *
     * @return \yii\db\ActiveQuery|BlockQuery
     */
    public function getBlocks ()
    {
        return $this->hasMany(Block::className(), ['id' => 'block_id'])->viaTable('folder_block', ['folder_id' => 'id']);
    }

    /**
     * Gets query for [[FolderDies]].
     *
     * @return \yii\db\ActiveQuery|FolderDieQuery
     */
    public function getFolderDies ()
    {
        return $this->hasMany(FolderDie::className(), ['folder_id' => 'id']);
    }

    /**
     * Gets query for [[Dies]].
     *
     * @return \yii\db\ActiveQuery|DieCutQuery
     */
    public function getDies ()
    {
        return $this->hasMany(DieCut::className(), ['id' => 'die_id'])->viaTable('folder_die', ['folder_id' => 'id']);
    }

    /**
     * Gets query for [[FolderPapers]].
     *
     * @return \yii\db\ActiveQuery|FolderPaperQuery
     */
    public function getFolderPapers ()
    {
        return $this->hasMany(FolderPaper::className(), ['folder_id' => 'id']);
    }

    /**
     * Gets query for [[Papers]].
     *
     * @return \yii\db\ActiveQuery|PaperQuery
     */
    public function getPapers ()
    {
        return $this->hasMany(Paper::className(), ['id' => 'paper_id'])->viaTable('folder_paper', ['folder_id' => 'id']);
    }

    /**
     * Gets query for [[FolderPrintingMethods]].
     *
     * @return \yii\db\ActiveQuery|FolderPrintingMethodQuery
     */
    public function getFolderPrintingMethods ()
    {
        return $this->hasMany(FolderPrintingMethod::className(), ['folder_id' => 'id']);
    }

    /**
     * Gets query for [[PrintingMethods]].
     *
     * @return \yii\db\ActiveQuery|PrintingMethodQuery
     */
    public function getPrintingMethods ()
    {
        return $this->hasMany(PrintingMethod::className(), ['id' => 'printing_method_id'])->viaTable('folder_printing_method', ['folder_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return FolderQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new FolderQuery(get_called_class());
    }

}
