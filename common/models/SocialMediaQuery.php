<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[SocialMedia]].
 *
 * @see SocialMedia
 */
class SocialMediaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return SocialMedia[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SocialMedia|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
