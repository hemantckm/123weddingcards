<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[InsertPrintingMethod]].
 *
 * @see InsertPrintingMethod
 */
class InsertPrintingMethodQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return InsertPrintingMethod[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return InsertPrintingMethod|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
