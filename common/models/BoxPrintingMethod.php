<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "box_printing_method".
 *
 * @property int $box_id
 * @property int $printing_method_id
 *
 * @property Box $box
 * @property PrintingMethod $printingMethod
 */
class BoxPrintingMethod extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'box_printing_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['box_id', 'printing_method_id'], 'required'],
            [['box_id', 'printing_method_id'], 'integer'],
            [['box_id', 'printing_method_id'], 'unique', 'targetAttribute' => ['box_id', 'printing_method_id']],
            [['box_id'], 'exist', 'skipOnError' => true, 'targetClass' => Box::className(), 'targetAttribute' => ['box_id' => 'id']],
            [['printing_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingMethod::className(), 'targetAttribute' => ['printing_method_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'box_id' => 'Box ID',
            'printing_method_id' => 'Printing Method ID',
        ];
    }

    /**
     * Gets query for [[Box]].
     *
     * @return \yii\db\ActiveQuery|BoxQuery
     */
    public function getBox ()
    {
        return $this->hasOne(Box::className(), ['id' => 'box_id']);
    }

    /**
     * Gets query for [[PrintingMethod]].
     *
     * @return \yii\db\ActiveQuery|PrintingMethodQuery
     */
    public function getPrintingMethod ()
    {
        return $this->hasOne(PrintingMethod::className(), ['id' => 'printing_method_id']);
    }

    /**
     * {@inheritdoc}
     * @return BoxPrintingMethodQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new BoxPrintingMethodQuery(get_called_class());
    }

}
