<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "addon_folder_printing_method".
 *
 * @property int $addon_folder_id
 * @property int $printing_method_id
 *
 * @property AddonFolder $addonFolder
 * @property PrintingMethod $printingMethod
 */
class AddonFolderPrintingMethod extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'addon_folder_printing_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['addon_folder_id', 'printing_method_id'], 'required'],
            [['addon_folder_id', 'printing_method_id'], 'integer'],
            [['addon_folder_id', 'printing_method_id'], 'unique', 'targetAttribute' => ['addon_folder_id', 'printing_method_id']],
            [['addon_folder_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddonFolder::className(), 'targetAttribute' => ['addon_folder_id' => 'id']],
            [['printing_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingMethod::className(), 'targetAttribute' => ['printing_method_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'addon_folder_id' => 'Addon Folder ID',
            'printing_method_id' => 'Printing Method ID',
        ];
    }

    /**
     * Gets query for [[AddonFolder]].
     *
     * @return \yii\db\ActiveQuery|AddonFolderQuery
     */
    public function getAddonFolder ()
    {
        return $this->hasOne(AddonFolder::className(), ['id' => 'addon_folder_id']);
    }

    /**
     * Gets query for [[PrintingMethod]].
     *
     * @return \yii\db\ActiveQuery|PrintingMethodQuery
     */
    public function getPrintingMethod ()
    {
        return $this->hasOne(PrintingMethod::className(), ['id' => 'printing_method_id']);
    }

    /**
     * {@inheritdoc}
     * @return AddonFolderPrintingMethodQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AddonFolderPrintingMethodQuery(get_called_class());
    }

}
