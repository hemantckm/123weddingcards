<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "scroll_block".
 *
 * @property int $scroll_id
 * @property int $block_id
 *
 * @property Block $block
 * @property Scroll $scroll
 */
class ScrollBlock extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'scroll_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['scroll_id', 'block_id'], 'required'],
            [['scroll_id', 'block_id'], 'integer'],
            [['scroll_id', 'block_id'], 'unique', 'targetAttribute' => ['scroll_id', 'block_id']],
            [['block_id'], 'exist', 'skipOnError' => true, 'targetClass' => Block::className(), 'targetAttribute' => ['block_id' => 'id']],
            [['scroll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Scroll::className(), 'targetAttribute' => ['scroll_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'scroll_id' => 'Scroll ID',
            'block_id' => 'Block ID',
        ];
    }

    /**
     * Gets query for [[Block]].
     *
     * @return \yii\db\ActiveQuery|BlockQuery
     */
    public function getBlock ()
    {
        return $this->hasOne(Block::className(), ['id' => 'block_id']);
    }

    /**
     * Gets query for [[Scroll]].
     *
     * @return \yii\db\ActiveQuery|ScrollQuery
     */
    public function getScroll ()
    {
        return $this->hasOne(Scroll::className(), ['id' => 'scroll_id']);
    }

    /**
     * {@inheritdoc}
     * @return ScrollBlockQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new ScrollBlockQuery(get_called_class());
    }

}
