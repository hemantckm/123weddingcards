<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Tassle]].
 *
 * @see Tassle
 */
class TassleQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Tassle[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tassle|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get Tassle List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, code')
                ->asArray()
                ->where(['status' => Tassle::STATUS_ACTIVE])
                ->orderBy('code asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'code');
    }

}
