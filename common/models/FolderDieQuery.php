<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[FolderDie]].
 *
 * @see FolderDie
 */
class FolderDieQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return FolderDie[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FolderDie|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
