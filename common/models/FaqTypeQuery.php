<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[FaqType]].
 *
 * @see FaqType
 */
class FaqTypeQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return FaqType[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FaqType|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get Faq Type List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, name')
                ->asArray()
                ->where(['status' => FaqType::STATUS_ACTIVE])
                ->orderBy('id asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'name');
    }

}
