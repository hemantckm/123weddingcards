<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "addon".
 *
 * @property int $id
 * @property int $code
 * @property int $card_id
 * @property int $addon_type_id
 * @property int $addon_style_id
 * @property float $price
 * @property float $length
 * @property float $width
 * @property float $weight
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property AddonStyle $addonStyle
 * @property AddonType $addonType
 * @property Card $card
 */
class Addon extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'addon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['code', 'card_id', 'addon_type_id', 'price', 'length', 'width', 'weight'], 'required'],
            [['addon_style_id'], 'safe'],
            [['card_id', 'addon_type_id', 'addon_style_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['code'], 'filter', 'filter' => 'trim'],
            [['code'], 'string', 'max' => 150],
            [['price', 'length', 'width', 'weight'], 'number'],
            [['code'], 'unique'],
            [['addon_style_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddonStyle::className(), 'targetAttribute' => ['addon_style_id' => 'id']],
            [['addon_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddonType::className(), 'targetAttribute' => ['addon_type_id' => 'id']],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['card_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'card_id' => 'Card',
            'addon_type_id' => 'Addon Type',
            'addon_style_id' => 'Addon Style',
            'price' => 'Price',
            'length' => 'Length',
            'width' => 'Width',
            'weight' => 'Weight',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[AddonStyle]].
     *
     * @return \yii\db\ActiveQuery|AddonStyleQuery
     */
    public function getAddonStyle ()
    {
        return $this->hasOne(AddonStyle::className(), ['id' => 'addon_style_id']);
    }

    /**
     * Gets query for [[AddonType]].
     *
     * @return \yii\db\ActiveQuery|AddonTypeQuery
     */
    public function getAddonType ()
    {
        return $this->hasOne(AddonType::className(), ['id' => 'addon_type_id']);
    }

    /**
     * Gets query for [[Card]].
     *
     * @return \yii\db\ActiveQuery|CardQuery
     */
    public function getCard ()
    {
        return $this->hasOne(Card::className(), ['id' => 'card_id']);
    }

    /**
     * {@inheritdoc}
     * @return AddonQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AddonQuery(get_called_class());
    }

}
