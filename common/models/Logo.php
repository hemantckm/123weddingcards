<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * This is the model class for table "logo".
 *
 * @property int $id
 * @property string $heading
 * @property string|null $image
 * @property int $default
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Logo extends ActiveRecord
{

    const STATUS_ACTIVE        = 1;
    const STATUS_INACTIVE      = 0;
    const STATUS_DELETE        = 2;
    const DEFAULT_ACTIVE       = 1;
    const DEFAULT_INACTIVE     = 0;
    const MAIN_DIRECTORY       = 'uploads/logo/';
    const ORIGINAL_FILEPATH    = 'original/';
    const THUMBNAIL_FILEPATH   = 'thumbnail/';
    const ORIGINAL_IMAGE_WIDTH = 100;
    const THUMB_IMAGE_HEIGHT   = 75;
    const THUMB_IMAGE_WIDTH    = 75;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'logo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['heading'], 'required'],
            [['default', 'status', 'created_at', 'updated_at'], 'integer'],
            [['heading'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 255],
            [['image'], 'safe'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,jpeg'],
            [['heading'], 'filter', 'filter' => 'trim'],
            [['heading'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'heading' => 'Heading',
            'image' => 'Image',
            'default' => 'Default',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Logo Upload.
     * @return type
     */
    public function upload ()
    {
        //File BaseName.
        $baseName = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $this->heading));

        //File Extension.
        $imageExtension = $this->image->extension;

        //Main Directory Path.
        FileHelper::createDirectory(Logo::MAIN_DIRECTORY);

        //Original File Path.
        $originalFilePath = Logo::MAIN_DIRECTORY . Logo::ORIGINAL_FILEPATH;
        FileHelper::createDirectory($originalFilePath);

        //Thumbnail File Path.
        $thumbnailFilePath = Logo::MAIN_DIRECTORY . Logo::THUMBNAIL_FILEPATH;
        FileHelper::createDirectory($thumbnailFilePath);

        //Original Image Save.
        $response = $this->image->saveAs($originalFilePath . $baseName . '.' . $imageExtension);

        //Original Image Rotation.
        if (($response == 1) && file_exists($originalFilePath . $baseName . '.' . $imageExtension)) {
            $imagine = Image::getImagine()->open($originalFilePath . $baseName . '.' . $imageExtension);
            $exif    = @exif_read_data($originalFilePath . $baseName . '.' . $imageExtension);

            //Rotate Image By Orientation.
            if ( ! empty($exif['Orientation'])) {
                switch ($exif['Orientation'])
                {
                    case 3:
                        $imagine->rotate(180);
                        break;
                    case 6:
                        $imagine->rotate(90);
                        break;
                    case 8:
                        $imagine->rotate(-90);
                        break;
                }
            }

            $imagine->save($originalFilePath . $baseName . '.' . $imageExtension, ['quality' => 70]);

            //Original Image Compress Image and Save.
            //1280x800 
            $sizes  = getimagesize($originalFilePath . $baseName . '.' . $imageExtension);
            $width  = Logo::ORIGINAL_IMAGE_WIDTH;
            $height = round($sizes[1] * $width / $sizes[0]);
            $imagine->resize(new Box($width, $height))->save($originalFilePath . $baseName . '.' . $imageExtension, ['quality' => 70]);

            //Thumbnail Image Save.
            Image::getImagine()
                    ->open($originalFilePath . $baseName . '.' . $imageExtension)
                    ->thumbnail(new Box(Logo::THUMB_IMAGE_WIDTH, Logo::THUMB_IMAGE_HEIGHT))
                    ->save($thumbnailFilePath . $baseName . '.' . $imageExtension);
            return $baseName . '.' . $imageExtension;
        }
    }

    /**
     * {@inheritdoc}
     * @return LogoQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new LogoQuery(get_called_class());
    }

}
