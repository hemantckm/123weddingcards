<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "question_answer".
 *
 * @property int $id
 * @property string $question
 * @property string|null $answer
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class QuestionAnswer extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'question_answer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['question', 'answer'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['question'], 'string', 'max' => 255],
            [['answer'], 'string'],
            [['question'], 'filter', 'filter' => 'trim'],
            [['question'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'question' => 'Question',
            'answer' => 'Answer',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * {@inheritdoc}
     * @return QuestionAnswerQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new QuestionAnswerQuery(get_called_class());
    }

}
