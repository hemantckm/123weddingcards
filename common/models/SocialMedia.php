<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "social_media".
 *
 * @property int $id
 * @property string $name
 * @property string $link
 * @property string|null $data
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class SocialMedia extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'social_media';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['name', 'link'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'data'], 'string', 'max' => 50],
            [['link'], 'string', 'max' => 255],
            [['link'], 'url'],
            [['name', 'link'], 'filter', 'filter' => 'trim'],
            [['name'], 'unique'],
            [['link'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'link' => 'Link',
            'data' => 'Data',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * {@inheritdoc}
     * @return SocialMediaQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new SocialMediaQuery(get_called_class());
    }

}
