<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "folder_paper".
 *
 * @property int $folder_id
 * @property int $paper_id
 *
 * @property Folder $folder
 * @property Paper $paper
 */
class FolderPaper extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'folder_paper';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['folder_id', 'paper_id'], 'required'],
            [['folder_id', 'paper_id'], 'integer'],
            [['folder_id', 'paper_id'], 'unique', 'targetAttribute' => ['folder_id', 'paper_id']],
            [['folder_id'], 'exist', 'skipOnError' => true, 'targetClass' => Folder::className(), 'targetAttribute' => ['folder_id' => 'id']],
            [['paper_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paper::className(), 'targetAttribute' => ['paper_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'folder_id' => 'Folder ID',
            'paper_id' => 'Paper ID',
        ];
    }

    /**
     * Gets query for [[Folder]].
     *
     * @return \yii\db\ActiveQuery|FolderQuery
     */
    public function getFolder()
    {
        return $this->hasOne(Folder::className(), ['id' => 'folder_id']);
    }

    /**
     * Gets query for [[Paper]].
     *
     * @return \yii\db\ActiveQuery|PaperQuery
     */
    public function getPaper()
    {
        return $this->hasOne(Paper::className(), ['id' => 'paper_id']);
    }

    /**
     * {@inheritdoc}
     * @return FolderPaperQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FolderPaperQuery(get_called_class());
    }
}
