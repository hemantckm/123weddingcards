<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[DieCut]].
 *
 * @see DieCut
 */
class DieCutQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return DieCut[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return DieCut|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get DieCut List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, code')
                ->asArray()
                ->where(['status' => DieCut::STATUS_ACTIVE])
                ->orderBy('code asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'code');
    }

}
