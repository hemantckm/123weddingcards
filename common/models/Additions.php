<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "additions".
 *
 * @property int $id
 * @property int|null $card_id
 * @property int|null $tassle_id
 * @property int|null $ribbon_id
 * @property int|null $rhinestone_id
 * @property float $extra_insert_weight
 * @property float $extra_insert_price
 * @property int|null $photocard
 * @property int|null $linear
 * @property int|null $velvet
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Card $card
 * @property Rhinestone $rhinestone
 * @property Ribbon $ribbon
 * @property Tassle $tassle
 */
class Additions extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;
    const PHOTOCARD       = [1 => 'Yes', 0 => 'No'];
    const LINEAR          = [1 => 'Yes', 0 => 'No'];
    const VELVET          = [1 => 'Yes', 0 => 'No'];

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'additions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['card_id'/*, 'tassle_id', 'ribbon_id', 'rhinestone_id', 'extra_insert_weight', 'extra_insert_price'*/], 'required'],
            [['card_id', 'tassle_id', 'ribbon_id', 'rhinestone_id', 'photocard', 'linear', 'velvet', 'status', 'created_at', 'updated_at'], 'integer'],
            [['extra_insert_weight', 'extra_insert_price'], 'number'],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['card_id' => 'id']],
            [['rhinestone_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rhinestone::className(), 'targetAttribute' => ['rhinestone_id' => 'id']],
            [['ribbon_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ribbon::className(), 'targetAttribute' => ['ribbon_id' => 'id']],
            [['tassle_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tassle::className(), 'targetAttribute' => ['tassle_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'card_id' => 'Card',
            'tassle_id' => 'Tassle',
            'ribbon_id' => 'Ribbon',
            'rhinestone_id' => 'Rhinestone',
            'extra_insert_weight' => 'Extra Insert Weight',
            'extra_insert_price' => 'Extra Insert Price',
            'photocard' => 'Photocard',
            'linear' => 'Linear',
            'velvet' => 'Velvet',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[Card]].
     *
     * @return \yii\db\ActiveQuery|CardQuery
     */
    public function getCard ()
    {
        return $this->hasOne(Card::className(), ['id' => 'card_id']);
    }

    /**
     * Gets query for [[Rhinestone]].
     *
     * @return \yii\db\ActiveQuery|RhinestoneQuery
     */
    public function getRhinestone ()
    {
        return $this->hasOne(Rhinestone::className(), ['id' => 'rhinestone_id']);
    }

    /**
     * Gets query for [[Ribbon]].
     *
     * @return \yii\db\ActiveQuery|RibbonQuery
     */
    public function getRibbon ()
    {
        return $this->hasOne(Ribbon::className(), ['id' => 'ribbon_id']);
    }

    /**
     * Gets query for [[Tassle]].
     *
     * @return \yii\db\ActiveQuery|TassleQuery
     */
    public function getTassle ()
    {
        return $this->hasOne(Tassle::className(), ['id' => 'tassle_id']);
    }

    /**
     * {@inheritdoc}
     * @return AdditionsQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AdditionsQuery(get_called_class());
    }

}
