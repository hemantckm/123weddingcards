<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "addon_envelope_printing_method".
 *
 * @property int $addon_envelope_id
 * @property int $printing_method_id
 *
 * @property AddonEnvelope $addonEnvelope
 * @property PrintingMethod $printingMethod
 */
class AddonEnvelopePrintingMethod extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'addon_envelope_printing_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['addon_envelope_id', 'printing_method_id'], 'required'],
            [['addon_envelope_id', 'printing_method_id'], 'integer'],
            [['addon_envelope_id', 'printing_method_id'], 'unique', 'targetAttribute' => ['addon_envelope_id', 'printing_method_id']],
            [['addon_envelope_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddonEnvelope::className(), 'targetAttribute' => ['addon_envelope_id' => 'id']],
            [['printing_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingMethod::className(), 'targetAttribute' => ['printing_method_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'addon_envelope_id' => 'Addon Envelope ID',
            'printing_method_id' => 'Printing Method ID',
        ];
    }

    /**
     * Gets query for [[AddonEnvelope]].
     *
     * @return \yii\db\ActiveQuery|AddonEnvelopeQuery
     */
    public function getAddonEnvelope ()
    {
        return $this->hasOne(AddonEnvelope::className(), ['id' => 'addon_envelope_id']);
    }

    /**
     * Gets query for [[PrintingMethod]].
     *
     * @return \yii\db\ActiveQuery|PrintingMethodQuery
     */
    public function getPrintingMethod ()
    {
        return $this->hasOne(PrintingMethod::className(), ['id' => 'printing_method_id']);
    }

    /**
     * {@inheritdoc}
     * @return AddonEnvelopePrintingMethodQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AddonEnvelopePrintingMethodQuery(get_called_class());
    }

}
