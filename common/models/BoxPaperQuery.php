<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[BoxPaper]].
 *
 * @see BoxPaper
 */
class BoxPaperQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return BoxPaper[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return BoxPaper|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
