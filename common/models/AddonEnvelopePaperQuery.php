<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[AddonEnvelopePaper]].
 *
 * @see AddonEnvelopePaper
 */
class AddonEnvelopePaperQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return AddonEnvelopePaper[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AddonEnvelopePaper|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
