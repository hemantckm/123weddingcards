<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "addon_folder_block".
 *
 * @property int $addon_folder_id
 * @property int $block_id
 *
 * @property AddonFolder $addonFolder
 * @property Block $block
 */
class AddonFolderBlock extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'addon_folder_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['addon_folder_id', 'block_id'], 'required'],
            [['addon_folder_id', 'block_id'], 'integer'],
            [['addon_folder_id', 'block_id'], 'unique', 'targetAttribute' => ['addon_folder_id', 'block_id']],
            [['addon_folder_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddonFolder::className(), 'targetAttribute' => ['addon_folder_id' => 'id']],
            [['block_id'], 'exist', 'skipOnError' => true, 'targetClass' => Block::className(), 'targetAttribute' => ['block_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'addon_folder_id' => 'Addon Folder ID',
            'block_id' => 'Block ID',
        ];
    }

    /**
     * Gets query for [[AddonFolder]].
     *
     * @return \yii\db\ActiveQuery|AddonFolderQuery
     */
    public function getAddonFolder ()
    {
        return $this->hasOne(AddonFolder::className(), ['id' => 'addon_folder_id']);
    }

    /**
     * Gets query for [[Block]].
     *
     * @return \yii\db\ActiveQuery|BlockQuery
     */
    public function getBlock ()
    {
        return $this->hasOne(Block::className(), ['id' => 'block_id']);
    }

    /**
     * {@inheritdoc}
     * @return AddonFolderBlockQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AddonFolderBlockQuery(get_called_class());
    }

}
