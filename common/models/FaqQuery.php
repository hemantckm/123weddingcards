<?php

namespace common\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the ActiveQuery class for [[Faq]].
 *
 * @see Faq
 */
class FaqQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Faq[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Faq|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get FAQ List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, heading')
                ->asArray()
                ->where(['status' => Faq::STATUS_ACTIVE])
                ->orderBy('heading asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'heading');
    }

    /**
     * Get All Data of FAQ for Front-end FAQ page.
     */
    public static function getFrontendData ()
    {
        $faqsArray     = array ();
        $faqModelDatas = (new \yii\db\Query())
                ->select(
                        [
                            'qa.id qa_id',
                            'qa.question',
                            'qa.answer',
                            'f.id faq_id',
                            'f.heading',
                            'f.feature_image',
                            'ft.id faq_type_id',
                            'ft.name faq_type_name',
                            'ft.icon fq_type_icon',
                        ]
                )
                ->from('question_answer_faq qaf')
                ->innerJoin('question_answer qa', 'qa.id = qaf.question_answer_id')
                ->innerJoin('faq f', 'f.id = qaf.faq_id')
                ->innerJoin('faq_type ft', 'ft.id = qaf.faq_type_id')
                ->andWhere([
                    'qaf.status' => QuestionAnswerFaq::STATUS_ACTIVE,
                    'qa.status' => QuestionAnswer::STATUS_ACTIVE,
                    'f.status' => Faq::STATUS_ACTIVE,
                    'f.default' => Faq::DEFAULT_ACTIVE,
                ])
                ->orderBy(['ft.id' => SORT_ASC])
                ->all();

        if ( ! empty($faqModelDatas)) {
            foreach ($faqModelDatas as $faqModelData) {
                $faqsArray[$faqModelData['faq_id']]['faq_id']                                                                = $faqModelData['faq_id'];
                $faqsArray[$faqModelData['faq_id']]['heading']                                                               = $faqModelData['heading'];
                $faqsArray[$faqModelData['faq_id']]['feature_image']                                                         = Url::to(Yii::$app->urlManagerBackend->baseUrl . '/' . Faq::MAIN_DIRECTORY . Faq::ORIGINAL_FILEPATH . $faqModelData['feature_image'], TRUE);
                $faqsArray[$faqModelData['faq_id']]['faq_type'][$faqModelData['faq_type_id']]['faq_type_name']               = $faqModelData['faq_type_name'];
                $faqsArray[$faqModelData['faq_id']]['faq_type'][$faqModelData['faq_type_id']]['fq_type_icon']                = Url::to(Yii::$app->urlManagerBackend->baseUrl . '/' . FaqType::MAIN_DIRECTORY . FaqType::THUMBNAIL_FILEPATH . $faqModelData['fq_type_icon'], TRUE);
                $faqsArray[$faqModelData['faq_id']]['faq_type'][$faqModelData['faq_type_id']]['question_answer'][]           = ['question' => $faqModelData['question'], 'answer' => $faqModelData['answer']];
            }
        }
        return $faqsArray;
    }

}
