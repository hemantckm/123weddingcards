<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "insert_printing_method".
 *
 * @property int $insert_id
 * @property int $printing_method_id
 *
 * @property Insert $insert
 * @property PrintingMethod $printingMethod
 */
class InsertPrintingMethod extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'insert_printing_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['insert_id', 'printing_method_id'], 'required'],
            [['insert_id', 'printing_method_id'], 'integer'],
            [['insert_id', 'printing_method_id'], 'unique', 'targetAttribute' => ['insert_id', 'printing_method_id']],
            [['insert_id'], 'exist', 'skipOnError' => true, 'targetClass' => Insert::className(), 'targetAttribute' => ['insert_id' => 'id']],
            [['printing_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingMethod::className(), 'targetAttribute' => ['printing_method_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'insert_id' => 'Insert ID',
            'printing_method_id' => 'Printing Method ID',
        ];
    }

    /**
     * Gets query for [[Insert]].
     *
     * @return \yii\db\ActiveQuery|InsertQuery
     */
    public function getInsert ()
    {
        return $this->hasOne(Insert::className(), ['id' => 'insert_id']);
    }

    /**
     * Gets query for [[PrintingMethod]].
     *
     * @return \yii\db\ActiveQuery|PrintingMethodQuery
     */
    public function getPrintingMethod ()
    {
        return $this->hasOne(PrintingMethod::className(), ['id' => 'printing_method_id']);
    }

    /**
     * {@inheritdoc}
     * @return InsertPrintingMethodQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new InsertPrintingMethodQuery(get_called_class());
    }

}
