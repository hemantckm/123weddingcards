<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[AddonEnvelopeDie]].
 *
 * @see AddonEnvelopeDie
 */
class AddonEnvelopeDieQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return AddonEnvelopeDie[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AddonEnvelopeDie|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
