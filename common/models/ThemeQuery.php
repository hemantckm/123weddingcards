<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Theme]].
 *
 * @see Theme
 */
class ThemeQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Theme[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Theme|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * String Capitalize
     * @param type $string
     * @return type
     */
    function doCapitalize ($string)
    {
        $exclude = array ('and', 'is', 'or');
        $words   = explode(' ', $string);
        foreach ($words as $key => $word) {
            if (in_array($word, $exclude)) {
                continue;
            }
            $words[$key] = ucfirst($word);
        }
        $newString = implode(' ', $words);
        return $newString;
    }

    /**
     * To Get Theme List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, name')
                ->asArray()
                ->where(['status' => Theme::STATUS_ACTIVE])
                ->orderBy('name asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'name');
    }

}
