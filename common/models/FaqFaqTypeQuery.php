<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[FaqFaqType]].
 *
 * @see FaqFaqType
 */
class FaqFaqTypeQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return FaqFaqType[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FaqFaqType|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get Faq FaqType By FAQ ID List
     * @return type
     */
    public function getFaqFaqType ($faqId)
    {
        $result = $this->select('faq_type_id')
                ->asArray()
                ->where(['faq_id' => $faqId])
                ->orderBy('faq_type_id asc')
                ->all();
        return $result;
    }

}
