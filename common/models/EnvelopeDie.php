<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "envelope_die".
 *
 * @property int $envelope_id
 * @property int $die_id
 *
 * @property DieCut $die
 * @property Envelope $envelope
 */
class EnvelopeDie extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'envelope_die';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['envelope_id', 'die_id'], 'required'],
            [['envelope_id', 'die_id'], 'integer'],
            [['envelope_id', 'die_id'], 'unique', 'targetAttribute' => ['envelope_id', 'die_id']],
            [['die_id'], 'exist', 'skipOnError' => true, 'targetClass' => DieCut::className(), 'targetAttribute' => ['die_id' => 'id']],
            [['envelope_id'], 'exist', 'skipOnError' => true, 'targetClass' => Envelope::className(), 'targetAttribute' => ['envelope_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'envelope_id' => 'Envelope ID',
            'die_id' => 'Die ID',
        ];
    }

    /**
     * Gets query for [[Die]].
     *
     * @return \yii\db\ActiveQuery|DieCutQuery
     */
    public function getDie ()
    {
        return $this->hasOne(DieCut::className(), ['id' => 'die_id']);
    }

    /**
     * Gets query for [[Envelope]].
     *
     * @return \yii\db\ActiveQuery|EnvelopeQuery
     */
    public function getEnvelope ()
    {
        return $this->hasOne(Envelope::className(), ['id' => 'envelope_id']);
    }

    /**
     * {@inheritdoc}
     * @return EnvelopeDieQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new EnvelopeDieQuery(get_called_class());
    }

}
