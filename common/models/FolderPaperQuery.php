<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[FolderPaper]].
 *
 * @see FolderPaper
 */
class FolderPaperQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return FolderPaper[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FolderPaper|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

}
