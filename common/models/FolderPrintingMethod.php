<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "folder_printing_method".
 *
 * @property int $folder_id
 * @property int $printing_method_id
 *
 * @property Folder $folder
 * @property PrintingMethod $printingMethod
 */
class FolderPrintingMethod extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'folder_printing_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['folder_id', 'printing_method_id'], 'required'],
            [['folder_id', 'printing_method_id'], 'integer'],
            [['folder_id', 'printing_method_id'], 'unique', 'targetAttribute' => ['folder_id', 'printing_method_id']],
            [['folder_id'], 'exist', 'skipOnError' => true, 'targetClass' => Folder::className(), 'targetAttribute' => ['folder_id' => 'id']],
            [['printing_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingMethod::className(), 'targetAttribute' => ['printing_method_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'folder_id' => 'Folder ID',
            'printing_method_id' => 'Printing Method ID',
        ];
    }

    /**
     * Gets query for [[Folder]].
     *
     * @return \yii\db\ActiveQuery|FolderQuery
     */
    public function getFolder ()
    {
        return $this->hasOne(Folder::className(), ['id' => 'folder_id']);
    }

    /**
     * Gets query for [[PrintingMethod]].
     *
     * @return \yii\db\ActiveQuery|PrintingMethodQuery
     */
    public function getPrintingMethod ()
    {
        return $this->hasOne(PrintingMethod::className(), ['id' => 'printing_method_id']);
    }

    /**
     * {@inheritdoc}
     * @return FolderPrintingMethodQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new FolderPrintingMethodQuery(get_called_class());
    }

}
