<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tube".
 *
 * @property int $id
 * @property int|null $scroll_id
 * @property float $length
 * @property float $weight
 * @property float $diameter
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Scroll $scroll
 */
class Tube extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'tube';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['scroll_id'], 'required'],
            [['scroll_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['length', 'weight', 'diameter'], 'number'],
            [['scroll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Scroll::className(), 'targetAttribute' => ['scroll_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'scroll_id' => 'Scroll',
            'length' => 'Length',
            'weight' => 'Weight',
            'diameter' => 'Diameter',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[Scroll]].
     *
     * @return \yii\db\ActiveQuery|ScrollQuery
     */
    public function getScroll ()
    {
        return $this->hasOne(Scroll::className(), ['id' => 'scroll_id']);
    }

    /**
     * {@inheritdoc}
     * @return TubeQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new TubeQuery(get_called_class());
    }

}
