<?php

namespace common\models;

use Yii;
use common\components\ActiveRecord;

/**
 * This is the model class for table "card_info".
 *
 * @property int $card_id
 * @property string|null $theme
 * @property string|null $envelope
 * @property string|null $folder
 * @property string|null $scroll
 * @property string|null $box
 * @property string|null $insert
 * @property string|null $additions
 * @property int $status
 *
 * @property Card $card
 */
class CardInfo extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'card_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['theme', 'envelope', 'folder', 'scroll', 'box', 'insert', 'additions'], 'string'],
            [['status'], 'integer'],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['card_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'card_id' => 'Card ID',
            'theme' => 'Theme',
            'envelope' => 'Envelope',
            'folder' => 'Folder',
            'scroll' => 'Scroll',
            'box' => 'Box',
            'insert' => 'Insert',
            'additions' => 'Additions',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[Card]].
     *
     * @return \yii\db\ActiveQuery|CardQuery
     */
    public function getCard ()
    {
        return $this->hasOne(Card::className(), ['id' => 'card_id']);
    }

    /**
     * {@inheritdoc}
     * @return CardInfoQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new CardInfoQuery(get_called_class());
    }

    /**
     * Update Card Info by CardId
     * @param type $cardId
     */
    public static function updateCardInfo ($cardId)
    {
        //  use card_id instead fetch all records
        // Card Themes (Multiple)
        $themes = \Yii::$app->db->createCommand("SELECT ct.card_id, GROUP_CONCAT(' ', t.name order by t.name) theme FROM card_theme ct INNER JOIN theme t ON FIND_IN_SET(t.id, ct.theme_id) > 0 Where ct.card_id = " . $cardId . " GROUP BY ct.card_id")->queryAll();
        $themes = \yii\helpers\ArrayHelper::map($themes, 'card_id', 'theme');

        // Envelopes (Multiple)
        $envelopeArray = array ();
        $envelopes     = \Yii::$app->db->createCommand("SELECT e.id FROM envelope e INNER JOIN card c ON FIND_IN_SET(e.card_id, c.id) > 0 Where e.card_id = " . $cardId . " GROUP BY e.card_id")->queryAll();
        if ( ! empty($envelopes)) {
            foreach ($envelopes as $envelope) {

                //Envelope Papers (Multiple)
                $envelopePapers = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', p.code order by p.code) paper FROM envelope_paper ep INNER JOIN paper p ON FIND_IN_SET(p.id, ep.paper_id) > 0 Where ep.envelope_id = " . $envelope['id'] . " GROUP BY ep.envelope_id")->queryAll();
                if ( ! empty($envelopePapers)) {
                    foreach ($envelopePapers as $envelopePaper) {
                        $envelopeArray[$envelope['id']]['paper'] = $envelopePaper['paper'];
                    }
                }

                //Envelope Paper Colors (Multiple)
                $envelopePaperColors     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', c.name order by c.name) color FROM envelope_paper ep INNER JOIN paper p ON FIND_IN_SET(p.id, ep.paper_id) > 0 INNER JOIN color c ON FIND_IN_SET(c.id,p.color_id) > 0 Where ep.envelope_id = " . $envelope['id'] . " GROUP BY p.id")->queryAll();
                $envelopePaperColorArray = array ();
                foreach ($envelopePaperColors as $envelopePaperColor) {
                    $envelopePaperColorArray[] = $envelopePaperColor['color'];
                }
                $envelopeArray[$envelope['id']]['paper_color'] = implode(', ', $envelopePaperColorArray);

                //Envelope Paper Type (Multiple)
                $envelopePaperTypes     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pt.name order by pt.name) paper_type FROM envelope_paper ep INNER JOIN paper p ON FIND_IN_SET(p.id, ep.paper_id) > 0 INNER JOIN paper_type pt ON FIND_IN_SET(pt.id,p.paper_type_id) > 0 Where ep.envelope_id = " . $envelope['id'] . " GROUP BY p.id")->queryAll();
                $envelopePaperTypeArray = array ();
                foreach ($envelopePaperTypes as $envelopePaperType) {
                    $envelopePaperTypeArray[] = $envelopePaperType['paper_type'];
                }
                $envelopeArray[$envelope['id']]['paper_type'] = implode(', ', $envelopePaperTypeArray);

                //Envelope DieCut (Multiple)
                $envelopeDies = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', dc.code order by dc.code) diecut FROM envelope_die ed INNER JOIN die_cut dc ON FIND_IN_SET(dc.id, ed.die_id) > 0 Where ed.envelope_id = " . $envelope['id'] . " GROUP BY ed.envelope_id")->queryAll();
                if ( ! empty($envelopeDies)) {
                    foreach ($envelopeDies as $envelopeDie) {
                        $envelopeArray[$envelope['id']]['diecut'] = $envelopeDie['diecut'];
                    }
                }

                //Envelope Block (Multiple)
                $envelopeBlocks = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', b.code order by b.code) block FROM envelope_block eb INNER JOIN block b ON FIND_IN_SET(b.id, eb.block_id) > 0 Where eb.envelope_id = " . $envelope['id'] . " GROUP BY eb.envelope_id")->queryAll();
                if ( ! empty($envelopeBlocks)) {
                    foreach ($envelopeBlocks as $envelopeBlock) {
                        $envelopeArray[$envelope['id']]['block'] = $envelopeBlock['block'];
                    }
                }

                //Envelope PrintingMethod (Multiple)
                $envelopePrintingMethods = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pm.name order by pm.name) printing_method FROM envelope_printing_method epm INNER JOIN printing_method pm ON FIND_IN_SET(pm.id, epm.printing_method_id) > 0 Where epm.envelope_id = " . $envelope['id'] . " GROUP BY epm.envelope_id")->queryAll();
                if ( ! empty($envelopePrintingMethods)) {
                    foreach ($envelopePrintingMethods as $envelopePrintingMethod) {
                        $envelopeArray[$envelope['id']]['printing_method'] = $envelopePrintingMethod['printing_method'];
                    }
                }
            }
        }

        // Folders (Multiple)
        $folderArray = array ();
        $folders     = \Yii::$app->db->createCommand("SELECT f.id FROM folder f INNER JOIN card c ON FIND_IN_SET(f.card_id, c.id) > 0 Where f.card_id = " . $cardId . " GROUP BY f.card_id")->queryAll();
        if ( ! empty($folders)) {
            foreach ($folders as $folder) {

                //Folder Papers (Multiple)
                $folderPapers = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', p.code order by p.code) paper FROM folder_paper fp INNER JOIN paper p ON FIND_IN_SET(p.id, fp.paper_id) > 0 Where fp.folder_id = " . $folder['id'] . " GROUP BY fp.folder_id")->queryAll();
                if ( ! empty($folderPapers)) {
                    foreach ($folderPapers as $folderPaper) {
                        $folderArray[$folder['id']]['paper'] = $folderPaper['paper'];
                    }
                }

                //Folder Paper Colors (Multiple)
                $folderPaperColors     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', c.name order by c.name) color FROM folder_paper fp INNER JOIN paper p ON FIND_IN_SET(p.id, fp.paper_id) > 0 INNER JOIN color c ON FIND_IN_SET(c.id,p.color_id) > 0 Where fp.folder_id = " . $folder['id'] . " GROUP BY p.id")->queryAll();
                $folderPaperColorArray = array ();
                foreach ($folderPaperColors as $folderPaperColor) {
                    $folderPaperColorArray[] = $folderPaperColor['color'];
                }
                $folderArray[$folder['id']]['paper_color'] = implode(', ', $folderPaperColorArray);

                //Folder Paper Type (Multiple)
                $folderPaperTypes     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pt.name order by pt.name) paper_type FROM folder_paper fp INNER JOIN paper p ON FIND_IN_SET(p.id, fp.paper_id) > 0 INNER JOIN paper_type pt ON FIND_IN_SET(pt.id,p.paper_type_id) > 0 Where fp.folder_id = " . $folder['id'] . " GROUP BY p.id")->queryAll();
                $folderPaperTypeArray = array ();
                foreach ($folderPaperTypes as $folderPaperType) {
                    $folderPaperTypeArray[] = $folderPaperType['paper_type'];
                }
                $folderArray[$folder['id']]['paper_type'] = implode(', ', $folderPaperTypeArray);

                //Folder DieCut (Multiple)
                $folderDies = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', dc.code order by dc.code) diecut FROM folder_die fd INNER JOIN die_cut dc ON FIND_IN_SET(dc.id, fd.die_id) > 0 Where fd.folder_id = " . $folder['id'] . " GROUP BY fd.folder_id")->queryAll();
                if ( ! empty($folderDies)) {
                    foreach ($folderDies as $folderDie) {
                        $folderArray[$folder['id']]['diecut'] = $folderDie['diecut'];
                    }
                }

                //Folder Block (Multiple)
                $folderBlocks = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', b.code order by b.code) block FROM folder_block fb INNER JOIN block b ON FIND_IN_SET(b.id, fb.block_id) > 0 Where fb.folder_id = " . $folder['id'] . " GROUP BY fb.folder_id")->queryAll();
                if ( ! empty($folderBlocks)) {
                    foreach ($folderBlocks as $folderBlock) {
                        $folderArray[$folder['id']]['block'] = $folderBlock['block'];
                    }
                }

                //Folder PrintingMethod (Multiple)
                $folderPrintingMethods = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pm.name order by pm.name) printing_method FROM folder_printing_method fpm INNER JOIN printing_method pm ON FIND_IN_SET(pm.id, fpm.printing_method_id) > 0 Where fpm.folder_id = " . $folder['id'] . " GROUP BY fpm.folder_id")->queryAll();
                if ( ! empty($folderPrintingMethods)) {
                    foreach ($folderPrintingMethods as $folderPrintingMethod) {
                        $folderArray[$folder['id']]['printing_method'] = $folderPrintingMethod['printing_method'];
                    }
                }
            }
        }

        // Scrolls (Multiple)
        $scrollArray = array ();
        $scrolls     = \Yii::$app->db->createCommand("SELECT s.id FROM scroll s INNER JOIN card c ON FIND_IN_SET(s.card_id, c.id) > 0 Where s.card_id = " . $cardId . " GROUP BY s.card_id")->queryAll();
        if ( ! empty($scrolls)) {
            foreach ($scrolls as $scroll) {

                //Scroll Papers (Multiple)
                $scrollPapers = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', p.code order by p.code) paper FROM scroll_paper sp INNER JOIN paper p ON FIND_IN_SET(p.id, sp.paper_id) > 0 Where sp.scroll_id = " . $scroll['id'] . " GROUP BY sp.scroll_id")->queryAll();
                if ( ! empty($scrollPapers)) {
                    foreach ($scrollPapers as $scrollPaper) {
                        $scrollArray[$scroll['id']]['paper'] = $scrollPaper['paper'];
                    }
                }

                //Scroll Paper Colors (Multiple)
                $scrollPaperColors     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', c.name order by c.name) color FROM scroll_paper sp INNER JOIN paper p ON FIND_IN_SET(p.id, sp.paper_id) > 0 INNER JOIN color c ON FIND_IN_SET(c.id,p.color_id) > 0 Where sp.scroll_id = " . $scroll['id'] . " GROUP BY p.id")->queryAll();
                $scrollPaperColorArray = array ();
                foreach ($scrollPaperColors as $scrollPaperColor) {
                    $scrollPaperColorArray[] = $scrollPaperColor['color'];
                }
                $scrollArray[$scroll['id']]['paper_color'] = implode(', ', $scrollPaperColorArray);

                //Scroll Paper Type (Multiple)
                $scrollPaperTypes     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pt.name order by pt.name) paper_type FROM scroll_paper sp INNER JOIN paper p ON FIND_IN_SET(p.id, sp.paper_id) > 0 INNER JOIN paper_type pt ON FIND_IN_SET(pt.id,p.paper_type_id) > 0 Where sp.scroll_id = " . $scroll['id'] . " GROUP BY p.id")->queryAll();
                $scrollPaperTypeArray = array ();
                foreach ($scrollPaperTypes as $scrollPaperType) {
                    $scrollPaperTypeArray[] = $scrollPaperType['paper_type'];
                }
                $scrollArray[$scroll['id']]['paper_type'] = implode(', ', $scrollPaperTypeArray);

                //Scroll DieCut (Multiple)
                $scrollDies = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', dc.code order by dc.code) diecut FROM scroll_die sd INNER JOIN die_cut dc ON FIND_IN_SET(dc.id, sd.die_id) > 0 Where sd.scroll_id = " . $scroll['id'] . " GROUP BY sd.scroll_id")->queryAll();
                if ( ! empty($scrollDies)) {
                    foreach ($scrollDies as $scrollDie) {
                        $scrollArray[$scroll['id']]['diecut'] = $scrollDie['diecut'];
                    }
                }

                //Scroll Block (Multiple)
                $scrollBlocks = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', b.code order by b.code) block FROM scroll_block sb INNER JOIN block b ON FIND_IN_SET(b.id, sb.block_id) > 0 Where sb.scroll_id = " . $scroll['id'] . " GROUP BY sb.scroll_id")->queryAll();
                if ( ! empty($scrollBlocks)) {
                    foreach ($scrollBlocks as $scrollBlock) {
                        $scrollArray[$scroll['id']]['block'] = $scrollBlock['block'];
                    }
                }

                //Scroll PrintingMethod (Multiple)
                $scrollPrintingMethods = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pm.name order by pm.name) printing_method FROM scroll_printing_method spm INNER JOIN printing_method pm ON FIND_IN_SET(pm.id, spm.printing_method_id) > 0 Where spm.scroll_id = " . $scroll['id'] . " GROUP BY spm.scroll_id")->queryAll();
                if ( ! empty($scrollPrintingMethods)) {
                    foreach ($scrollPrintingMethods as $scrollPrintingMethod) {
                        $scrollArray[$scroll['id']]['printing_method'] = $scrollPrintingMethod['printing_method'];
                    }
                }

                //Scroll/Tube (Multiple)
                $scrollTubes = \Yii::$app->db->createCommand("SELECT length,weight,diameter FROM tube t Where t.scroll_id = " . $scroll['id'] . " GROUP BY t.scroll_id")->queryAll();
                if ( ! empty($scrollTubes)) {
                    foreach ($scrollTubes as $scrollTube) {
                        $scrollArray[$scroll['id']]['length']   = $scrollTube['length'];
                        $scrollArray[$scroll['id']]['weight']   = $scrollTube['weight'];
                        $scrollArray[$scroll['id']]['diameter'] = $scrollTube['diameter'];
                    }
                }
            }
        }

        // Boxs (Multiple)
        $boxArray = array ();
        $boxs     = \Yii::$app->db->createCommand("SELECT b.id FROM box b INNER JOIN card c ON FIND_IN_SET(b.card_id, c.id) > 0 Where b.card_id = " . $cardId . " GROUP BY b.card_id")->queryAll();
        if ( ! empty($boxs)) {
            foreach ($boxs as $box) {

                //Box Papers (Multiple)
                $boxPapers = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', p.code order by p.code) paper FROM box_paper bp INNER JOIN paper p ON FIND_IN_SET(p.id, bp.paper_id) > 0 Where bp.box_id = " . $box['id'] . " GROUP BY bp.box_id")->queryAll();
                if ( ! empty($boxPapers)) {
                    foreach ($boxPapers as $boxPaper) {
                        $boxArray[$box['id']]['paper'] = $boxPaper['paper'];
                    }
                }

                //Box Paper Colors (Multiple)
                $boxPaperColors     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', c.name order by c.name) color FROM box_paper bp INNER JOIN paper p ON FIND_IN_SET(p.id, bp.paper_id) > 0 INNER JOIN color c ON FIND_IN_SET(c.id,p.color_id) > 0 Where bp.box_id = " . $box['id'] . " GROUP BY p.id")->queryAll();
                $boxPaperColorArray = array ();
                foreach ($boxPaperColors as $boxPaperColor) {
                    $boxPaperColorArray[] = $boxPaperColor['color'];
                }
                $boxArray[$box['id']]['paper_color'] = implode(', ', $boxPaperColorArray);

                //Box Paper Type (Multiple)
                $boxPaperTypes     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pt.name order by pt.name) paper_type FROM box_paper bp INNER JOIN paper p ON FIND_IN_SET(p.id, bp.paper_id) > 0 INNER JOIN paper_type pt ON FIND_IN_SET(pt.id,p.paper_type_id) > 0 Where bp.box_id = " . $box['id'] . " GROUP BY p.id")->queryAll();
                $boxPaperTypeArray = array ();
                foreach ($boxPaperTypes as $boxPaperType) {
                    $boxPaperTypeArray[] = $boxPaperType['paper_type'];
                }
                $boxArray[$box['id']]['paper_type'] = implode(', ', $boxPaperTypeArray);

                //Box DieCut (Multiple)
                $boxDies = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', dc.code order by dc.code) diecut FROM box_die bd INNER JOIN die_cut dc ON FIND_IN_SET(dc.id, bd.die_id) > 0 Where bd.box_id = " . $box['id'] . " GROUP BY bd.box_id")->queryAll();
                if ( ! empty($boxDies)) {
                    foreach ($boxDies as $boxDie) {
                        $boxArray[$box['id']]['diecut'] = $boxDie['diecut'];
                    }
                }

                //Box Block (Multiple)
                $boxBlocks = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', b.code order by b.code) block FROM box_block bb INNER JOIN block b ON FIND_IN_SET(b.id, bb.block_id) > 0 Where bb.box_id = " . $box['id'] . " GROUP BY bb.box_id")->queryAll();
                if ( ! empty($boxBlocks)) {
                    foreach ($boxBlocks as $boxBlock) {
                        $boxArray[$box['id']]['block'] = $boxBlock['block'];
                    }
                }

                //Box PrintingMethod (Multiple)
                $boxPrintingMethods = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pm.name order by pm.name) printing_method FROM box_printing_method bpm INNER JOIN printing_method pm ON FIND_IN_SET(pm.id, bpm.printing_method_id) > 0 Where bpm.box_id = " . $box['id'] . " GROUP BY bpm.box_id")->queryAll();
                if ( ! empty($boxPrintingMethods)) {
                    foreach ($boxPrintingMethods as $boxPrintingMethod) {
                        $boxArray[$box['id']]['printing_method'] = $boxPrintingMethod['printing_method'];
                    }
                }

                //Box All Values (Single)
                $boxAlls = \Yii::$app->db->createCommand("SELECT box_type_id,length,width,thickness FROM box b Where b.id = " . $box['id'] . " GROUP BY b.id")->queryAll();
                if ( ! empty($boxAlls)) {
                    foreach ($boxAlls as $boxAll) {
                        $boxArray[$box['id']]['length']    = $boxAll['length'];
                        $boxArray[$box['id']]['width']     = $boxAll['width'];
                        $boxArray[$box['id']]['thickness'] = $boxAll['thickness'];
                        //Box Type.
                        if ( ! empty($boxAll['box_type_id'])) {
                            $boxTypeModel = BoxType::findOne(['id' => $boxAll['box_type_id']]);
                            if ( ! empty($boxTypeModel)) {
                                $boxArray[$box['id']]['box_type'] = $boxTypeModel['name'];
                            }
                        }
                    }
                }
            }
        }

        // Inserts (Multiple)
        $insertArray = array ();
        $inserts     = (new \yii\db\Query())
                ->select(['i.id'])
                ->from('insert i')
                ->innerJoin('card c', 'c.id = i.card_id')
                ->andWhere(['i.card_id' => $cardId])
                ->all();
        if ( ! empty($inserts)) {
            foreach ($inserts as $insert) {
                //Insert Papers (Multiple)
                $insertPapers = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', p.code order by p.code) paper FROM insert_paper ip INNER JOIN paper p ON FIND_IN_SET(p.id, ip.paper_id) > 0 Where ip.insert_id = " . $insert['id'] . " GROUP BY ip.insert_id")->queryAll();
                if ( ! empty($insertPapers)) {
                    foreach ($insertPapers as $insertPaper) {
                        $insertArray[$insert['id']]['paper'] = $insertPaper['paper'];
                    }
                }

                //Insert Paper Colors (Multiple)
                $insertPaperColors     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', c.name order by c.name) color FROM insert_paper ip INNER JOIN paper p ON FIND_IN_SET(p.id, ip.paper_id) > 0 INNER JOIN color c ON FIND_IN_SET(c.id,p.color_id) > 0 Where ip.insert_id = " . $insert['id'] . " GROUP BY p.id")->queryAll();
                $insertPaperColorArray = array ();
                foreach ($insertPaperColors as $insertPaperColor) {
                    $insertPaperColorArray[] = $insertPaperColor['color'];
                }
                $insertArray[$insert['id']]['paper_color'] = implode(', ', $insertPaperColorArray);

                //Insert Paper Type (Multiple)
                $insertPaperTypes     = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pt.name order by pt.name) paper_type FROM insert_paper ip INNER JOIN paper p ON FIND_IN_SET(p.id, ip.paper_id) > 0 INNER JOIN paper_type pt ON FIND_IN_SET(pt.id,p.paper_type_id) > 0 Where ip.insert_id = " . $insert['id'] . " GROUP BY p.id")->queryAll();
                $insertPaperTypeArray = array ();
                foreach ($insertPaperTypes as $insertPaperType) {
                    $insertPaperTypeArray[] = $insertPaperType['paper_type'];
                }
                $insertArray[$insert['id']]['paper_type'] = implode(', ', $insertPaperTypeArray);

                //Insert DieCut (Multiple)
                $insertDies = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', dc.code order by dc.code) diecut FROM insert_die id INNER JOIN die_cut dc ON FIND_IN_SET(dc.id, id.die_id) > 0 Where id.insert_id = " . $insert['id'] . " GROUP BY id.insert_id")->queryAll();
                if ( ! empty($insertDies)) {
                    foreach ($insertDies as $insertDie) {
                        $insertArray[$insert['id']]['diecut'] = $insertDie['diecut'];
                    }
                }

                //Insert Block (Multiple)
                $insertBlocks = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', b.code order by b.code) block FROM insert_block ib INNER JOIN block b ON FIND_IN_SET(b.id, ib.block_id) > 0 Where ib.insert_id = " . $insert['id'] . " GROUP BY ib.insert_id")->queryAll();
                if ( ! empty($insertBlocks)) {
                    foreach ($insertBlocks as $insertBlock) {
                        $insertArray[$insert['id']]['block'] = $insertBlock['block'];
                    }
                }

                //Insert PrintingMethod (Multiple)
                $insertPrintingMethods = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', pm.name order by pm.name) printing_method FROM insert_printing_method ipm INNER JOIN printing_method pm ON FIND_IN_SET(pm.id, ipm.printing_method_id) > 0 Where ipm.insert_id = " . $insert['id'] . " GROUP BY ipm.insert_id")->queryAll();
                if ( ! empty($insertPrintingMethods)) {
                    foreach ($insertPrintingMethods as $insertPrintingMethod) {
                        $insertArray[$insert['id']]['printing_method'] = $insertPrintingMethod['printing_method'];
                    }
                }
            }
        }

        // Additions (Single)
        $additionsArray = array ();
        $additions      = \Yii::$app->db->createCommand("SELECT a.id FROM additions a INNER JOIN card c ON FIND_IN_SET(a.card_id, c.id) > 0 Where a.card_id = " . $cardId . " GROUP BY a.card_id")->queryAll();
        if ( ! empty($additions)) {
            foreach ($additions as $addition) {
                //Addition Tassle(Single)
                $additionTassles = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', t.code order by t.code) tassle FROM additions a INNER JOIN tassle t ON FIND_IN_SET(t.id, a.tassle_id) > 0 Where a.id = " . $addition['id'] . " GROUP BY a.id")->queryAll();
                if ( ! empty($additionTassles)) {
                    foreach ($additionTassles as $additionTassle) {
                        $additionsArray[$addition['id']]['tassle'] = $additionTassle['tassle'];
                    }
                }

                //Addition Ribbon(Single)
                $additionRibbons = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', r.code order by r.code) ribbon FROM additions a INNER JOIN ribbon r ON FIND_IN_SET(r.id, a.ribbon_id) > 0 Where a.id = " . $addition['id'] . " GROUP BY a.id")->queryAll();
                if ( ! empty($additionRibbons)) {
                    foreach ($additionRibbons as $additionRibbon) {
                        $additionsArray[$addition['id']]['ribbon'] = $additionRibbon['ribbon'];
                    }
                }

                //Addition Rhinestone(Single)
                $additionRhinestones = \Yii::$app->db->createCommand("SELECT GROUP_CONCAT(' ', r.code order by r.code) rhinestone FROM additions a INNER JOIN rhinestone r ON FIND_IN_SET(r.id, a.rhinestone_id) > 0 Where a.id = " . $addition['id'] . " GROUP BY a.id")->queryAll();
                if ( ! empty($additionRhinestones)) {
                    foreach ($additionRhinestones as $additionRhinestone) {
                        $additionsArray[$addition['id']]['rhinestone'] = $additionRhinestone['rhinestone'];
                    }
                }

                //Addition All Values (Single)
                $additionAlls = \Yii::$app->db->createCommand("SELECT extra_insert_weight,extra_insert_price,photocard,`linear`,velvet FROM additions a Where a.id = " . $addition['id'] . " GROUP BY a.id")->queryAll();
                if ( ! empty($additionAlls)) {
                    foreach ($additionAlls as $additionAll) {
                        $additionsArray[$addition['id']]['extra_insert_weight'] = $additionAll['extra_insert_weight'];
                        $additionsArray[$addition['id']]['extra_insert_price']  = $additionAll['extra_insert_price'];
                        $additionsArray[$addition['id']]['photocard']           = $additionAll['photocard'];
                        $additionsArray[$addition['id']]['linear']              = $additionAll['linear'];
                        $additionsArray[$addition['id']]['velvet']              = $additionAll['velvet'];
                    }
                }
            }
        }

        // Get CardInfo.
        $fields = ['c.id', 'c.name', 'c.status'];
        $card   = (new \yii\db\Query())
                ->select($fields)
                ->from('card c')
                ->where(['c.id' => $cardId])
                ->one();

        if (($cardInfo = CardInfo::findOne($cardId)) == null) {
            $cardInfo = new CardInfo();
        }
        $cardInfo->card_id   = $cardId;
        $cardInfo->theme     = ! empty($themes[$cardId]) ? trim($themes[$cardId]) : '';
        $cardInfo->envelope  = ! empty($envelopeArray) ? trim(json_encode($envelopeArray, TRUE)) : '';
        $cardInfo->folder    = ! empty($folderArray) ? trim(json_encode($folderArray, TRUE)) : '';
        $cardInfo->scroll    = ! empty($scrollArray) ? trim(json_encode($scrollArray, TRUE)) : '';
        $cardInfo->box       = ! empty($boxArray) ? trim(json_encode($boxArray, TRUE)) : '';
        $cardInfo->insert    = ! empty($insertArray) ? trim(json_encode($insertArray, TRUE)) : '';
        $cardInfo->additions = ! empty($additionsArray) ? trim(json_encode($additionsArray, TRUE)) : '';
        $cardInfo->status    = $card['status'];

        if ( ! $cardInfo->save()) {
            print_r($cardInfo->getErrors());
        }
    }

}
