<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[EnvelopeBlock]].
 *
 * @see EnvelopeBlock
 */
class EnvelopeBlockQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return EnvelopeBlock[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return EnvelopeBlock|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
