<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Website]].
 *
 * @see Website
 */
class WebsiteQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Website[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Website|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * Finds the Code For Website
     * @return Code
     */
    public static function getUniqueCode ($code)
    {
        $model = Website::find()->orderBy(['id' => SORT_DESC])->limit(1)->one();
        if ( ! empty($model)) {
            $incrementValue = preg_replace('/[^0-9]/', '', $model->code) + 1;
            return $code . '-' . (sprintf("%02d", $incrementValue));
        } else {
            return $code . "-01";
        }
    }

}
