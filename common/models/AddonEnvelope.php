<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "addon_envelope".
 *
 * @property int $id
 * @property int|null $addon_id
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Addon $addon
 */
class AddonEnvelope extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * AddonEnvelope Paper Function Define.
     * @var type 
     */
    public $addon_envelope_paper;

    /**
     * AddonEnvelope Die Function Define.
     * @var type 
     */
    public $addon_envelope_die;

    /**
     * AddonEnvelope Block Function Define.
     * @var type 
     */
    public $addon_envelope_block;

    /**
     * AddonEnvelope Printing Method Function Define.
     * @var type 
     */
    public $addon_envelope_printing_method;

    /**
     * Paper Color Function Define.
     * @var type 
     */
    public $paper_color;

    /**
     * Paper Type Function Define.
     * @var type 
     */
    public $paper_type;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'addon_envelope';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['addon_id'/* , 'addon_envelope_paper', 'addon_envelope_die', 'addon_envelope_block', 'addon_envelope_printing_method' */], 'required'],
            [['addon_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['addon_id'], 'exist', 'skipOnError' => true, 'targetClass' => Addon::className(), 'targetAttribute' => ['addon_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'addon_id' => 'Addon',
            'addon_envelope_paper' => 'Paper',
            'addon_envelope_die' => 'Die',
            'addon_envelope_block' => 'Block',
            'addon_envelope_printing_method' => 'Printing Method',
            'paper_color' => 'Paper Color',
            'paper_type' => 'Paper Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[Addon]].
     *
     * @return \yii\db\ActiveQuery|AddonQuery
     */
    public function getAddon ()
    {
        return $this->hasOne(Addon::className(), ['id' => 'addon_id']);
    }

    /**
     * {@inheritdoc}
     * @return AddonEnvelopeQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AddonEnvelopeQuery(get_called_class());
    }

}
