<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[FolderPrintingMethod]].
 *
 * @see FolderPrintingMethod
 */
class FolderPrintingMethodQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return FolderPrintingMethod[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FolderPrintingMethod|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
