<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "insert_block".
 *
 * @property int $insert_id
 * @property int $block_id
 *
 * @property Block $block
 * @property Insert $insert
 */
class InsertBlock extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'insert_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['insert_id', 'block_id'], 'required'],
            [['insert_id', 'block_id'], 'integer'],
            [['insert_id', 'block_id'], 'unique', 'targetAttribute' => ['insert_id', 'block_id']],
            [['block_id'], 'exist', 'skipOnError' => true, 'targetClass' => Block::className(), 'targetAttribute' => ['block_id' => 'id']],
            [['insert_id'], 'exist', 'skipOnError' => true, 'targetClass' => Insert::className(), 'targetAttribute' => ['insert_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'insert_id' => 'Insert ID',
            'block_id' => 'Block ID',
        ];
    }

    /**
     * Gets query for [[Block]].
     *
     * @return \yii\db\ActiveQuery|BlockQuery
     */
    public function getBlock ()
    {
        return $this->hasOne(Block::className(), ['id' => 'block_id']);
    }

    /**
     * Gets query for [[Insert]].
     *
     * @return \yii\db\ActiveQuery|InsertQuery
     */
    public function getInsert ()
    {
        return $this->hasOne(Insert::className(), ['id' => 'insert_id']);
    }

    /**
     * {@inheritdoc}
     * @return InsertBlockQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new InsertBlockQuery(get_called_class());
    }

}
