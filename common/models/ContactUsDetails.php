<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * This is the model class for table "contact_us_details".
 *
 * @property int $id
 * @property int|null $contact_us_id
 * @property string $name
 * @property string|null $icon
 * @property string|null $description
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property ContactUs $contactUs
 */
class ContactUsDetails extends ActiveRecord
{

    const STATUS_ACTIVE        = 1;
    const STATUS_INACTIVE      = 0;
    const STATUS_DELETE        = 2;
    const MAIN_DIRECTORY       = 'uploads/contact_us_detail/';
    const ORIGINAL_FILEPATH    = 'original/';
    const THUMBNAIL_FILEPATH   = 'thumbnail/';
    const ORIGINAL_IMAGE_WIDTH = 100;
    const THUMB_IMAGE_HEIGHT   = 47;
    const THUMB_IMAGE_WIDTH    = 47;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'contact_us_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['name'], 'required'],
            [['contact_us_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 50],
            [['icon'], 'string', 'max' => 255],
            [['icon'], 'safe'],
            [['icon'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,jpeg'],
            [['name'], 'filter', 'filter' => 'trim'],
            [['name'], 'unique'],
            [['contact_us_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContactUs::className(), 'targetAttribute' => ['contact_us_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'contact_us_id' => 'Contact Us',
            'name' => 'Name',
            'icon' => 'Icon',
            'description' => 'Description',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * ContactUsDetails Icon Image Upload.
     * @return type
     */
    public function upload ()
    {
        //File BaseName.
        $baseName = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $this->name));

        //File Extension.
        $imageExtension = $this->icon->extension;

        //Main Directory Path.
        FileHelper::createDirectory(ContactUsDetails::MAIN_DIRECTORY);

        //Original File Path.
        $originalFilePath = ContactUsDetails::MAIN_DIRECTORY . ContactUsDetails::ORIGINAL_FILEPATH;
        FileHelper::createDirectory($originalFilePath);

        //Thumbnail File Path.
        $thumbnailFilePath = ContactUsDetails::MAIN_DIRECTORY . ContactUsDetails::THUMBNAIL_FILEPATH;
        FileHelper::createDirectory($thumbnailFilePath);

        //Original Image Save.
        $response = $this->icon->saveAs($originalFilePath . $baseName . '.' . $imageExtension);

        //Original Image Rotation.
        if (($response == 1) && file_exists($originalFilePath . $baseName . '.' . $imageExtension)) {
            $imagine = Image::getImagine()->open($originalFilePath . $baseName . '.' . $imageExtension);
            $exif    = @exif_read_data($originalFilePath . $baseName . '.' . $imageExtension);

            //Rotate Image By Orientation.
            if ( ! empty($exif['Orientation'])) {
                switch ($exif['Orientation'])
                {
                    case 3:
                        $imagine->rotate(180);
                        break;
                    case 6:
                        $imagine->rotate(90);
                        break;
                    case 8:
                        $imagine->rotate(-90);
                        break;
                }
            }

            $imagine->save($originalFilePath . $baseName . '.' . $imageExtension, ['quality' => 70]);

            //Original Image Compress Image and Save.
            //1280x800 
            $sizes  = getimagesize($originalFilePath . $baseName . '.' . $imageExtension);
            $width  = ContactUsDetails::ORIGINAL_IMAGE_WIDTH;
            $height = round($sizes[1] * $width / $sizes[0]);
            $imagine->resize(new Box($width, $height))->save($originalFilePath . $baseName . '.' . $imageExtension, ['quality' => 70]);

            //Thumbnail Image Save.
            Image::getImagine()
                    ->open($originalFilePath . $baseName . '.' . $imageExtension)
                    ->thumbnail(new Box(ContactUsDetails::THUMB_IMAGE_WIDTH, ContactUsDetails::THUMB_IMAGE_HEIGHT))
                    ->save($thumbnailFilePath . $baseName . '.' . $imageExtension);
            return $baseName . '.' . $imageExtension;
        }
    }

    /**
     * Gets query for [[ContactUs]].
     *
     * @return \yii\db\ActiveQuery|ContactUsQuery
     */
    public function getContactUs ()
    {
        return $this->hasOne(ContactUs::className(), ['id' => 'contact_us_id']);
    }

    /**
     * {@inheritdoc}
     * @return ContactUsDetailsQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new ContactUsDetailsQuery(get_called_class());
    }

}
