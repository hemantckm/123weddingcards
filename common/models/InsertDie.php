<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "insert_die".
 *
 * @property int $insert_id
 * @property int $die_id
 *
 * @property DieCut $die
 * @property Insert $insert
 */
class InsertDie extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'insert_die';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['insert_id', 'die_id'], 'required'],
            [['insert_id', 'die_id'], 'integer'],
            [['insert_id', 'die_id'], 'unique', 'targetAttribute' => ['insert_id', 'die_id']],
            [['die_id'], 'exist', 'skipOnError' => true, 'targetClass' => DieCut::className(), 'targetAttribute' => ['die_id' => 'id']],
            [['insert_id'], 'exist', 'skipOnError' => true, 'targetClass' => Insert::className(), 'targetAttribute' => ['insert_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'insert_id' => 'Insert ID',
            'die_id' => 'Die ID',
        ];
    }

    /**
     * Gets query for [[Die]].
     *
     * @return \yii\db\ActiveQuery|DieCutQuery
     */
    public function getDie ()
    {
        return $this->hasOne(DieCut::className(), ['id' => 'die_id']);
    }

    /**
     * Gets query for [[Insert]].
     *
     * @return \yii\db\ActiveQuery|InsertQuery
     */
    public function getInsert ()
    {
        return $this->hasOne(Insert::className(), ['id' => 'insert_id']);
    }

    /**
     * {@inheritdoc}
     * @return InsertDieQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new InsertDieQuery(get_called_class());
    }

}
