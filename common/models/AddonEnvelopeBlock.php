<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "addon_envelope_block".
 *
 * @property int $addon_envelope_id
 * @property int $block_id
 *
 * @property AddonEnvelope $addonEnvelope
 * @property Block $block
 */
class AddonEnvelopeBlock extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'addon_envelope_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['addon_envelope_id', 'block_id'], 'required'],
            [['addon_envelope_id', 'block_id'], 'integer'],
            [['addon_envelope_id', 'block_id'], 'unique', 'targetAttribute' => ['addon_envelope_id', 'block_id']],
            [['addon_envelope_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddonEnvelope::className(), 'targetAttribute' => ['addon_envelope_id' => 'id']],
            [['block_id'], 'exist', 'skipOnError' => true, 'targetClass' => Block::className(), 'targetAttribute' => ['block_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'addon_envelope_id' => 'Addon Envelope ID',
            'block_id' => 'Block ID',
        ];
    }

    /**
     * Gets query for [[AddonEnvelope]].
     *
     * @return \yii\db\ActiveQuery|AddonEnvelopeQuery
     */
    public function getAddonEnvelope()
    {
        return $this->hasOne(AddonEnvelope::className(), ['id' => 'addon_envelope_id']);
    }

    /**
     * Gets query for [[Block]].
     *
     * @return \yii\db\ActiveQuery|BlockQuery
     */
    public function getBlock()
    {
        return $this->hasOne(Block::className(), ['id' => 'block_id']);
    }

    /**
     * {@inheritdoc}
     * @return AddonEnvelopeBlockQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AddonEnvelopeBlockQuery(get_called_class());
    }
}
