<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Paper]].
 *
 * @see Paper
 */
class PaperQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Paper[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Paper|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get Paper List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, code')
                ->asArray()
                ->where(['status' => Paper::STATUS_ACTIVE])
                ->orderBy('code asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'code');
    }

    /**
     * Get the Selected Paper and Get all Details.
     */
    public function getPaperInfo ($id)
    {
        $result = (new \yii\db\Query())
                ->select(['p.*', 'c.name color_name','pt.name paper_type_name'])
                ->from('paper p')
                ->innerJoin('color c', 'p.color_id = c.id')
                ->innerJoin('paper_type pt', 'p.paper_type_id = pt.id')
                ->andWhere(['p.id' => $id])
                ->one();
        return $result;
    }

}
