<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "addon_style".
 *
 * @property int $id
 * @property int $addon_type_id
 * @property string $name
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property AddonType $addonType
 */
class AddonStyle extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'addon_style';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['addon_type_id', 'name'], 'required'],
            [['addon_type_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'filter', 'filter' => 'trim'],
            //[['name'], 'unique'],
            [['addon_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddonType::className(), 'targetAttribute' => ['addon_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'addon_type_id' => 'Addon Type',
            'name' => 'Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[AddonType]].
     *
     * @return \yii\db\ActiveQuery|AddonTypeQuery
     */
    public function getAddonType ()
    {
        return $this->hasOne(AddonType::className(), ['id' => 'addon_type_id']);
    }

    /**
     * {@inheritdoc}
     * @return AddonStyleQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AddonStyleQuery(get_called_class());
    }

}
