<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Scroll]].
 *
 * @see Scroll
 */
class ScrollQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Scroll[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Scroll|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

}
