<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "envelope".
 *
 * @property int $id
 * @property int|null $card_id
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Card $card
 * @property EnvelopeBlock[] $envelopeBlocks
 * @property Block[] $blocks
 * @property EnvelopeDie[] $envelopeDies
 * @property DieCut[] $dies
 * @property EnvelopePaper[] $envelopePapers
 * @property Paper[] $papers
 * @property EnvelopePrintingMethod[] $envelopePrintingMethods
 * @property PrintingMethod[] $printingMethods
 */
class Envelope extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * Envelope Paper Function Define.
     * @var type 
     */
    public $envelope_paper;

    /**
     * Envelope Die Function Define.
     * @var type 
     */
    public $envelope_die;

    /**
     * Envelope Block Function Define.
     * @var type 
     */
    public $envelope_block;

    /**
     * Envelope Printing Method Function Define.
     * @var type 
     */
    public $envelope_printing_method;

    /**
     * Paper Color Function Define.
     * @var type 
     */
    public $paper_color;

    /**
     * Paper Type Function Define.
     * @var type 
     */
    public $paper_type;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'envelope';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['card_id'/*, 'envelope_paper', 'envelope_die', 'envelope_block', 'envelope_printing_method'*/], 'required'],
            [['card_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['card_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'card_id' => 'Card',
            'envelope_paper' => 'Paper',
            'envelope_die' => 'Die',
            'envelope_block' => 'Block',
            'envelope_printing_method' => 'Printing Method',
            'paper_color' => 'Paper Color',
            'paper_type' => 'Paper Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[Card]].
     *
     * @return \yii\db\ActiveQuery|CardQuery
     */
    public function getCard ()
    {
        return $this->hasOne(Card::className(), ['id' => 'card_id']);
    }

    /**
     * Gets query for [[EnvelopeBlocks]].
     *
     * @return \yii\db\ActiveQuery|EnvelopeBlockQuery
     */
    public function getEnvelopeBlocks ()
    {
        return $this->hasMany(EnvelopeBlock::className(), ['envelope_id' => 'id']);
    }

    /**
     * Gets query for [[Blocks]].
     *
     * @return \yii\db\ActiveQuery|BlockQuery
     */
    public function getBlocks ()
    {
        return $this->hasMany(Block::className(), ['id' => 'block_id'])->viaTable('envelope_block', ['envelope_id' => 'id']);
    }

    /**
     * Gets query for [[EnvelopeDies]].
     *
     * @return \yii\db\ActiveQuery|EnvelopeDieQuery
     */
    public function getEnvelopeDies ()
    {
        return $this->hasMany(EnvelopeDie::className(), ['envelope_id' => 'id']);
    }

    /**
     * Gets query for [[Dies]].
     *
     * @return \yii\db\ActiveQuery|DieCutQuery
     */
    public function getDies ()
    {
        return $this->hasMany(DieCut::className(), ['id' => 'die_id'])->viaTable('envelope_die', ['envelope_id' => 'id']);
    }

    /**
     * Gets query for [[EnvelopePapers]].
     *
     * @return \yii\db\ActiveQuery|EnvelopePaperQuery
     */
    public function getEnvelopePapers ()
    {
        return $this->hasMany(EnvelopePaper::className(), ['envelope_id' => 'id']);
    }

    /**
     * Gets query for [[Papers]].
     *
     * @return \yii\db\ActiveQuery|PaperQuery
     */
    public function getPapers ()
    {
        return $this->hasMany(Paper::className(), ['id' => 'paper_id'])->viaTable('envelope_paper', ['envelope_id' => 'id']);
    }

    /**
     * Gets query for [[EnvelopePrintingMethods]].
     *
     * @return \yii\db\ActiveQuery|EnvelopePrintingMethodQuery
     */
    public function getEnvelopePrintingMethods ()
    {
        return $this->hasMany(EnvelopePrintingMethod::className(), ['envelope_id' => 'id']);
    }

    /**
     * Gets query for [[PrintingMethods]].
     *
     * @return \yii\db\ActiveQuery|PrintingMethodQuery
     */
    public function getPrintingMethods ()
    {
        return $this->hasMany(PrintingMethod::className(), ['id' => 'printing_method_id'])->viaTable('envelope_printing_method', ['envelope_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return EnvelopeQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new EnvelopeQuery(get_called_class());
    }

}
