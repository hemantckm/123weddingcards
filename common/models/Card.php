<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "card".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property float $sample_price
 * @property float $bulk_price
 * @property float $print_charge
 * @property float $length
 * @property float $width
 * @property float $thickness
 * @property float $weight
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property CardTheme[] $cardThemes
 * @property Theme[] $themes
 */
class Card extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;
    const THEME_EXIST     = 'Scroll';

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'card';
    }

    /**
     * Card Theme Function Define.
     * @var type 
     */
    public $card_theme;

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['name', 'slug', 'sample_price', 'bulk_price', 'print_charge', 'card_theme', 'length', 'width', 'weight', 'status'], 'required'],
            [['sample_price', 'bulk_price', 'print_charge', 'thickness'], 'number'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 100],
            [['name', 'slug'], 'filter', 'filter' => 'trim'],
            [['name'], 'unique'],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'sample_price' => 'Sample Price',
            'bulk_price' => 'Bulk Price',
            'print_charge' => 'Printing Charges',
            'card_theme' => 'Card Theme',
            'length' => 'Length',
            'width' => 'Width',
            'thickness' => 'Thickness',
            'weight' => 'Weight',
            'status' => 'Availability',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[CardThemes]].
     *
     * @return \yii\db\ActiveQuery|CardThemeQuery
     */
    public function getCardThemes ()
    {
        return $this->hasMany(CardTheme::className(), ['card_id' => 'id']);
    }

    /**
     * Gets query for [[Themes]].
     *
     * @return \yii\db\ActiveQuery|ThemeQuery
     */
    public function getThemes ()
    {
        return $this->hasMany(Theme::className(), ['id' => 'theme_id'])->viaTable('card_theme', ['card_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CardQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new CardQuery(get_called_class());
    }

}
