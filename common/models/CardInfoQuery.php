<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[CardInfo]].
 *
 * @see CardInfo
 */
class CardInfoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CardInfo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CardInfo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
