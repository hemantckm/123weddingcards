<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[EnvelopePrintingMethod]].
 *
 * @see EnvelopePrintingMethod
 */
class EnvelopePrintingMethodQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return EnvelopePrintingMethod[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return EnvelopePrintingMethod|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
