<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[EnvelopeDie]].
 *
 * @see EnvelopeDie
 */
class EnvelopeDieQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return EnvelopeDie[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return EnvelopeDie|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
