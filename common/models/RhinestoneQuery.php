<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Rhinestone]].
 *
 * @see Rhinestone
 */
class RhinestoneQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Rhinestone[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Rhinestone|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get Rhinestone List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, code')
                ->asArray()
                ->where(['status' => Rhinestone::STATUS_ACTIVE])
                ->orderBy('code asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'code');
    }

}
