<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * This is the model class for table "faq".
 *
 * @property int $id
 * @property string $heading
 * @property string|null $feature_image
 * @property int $default
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Faq extends ActiveRecord
{

    const STATUS_ACTIVE        = 1;
    const STATUS_INACTIVE      = 0;
    const STATUS_DELETE        = 2;
    const DEFAULT_ACTIVE       = 1;
    const DEFAULT_INACTIVE     = 0;
    const MAIN_DIRECTORY       = 'uploads/faq/';
    const ORIGINAL_FILEPATH    = 'original/';
    const THUMBNAIL_FILEPATH   = 'thumbnail/';
    const ORIGINAL_IMAGE_WIDTH = 1890;
    const THUMB_IMAGE_HEIGHT   = 75;
    const THUMB_IMAGE_WIDTH    = 75;

    /**
     * Faq FaqType Function Define.
     * @var type 
     */
    public $faq_faq_type;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['heading', 'faq_faq_type'], 'required'],
            [['default', 'status', 'created_at', 'updated_at'], 'integer'],
            [['heading'], 'string', 'max' => 100],
            [['feature_image'], 'string', 'max' => 255],
            [['feature_image'], 'safe'],
            [['feature_image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,jpeg'],
            [['heading'], 'filter', 'filter' => 'trim'],
            [['heading'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'heading' => 'Heading',
            'feature_image' => 'Feature Image',
            'faq_faq_type' => 'Faq Type',
            'default' => 'Default',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * FAQ Feature Image Upload.
     * @return type
     */
    public function upload ()
    {
        //File BaseName.
        $baseName = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $this->heading));

        //File Extension.
        $imageExtension = $this->feature_image->extension;

        //Main Directory Path.
        FileHelper::createDirectory(Faq::MAIN_DIRECTORY);

        //Original File Path.
        $originalFilePath = Faq::MAIN_DIRECTORY . Faq::ORIGINAL_FILEPATH;
        FileHelper::createDirectory($originalFilePath);

        //Thumbnail File Path.
        $thumbnailFilePath = Faq::MAIN_DIRECTORY . Faq::THUMBNAIL_FILEPATH;
        FileHelper::createDirectory($thumbnailFilePath);

        //Original Image Save.
        $response = $this->feature_image->saveAs($originalFilePath . $baseName . '.' . $imageExtension);

        //Original Image Rotation.
        if (($response == 1) && file_exists($originalFilePath . $baseName . '.' . $imageExtension)) {
            $imagine = Image::getImagine()->open($originalFilePath . $baseName . '.' . $imageExtension);
            $exif    = @exif_read_data($originalFilePath . $baseName . '.' . $imageExtension);

            //Rotate Image By Orientation.
            if ( ! empty($exif['Orientation'])) {
                switch ($exif['Orientation'])
                {
                    case 3:
                        $imagine->rotate(180);
                        break;
                    case 6:
                        $imagine->rotate(90);
                        break;
                    case 8:
                        $imagine->rotate(-90);
                        break;
                }
            }

            $imagine->save($originalFilePath . $baseName . '.' . $imageExtension, ['quality' => 70]);

            //Original Image Compress Image and Save.
            //1280x800 
            $sizes  = getimagesize($originalFilePath . $baseName . '.' . $imageExtension);
            $width  = Faq::ORIGINAL_IMAGE_WIDTH;
            $height = round($sizes[1] * $width / $sizes[0]);
            $imagine->resize(new Box($width, $height))->save($originalFilePath . $baseName . '.' . $imageExtension, ['quality' => 70]);

            //Thumbnail Image Save.
            Image::getImagine()
                    ->open($originalFilePath . $baseName . '.' . $imageExtension)
                    ->thumbnail(new Box(Faq::THUMB_IMAGE_WIDTH, Faq::THUMB_IMAGE_HEIGHT))
                    ->save($thumbnailFilePath . $baseName . '.' . $imageExtension);
            return $baseName . '.' . $imageExtension;
        }
    }

    /**
     * {@inheritdoc}
     * @return FaqQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new FaqQuery(get_called_class());
    }

}
