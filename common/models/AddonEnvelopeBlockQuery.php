<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[AddonEnvelopeBlock]].
 *
 * @see AddonEnvelopeBlock
 */
class AddonEnvelopeBlockQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return AddonEnvelopeBlock[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AddonEnvelopeBlock|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
