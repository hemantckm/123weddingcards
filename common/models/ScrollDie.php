<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "scroll_die".
 *
 * @property int $scroll_id
 * @property int $die_id
 *
 * @property DieCut $die
 * @property Scroll $scroll
 */
class ScrollDie extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'scroll_die';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['scroll_id', 'die_id'], 'required'],
            [['scroll_id', 'die_id'], 'integer'],
            [['scroll_id', 'die_id'], 'unique', 'targetAttribute' => ['scroll_id', 'die_id']],
            [['die_id'], 'exist', 'skipOnError' => true, 'targetClass' => DieCut::className(), 'targetAttribute' => ['die_id' => 'id']],
            [['scroll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Scroll::className(), 'targetAttribute' => ['scroll_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'scroll_id' => 'Scroll ID',
            'die_id' => 'Die ID',
        ];
    }

    /**
     * Gets query for [[Die]].
     *
     * @return \yii\db\ActiveQuery|DieCutQuery
     */
    public function getDie ()
    {
        return $this->hasOne(DieCut::className(), ['id' => 'die_id']);
    }

    /**
     * Gets query for [[Scroll]].
     *
     * @return \yii\db\ActiveQuery|ScrollQuery
     */
    public function getScroll ()
    {
        return $this->hasOne(Scroll::className(), ['id' => 'scroll_id']);
    }

    /**
     * {@inheritdoc}
     * @return ScrollDieQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new ScrollDieQuery(get_called_class());
    }

}
