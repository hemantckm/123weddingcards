<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "folder_block".
 *
 * @property int $folder_id
 * @property int $block_id
 *
 * @property Block $block
 * @property Folder $folder
 */
class FolderBlock extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'folder_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['folder_id', 'block_id'], 'required'],
            [['folder_id', 'block_id'], 'integer'],
            [['folder_id', 'block_id'], 'unique', 'targetAttribute' => ['folder_id', 'block_id']],
            [['block_id'], 'exist', 'skipOnError' => true, 'targetClass' => Block::className(), 'targetAttribute' => ['block_id' => 'id']],
            [['folder_id'], 'exist', 'skipOnError' => true, 'targetClass' => Folder::className(), 'targetAttribute' => ['folder_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'folder_id' => 'Folder ID',
            'block_id' => 'Block ID',
        ];
    }

    /**
     * Gets query for [[Block]].
     *
     * @return \yii\db\ActiveQuery|BlockQuery
     */
    public function getBlock()
    {
        return $this->hasOne(Block::className(), ['id' => 'block_id']);
    }

    /**
     * Gets query for [[Folder]].
     *
     * @return \yii\db\ActiveQuery|FolderQuery
     */
    public function getFolder()
    {
        return $this->hasOne(Folder::className(), ['id' => 'folder_id']);
    }

    /**
     * {@inheritdoc}
     * @return FolderBlockQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FolderBlockQuery(get_called_class());
    }
}
