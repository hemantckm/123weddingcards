<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[CardTheme]].
 *
 * @see CardTheme
 */
class CardThemeQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return CardTheme[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CardTheme|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get Card Theme By Card ID List
     * @return type
     */
    public function getCardTheme ($cardId)
    {
        $result = $this->select('theme_id')
                ->asArray()
                ->where(['card_id' => $cardId])
                ->orderBy('theme_id asc')
                ->all();
        return $result;
    }

    /**
     * Getting All Card Themes Name with comma separator
     * @param type $cardId
     * @return type
     */
    public function getCardThemes ($cardId)
    {
        $results = (new \yii\db\Query())
                ->select(['t.id', 't.name'])
                ->from('card_theme ct')
                ->innerJoin('theme t', 't.id = ct.theme_id')
                ->andWhere(['ct.card_id' => $cardId])
                ->all();
        if ( ! empty($results)) {
            return \yii\helpers\ArrayHelper::map($results, 'id', 'name');
        } else {
            return array ();
        }
    }

}
