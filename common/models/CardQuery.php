<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Card]].
 *
 * @see Card
 */
class CardQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Card[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Card|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * String Capitalize
     * @param type $string
     * @return type
     */
    function doCapitalize ($string)
    {
        $exclude = array ('and', 'is', 'or');
        $words   = explode(' ', $string);
        foreach ($words as $key => $word) {
            if (in_array($word, $exclude)) {
                continue;
            }
            $words[$key] = ucfirst(strtolower($word));
        }
        $newString = implode(' ', $words);
        return $newString;
    }

    /**
     * To Get Card List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, name')
                ->asArray()
                ->where(['status' => Card::STATUS_ACTIVE])
                ->orderBy('name asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'name');
    }

}
