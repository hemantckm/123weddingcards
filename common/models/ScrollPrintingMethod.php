<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "scroll_printing_method".
 *
 * @property int $scroll_id
 * @property int $printing_method_id
 *
 * @property PrintingMethod $printingMethod
 * @property Scroll $scroll
 */
class ScrollPrintingMethod extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'scroll_printing_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['scroll_id', 'printing_method_id'], 'required'],
            [['scroll_id', 'printing_method_id'], 'integer'],
            [['scroll_id', 'printing_method_id'], 'unique', 'targetAttribute' => ['scroll_id', 'printing_method_id']],
            [['printing_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrintingMethod::className(), 'targetAttribute' => ['printing_method_id' => 'id']],
            [['scroll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Scroll::className(), 'targetAttribute' => ['scroll_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'scroll_id' => 'Scroll ID',
            'printing_method_id' => 'Printing Method ID',
        ];
    }

    /**
     * Gets query for [[PrintingMethod]].
     *
     * @return \yii\db\ActiveQuery|PrintingMethodQuery
     */
    public function getPrintingMethod ()
    {
        return $this->hasOne(PrintingMethod::className(), ['id' => 'printing_method_id']);
    }

    /**
     * Gets query for [[Scroll]].
     *
     * @return \yii\db\ActiveQuery|ScrollQuery
     */
    public function getScroll ()
    {
        return $this->hasOne(Scroll::className(), ['id' => 'scroll_id']);
    }

    /**
     * {@inheritdoc}
     * @return ScrollPrintingMethodQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new ScrollPrintingMethodQuery(get_called_class());
    }

}
