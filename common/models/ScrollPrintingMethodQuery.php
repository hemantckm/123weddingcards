<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ScrollPrintingMethod]].
 *
 * @see ScrollPrintingMethod
 */
class ScrollPrintingMethodQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ScrollPrintingMethod[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ScrollPrintingMethod|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
