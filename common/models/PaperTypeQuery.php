<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PaperType]].
 *
 * @see PaperType
 */
class PaperTypeQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return PaperType[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PaperType|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get Paper Type List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, name')
                ->asArray()
                ->where(['status' => PaperType::STATUS_ACTIVE])
                ->orderBy('name asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'name');
    }

}
