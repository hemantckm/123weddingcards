<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "box_paper".
 *
 * @property int $box_id
 * @property int $paper_id
 *
 * @property Box $box
 * @property Paper $paper
 */
class BoxPaper extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'box_paper';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['box_id', 'paper_id'], 'required'],
            [['box_id', 'paper_id'], 'integer'],
            [['box_id', 'paper_id'], 'unique', 'targetAttribute' => ['box_id', 'paper_id']],
            [['box_id'], 'exist', 'skipOnError' => true, 'targetClass' => Box::className(), 'targetAttribute' => ['box_id' => 'id']],
            [['paper_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paper::className(), 'targetAttribute' => ['paper_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'box_id' => 'Box ID',
            'paper_id' => 'Paper ID',
        ];
    }

    /**
     * Gets query for [[Box]].
     *
     * @return \yii\db\ActiveQuery|BoxQuery
     */
    public function getBox ()
    {
        return $this->hasOne(Box::className(), ['id' => 'box_id']);
    }

    /**
     * Gets query for [[Paper]].
     *
     * @return \yii\db\ActiveQuery|PaperQuery
     */
    public function getPaper ()
    {
        return $this->hasOne(Paper::className(), ['id' => 'paper_id']);
    }

    /**
     * {@inheritdoc}
     * @return BoxPaperQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new BoxPaperQuery(get_called_class());
    }

}
