<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "addon_folder_paper".
 *
 * @property int $addon_folder_id
 * @property int $paper_id
 *
 * @property AddonFolder $addonFolder
 * @property Paper $paper
 */
class AddonFolderPaper extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'addon_folder_paper';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['addon_folder_id', 'paper_id'], 'required'],
            [['addon_folder_id', 'paper_id'], 'integer'],
            [['addon_folder_id', 'paper_id'], 'unique', 'targetAttribute' => ['addon_folder_id', 'paper_id']],
            [['addon_folder_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddonFolder::className(), 'targetAttribute' => ['addon_folder_id' => 'id']],
            [['paper_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paper::className(), 'targetAttribute' => ['paper_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'addon_folder_id' => 'Addon Folder ID',
            'paper_id' => 'Paper ID',
        ];
    }

    /**
     * Gets query for [[AddonFolder]].
     *
     * @return \yii\db\ActiveQuery|AddonFolderQuery
     */
    public function getAddonFolder ()
    {
        return $this->hasOne(AddonFolder::className(), ['id' => 'addon_folder_id']);
    }

    /**
     * Gets query for [[Paper]].
     *
     * @return \yii\db\ActiveQuery|PaperQuery
     */
    public function getPaper ()
    {
        return $this->hasOne(Paper::className(), ['id' => 'paper_id']);
    }

    /**
     * {@inheritdoc}
     * @return AddonFolderPaperQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AddonFolderPaperQuery(get_called_class());
    }

}
