<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "insert".
 *
 * @property int $id
 * @property int|null $card_id
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Card $card
 */
class Insert extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * Insert Paper Function Define.
     * @var type 
     */
    public $insert_paper;

    /**
     * Insert Die Function Define.
     * @var type 
     */
    public $insert_die;

    /**
     * Insert Block Function Define.
     * @var type 
     */
    public $insert_block;

    /**
     * Insert Printing Method Function Define.
     * @var type 
     */
    public $insert_printing_method;

    /**
     * Paper Color Function Define.
     * @var type 
     */
    public $paper_color;

    /**
     * Paper Type Function Define.
     * @var type 
     */
    public $paper_type;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'insert';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['card_id'/*, 'insert_paper', 'insert_die', 'insert_block', 'insert_printing_method'*/], 'required'],
            [['card_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['card_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'card_id' => 'Card',
            'insert_paper' => 'Paper',
            'insert_die' => 'Die',
            'insert_block' => 'Block',
            'insert_printing_method' => 'Printing Method',
            'paper_color' => 'Paper Color',
            'paper_type' => 'Paper Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[Card]].
     *
     * @return \yii\db\ActiveQuery|CardQuery
     */
    public function getCard ()
    {
        return $this->hasOne(Card::className(), ['id' => 'card_id']);
    }

    /**
     * {@inheritdoc}
     * @return InsertQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new InsertQuery(get_called_class());
    }

}
