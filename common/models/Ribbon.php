<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "ribbon".
 *
 * @property int $id
 * @property string $code
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Ribbon extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'ribbon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['code'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['code'], 'string', 'max' => 50],
            [['code'], 'filter', 'filter' => 'trim'],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * {@inheritdoc}
     * @return RibbonQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new RibbonQuery(get_called_class());
    }

}
