<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[QuestionAnswerFaq]].
 *
 * @see QuestionAnswerFaq
 */
class QuestionAnswerFaqQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return QuestionAnswerFaq[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return QuestionAnswerFaq|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
