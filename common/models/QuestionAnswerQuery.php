<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[QuestionAnswer]].
 *
 * @see QuestionAnswer
 */
class QuestionAnswerQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return QuestionAnswer[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return QuestionAnswer|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get QuestionAnswer List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, question')
                ->asArray()
                ->where(['status' => QuestionAnswer::STATUS_ACTIVE])
                ->orderBy('question asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'question');
    }

}
