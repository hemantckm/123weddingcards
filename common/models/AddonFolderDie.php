<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "addon_folder_die".
 *
 * @property int $addon_folder_id
 * @property int $die_id
 *
 * @property AddonFolder $addonFolder
 * @property DieCut $die
 */
class AddonFolderDie extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'addon_folder_die';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['addon_folder_id', 'die_id'], 'required'],
            [['addon_folder_id', 'die_id'], 'integer'],
            [['addon_folder_id', 'die_id'], 'unique', 'targetAttribute' => ['addon_folder_id', 'die_id']],
            [['addon_folder_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddonFolder::className(), 'targetAttribute' => ['addon_folder_id' => 'id']],
            [['die_id'], 'exist', 'skipOnError' => true, 'targetClass' => DieCut::className(), 'targetAttribute' => ['die_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'addon_folder_id' => 'Addon Folder ID',
            'die_id' => 'Die ID',
        ];
    }

    /**
     * Gets query for [[AddonFolder]].
     *
     * @return \yii\db\ActiveQuery|AddonFolderQuery
     */
    public function getAddonFolder ()
    {
        return $this->hasOne(AddonFolder::className(), ['id' => 'addon_folder_id']);
    }

    /**
     * Gets query for [[Die]].
     *
     * @return \yii\db\ActiveQuery|DieCutQuery
     */
    public function getDie ()
    {
        return $this->hasOne(DieCut::className(), ['id' => 'die_id']);
    }

    /**
     * {@inheritdoc}
     * @return AddonFolderDieQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new AddonFolderDieQuery(get_called_class());
    }

}
