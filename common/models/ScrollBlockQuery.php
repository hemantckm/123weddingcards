<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ScrollBlock]].
 *
 * @see ScrollBlock
 */
class ScrollBlockQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ScrollBlock[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ScrollBlock|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
