<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[BoxDie]].
 *
 * @see BoxDie
 */
class BoxDieQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return BoxDie[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return BoxDie|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
