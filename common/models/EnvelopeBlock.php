<?php

namespace common\models;

use common\components\ActiveRecord;

/**
 * This is the model class for table "envelope_block".
 *
 * @property int $envelope_id
 * @property int $block_id
 *
 * @property Block $block
 * @property Envelope $envelope
 */
class EnvelopeBlock extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'envelope_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['envelope_id', 'block_id'], 'required'],
            [['envelope_id', 'block_id'], 'integer'],
            [['envelope_id', 'block_id'], 'unique', 'targetAttribute' => ['envelope_id', 'block_id']],
            [['block_id'], 'exist', 'skipOnError' => true, 'targetClass' => Block::className(), 'targetAttribute' => ['block_id' => 'id']],
            [['envelope_id'], 'exist', 'skipOnError' => true, 'targetClass' => Envelope::className(), 'targetAttribute' => ['envelope_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'envelope_id' => 'Envelope ID',
            'block_id' => 'Block ID',
        ];
    }

    /**
     * Gets query for [[Block]].
     *
     * @return \yii\db\ActiveQuery|BlockQuery
     */
    public function getBlock ()
    {
        return $this->hasOne(Block::className(), ['id' => 'block_id']);
    }

    /**
     * Gets query for [[Envelope]].
     *
     * @return \yii\db\ActiveQuery|EnvelopeQuery
     */
    public function getEnvelope ()
    {
        return $this->hasOne(Envelope::className(), ['id' => 'envelope_id']);
    }

    /**
     * {@inheritdoc}
     * @return EnvelopeBlockQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new EnvelopeBlockQuery(get_called_class());
    }

}
