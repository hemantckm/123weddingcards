<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[AddonType]].
 *
 * @see AddonType
 */
class AddonTypeQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return AddonType[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AddonType|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

    /**
     * To Get Addon Type List
     * @return type
     */
    public function getList ()
    {
        $result = $this->select('id, name')
                ->asArray()
                ->where(['status' => AddonType::STATUS_ACTIVE])
                ->orderBy('name asc')
                ->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', 'name');
    }

}
