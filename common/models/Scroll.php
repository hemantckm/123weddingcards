<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "scroll".
 *
 * @property int $id
 * @property int|null $card_id
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Card $card
 * @property ScrollBlock[] $scrollBlocks
 * @property Block[] $blocks
 * @property ScrollDie[] $scrollDies
 * @property DieCut[] $dies
 * @property ScrollPaper[] $scrollPapers
 * @property Paper[] $papers
 * @property ScrollPrintingMethod[] $scrollPrintingMethods
 * @property PrintingMethod[] $printingMethods
 * @property Tube[] $tubes
 */
class Scroll extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * Scroll Paper Function Define.
     * @var type 
     */
    public $scroll_paper;

    /**
     * Scroll Die Function Define.
     * @var type 
     */
    public $scroll_die;

    /**
     * Scroll Block Function Define.
     * @var type 
     */
    public $scroll_block;

    /**
     * Scroll Printing Method Function Define.
     * @var type 
     */
    public $scroll_printing_method;

    /**
     * Paper Color Function Define.
     * @var type 
     */
    public $paper_color;

    /**
     * Paper Type Function Define.
     * @var type 
     */
    public $paper_type;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'scroll';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['card_id'/* , 'scroll_paper', 'scroll_die', 'scroll_block', 'scroll_printing_method' */], 'required'],
            [['card_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['card_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'card_id' => 'Card',
            'scroll_paper' => 'Paper',
            'scroll_die' => 'Die',
            'scroll_block' => 'Block',
            'scroll_printing_method' => 'Printing Method',
            'paper_color' => 'Paper Color',
            'paper_type' => 'Paper Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[Card]].
     *
     * @return \yii\db\ActiveQuery|CardQuery
     */
    public function getCard ()
    {
        return $this->hasOne(Card::className(), ['id' => 'card_id']);
    }

    /**
     * Gets query for [[ScrollBlocks]].
     *
     * @return \yii\db\ActiveQuery|ScrollBlockQuery
     */
    public function getScrollBlocks ()
    {
        return $this->hasMany(ScrollBlock::className(), ['scroll_id' => 'id']);
    }

    /**
     * Gets query for [[Blocks]].
     *
     * @return \yii\db\ActiveQuery|BlockQuery
     */
    public function getBlocks ()
    {
        return $this->hasMany(Block::className(), ['id' => 'block_id'])->viaTable('scroll_block', ['scroll_id' => 'id']);
    }

    /**
     * Gets query for [[ScrollDies]].
     *
     * @return \yii\db\ActiveQuery|ScrollDieQuery
     */
    public function getScrollDies ()
    {
        return $this->hasMany(ScrollDie::className(), ['scroll_id' => 'id']);
    }

    /**
     * Gets query for [[Dies]].
     *
     * @return \yii\db\ActiveQuery|DieCutQuery
     */
    public function getDies ()
    {
        return $this->hasMany(DieCut::className(), ['id' => 'die_id'])->viaTable('scroll_die', ['scroll_id' => 'id']);
    }

    /**
     * Gets query for [[ScrollPapers]].
     *
     * @return \yii\db\ActiveQuery|ScrollPaperQuery
     */
    public function getScrollPapers ()
    {
        return $this->hasMany(ScrollPaper::className(), ['scroll_id' => 'id']);
    }

    /**
     * Gets query for [[Papers]].
     *
     * @return \yii\db\ActiveQuery|PaperQuery
     */
    public function getPapers ()
    {
        return $this->hasMany(Paper::className(), ['id' => 'paper_id'])->viaTable('scroll_paper', ['scroll_id' => 'id']);
    }

    /**
     * Gets query for [[ScrollPrintingMethods]].
     *
     * @return \yii\db\ActiveQuery|ScrollPrintingMethodQuery
     */
    public function getScrollPrintingMethods ()
    {
        return $this->hasMany(ScrollPrintingMethod::className(), ['scroll_id' => 'id']);
    }

    /**
     * Gets query for [[PrintingMethods]].
     *
     * @return \yii\db\ActiveQuery|PrintingMethodQuery
     */
    public function getPrintingMethods ()
    {
        return $this->hasMany(PrintingMethod::className(), ['id' => 'printing_method_id'])->viaTable('scroll_printing_method', ['scroll_id' => 'id']);
    }

    /**
     * Gets query for [[Tubes]].
     *
     * @return \yii\db\ActiveQuery|TubeQuery
     */
    public function getTubes ()
    {
        return $this->hasMany(Tube::className(), ['scroll_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ScrollQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new ScrollQuery(get_called_class());
    }

}
