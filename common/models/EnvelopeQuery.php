<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Envelope]].
 *
 * @see Envelope
 */
class EnvelopeQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Envelope[]|array
     */
    public function all ($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Envelope|array|null
     */
    public function one ($db = null)
    {
        return parent::one($db);
    }

}
