<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "question_answer_faq".
 *
 * @property int $id
 * @property int|null $question_answer_id
 * @property int|null $faq_id
 * @property int|null $faq_type_id
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Faq $faq
 * @property FaqType $faqType
 * @property QuestionAnswer $questionAnswer
 */
class QuestionAnswerFaq extends ActiveRecord
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName ()
    {
        return 'question_answer_faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['question_answer_id', 'faq_id', 'faq_type_id'], 'required'],
            [['question_answer_id', 'faq_id', 'faq_type_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['faq_id'], 'exist', 'skipOnError' => true, 'targetClass' => Faq::className(), 'targetAttribute' => ['faq_id' => 'id']],
            [['faq_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => FaqType::className(), 'targetAttribute' => ['faq_type_id' => 'id']],
            [['question_answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionAnswer::className(), 'targetAttribute' => ['question_answer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels ()
    {
        return [
            'id' => 'ID',
            'question_answer_id' => 'Question',
            'faq_id' => 'Faq',
            'faq_type_id' => 'Faq Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * History Management For This Model.
     * @return type
     */
    public function behaviors ()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            'LoggableBehavior' => [
                'class' => 'sammaye\audittrail\LoggableBehavior',
                'ignored' => ['created_at', 'updated_at']
            ]
        ];
    }

    /**
     * Gets query for [[Faq]].
     *
     * @return \yii\db\ActiveQuery|FaqQuery
     */
    public function getFaq ()
    {
        return $this->hasOne(Faq::className(), ['id' => 'faq_id']);
    }

    /**
     * Gets query for [[FaqType]].
     *
     * @return \yii\db\ActiveQuery|FaqTypeQuery
     */
    public function getFaqType ()
    {
        return $this->hasOne(FaqType::className(), ['id' => 'faq_type_id']);
    }

    /**
     * Gets query for [[QuestionAnswer]].
     *
     * @return \yii\db\ActiveQuery|QuestionAnswerQuery
     */
    public function getQuestionAnswer ()
    {
        return $this->hasOne(QuestionAnswer::className(), ['id' => 'question_answer_id']);
    }

    /**
     * {@inheritdoc}
     * @return QuestionAnswerFaqQuery the active query used by this AR class.
     */
    public static function find ()
    {
        return new QuestionAnswerFaqQuery(get_called_class());
    }

}
