<?php

namespace backend\helpers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Html
 *
 * @author irc123
 */
class Html extends \yii\helpers\Html
{

    public static function getEditButton ($url)
    {
        return parent::a("<span class='glyphicon glyphicon-pencil'></span>", $url, ['title' => 'Update', 'class' => 'btn btn-primary margin-r-5']);
    }

    public static function getEditTargetBlankButton ($url)
    {
        return parent::a("<span class='glyphicon glyphicon-pencil'></span>", $url, ['title' => 'Update', 'target' => '_blank', 'class' => 'btn btn-primary margin-r-5']);
    }

    public static function getViewTargetBlankButton ($url)
    {
        return parent::a("<span class='glyphicon glyphicon-eye-open'></span>", $url, ['title' => 'View', 'target' => '_blank', 'class' => 'btn btn-info margin-r-5']);
    }

    public static function getViewButton ($url)
    {
        return parent::a("<span class='glyphicon glyphicon-eye-open'></span>", $url, ['title' => 'View', 'class' => 'btn btn-info margin-r-5']);
    }

    public static function getDeleteButton ($url)
    {
        return parent::a("<span class='glyphicon glyphicon-trash'></span>", $url, ['title' => 'Delete', 'aria-label' => 'Delete', 'data-pjax' => '0', 'data-confirm' => "Are you sure you want to delete this item?", 'data-method' => "post", 'class' => 'btn btn-danger margin-r-5']);
    }

    public static function getActiveButton ($url)
    {
        return parent::a("<span class='glyphicon glyphicon-ok'></span>", $url, ['title' => 'Active', 'aria-label' => 'Delete', 'data-pjax' => '0', 'data-confirm' => "Are you sure you want to active this item?", 'data-method' => "post", 'class' => 'btn btn-success margin-r-5']);
    }

    public static function getPrintButton ($url)
    {
        return parent::a("<span class='glyphicon glyphicon-print'></span>", $url, ['title' => 'Print', 'target' => '_blank', 'class' => 'btn btn-default margin-r-5']);
    }

    public static function getPDFButton ($url)
    {
        return parent::a("<span class='glyphicon glyphicon-download-alt'></span>", $url, ['title' => 'PDF', 'target' => '_blank', 'class' => 'btn btn-default margin-r-5']);
    }

    public static function getPaymentButton ($url)
    {
        return parent::a("<span class='glyphicon glyphicon-credit-card'></span>", $url, ['title' => 'Invoice Payment', 'target' => '_blank', 'class' => 'btn btn-success margin-r-5']);
    }

    public static function addNewLink ($url)
    {
        return parent::a("Add New", $url, ['title' => 'Add New', 'class' => 'labelRight', 'target' => '_blank']);
    }

    public static function refreshLink ()
    {
        return '<i class="fa fa-refresh refreshData"></i>';
    }

    public static function customLabel ($label, $addNew)
    {
        return '<div><span class="labelLeft">' . $label . '</span>' . self::refreshLink() . self::addNewLink($addNew) . '</div>';
    }

    public static function getStoreUpdateButton ($url)
    {
        return parent::a("<span class='glyphicon glyphicon-remove'></span>", $url, ['title' => 'Store Update', 'class' => 'btn btn-danger margin-r-5', 'target' => '_blank']);
    }

    /**
     * For Download Single Item.
     */
    public static function getDownloadButton ($url, $downloadImageName)
    {
        return parent::a("<span class='glyphicon glyphicon-download'></span>", $url, ['title' => 'Download', 'class' => 'btn btn-danger', 'style' => 'margin-top:5px;', 'download' => $downloadImageName]);
    }

    /**
     * For Download All Items.
     */
    public static function getDownloadAllButton ($downloadAllImages)
    {
        return parent::a("<span class='glyphicon glyphicon-download'></span>", "javascript:void(0);", ['title' => 'Download All', 'class' => 'btn btn-danger downloadAllImages', 'downloadAllImages' => json_encode($downloadAllImages, TRUE)]);
    }

}
