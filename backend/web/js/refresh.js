$(document).ready(function () {
    $(".refreshData").click(function () {
        var select = $(this).closest(".form-group").find('select');
        var stateId = 0;
        var url = select.attr("data-ajaxUrl");
        //$("#loader").show();
        $.ajax({
            url: url,
            data: {id: stateId},
            type: "POST",
            datatype: "jsonp",
            success: function (result) {
                //$("#loader").hide();
                select.empty();
                $.each(result, function (key, value) {
                    if (key > 0) {
                        select.append('<option value="' + key + '"> ' + value + '</option>');
                    }
                });
            }
        });
    });

    /**
     * Refresh The Product Movement Index Page Form.
     */
    $(".productMovementSearch").click(function () {
        var page = $(this).attr('data-page');
        window.location.href = window.location.origin + window.location.pathname + '?r=product-movement/' + page;
    });
});

