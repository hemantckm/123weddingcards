$(document).ready(function () {
    // Common Modules Which Have Checkbox for The Index Page of Datatable Searching 
    var table = $("#example").DataTable({
        "processing": true,
        "serverSide": true,
        "lengthMenu": [[100, 50, 20, -1], [100, 50, 20, "All"]],
        "ajax": {
            "url": $("#example").attr("data-ajaxUrl"),
            "type": "POST"
        },
        "deferRender": true,
        'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '<input type="checkbox" name="checkbox_id[]" value="'
                            + full[0] + '">';
                }
            }],
    });
    // Handle click on "Select all" control
    $('#example-select-all').on('click', function () {
        // Check/uncheck all checkboxes in the table
        var rows = table.rows({'search': 'applied'}).nodes();
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });
    // Handle click on checkbox to set state of "Select all" control
    $('#example tbody').on('change', 'input[type="checkbox"]', function () {
        // If checkbox is not checked
        if (!this.checked) {
            var el = $('#example-select-all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if (el && el.checked && ('indeterminate' in el)) {
                // Set visual state of "Select all" control 
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });
});