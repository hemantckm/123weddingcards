/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    /**
     * Add Multipel Contact-Us Details of DynamicForm.
     */
    $(".contactUsDetailsForm_dynamicform_wrapper").on("afterInsert", function (e, item) {
        $(".contactUsDetailsForm_dynamicform_wrapper .panel-title-insert").each(function (index) {
            $(this).html("Detail: " + (index + 1))
        });
    });

    $(".contactUsDetailsForm_dynamicform_wrapper").on("afterDelete", function (e) {
        $(".contactUsDetailsForm_dynamicform_wrapper .panel-title-insert").each(function (index) {
            $(this).html("Detail: " + (index + 1))
        });
    });
});

