/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    /*================================ Addon Envelope Section ============================*/
    /**
     * Fetch The Selected Addon Envelope Paper Code in Update Case.
     * @type jQuery
     */
    //var selected_addon_envelope_paper_array = $("#addonenvelope-addon_envelope_paper").find(":selected");
    var selected_addon_envelope_paper_array = $("#addonenvelope-addon_envelope_paper").select2().val();
    if (typeof selected_addon_envelope_paper_array !== 'undefined' && selected_addon_envelope_paper_array.length > 0) {
        addon_envelope(selected_addon_envelope_paper_array);
    } else {
        $('#addonenvelope-paper_color').val(null).trigger('change'); //Clean Addon Envelope Paper Color.
        $('#addonenvelope-paper_type').val(null).trigger('change'); //Clean Addon Envelope Paper Type.
    }
    $(document).on('change', '#addonenvelope-addon_envelope_paper', function () {
        var addon_envelope_paper_array = $(this).select2('val');
        if (typeof addon_envelope_paper_array !== 'undefined' && addon_envelope_paper_array.length > 0) {
            addon_envelope(addon_envelope_paper_array);
        } else {
            $('#addonenvelope-paper_color').val(null).trigger('change'); //Clean Addon Envelope Paper Color.
            $('#addonenvelope-paper_type').val(null).trigger('change'); //Clean Addon Envelope Paper Type.
        }
    });

    /**
     * Gelling the all information about the selected Addon Envelope PaperCode.
     * @param {type} addon_envelope_papers
     * @returns {undefined}
     */
    function addon_envelope(addon_envelope_papers) {
        if (typeof addon_envelope_papers !== 'undefined' && addon_envelope_papers.length > 0) {
            $('#addonenvelope-paper_color').val(null).trigger('change'); //Clean Addon Envelope Paper Color.
            $('#addonenvelope-paper_type').val(null).trigger('change'); //Clean Addon Envelope Paper Type.
            $.each(addon_envelope_papers, function (key, addon_envelope_paper) {
                $.ajax({
                    dataType: "json",
                    contentType: "application/json",
                    url: addonUrls.addonPapers,
                    cache: false,
                    data: {addon_paper: addon_envelope_paper},
                    success: function (data) {
                        //$("#loader").hide();
                        // Create a DOM Option and pre-select PaperColor by default
                        var addon_envelope_paper_color = new Option(data.color_name, data.color_id, true, true);
                        // Append it to the select
                        $('#addonenvelope-paper_color').append(addon_envelope_paper_color).trigger('change');

                        // Create a DOM Option and pre-select PaperType by default
                        var addon_envelope_paper_type = new Option(data.paper_type_name, data.color_id, true, true);
                        // Append it to the select
                        $('#addonenvelope-paper_type').append(addon_envelope_paper_type).trigger('change');
                    }
                });
            });
        } else {
            $('#addonenvelope-paper_color').val(null).trigger('change'); //Clean Addon Envelope Paper Color.
            $('#addonenvelope-paper_type').val(null).trigger('change'); //Clean Addon Envelope Paper Type.
        }
    }

    /*================================ Addon Folder Section ============================*/
    /**
     * Fetch The Selected Addon Folder Paper Code in Update Case.
     * @type jQuery
     */
    //var selected_folder_paper_array = $("#addonfolder-addon_folder_paper").find(":selected").val();
    var selected_addon_folder_paper_array = $("#addonfolder-addon_folder_paper").select2().val();
    if (typeof selected_addon_folder_paper_array !== 'undefined' && selected_addon_folder_paper_array.length > 0) {
        addon_folder(selected_addon_folder_paper_array);
    } else {
        $('#addonfolder-paper_color').val(null).trigger('change'); //Clean Addon Folder Paper Color.
        $('#addonfolder-paper_type').val(null).trigger('change'); //Clean Addon Folder Paper Type.
    }
    $(document).on('change', '#addonfolder-addon_folder_paper', function () {
        var addon_folder_paper_array = $(this).select2('val');
        if (typeof addon_folder_paper_array !== 'undefined' && addon_folder_paper_array.length > 0) {
            addon_folder(addon_folder_paper_array);
        } else {
            $('#addonfolder-paper_color').val(null).trigger('change'); //Clean Addon Folder Paper Color.
            $('#addonfolder-paper_type').val(null).trigger('change'); //Clean Addon Folder Paper Type.
        }
    });

    /**
     * Gelling the all information about the selected Addon Folder PaperCode.
     * @param {type} addon_folder_papers
     * @returns {undefined}
     */
    function addon_folder(addon_folder_papers) {
        if (typeof addon_folder_papers !== 'undefined' && addon_folder_papers.length > 0) {
            $('#addonfolder-paper_color').val(null).trigger('change'); //Clean Addon Folder Paper Color.
            $('#addonfolder-paper_type').val(null).trigger('change'); //Clean Addon Folder Paper Type.
            $.each(addon_folder_papers, function (key, addon_folder_paper) {
                //$("#loader").show();
                $.ajax({
                    dataType: "json",
                    contentType: "application/json",
                    url: addonUrls.addonPapers,
                    cache: false,
                    data: {addon_paper: addon_folder_paper},
                    success: function (data) {
                        //$("#loader").hide();
                        // Create a DOM Option and pre-select PaperColor by default
                        var addon_folder_paper_color = new Option(data.color_name, data.color_id, true, true);
                        // Append it to the select
                        $('#addonfolder-paper_color').append(addon_folder_paper_color).trigger('change');

                        // Create a DOM Option and pre-select PaperType by default
                        var addon_folder_paper_type = new Option(data.paper_type_name, data.color_id, true, true);
                        // Append it to the select
                        $('#addonfolder-paper_type').append(addon_folder_paper_type).trigger('change');
                    }
                });
            });
        } else {
            $('#addonfolder-paper_color').val(null).trigger('change'); //Clean Addon Folder Paper Color.
            $('#addonfolder-paper_type').val(null).trigger('change'); //Clean Addon Folder Paper Type.
        }
    }
});

