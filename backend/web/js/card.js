/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    /**
     * Add Multipel Inserts of DynamicForm.
     */
    $(".cardInsertForm_dynamicform_wrapper").on("afterInsert", function (e, item) {
        $(".cardInsertForm_dynamicform_wrapper .panel-title-insert").each(function (index) {
            $(this).html("Insert: " + (index + 1))
        });
    });

    $(".cardInsertForm_dynamicform_wrapper").on("afterDelete", function (e) {
        $(".cardInsertForm_dynamicform_wrapper .panel-title-insert").each(function (index) {
            $(this).html("Insert: " + (index + 1))
        });
    });

    /*================================ Card Theme Field Section ============================*/
    /**
     * Fetch The Selected Card Theme in Update Case.
     * @type jQuery
     */
    var selected_card_theme_array = $("#card-card_theme").select2().val();
    if (typeof selected_card_theme_array !== 'undefined' && selected_card_theme_array.length > 0) {
        card_theme(selected_card_theme_array);
    } else {
        $(".scrollSections").hide();
    }
    $(document).on('change', '#card-card_theme', function () {
        var card_theme_array = $(this).select2('val');
        if (typeof card_theme_array !== 'undefined' && card_theme_array.length > 0) {
            card_theme(card_theme_array);
        } else {
            $(".scrollSections").hide();
        }
    });

    /**
     * Getting and Check Card Theme have Scroll or Not.
     * @param {type} card_themes
     * @returns {undefined}
     */
    function card_theme(card_themes) {
        if (typeof card_themes !== 'undefined' && card_themes.length > 0) {
            $.ajax({
                type: "POST",
                url: cardUrls.cardThemes,
                cache: false,
                data: {card_theme: card_themes},
                success: function (data) {
                    if (data.indexOf('Scroll') != -1) {
                        $(".scrollSections").show();
                    } else {
                        $(".scrollSections").hide();
                    }
                }
            });
        } else {
            $(".scrollSections").hide();
        }
    }

    /*================================ Card Envelope Section ============================*/
    /**
     * Fetch The Selected Envelope Paper Code in Update Case.
     * @type jQuery
     */
    //var selected_envelope_paper_array = $("#envelope-envelope_paper").find(":selected");
    var selected_envelope_paper_array = $("#envelope-envelope_paper").select2().val();
    if (typeof selected_envelope_paper_array !== 'undefined' && selected_envelope_paper_array.length > 0) {
        card_envelope(selected_envelope_paper_array);
    } else {
        $('#envelope-paper_color').val(null).trigger('change'); //Clean Envelope Paper Color.
        $('#envelope-paper_type').val(null).trigger('change'); //Clean Envelope Paper Type.
    }
    $(document).on('change', '#envelope-envelope_paper', function () {
        var envelope_paper_array = $(this).select2('val');
        if (typeof envelope_paper_array !== 'undefined' && envelope_paper_array.length > 0) {
            $('#card_envelope_info').prop('disabled', false); //Card Envelope Copy Button Disabled False.
            card_envelope(envelope_paper_array);
        } else {
            $('#card_envelope_info').bootstrapToggle('off', true); //Card Envelope Copy Button Value Set Off.
            $('#card_envelope_info').prop('disabled', true); //Card Envelope Copy Button Disabled True.
            $('#envelope-paper_color').val(null).trigger('change'); //Clean Envelope Paper Color.
            $('#envelope-paper_type').val(null).trigger('change'); //Clean Envelope Paper Type.
        }
    });

    /**
     * Gelling the all information about the selected Envelope PaperCode.
     * @param {type} envelope_papers
     * @returns {undefined}
     */
    function card_envelope(envelope_papers) {
        if (typeof envelope_papers !== 'undefined' && envelope_papers.length > 0) {
            $('#envelope-paper_color').val(null).trigger('change'); //Clean Envelope Paper Color.
            $('#envelope-paper_type').val(null).trigger('change'); //Clean Envelope Paper Type.
            $.each(envelope_papers, function (key, envelope_paper) {
                //$("#loader").show();
                $.ajax({
                    dataType: "json",
                    contentType: "application/json",
                    url: cardUrls.cardPapers,
                    cache: false,
                    data: {card_paper: envelope_paper},
                    success: function (data) {
                        //$("#loader").hide();
                        // Create a DOM Option and pre-select PaperColor by default
                        var envelope_paper_color = new Option(data.color_name, data.color_id, true, true);
                        // Append it to the select
                        $('#envelope-paper_color').append(envelope_paper_color).trigger('change');

                        // Create a DOM Option and pre-select PaperType by default
                        var envelope_paper_type = new Option(data.paper_type_name, data.color_id, true, true);
                        // Append it to the select
                        $('#envelope-paper_type').append(envelope_paper_type).trigger('change');
                    }
                });
            });
        } else {
            $('#envelope-paper_color').val(null).trigger('change'); //Clean Envelope Paper Color.
            $('#envelope-paper_type').val(null).trigger('change'); //Clean Envelope Paper Type.
        }
    }

    /**
     * Card Envelope Data Copy For Other Sections.
     */
    $(document).on("change", '#card_envelope_info', function () {
        if ($(this).is(":checked")) {
            var envelope_paper_array = $('#envelope-envelope_paper').select2('val'); //Getting Card Envelope Papers.
            if (typeof envelope_paper_array !== 'undefined' && envelope_paper_array.length > 0) {
                $('#folder-folder_paper').val(envelope_paper_array).trigger("change"); //Set Folder Paper Code.
                $('#scroll-scroll_paper').val(envelope_paper_array).trigger("change"); //Set Scroll Paper Code.
                $('#box-box_paper').val(envelope_paper_array).trigger("change"); //Set Box Paper Code.
                $('#insert-0-insert_paper').val(envelope_paper_array).trigger("change"); //Set Insert-1 Paper Code.
            } else {
                $('#folder-folder_paper').val(null).trigger("change"); //Clean Folder Paper Code.
                $('#scroll-scroll_paper').val(null).trigger("change"); //Clear Scroll Paper Code.
                $('#box-box_paper').val(null).trigger("change"); //Clear Box Paper Code.
                $('#insert-0-insert_paper').val(null).trigger("change"); //Clear Insert-1 Paper Code.
            }
        } else {
            $('#folder-folder_paper').val(null).trigger("change"); //Clean Folder Paper Code.
            $('#scroll-scroll_paper').val(null).trigger("change"); //Clear Scroll Paper Code.
            $('#box-box_paper').val(null).trigger("change"); //Clear Box Paper Code.
            $('#insert-0-insert_paper').val(null).trigger("change"); //Clear Insert-1 Paper Code.
        }
    });

    /*================================ Card Folder Section ============================*/
    /**
     * Fetch The Selected Folder Paper Code in Update Case.
     * @type jQuery
     */
    //var selected_folder_paper_array = $("#folder-folder_paper").find(":selected").val();
    var selected_folder_paper_array = $("#folder-folder_paper").select2().val();
    if (typeof selected_folder_paper_array !== 'undefined' && selected_folder_paper_array.length > 0) {
        card_folder(selected_folder_paper_array);
    } else {
        $('#folder-paper_color').val(null).trigger('change'); //Clean Folder Paper Color.
        $('#folder-paper_type').val(null).trigger('change'); //Clean Folder Paper Type.
    }
    $(document).on('change', '#folder-folder_paper', function () {
        var folder_paper_array = $(this).select2('val');
        if (typeof folder_paper_array !== 'undefined' && folder_paper_array.length > 0) {
            card_folder(folder_paper_array);
        } else {
            $('#folder-paper_color').val(null).trigger('change'); //Clean Folder Paper Color.
            $('#folder-paper_type').val(null).trigger('change'); //Clean Folder Paper Type.
        }
    });

    /**
     * Gelling the all information about the selected Folder PaperCode.
     * @param {type} folder_papers
     * @returns {undefined}
     */
    function card_folder(folder_papers) {
        if (typeof folder_papers !== 'undefined' && folder_papers.length > 0) {
            $('#folder-paper_color').val(null).trigger('change'); //Clean Folder Paper Color.
            $('#folder-paper_type').val(null).trigger('change'); //Clean Folder Paper Type.
            $.each(folder_papers, function (key, folder_paper) {
                //$("#loader").show();
                $.ajax({
                    dataType: "json",
                    contentType: "application/json",
                    url: cardUrls.cardPapers,
                    cache: false,
                    data: {card_paper: folder_paper},
                    success: function (data) {
                        //$("#loader").hide();
                        // Create a DOM Option and pre-select PaperColor by default
                        var folder_paper_color = new Option(data.color_name, data.color_id, true, true);
                        // Append it to the select
                        $('#folder-paper_color').append(folder_paper_color).trigger('change');

                        // Create a DOM Option and pre-select PaperType by default
                        var folder_paper_type = new Option(data.paper_type_name, data.color_id, true, true);
                        // Append it to the select
                        $('#folder-paper_type').append(folder_paper_type).trigger('change');
                    }
                });
            });
        } else {
            $('#folder-paper_color').val(null).trigger('change'); //Clean Folder Paper Color.
            $('#folder-paper_type').val(null).trigger('change'); //Clean Folder Paper Type.
        }
    }

    /*================================ Card Scroll Section ============================*/
    /**
     * Fetch The Selected Scroll Paper Code in Update Case.
     * @type jQuery
     */
    //var selected_scroll_paper_array = $("#scroll-scroll_paper").find(":selected").val();
    var selected_scroll_paper_array = $("#scroll-scroll_paper").select2().val();
    if (typeof selected_scroll_paper_array !== 'undefined' && selected_scroll_paper_array.length > 0) {
        card_scroll(selected_scroll_paper_array);
    } else {
        $('#scroll-paper_color').val(null).trigger('change'); //Clean Scroll Paper Color.
        $('#scroll-paper_type').val(null).trigger('change'); //Clean Scroll Paper Type.
    }
    $(document).on('change', '#scroll-scroll_paper', function () {
        var scroll_paper_array = $(this).select2('val');
        if (typeof scroll_paper_array !== 'undefined' && scroll_paper_array.length > 0) {
            card_scroll(scroll_paper_array);
        } else {
            $('#scroll-paper_color').val(null).trigger('change'); //Clean Scroll Paper Color.
            $('#scroll-paper_type').val(null).trigger('change'); //Clean Scroll Paper Type.
        }
    });

    /**
     * Gelling the all information about the selected Scroll PaperCode.
     * @param {type} scroll_papers
     * @returns {undefined}
     */
    function card_scroll(scroll_papers) {
        if (typeof scroll_papers !== 'undefined' && scroll_papers.length > 0) {
            $('#scroll-paper_color').val(null).trigger('change'); //Clean Scroll Paper Color.
            $('#scroll-paper_type').val(null).trigger('change'); //Clean Scroll Paper Type.
            $.each(scroll_papers, function (key, scroll_paper) {
                //$("#loader").show();
                $.ajax({
                    dataType: "json",
                    contentType: "application/json",
                    url: cardUrls.cardPapers,
                    cache: false,
                    data: {card_paper: scroll_paper},
                    success: function (data) {
                        //$("#loader").hide();
                        // Create a DOM Option and pre-select PaperColor by default
                        var scroll_paper_color = new Option(data.color_name, data.color_id, true, true);
                        // Append it to the select
                        $('#scroll-paper_color').append(scroll_paper_color).trigger('change');

                        // Create a DOM Option and pre-select PaperType by default
                        var scroll_paper_type = new Option(data.paper_type_name, data.color_id, true, true);
                        // Append it to the select
                        $('#scroll-paper_type').append(scroll_paper_type).trigger('change');
                    }
                });
            });
        } else {
            $('#scroll-paper_color').val(null).trigger('change'); //Clean Scroll Paper Color.
            $('#scroll-paper_type').val(null).trigger('change'); //Clean Scroll Paper Type.
        }
    }

    /*================================ Card Box Section ============================*/
    /**
     * Fetch The Selected Box Paper Code in Update Case.
     * @type jQuery
     */
    //var selected_box_paper_array = $("#box-box_paper").find(":selected").val();
    var selected_box_paper_array = $("#box-box_paper").select2().val();
    if (typeof selected_box_paper_array !== 'undefined' && selected_box_paper_array.length > 0) {
        card_box(selected_box_paper_array);
    } else {
        $('#box-paper_color').val(null).trigger('change'); //Clean Box Paper Color.
        $('#box-paper_type').val(null).trigger('change'); //Clean Box Paper Type.
    }
    $(document).on('change', '#box-box_paper', function () {
        var box_paper_array = $(this).select2('val');
        if (typeof box_paper_array !== 'undefined' && box_paper_array.length > 0) {
            card_box(box_paper_array);
        } else {
            $('#box-paper_color').val(null).trigger('change'); //Clean Box Paper Color.
            $('#box-paper_type').val(null).trigger('change'); //Clean Box Paper Type.
        }
    });

    /**
     * Gelling the all information about the selected Box PaperCode.
     * @param {type} box_papers
     * @returns {undefined}
     */
    function card_box(box_papers) {
        if (typeof box_papers !== 'undefined' && box_papers.length > 0) {
            $('#box-paper_color').val(null).trigger('change'); //Clean Box Paper Color.
            $('#box-paper_type').val(null).trigger('change'); //Clean Box Paper Type.
            $.each(box_papers, function (key, box_paper) {
                //$("#loader").show();
                $.ajax({
                    dataType: "json",
                    contentType: "application/json",
                    url: cardUrls.cardPapers,
                    cache: false,
                    data: {card_paper: box_paper},
                    success: function (data) {
                        //$("#loader").hide();
                        // Create a DOM Option and pre-select PaperColor by default
                        var box_paper_color = new Option(data.color_name, data.color_id, true, true);
                        // Append it to the select
                        $('#box-paper_color').append(box_paper_color).trigger('change');

                        // Create a DOM Option and pre-select PaperType by default
                        var box_paper_type = new Option(data.paper_type_name, data.color_id, true, true);
                        // Append it to the select
                        $('#box-paper_type').append(box_paper_type).trigger('change');
                    }
                });
            });
        } else {
            $('#box-paper_color').val(null).trigger('change'); //Clean Box Paper Color.
            $('#box-paper_type').val(null).trigger('change'); //Clean Box Paper Type.
        }
    }

    /*================================ Card Inserts Section ============================*/
    /**
     * Fetch The Selected Insert Paper Code in Update Case.
     * @type jQuery
     */
    var selected_insert_paper_array = $(".insert_paper_code").length;
    if (typeof selected_insert_paper_array !== 'undefined' && selected_insert_paper_array > 0) {
        for (var i = 0; i < selected_insert_paper_array; i++) {
            var selected_insert_paper_array1 = $('#insert-' + i + '-insert_paper').select2().val();
            if (typeof selected_insert_paper_array1 !== 'undefined' && selected_insert_paper_array1.length > 0) {
                card_insert(selected_insert_paper_array1, i);
            } else {
                $('#insert-' + i + '-paper_color').val(null).trigger('change'); //Clean Insert Paper Color.
                $('#insert-' + i + '-paper_type').val(null).trigger('change'); //Clean Insert Paper Type.
            }
        }
    }

    $(document).on('change', '.insert_paper_code', function () {
        var insert_paper_index = $(this).attr('id').match(/(\d+)/);
        if ($.isNumeric(insert_paper_index[0])) {
            var insert_paper_array = $('#insert-' + insert_paper_index[0] + '-insert_paper').select2('val');
            if (typeof insert_paper_array !== 'undefined' && insert_paper_array.length > 0) {
                card_insert(insert_paper_array, insert_paper_index[0]);
            } else {
                $('#insert-' + insert_paper_index[0] + '-paper_color').val(null).trigger('change'); //Clean Insert Paper Color.
                $('#insert-' + insert_paper_index[0] + '-paper_type').val(null).trigger('change'); //Clean Insert Paper Type.
            }
        }
    });

    /**
     * Gelling the all information about the selected Insert PaperCode.
     * @param {type} insert_papers
     * @returns {undefined}
     */
    function card_insert(insert_papers, insert_index) {
        if (typeof insert_papers !== 'undefined' && insert_papers.length > 0) {
            $('#insert-' + insert_index + '-paper_color').val(null).trigger('change'); //Clean Insert Paper Color.
            $('#insert-' + insert_index + '-paper_type').val(null).trigger('change'); //Clean Insert Paper Type.
            $.each(insert_papers, function (key, insert_paper) {
                //$("#loader").show();
                $.ajax({
                    dataType: "json",
                    contentType: "application/json",
                    url: cardUrls.cardPapers,
                    cache: false,
                    data: {card_paper: insert_paper},
                    success: function (data) {
                        //$("#loader").hide();
                        // Create a DOM Option and pre-select PaperColor by default
                        var insert_paper_color = new Option(data.color_name, data.color_id, true, true);
                        // Append it to the select
                        $('#insert-' + insert_index + '-paper_color').append(insert_paper_color).trigger('change');

                        // Create a DOM Option and pre-select PaperType by default
                        var insert_paper_type = new Option(data.paper_type_name, data.color_id, true, true);
                        // Append it to the select
                        $('#insert-' + insert_index + '-paper_type').append(insert_paper_type).trigger('change');
                    }
                });
            });
        } else {
            $('#insert-' + insert_index + '-paper_color').val(null).trigger('change'); //Clean Insert Paper Color.
            $('#insert-' + insert_index + '-paper_type').val(null).trigger('change'); //Clean Insert Paper Type.
        }
    }

});

