<?php

namespace backend\controllers;

use Yii;
use common\models\Invite;
use backend\models\InviteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use backend\helpers\Html;

/**
 * InviteController implements the CRUD actions for Invite model.
 */
class InviteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Invite models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new InviteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new Invite();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\Invite::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single Invite model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        $model           = $this->findModel($id);
        $images          = array ();
        $time            = '?r=' . time();
        $images['small'] = \common\models\FaqType::NO_IMAGE_SMALL . $time;
        $images['full']  = \common\models\FaqType::NO_IMAGE_FULL . $time;
        if ( ! empty($model->image)) {
            $originalFilePath = '/' . Invite::MAIN_DIRECTORY . Invite::ORIGINAL_FILEPATH . $model->image;
            $thumbFilePath    = '/' . Invite::MAIN_DIRECTORY . Invite::THUMBNAIL_FILEPATH . $model->image;
            if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                $images['small'] = $thumbFilePath . $time;
                $images['full']  = $originalFilePath . $time;
            }
        }
        $image = Html::a(Html::img(Yii::getAlias('@web') . $images['small'], ['class' => 'img-thumbnail', 'style' => 'margin-bottom:10px']), Yii::getAlias('@web') . $images['full'], ['data-fancybox' => 'gallery']);

        return $this->render('view', [
                    'model' => $model,
                    'image' => $image,
        ]);
    }

    /**
     * Creates a new Invite model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model = new Invite();

        if ($model->load(Yii::$app->request->post())) {
            /**
             * Invite Image Upload.
             */
            if ($model->image = UploadedFile::getInstance($model, 'image')) {
                $model->image = $model->upload();
            }

            /* check if any other Invite set default or not. */
            if ($model->default) {
                $inviteModel = Invite::find()->andWhere(['default' => Invite::DEFAULT_ACTIVE])->one();
                if ( ! empty($inviteModel)) {
                    Invite::updateAll(['default' => Invite::DEFAULT_INACTIVE], ['id' => $inviteModel->id]);
                }
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->heading = ucwords(strtolower($model->heading));
                    $model->save();

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Invite Create Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Invite model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            /**
             * Invite Image Upload.
             */
            $model->image = UploadedFile::getInstance($model, "image");
            $oldImage     = $model->getOldAttribute('image');
            if (empty($model->image)) {
                $model->image = $oldImage;
            } else {
                if ( ! empty($oldImage)) {
                    $originalFilePath = '/' . Invite::MAIN_DIRECTORY . Invite::ORIGINAL_FILEPATH . $oldImage;
                    $thumbFilePath    = '/' . Invite::MAIN_DIRECTORY . Invite::THUMBNAIL_FILEPATH . $oldImage;
                    if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                        unlink(Yii::$app->basePath . '/web' . $originalFilePath);
                        unlink(Yii::$app->basePath . '/web' . $thumbFilePath);
                    }
                }
                $model->image = $model->upload();
            }

            /* check if any other Invite set default or not. */
            if ($model->default) {
                $inviteModel = Invite::find()->andWhere(['default' => Invite::DEFAULT_ACTIVE])->one();
                if ( ! empty($inviteModel)) {
                    Invite::updateAll(['default' => Invite::DEFAULT_INACTIVE], ['id' => $inviteModel->id]);
                }
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->heading = ucwords(strtolower($model->heading));
                    $model->save();

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Invite Update Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Invite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == Invite::STATUS_DELETE) ? Invite::STATUS_ACTIVE : Invite::STATUS_DELETE;
        $model->save();
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Invite model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invite the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = Invite::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        $searchModel = new InviteSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {

            $view   = Html::getViewButton(['invite/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['invite/update', 'id' => $result['id']]);

            /**
             * Invite Image Show.
             */
            $images          = array ();
            $time            = '?r=' . time();
            $images['small'] = \common\models\FaqType::NO_IMAGE_SMALL . $time;
            $images['full']  = \common\models\FaqType::NO_IMAGE_FULL . $time;
            if ( ! empty($result['image'])) {
                $originalFilePath = '/' . Invite::MAIN_DIRECTORY . Invite::ORIGINAL_FILEPATH . $result['image'];
                $thumbFilePath    = '/' . Invite::MAIN_DIRECTORY . Invite::THUMBNAIL_FILEPATH . $result['image'];
                if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                    $images['small'] = $thumbFilePath . $time;
                    $images['full']  = $originalFilePath . $time;
                }
            }
            $image = Html::a(Html::img(Yii::getAlias('@web') . $images['small'], ['class' => 'img-thumbnail']), Yii::getAlias('@web') . $images['full'], ['data-fancybox' => 'gallery']);

            /**
             * Invite Status.
             */
            if ($result['status'] == Invite::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . Invite::getConstantList('STATUS_', Invite::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . Invite::getConstantList('STATUS_', Invite::className())[$result['status']] . '</span>';
            }

            /**
             * Invite Default.
             */
            if ($result['default'] == Invite::DEFAULT_ACTIVE) {
                $default = '<span class="label label-success">' . Invite::getConstantList('DEFAULT_', Invite::className())[$result['default']] . '</span>';
            } else {
                $default = '<span class="label label-danger">' . Invite::getConstantList('DEFAULT_', Invite::className())[$result['default']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['invite/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == Invite::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['heading'],
                $image,
                $default,
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
