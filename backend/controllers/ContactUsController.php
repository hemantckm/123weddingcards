<?php

namespace backend\controllers;

use Yii;
use common\models\ContactUs;
use backend\models\ContactUsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use backend\helpers\Html;
use common\components\CommonComponent;

/**
 * ContactUsController implements the CRUD actions for ContactUs model.
 */
class ContactUsController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ContactUs models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new ContactUsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new ContactUs();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\ContactUs::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single ContactUs model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        $model = $this->findModel($id);

        /**
         * Contact-Us Detail Section
         */
        $contactUsDatas       = ContactUs::find()->getContactUsData($id);
        $contactUsDetailModel = new \common\models\ContactUsDetails(); //New Contact-Us Details Model View.
        $contactUsTcModel     = new \common\models\ContactUsTc(); //New Contact-Us TC Model View.

        return $this->render('view', [
                    'model' => $model,
                    'contactUsDatas' => $contactUsDatas,
                    'contactUsDetailModel' => $contactUsDetailModel,
                    'contactUsTcModel' => $contactUsTcModel,
        ]);
    }

    /**
     * Creates a new ContactUs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model = new ContactUs();

        if (empty($contactUsDetailModels)) {
            $contactUsDetailModels = [new \common\models\ContactUsDetails()]; //New ContactUs Details Model Create.
        }

        if (empty($contactUsTcModel)) {
            $contactUsTcModel = new \common\models\ContactUsTc(); //New ContactUs TC Model Create.
        }

        if ($model->load(Yii::$app->request->post())) {
            $ContactUsDetails = Yii::$app->request->post('ContactUsDetails'); //Get Contact-Us Details Section Data.
            $ContactUsTcs     = Yii::$app->request->post('ContactUsTc'); //Get Contact-Us-Tc Section Data.

            /**
             * ContactUs Feature Image Upload.
             */
            if ($model->feature_image = UploadedFile::getInstance($model, 'feature_image')) {
                $model->feature_image = $model->upload();
            }

            /* check if any other ContactUs set default or not. */
            if ($model->default) {
                $contactUsModel = ContactUs::find()->andWhere(['default' => ContactUs::DEFAULT_ACTIVE])->one();
                if ( ! empty($contactUsModel)) {
                    ContactUs::updateAll(['default' => ContactUs::DEFAULT_INACTIVE], ['id' => $contactUsModel->id]);
                }
            }
            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->heading = ucwords(strtolower($model->heading));
                    $model->save();
                    $data           = ['id' => $model->id];

                    //Contact-Us Details
                    CommonComponent::ContactUsDetails($contactUsDetailModels, $ContactUsDetails, $data);

                    //Contact-US-TC Details
                    if ( ! empty($ContactUsTcs)) {
                        $contactUsTcModel->heading       = $ContactUsTcs['heading'];
                        $contactUsTcModel->description   = $ContactUsTcs['description'];
                        $contactUsTcModel->contact_us_id = $model->id;
                        $contactUsTcModel->save();
                    }

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "ContactUs Create Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
                    'contactUsDetailModels' => $contactUsDetailModels,
                    'contactUsTcModel' => $contactUsTcModel,
        ]);
    }

    /**
     * Updates an existing ContactUs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model = $this->findModel($id);

        /* ========= Contact-Us Details Model =========== */
        if (empty($contactUsDetailModels)) {
            $contactUsDetailModels = \common\models\ContactUsDetails::find()->andWhere(['contact_us_id' => $id])->all(); //Get ContactUs Details Model Data.
            if (empty($contactUsDetailModels)) {
                $contactUsDetailModels = [new \common\models\ContactUsDetails()]; //New ContactUs Details Model Create.
            }
        }

        /* ========= Contact-Us TC Model =========== */
        if (empty($contactUsTcModel)) {
            $contactUsTcModel = \common\models\ContactUsTc::find()->andWhere(['contact_us_id' => $id])->one(); //Get ContactUs TC Model Data.
            if (empty($contactUsTcModel)) {
                $contactUsTcModel = new \common\models\ContactUsTc(); //New ContactUs TC Model Create.
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $ContactUsDetails = Yii::$app->request->post('ContactUsDetails'); //Get Contact-Us Details Section Data.
            $ContactUsTcs     = Yii::$app->request->post('ContactUsTc'); //Get Contact-Us-Tc Section Data.

            /**
             * ContactUs Feature Image Upload.
             */
            $model->feature_image = UploadedFile::getInstance($model, "feature_image");
            $oldFeatureImage      = $model->getOldAttribute('feature_image');
            if (empty($model->feature_image)) {
                $model->feature_image = $oldFeatureImage;
            } else {
                if ( ! empty($oldFeatureImage)) {
                    $originalFilePath = '/' . ContactUs::MAIN_DIRECTORY . ContactUs::ORIGINAL_FILEPATH . $oldFeatureImage;
                    $thumbFilePath    = '/' . ContactUs::MAIN_DIRECTORY . ContactUs::THUMBNAIL_FILEPATH . $oldFeatureImage;
                    if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                        unlink(Yii::$app->basePath . '/web' . $originalFilePath);
                        unlink(Yii::$app->basePath . '/web' . $thumbFilePath);
                    }
                }
                $model->feature_image = $model->upload();
            }

            /* check if any other ContactUs set default or not. */
            if ($model->default) {
                $contactUsModel = ContactUs::find()->andWhere(['default' => ContactUs::DEFAULT_ACTIVE])->one();
                if ( ! empty($contactUsModel)) {
                    ContactUs::updateAll(['default' => ContactUs::DEFAULT_INACTIVE], ['id' => $contactUsModel->id]);
                }
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->heading = ucwords(strtolower($model->heading));
                    $model->save();
                    $data           = ['id' => $model->id];

                    //Contact-Us Details
//                    if ( ! empty($contactUsDetailModels)) {
//                        foreach ($contactUsDetailModels as $contactUsDetailModel) {
//                            if ( ! empty($contactUsDetailModel->id)) {
//                                \common\models\ContactUsDetails::deleteAll(['id' => $contactUsDetailModel->id, 'contact_us_id' => $id]);
//                            }
//                        }
//                        $contactUsDetailModels = [new \common\models\ContactUsDetails()]; //New Contact-Us Details Model Create.
//                    }
                    CommonComponent::ContactUsDetails($contactUsDetailModels, $ContactUsDetails, $data);

                    //Contact-US-TC Details
                    if ( ! empty($ContactUsTcs)) {
                        if ( ! empty($contactUsTcModel)) {
                            \common\models\ContactUsTc::deleteAll(['id' => $contactUsTcModel->id, 'contact_us_id' => $id]);
                        }
                        $contactUsTcModel                = new \common\models\ContactUsTc();
                        $contactUsTcModel->heading       = $ContactUsTcs['heading'];
                        $contactUsTcModel->description   = $ContactUsTcs['description'];
                        $contactUsTcModel->contact_us_id = $model->id;
                        $contactUsTcModel->save();
                    }

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "ContactUs Update Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
                    'contactUsDetailModels' => $contactUsDetailModels,
                    'contactUsTcModel' => $contactUsTcModel,
        ]);
    }

    /**
     * Deletes an existing ContactUs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == ContactUs::STATUS_DELETE) ? ContactUs::STATUS_ACTIVE : ContactUs::STATUS_DELETE;
        $model->save();
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the ContactUs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContactUs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = ContactUs::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        $searchModel = new ContactUsSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {

            $view   = Html::getViewButton(['contact-us/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['contact-us/update', 'id' => $result['id']]);

            /**
             * Contact-Us Feature Image Show.
             */
            $feature_images          = array ();
            $time                    = '?r=' . time();
            $feature_images['small'] = \common\models\FaqType::NO_IMAGE_SMALL . $time;
            $feature_images['full']  = \common\models\FaqType::NO_IMAGE_FULL . $time;
            if ( ! empty($result['feature_image'])) {
                $originalFilePath = '/' . ContactUs::MAIN_DIRECTORY . ContactUs::ORIGINAL_FILEPATH . $result['feature_image'];
                $thumbFilePath    = '/' . ContactUs::MAIN_DIRECTORY . ContactUs::THUMBNAIL_FILEPATH . $result['feature_image'];
                if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                    $feature_images['small'] = $thumbFilePath . $time;
                    $feature_images['full']  = $originalFilePath . $time;
                }
            }
            $feature_image = Html::a(Html::img(Yii::getAlias('@web') . $feature_images['small'], ['class' => 'img-thumbnail']), Yii::getAlias('@web') . $feature_images['full'], ['data-fancybox' => 'gallery']);

            /**
             * Contact-Us Status.
             */
            if ($result['status'] == ContactUs::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . ContactUs::getConstantList('STATUS_', ContactUs::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . ContactUs::getConstantList('STATUS_', ContactUs::className())[$result['status']] . '</span>';
            }

            /**
             * Contact-Us Default.
             */
            if ($result['default'] == ContactUs::DEFAULT_ACTIVE) {
                $default = '<span class="label label-success">' . ContactUs::getConstantList('DEFAULT_', ContactUs::className())[$result['default']] . '</span>';
            } else {
                $default = '<span class="label label-danger">' . ContactUs::getConstantList('DEFAULT_', ContactUs::className())[$result['default']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['contact-us/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == ContactUs::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['heading'],
                $feature_image,
                $default,
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
