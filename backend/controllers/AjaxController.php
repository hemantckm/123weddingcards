<?php

namespace backend\controllers;

use Yii;

class AjaxController extends \yii\web\Controller
{

    public function actionIndex ()
    {
        return $this->render('index');
    }

}
