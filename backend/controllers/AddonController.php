<?php

namespace backend\controllers;

use Yii;
use common\models\Addon;
use backend\models\AddonSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\CommonComponent;
use yii\web\Response;
use backend\helpers\Html;
use yii\helpers\Json;

/**
 * AddonController implements the CRUD actions for Add-on model.
 */
class AddonController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Add-on models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel        = new AddonSearch();
        $dataProvider       = $searchModel->search(Yii::$app->request->queryParams);
        $model              = new Addon();
        $cardModel          = new \common\models\Card();
        $addonEnvelopeModel = new \common\models\AddonEnvelope();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\Addon::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
                    'cardModel' => $cardModel,
                    'addonEnvelopeModel' => $addonEnvelopeModel,
        ]);
    }

    /**
     * Displays a single Add-on model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        $model = $this->findModel($id);

        //Getting Data From AddonInfo.
        $addonsInfo         = \common\models\AddonInfo::find()->andWhere(['addon_id' => $id])->all();
        $addonEnvelopeModel = new \common\models\AddonEnvelope(); //New Addon Envelope Model View.
        $addonFolderModel   = new \common\models\AddonFolder(); //New Addon Folder Model View.

        return $this->render('view', [
                    'model' => $model,
                    'addonsInfo' => $addonsInfo,
                    'addonEnvelopeModel' => $addonEnvelopeModel,
                    'addonFolderModel' => $addonFolderModel,
        ]);
    }

    /**
     * Creates a new Add-on model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model              = new Addon();
        $addonEnvelopeModel = new \common\models\AddonEnvelope(); //New AddonEnvelope Model Create.
        $addonFolderModel   = new \common\models\AddonFolder(); //New AddonFolder Model Create.

        $cards           = \common\models\Card::find()->getList(); //Get the list of Card.
        $addonTypes      = \common\models\AddonType::find()->getList(); //Get the list of AddonType.
        $addonStyles     = \common\models\AddonStyle::find()->getList(); //Get the list of AddonStyle.
        $papers          = \common\models\Paper::find()->getList(); //Get the list of Paper.
        $dies            = \common\models\DieCut::find()->getList(); //Get the list of DieCut.
        $colors          = \common\models\Color::find()->getList(); //Get the list of Colors.
        $paperTypes      = \common\models\PaperType::find()->getList(); //Get the list of PaperType.
        $blocks          = \common\models\Block::find()->getList(); //Get the list of Block.
        $printingMethods = \common\models\PrintingMethod::find()->getList(); //Get the list of PrintingMethod.

        if ($model->load(Yii::$app->request->post())) {
            $addonEnvelopeDetails = Yii::$app->request->post('AddonEnvelope'); //Get AddonEnvelope Section Data.
            $addonFolderDetails   = Yii::$app->request->post('AddonFolder'); //Get AddonFolder Section Data.
            $model->code          = preg_replace('/-+/', '-', str_replace(' ', '-', $model->addonType->code . ' ' . $model->card->name));

            // validate all models
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->save(false);
                    $data = ['id' => $model->id];

                    //Addon Envelope Details
                    CommonComponent::AddonEnvelope($addonEnvelopeModel, $addonEnvelopeDetails, $data);

                    //Addon Folder Details
                    CommonComponent::AddonFolder($addonFolderModel, $addonFolderDetails, $data);

                    /* ## Update AddonInfo ## */
                    if ( ! empty($model->id)) {
                        \common\models\AddonInfo::updateAddonInfo($model->id);
                    }

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Addon Create Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
                    'addonEnvelopeModel' => $addonEnvelopeModel,
                    'addonFolderModel' => $addonFolderModel,
                    'addonTypes' => $addonTypes,
                    'addonStyles' => $addonStyles,
                    'cards' => $cards,
                    'papers' => $papers,
                    'dies' => $dies,
                    'colors' => $colors,
                    'paperTypes' => $paperTypes,
                    'blocks' => $blocks,
                    'printingMethods' => $printingMethods,
        ]);
    }

    /**
     * Updates an existing Add-on model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model           = $this->findModel($id);
        $cards           = \common\models\Card::find()->getList(); //Get the list of Card.
        $addonTypes      = \common\models\AddonType::find()->getList(); //Get the list of AddonType.
        $addonStyles     = \common\models\AddonStyle::find()->getList(); //Get the list of AddonStyle.
        $papers          = \common\models\Paper::find()->getList(); //Get the list of Paper.
        $dies            = \common\models\DieCut::find()->getList(); //Get the list of DieCut.
        $colors          = \common\models\Color::find()->getList(); //Get the list of Colors.
        $paperTypes      = \common\models\PaperType::find()->getList(); //Get the list of PaperType.
        $blocks          = \common\models\Block::find()->getList(); //Get the list of Block.
        $printingMethods = \common\models\PrintingMethod::find()->getList(); //Get the list of PrintingMethod.

        /* ========= Addon Envelope Model =========== */
        $addonEnvelopePaperModelId          = [];
        $addonEnvelopeDieModelId            = [];
        $addonEnvelopeBlockModelId          = [];
        $addonEnvelopePrintingMethodModelId = [];
        if (empty($addonEnvelopeModel)) {
            $addonEnvelopeModel = \common\models\AddonEnvelope::find()->andWhere(['addon_id' => [$id]])->one();
            if ( ! empty($addonEnvelopeModel)) {
                //Addon Envelope Paper Model
                $addonEnvelopePaperModels = \common\models\AddonEnvelopePaper::find()->andWhere(['addon_envelope_id' => $addonEnvelopeModel->id])->all();
                foreach ($addonEnvelopePaperModels as $addonEnvelopePaperModel) {
                    $addonEnvelopePaperModelId[] = $addonEnvelopePaperModel->paper_id;
                }
                //Addon Envelope Die Model
                $addonEnvelopeDieModels = \common\models\AddonEnvelopeDie::find()->andWhere(['addon_envelope_id' => $addonEnvelopeModel->id])->all();
                foreach ($addonEnvelopeDieModels as $addonEnvelopeDieModel) {
                    $addonEnvelopeDieModelId[] = $addonEnvelopeDieModel->die_id;
                }
                //Addon Envelope Block Model
                $addonEnvelopeBlockModels = \common\models\AddonEnvelopeBlock::find()->andWhere(['addon_envelope_id' => $addonEnvelopeModel->id])->all();
                foreach ($addonEnvelopeBlockModels as $addonEnvelopeBlockModel) {
                    $addonEnvelopeBlockModelId[] = $addonEnvelopeBlockModel->block_id;
                }

                //Addon Envelope Printing Method Model
                $addonEnvelopePrintingMethodModels = \common\models\AddonEnvelopePrintingMethod::find()->andWhere(['addon_envelope_id' => $addonEnvelopeModel->id])->all();
                foreach ($addonEnvelopePrintingMethodModels as $addonEnvelopePrintingMethodModel) {
                    $addonEnvelopePrintingMethodModelId[] = $addonEnvelopePrintingMethodModel->printing_method_id;
                }
            } else {
                $addonEnvelopeModel = new \common\models\AddonEnvelope(); //New AddonEnvelope Model Create.
            }
        }

        /* ========= Addon Folder Model =========== */
        $addonFolderPaperModelId          = [];
        $addonFolderDieModelId            = [];
        $addonFolderBlockModelId          = [];
        $addonFolderPrintingMethodModelId = [];
        if (empty($addonFolderModel)) {
            $addonFolderModel = \common\models\AddonFolder::find()->andWhere(['addon_id' => [$id]])->one();
            if ( ! empty($addonFolderModel)) {
                //Addon Folder Paper Model
                $addonFolderPaperModels = \common\models\AddonFolderPaper::find()->andWhere(['addon_folder_id' => $addonFolderModel->id])->all();
                foreach ($addonFolderPaperModels as $addonFolderPaperModel) {
                    $addonFolderPaperModelId[] = $addonFolderPaperModel->paper_id;
                }
                //Addon Folder Die Model
                $addonFolderDieModels = \common\models\AddonFolderDie::find()->andWhere(['addon_folder_id' => $addonFolderModel->id])->all();
                foreach ($addonFolderDieModels as $addonFolderDieModel) {
                    $addonFolderDieModelId[] = $addonFolderDieModel->die_id;
                }
                //Addon Folder Block Model
                $addonFolderBlockModels = \common\models\AddonFolderBlock::find()->andWhere(['addon_folder_id' => $addonFolderModel->id])->all();
                foreach ($addonFolderBlockModels as $addonFolderBlockModel) {
                    $addonFolderBlockModelId[] = $addonFolderBlockModel->block_id;
                }

                //Addon Folder Printing Method Model
                $addonFolderPrintingMethodModels = \common\models\AddonFolderPrintingMethod::find()->andWhere(['addon_folder_id' => $addonFolderModel->id])->all();
                foreach ($addonFolderPrintingMethodModels as $addonFolderPrintingMethodModel) {
                    $addonFolderPrintingMethodModelId[] = $addonFolderPrintingMethodModel->printing_method_id;
                }
            } else {
                $addonFolderModel = new \common\models\AddonFolder(); //New AddonFolder Model Create.
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $addonEnvelopeDetails = Yii::$app->request->post('AddonEnvelope'); //Get AddonEnvelope Section Data.
            $addonFolderDetails   = Yii::$app->request->post('AddonFolder'); //Get AddonFolder Section Data.
            $model->code          = preg_replace('/-+/', '-', str_replace(' ', '-', $model->addonType->code . ' ' . $model->card->name));

            // validate all models
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->save(false);
                    $data = ['id' => $model->id];

                    //Addon Envelope Details
                    if ( ! empty($addonEnvelopeModel->id)) {
                        \common\models\AddonEnvelope::deleteAll(['id' => $addonEnvelopeModel->id, 'addon_id' => $model->id]);
                        $addonEnvelopeModel = new \common\models\AddonEnvelope(); //New Addon Envelope Model Create.
                    }
                    CommonComponent::AddonEnvelope($addonEnvelopeModel, $addonEnvelopeDetails, $data);

                    //Addon Folder Details
                    if ( ! empty($addonFolderModel->id)) {
                        \common\models\AddonFolder::deleteAll(['id' => $addonFolderModel->id, 'addon_id' => $model->id]);
                        $addonFolderModel = new \common\models\AddonFolder(); //New Addon Folder Model Create.
                    }
                    CommonComponent::AddonFolder($addonFolderModel, $addonFolderDetails, $data);

                    /* ## Update AddonInfo ## */
                    if ( ! empty($model->id)) {
                        \common\models\AddonInfo::updateAddonInfo($model->id);
                    }

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Addon Update Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
                    'addonEnvelopeModel' => $addonEnvelopeModel,
                    'addonFolderModel' => $addonFolderModel,
                    'addonTypes' => $addonTypes,
                    'addonStyles' => $addonStyles,
                    'cards' => $cards,
                    'papers' => $papers,
                    'dies' => $dies,
                    'colors' => $colors,
                    'paperTypes' => $paperTypes,
                    'blocks' => $blocks,
                    'printingMethods' => $printingMethods,
                    'addonEnvelopePaperModelId' => $addonEnvelopePaperModelId,
                    'addonEnvelopeDieModelId' => $addonEnvelopeDieModelId,
                    'addonEnvelopeBlockModelId' => $addonEnvelopeBlockModelId,
                    'addonEnvelopePrintingMethodModelId' => $addonEnvelopePrintingMethodModelId,
                    'addonFolderPaperModelId' => $addonFolderPaperModelId,
                    'addonFolderDieModelId' => $addonFolderDieModelId,
                    'addonFolderBlockModelId' => $addonFolderBlockModelId,
                    'addonFolderPrintingMethodModelId' => $addonFolderPrintingMethodModelId,
        ]);
    }

    /**
     * GET All DataInfo of The Select Add-on PaperCode in Add-on.
     * @param type $addon_paper
     * @return type
     */
    public function actionAddonPaper ($addon_paper)
    {
        $paperInfo = \common\models\Paper::find()->getPaperInfo($addon_paper);
        return Json::encode($paperInfo);
    }

    /**
     * Deletes an existing Add-on model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == Addon::STATUS_DELETE) ? Addon::STATUS_ACTIVE : Addon::STATUS_DELETE;
        $model->save(false);
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Add-on model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Addon the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = Addon::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Dependency DropDown For The Add-on Style.
     * @return type
     */
    public function actionAddonStyle ()
    {
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $addon_type_id   = $parents[0];
                $addon_styles    = \common\models\AddonStyle::find()->where(['addon_type_id' => $addon_type_id])->all();
                $all_addon_style = array ();
                $i               = 0;
                foreach ($addon_styles as $addon_style) {
                    $all_addon_style[$i]['id']   = $addon_style->id;
                    $all_addon_style[$i]['name'] = $addon_style->name;
                    $i ++;
                }
                return Json::encode(['output' => $all_addon_style, 'selected' => '']);
            }
        }
        return Json::encode(['output' => '', 'selected' => '']);
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        //$searchModel = new AddonSearch();
        //$results     = $searchModel->dataTable($searchParams);
        $searchModel = new \backend\models\AddonInfoSearch();
        $results     = $searchModel->dataTable($searchParams);
        foreach ($results['records'] as $result) {
            //Getting EnvelopePaper Colors.
            $envelopeJsonArray  = json_decode($result['envelope'], TRUE);
            $envelopePaperArray = array ();
            if ( ! empty($envelopeJsonArray)) {
                foreach ($envelopeJsonArray as $envelopeArray) {
                    $envelopePaperArray[] = $envelopeArray['paper_color'];
                }
            }
            $envelopePaperColors = implode(', ', $envelopePaperArray);

            $view   = Html::getViewButton(['addon/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['addon/update', 'id' => $result['id']]);

            if ($result['status'] == Addon::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . Addon::getConstantList('STATUS_', Addon::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . Addon::getConstantList('STATUS_', Addon::className())[$result['status']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['addon/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == Addon::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['code'],
                $result['name'],
                $result['addon_type'],
                $result['addon_style'],
                ! empty($envelopePaperColors) ? $envelopePaperColors : '',
                $result['price'],
                $result['length'],
                $result['width'],
                $result['weight'],
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
