<?php

namespace backend\controllers;

use Yii;
use common\models\Vendor;
use backend\models\VendorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use backend\helpers\Html;

/**
 * VendorController implements the CRUD actions for Vendor model.
 */
class VendorController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vendor models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new VendorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new Vendor();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\Vendor::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single Vendor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        $model           = $this->findModel($id);
        $images          = array ();
        $time            = '?r=' . time();
        $images['small'] = \common\models\FaqType::NO_IMAGE_SMALL . $time;
        $images['full']  = \common\models\FaqType::NO_IMAGE_FULL . $time;
        if ( ! empty($model->image)) {
            $originalFilePath = '/' . Vendor::MAIN_DIRECTORY . Vendor::ORIGINAL_FILEPATH . $model->image;
            $thumbFilePath    = '/' . Vendor::MAIN_DIRECTORY . Vendor::THUMBNAIL_FILEPATH . $model->image;
            if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                $images['small'] = $thumbFilePath . $time;
                $images['full']  = $originalFilePath . $time;
            }
        }
        $image = Html::a(Html::img(Yii::getAlias('@web') . $images['small'], ['class' => 'img-thumbnail', 'style' => 'margin-bottom:10px']), Yii::getAlias('@web') . $images['full'], ['data-fancybox' => 'gallery']);

        return $this->render('view', [
                    'model' => $model,
                    'image' => $image,
        ]);
    }

    /**
     * Creates a new Vendor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model = new Vendor();

        if ($model->load(Yii::$app->request->post())) {
            /**
             * Vendor Image Upload.
             */
            if ($model->image = UploadedFile::getInstance($model, 'image')) {
                $model->image = $model->upload();
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->name = ucwords(strtolower($model->name));
                    $model->save();

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Vendor Create Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Vendor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            /**
             * Vendor Image Upload.
             */
            $model->image = UploadedFile::getInstance($model, "image");
            $oldImage     = $model->getOldAttribute('image');
            if (empty($model->image)) {
                $model->image = $oldImage;
            } else {
                if ( ! empty($oldImage)) {
                    $originalFilePath = '/' . Vendor::MAIN_DIRECTORY . Vendor::ORIGINAL_FILEPATH . $oldImage;
                    $thumbFilePath    = '/' . Vendor::MAIN_DIRECTORY . Vendor::THUMBNAIL_FILEPATH . $oldImage;
                    if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                        unlink(Yii::$app->basePath . '/web' . $originalFilePath);
                        unlink(Yii::$app->basePath . '/web' . $thumbFilePath);
                    }
                }
                $model->image = $model->upload();
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->name = ucwords(strtolower($model->name));
                    $model->save();

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Vendor Update Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vendor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == Vendor::STATUS_DELETE) ? Vendor::STATUS_ACTIVE : Vendor::STATUS_DELETE;
        $model->save();
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Vendor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vendor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = Vendor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        $searchModel = new VendorSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {

            $view   = Html::getViewButton(['vendor/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['vendor/update', 'id' => $result['id']]);

            /**
             * Vendor Image Show.
             */
            $images          = array ();
            $time            = '?r=' . time();
            $images['small'] = \common\models\FaqType::NO_IMAGE_SMALL . $time;
            $images['full']  = \common\models\FaqType::NO_IMAGE_FULL . $time;
            if ( ! empty($result['image'])) {
                $originalFilePath = '/' . Vendor::MAIN_DIRECTORY . Vendor::ORIGINAL_FILEPATH . $result['image'];
                $thumbFilePath    = '/' . Vendor::MAIN_DIRECTORY . Vendor::THUMBNAIL_FILEPATH . $result['image'];
                if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                    $images['small'] = $thumbFilePath . $time;
                    $images['full']  = $originalFilePath . $time;
                }
            }
            $image = Html::a(Html::img(Yii::getAlias('@web') . $images['small'], ['class' => 'img-thumbnail']), Yii::getAlias('@web') . $images['full'], ['data-fancybox' => 'gallery']);

            /**
             * Vendor Status.
             */
            if ($result['status'] == Vendor::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . Vendor::getConstantList('STATUS_', Vendor::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . Vendor::getConstantList('STATUS_', Vendor::className())[$result['status']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['vendor/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == Vendor::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['name'],
                $image,
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
