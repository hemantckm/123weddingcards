<?php

namespace backend\controllers;

use Yii;
use common\models\Color;
use backend\models\ColorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use backend\helpers\Html;

/**
 * ColorController implements the CRUD actions for Color model.
 */
class ColorController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Color models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new ColorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new Color();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\Color::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single Color model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Color model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model = new Color();

        if ($model->load(Yii::$app->request->post())) {
            $model->name = ucwords(strtolower($model->name));
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Color Create Successfully!");
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Color model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->name = ucwords(strtolower($model->name));
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Color Update Successfully!");
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Color model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == Color::STATUS_DELETE) ? Color::STATUS_ACTIVE : Color::STATUS_DELETE;
        $model->save();
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Color model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Color the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = Color::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Get Color List
     * @return type
     */
    public function actionList ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $colors                       = Color::find()->getList();
        $colors                       += [0 => 'Select Paper Color'];
        return $colors;
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        $searchModel = new ColorSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {

            $view   = Html::getViewButton(['color/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['color/update', 'id' => $result['id']]);

            if ($result['status'] == Color::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . Color::getConstantList('STATUS_', Color::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . Color::getConstantList('STATUS_', Color::className())[$result['status']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['color/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == Color::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['name'],
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
