<?php

namespace backend\controllers;

use Yii;
use common\models\About;
use backend\models\AboutSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use backend\helpers\Html;

/**
 * AboutController implements the CRUD actions for About model.
 */
class AboutController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all About models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new AboutSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new About();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\About::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single About model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        $model           = $this->findModel($id);
        $images          = array ();
        $time            = '?r=' . time();
        $images['small'] = \common\models\FaqType::NO_IMAGE_SMALL . $time;
        $images['full']  = \common\models\FaqType::NO_IMAGE_FULL . $time;
        if ( ! empty($model->image)) {
            $originalFilePath = '/' . About::MAIN_DIRECTORY . About::ORIGINAL_FILEPATH . $model->image;
            $thumbFilePath    = '/' . About::MAIN_DIRECTORY . About::THUMBNAIL_FILEPATH . $model->image;
            if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                $images['small'] = $thumbFilePath . $time;
                $images['full']  = $originalFilePath . $time;
            }
        }
        $image = Html::a(Html::img(Yii::getAlias('@web') . $images['small'], ['class' => 'img-thumbnail', 'style' => 'margin-bottom:10px']), Yii::getAlias('@web') . $images['full'], ['data-fancybox' => 'gallery']);


        return $this->render('view', [
                    'model' => $model,
                    'image' => $image,
        ]);
    }

    /**
     * Creates a new About model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model = new About();

        if ($model->load(Yii::$app->request->post())) {
            /**
             * About Feature Image Upload.
             */
            if ($model->image = UploadedFile::getInstance($model, 'image')) {
                $model->image = $model->upload();
            }

            /* check if any other About set default or not. */
            if ($model->default) {
                $aboutModel = About::find()->andWhere(['default' => About::DEFAULT_ACTIVE])->one();
                if ( ! empty($aboutModel)) {
                    About::updateAll(['default' => About::DEFAULT_INACTIVE], ['id' => $aboutModel->id]);
                }
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->heading1 = ucwords(strtolower($model->heading1));
                    $model->heading2 = ucwords(strtolower($model->heading2));
                    $model->save();

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "About Create Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing About model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            /**
             * About Image Upload.
             */
            $model->image = UploadedFile::getInstance($model, "image");
            $oldImage     = $model->getOldAttribute('image');
            if (empty($model->image)) {
                $model->image = $oldImage;
            } else {
                if ( ! empty($oldImage)) {
                    $originalFilePath = '/' . About::MAIN_DIRECTORY . About::ORIGINAL_FILEPATH . $oldImage;
                    $thumbFilePath    = '/' . About::MAIN_DIRECTORY . About::THUMBNAIL_FILEPATH . $oldImage;
                    if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                        unlink(Yii::$app->basePath . '/web' . $originalFilePath);
                        unlink(Yii::$app->basePath . '/web' . $thumbFilePath);
                    }
                }
                $model->image = $model->upload();
            }

            /* check if any other About set default or not. */
            if ($model->default) {
                $aboutModel = About::find()->andWhere(['default' => About::DEFAULT_ACTIVE])->one();
                if ( ! empty($aboutModel)) {
                    About::updateAll(['default' => About::DEFAULT_INACTIVE], ['id' => $aboutModel->id]);
                }
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->heading1 = ucwords(strtolower($model->heading1));
                    $model->heading2 = ucwords(strtolower($model->heading2));
                    $model->save();

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "About Update Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing About model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == About::STATUS_DELETE) ? About::STATUS_ACTIVE : About::STATUS_DELETE;
        $model->save();
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the About model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return About the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = About::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        $searchModel = new AboutSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {

            $view   = Html::getViewButton(['about/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['about/update', 'id' => $result['id']]);

            /**
             * About Feature Image Show.
             */
            $images          = array ();
            $time            = '?r=' . time();
            $images['small'] = \common\models\FaqType::NO_IMAGE_SMALL . $time;
            $images['full']  = \common\models\FaqType::NO_IMAGE_FULL . $time;
            if ( ! empty($result['image'])) {
                $originalFilePath = '/' . About::MAIN_DIRECTORY . About::ORIGINAL_FILEPATH . $result['image'];
                $thumbFilePath    = '/' . About::MAIN_DIRECTORY . About::THUMBNAIL_FILEPATH . $result['image'];
                if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                    $images['small'] = $thumbFilePath . $time;
                    $images['full']  = $originalFilePath . $time;
                }
            }
            $image = Html::a(Html::img(Yii::getAlias('@web') . $images['small'], ['class' => 'img-thumbnail']), Yii::getAlias('@web') . $images['full'], ['data-fancybox' => 'gallery']);

            /**
             * About Status.
             */
            if ($result['status'] == About::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . About::getConstantList('STATUS_', About::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . About::getConstantList('STATUS_', About::className())[$result['status']] . '</span>';
            }

            /**
             * About Default.
             */
            if ($result['default'] == About::DEFAULT_ACTIVE) {
                $default = '<span class="label label-success">' . About::getConstantList('DEFAULT_', About::className())[$result['default']] . '</span>';
            } else {
                $default = '<span class="label label-danger">' . About::getConstantList('DEFAULT_', About::className())[$result['default']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['about/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == About::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['heading1'],
                $result['heading2'],
                $image,
                $default,
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
