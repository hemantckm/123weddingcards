<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use backend\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\SignupForm;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{

    public $password;

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
                    'model' => $model,
                    'userImages' => $this->profileImageShow($model->image),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model = new SignupForm();
        if ( ! empty(Yii::$app->request->post())) {
            if ($model->image = UploadedFile::getInstance($model, 'image')) {
                $userImage    = $model->upload();
                $model->image = $userImage;
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $user  = new User();
        $model = $this->findModel($id);
        if ( ! empty(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, "image");
            $oldUserImage = $model->getOldAttribute('image');
            $userImage    = $model->image;
            if ($userImage == '') {
                $model->image = $oldUserImage;
            } else {
                if ( ! empty($oldUserImage)) {
                    $originalFilePath = '/' . User::MAIN_DIRECTORY . User::ORIGINAL_FILEPATH . $oldUserImage;
                    $thumbFilePath    = '/' . User::MAIN_DIRECTORY . User::THUMBNAIL_FILEPATH . $oldUserImage;
                    if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                        unlink(Yii::$app->basePath . '/web' . $originalFilePath);
                        unlink(Yii::$app->basePath . '/web' . $thumbFilePath);
                    }
                }
                $userImage    = $model->upload();
                $model->image = $userImage;
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            //$model->master_admin = 0;
            if ( ! empty(Yii::$app->request->post()['User']['password'])) {
                $data                 = $user->setPassword(Yii::$app->request->post()['User']['password']);
                $model->password_hash = $user->password_hash;
            }
            $model->email = Yii::$app->request->post('User')['email'];
            $model->save(false);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
                    'model' => $model,
                    'userImages' => $this->profileImageShow($model->image),
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = User::STATUS_DELETE;
        $model->save(false);
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * User Change Password
     * @return mixed
     */
    public function actionChangePassword ()
    {
        $user  = new User();
        $model = User::find()->where(['username' => Yii::$app->user->identity->username, 'id' => Yii::$app->user->identity->id])->one();

        if ( ! empty(Yii::$app->request->post())) {
            if ( ! empty($_POST['User'])) {
                $data                 = $user->setPassword(Yii::$app->request->post()['User']['newpassword']);
                $model->password_hash = $user->password_hash;

                if ($model->load(Yii::$app->request->post())) {
                    if ($model->save()) {
                        Yii::$app->session->setFlash('success', "Password Changed successfully.");
                        return $this->redirect(['site/index']);
                    } else {
                        Yii::$app->session->setFlash('error', "Password Incorrect.");
                        return $this->render('change-password', array ('model' => $model));
                    }
                }
            }
        }
        return $this->render('change-password', array ('model' => $user));
    }

    /**
     * Profile Page.
     * @return type
     */
    public function actionProfile ()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('profile', [
                    'model' => $model,
                    'userImages' => $this->profileImageShow(),
        ]);
    }

    /**
     * Show the Profile of the User.
     * @return string
     */
    protected function profileImageShow ($imageName = null)
    {
        $userImages          = array ();
        $time                = '?r=' . time();
        $userImages['small'] = \common\models\User::NO_IMAGE_SMALL . $time;
        $userImages['full']  = \common\models\User::NO_IMAGE_FULL . $time;

        $image = $imageName ? $imageName : Yii::$app->user->identity->image;

        if ( ! empty($image)) {
            $originalFilePath = '/' . User::MAIN_DIRECTORY . User::ORIGINAL_FILEPATH . $image;
            $thumbFilePath    = '/' . User::MAIN_DIRECTORY . User::THUMBNAIL_FILEPATH . $image;
            if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                $userImages['small'] = $thumbFilePath . $time;
                $userImages['full']  = $originalFilePath . $time;
            }
        }

        return $userImages;
    }

}
