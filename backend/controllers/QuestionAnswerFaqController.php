<?php

namespace backend\controllers;

use Yii;
use common\models\QuestionAnswerFaq;
use backend\models\QuestionAnswerFaqSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use backend\helpers\Html;
use yii\helpers\Json;

/**
 * QuestionAnswerFaqController implements the CRUD actions for QuestionAnswerFaq model.
 */
class QuestionAnswerFaqController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all QuestionAnswerFaq models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new QuestionAnswerFaqSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new QuestionAnswerFaq();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\QuestionAnswerFaq::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single QuestionAnswerFaq model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new QuestionAnswerFaq model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model     = new QuestionAnswerFaq();
        $questions = \common\models\QuestionAnswer::find()->getList(); //Get the list of QuestionAnswer.
        $faqs      = \common\models\Faq::find()->getList(); //Get the list of FAQ.

        if ($model->load(Yii::$app->request->post())) {

            /* Check If Same Question Exist with Same FAQ and FAQ Type. */
            $questionFaqFaqTypeModel = QuestionAnswerFaq::find()->andWhere([
                        'question_answer_id' => $model->question_answer_id,
                        'faq_id' => $model->faq_id,
                        'faq_type_id' => $model->faq_type_id,
                    ])->one();
            if ( ! empty($questionFaqFaqTypeModel)) {
                QuestionAnswerFaq::updateAll(['status' => QuestionAnswerFaq::STATUS_INACTIVE], ['id' => $questionFaqFaqTypeModel->id]);
            }

            // validate all models
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->save();

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Q/A FAQ Create Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $exc) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
                    'questions' => $questions,
                    'faqs' => $faqs,
        ]);
    }

    /**
     * Updates an existing QuestionAnswerFaq model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model     = $this->findModel($id);
        $questions = \common\models\QuestionAnswer::find()->getList(); //Get the list of QuestionAnswer.
        $faqs      = \common\models\Faq::find()->getList(); //Get the list of FAQ.

        if ($model->load(Yii::$app->request->post())) {

            /* Check If Same Question Exist with Same FAQ and FAQ Type. */
            $questionFaqFaqTypeModel = QuestionAnswerFaq::find()->andWhere([
                        'question_answer_id' => $model->question_answer_id,
                        'faq_id' => $model->faq_id,
                        'faq_type_id' => $model->faq_type_id,
                    ])->one();
            if ( ! empty($questionFaqFaqTypeModel)) {
                QuestionAnswerFaq::updateAll(['status' => QuestionAnswerFaq::STATUS_INACTIVE], ['id' => $questionFaqFaqTypeModel->id]);
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->save();

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Q/A FAQ Update Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $exc) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
                    'questions' => $questions,
                    'faqs' => $faqs,
        ]);
    }

    /**
     * Deletes an existing QuestionAnswerFaq model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == QuestionAnswerFaq::STATUS_DELETE) ? QuestionAnswerFaq::STATUS_ACTIVE : QuestionAnswerFaq::STATUS_DELETE;
        $model->save();
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the QuestionAnswerFaq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QuestionAnswerFaq the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = QuestionAnswerFaq::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Dependency DropDown For The FAQ Type.
     * @return type
     */
    public function actionFaqType ()
    {
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $faq_id       = $parents[0];
                $faq_types    = \common\models\FaqFaqType::find()->where(['faq_id' => $faq_id])->all();
                $all_faq_type = array ();
                $i            = 0;
                foreach ($faq_types as $faq_type) {
                    $faqTypeModel = \common\models\FaqType::find()->andWhere(['id' => $faq_type->faq_type_id])->one();
                    if ( ! empty($faqTypeModel)) {
                        $all_faq_type[$i]['id']   = $faqTypeModel->id;
                        $all_faq_type[$i]['name'] = $faqTypeModel->name;
                        $i ++;
                    }
                }
                return Json::encode(['output' => $all_faq_type, 'selected' => '']);
            }
        }
        return Json::encode(['output' => '', 'selected' => '']);
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        $searchModel = new QuestionAnswerFaqSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {

            $view   = Html::getViewButton(['question-answer-faq/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['question-answer-faq/update', 'id' => $result['id']]);

            /**
             * QuestionAnswerFaq Status.
             */
            if ($result['status'] == QuestionAnswerFaq::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . QuestionAnswerFaq::getConstantList('STATUS_', QuestionAnswerFaq::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . QuestionAnswerFaq::getConstantList('STATUS_', QuestionAnswerFaq::className())[$result['status']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['question-answer-faq/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == QuestionAnswerFaq::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['question'],
                $result['heading'],
                $result['name'],
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
