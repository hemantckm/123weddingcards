<?php

namespace backend\controllers;

use Yii;
use common\models\Theme;
use common\models\CardTheme;
use common\models\Paper;
use common\models\PaperType;
use common\models\DieCut;
use common\models\Color;
use common\models\Block;
use common\models\PrintingMethod;
use common\models\Envelope;
use common\models\Folder;
use common\models\Insert;
use common\models\Tassle;
use common\models\Ribbon;
use common\models\Rhinestone;
use common\models\BoxType;
use common\models\Additions;
use common\models\Scroll;
use common\models\Tube;
use common\models\Box;
use common\models\Card;
use backend\models\CardSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\CommonComponent;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use backend\helpers\Html;
use yii\helpers\Json;

/**
 * CardController implements the CRUD actions for Card model.
 */
class CardController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Card models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new CardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new Card();
        $folderModel  = new Folder();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\Card::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
                    'folderModel' => $folderModel,
        ]);
    }

    /**
     * Displays a single Card model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        $model = $this->findModel($id);

        //Getting Data From CardInfo.
        $cardsInfo      = \common\models\CardInfo::find()->andWhere(['card_id' => $id])->all();
        $envelopeModel  = new Envelope(); //New Envelope Model View.
        $folderModel    = new Folder(); //New Folder Model View.
        $scrollModel    = new Scroll(); //New Scroll Model View.
        $tubeModel      = new Tube(); //New Tube Model View.
        $boxModel       = new Box(); //New Box Model View.
        $additionsModel = new Additions(); //New Additions Model View.
        $insertModel    = new Insert(); //New Insert Model View.

        return $this->render('view', [
                    'model' => $model,
                    'cardsInfo' => $cardsInfo,
                    'envelopeModel' => $envelopeModel,
                    'folderModel' => $folderModel,
                    'scrollModel' => $scrollModel,
                    'tubeModel' => $tubeModel,
                    'boxModel' => $boxModel,
                    'additionsModel' => $additionsModel,
                    'insertModel' => $insertModel,
        ]);
    }

    /**
     * Creates a new Card model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model          = new Card();
        $envelopeModel  = new Envelope(); //New Envelope Model Create.
        $folderModel    = new Folder(); //New Folder Model Create.
        $scrollModel    = new Scroll(); //New Scroll Model Create.
        $boxModel       = new Box(); //New Box Model Create.
        $tubeModel      = new Tube(); //New Tube Model Create.
        $additionsModel = new Additions(); //New Additions Model Create.

        if (empty($insertModels)) {
            $insertModels = [new Insert()]; //New Insert Model Create.
        }

        $papers          = Paper::find()->getList(); //Get the list of Paper.
        $themes          = Theme::find()->getList(); //Get the list of theme.
        $dies            = DieCut::find()->getList(); //Get the list of DieCut.
        $colors          = Color::find()->getList(); //Get the list of Colors.
        $paperTypes      = PaperType::find()->getList(); //Get the list of PaperType.
        $blocks          = Block::find()->getList(); //Get the list of Block.
        $printingMethods = PrintingMethod::find()->getList(); //Get the list of PrintingMethod.
        $tassles         = Tassle::find()->getList(); //Get the list of Tassle.
        $ribbons         = Ribbon::find()->getList(); //Get the list of Ribbon.
        $rhinestones     = Rhinestone::find()->getList(); //Get the list of Rhinestone.
        $boxTypes        = BoxType::find()->getList(); //Get the list of BoxType.

        if ($model->load(Yii::$app->request->post())) {
            $envelopeDetails  = Yii::$app->request->post('Envelope'); //Get Envelope Section Data.
            $folderDetails    = Yii::$app->request->post('Folder'); //Get Folder Section Data.
            $scrollDetails    = Yii::$app->request->post('Scroll'); //Get Scroll Section Data.
            $boxDetails       = Yii::$app->request->post('Box'); //Get Box Section Data.
            $tubeDetails      = Yii::$app->request->post('Tube'); //Get Tube Section Data.
            $insertDetails    = Yii::$app->request->post('Insert'); //Get Insert Section Data.
            $additionsDetails = Yii::$app->request->post('Additions'); //Get Additions Section Data.
            $cardThemes       = Yii::$app->request->post('Card')['card_theme']; //Get CardTheme.

            /*             * ======================= Check Scroll Theme Exist or Not =============== * */
            $cardThemeExist = false; //Set a Flag for Scroll Theme.
            if ( ! empty($cardThemes)) {
                $themeArray  = array ();
                $themeModels = Theme::findAll(['id' => $cardThemes]);
                if ( ! empty($themeModels)) {
                    foreach ($themeModels as $themeModel) {
                        $themeArray[] = $themeModel->name;
                    }
                }
                if (in_array(Card::THEME_EXIST, $themeArray)) {
                    $cardThemeExist = true;
                }
            }

            $model->slug = preg_replace('/-+/', '-', str_replace(' ', '-', strtolower($model->name)));

            // validate all models
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->name = Card::find()->doCapitalize(strtolower($model->name));
                    $model->save();
                    $data        = ['id' => $model->id];

                    //Card Theme.
                    if ( ! empty($cardThemes)) {
                        foreach ($cardThemes as $cardTheme) {
                            $cardThemeModel           = new CardTheme();
                            $cardThemeModel->card_id  = $model->id;
                            $cardThemeModel->theme_id = $cardTheme;
                            $cardThemeModel->save();
                        }
                    }

                    //Envelope Details
                    CommonComponent::Envelope($envelopeModel, $envelopeDetails, $data);

                    //Folder Details
                    CommonComponent::Folder($folderModel, $folderDetails, $data);

                    if ($cardThemeExist) {
                        //Scroll/Tube Details
                        CommonComponent::ScrollTube($scrollModel, $scrollDetails, $tubeModel, $tubeDetails, $data);

                        //Box Details
                        CommonComponent::Box($boxModel, $boxDetails, $data);
                    }

                    //Insert Details
                    CommonComponent::Insert($insertModels, $insertDetails, $data);

                    //Additions Details
                    CommonComponent::Additions($additionsModel, $additionsDetails, $data);

                    /* ## Update CardInfo ## */
                    if ( ! empty($model->id)) {
                        \common\models\CardInfo::updateCardInfo($model->id);
                    }

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Card Create Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
                    'themes' => $themes,
                    'papers' => $papers,
                    'dies' => $dies,
                    'colors' => $colors,
                    'paperTypes' => $paperTypes,
                    'blocks' => $blocks,
                    'printingMethods' => $printingMethods,
                    'envelopeModel' => $envelopeModel,
                    'folderModel' => $folderModel,
                    'scrollModel' => $scrollModel,
                    'boxModel' => $boxModel,
                    'tubeModel' => $tubeModel,
                    'insertModels' => $insertModels,
                    'tassles' => $tassles,
                    'ribbons' => $ribbons,
                    'rhinestones' => $rhinestones,
                    'boxTypes' => $boxTypes,
                    'additionsModel' => $additionsModel,
        ]);
    }

    /**
     * Updates an existing Card model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model = $this->findModel($id);

        $papers          = Paper::find()->getList(); //Get the list of Paper.
        $themes          = Theme::find()->getList(); //Get the list of theme.
        $dies            = DieCut::find()->getList(); //Get the list of DieCut.
        $colors          = Color::find()->getList(); //Get the list of Colors.
        $paperTypes      = PaperType::find()->getList(); //Get the list of PaperType.
        $blocks          = Block::find()->getList(); //Get the list of Block.
        $printingMethods = PrintingMethod::find()->getList(); //Get the list of PrintingMethod.
        $tassles         = Tassle::find()->getList(); //Get the list of Tassle.
        $ribbons         = Ribbon::find()->getList(); //Get the list of Ribbon.
        $rhinestones     = Rhinestone::find()->getList(); //Get the list of Rhinestone.
        $boxTypes        = BoxType::find()->getList(); //Get the list of BoxType.

        /* ========= Card Theme =========== */
        $cardThemeModelId = [];
        if (empty($cardThemeModels)) {
            /* ======== Card Theme Model ======== */
            $cardThemeModels = CardTheme::find()->getCardTheme($id);
            foreach ($cardThemeModels as $cardThemeModel) {
                /* ======== Fetch Theme ID From Card Theme Model ======== */
                $cardThemeModelId[] = $cardThemeModel['theme_id'];
            }
        }

        /* ========= Envelope Model =========== */
        $envelopePaperModelId          = [];
        $envelopeDieModelId            = [];
        $envelopeBlockModelId          = [];
        $envelopePrintingMethodModelId = [];
        if (empty($envelopeModel)) {
            $envelopeModel = Envelope::find()->andWhere(['card_id' => [$id]])->one();
            if ( ! empty($envelopeModel)) {
                //Envelope Paper Model
                $envelopePaperModels = \common\models\EnvelopePaper::find()->andWhere(['envelope_id' => $envelopeModel->id])->all();
                foreach ($envelopePaperModels as $envelopePaperModel) {
                    $envelopePaperModelId[] = $envelopePaperModel->paper_id;
                }
                //Envelope Die Model
                $envelopeDieModels = \common\models\EnvelopeDie::find()->andWhere(['envelope_id' => $envelopeModel->id])->all();
                foreach ($envelopeDieModels as $envelopeDieModel) {
                    $envelopeDieModelId[] = $envelopeDieModel->die_id;
                }
                //Envelope Block Model
                $envelopeBlockModels = \common\models\EnvelopeBlock::find()->andWhere(['envelope_id' => $envelopeModel->id])->all();
                foreach ($envelopeBlockModels as $envelopeBlockModel) {
                    $envelopeBlockModelId[] = $envelopeBlockModel->block_id;
                }

                //Envelope Printing Method Model
                $envelopePrintingMethodModels = \common\models\EnvelopePrintingMethod::find()->andWhere(['envelope_id' => $envelopeModel->id])->all();
                foreach ($envelopePrintingMethodModels as $envelopePrintingMethodModel) {
                    $envelopePrintingMethodModelId[] = $envelopePrintingMethodModel->printing_method_id;
                }
            } else {
                $envelopeModel = new Envelope();
            }
        }

        /* ========= Folder Model =========== */
        $folderPaperModelId          = [];
        $folderDieModelId            = [];
        $folderBlockModelId          = [];
        $folderPrintingMethodModelId = [];
        if (empty($folderModel)) {
            $folderModel = Folder::find()->andWhere(['card_id' => [$id]])->one();
            if ( ! empty($folderModel)) {
                //Folder Paper Model
                $folderPaperModels = \common\models\FolderPaper::find()->andWhere(['folder_id' => $folderModel->id])->all();
                foreach ($folderPaperModels as $folderPaperModel) {
                    $folderPaperModelId[] = $folderPaperModel->paper_id;
                }
                //Folder Die Model
                $folderDieModels = \common\models\FolderDie::find()->andWhere(['folder_id' => $folderModel->id])->all();
                foreach ($folderDieModels as $folderDieModel) {
                    $folderDieModelId[] = $folderDieModel->die_id;
                }
                //Folder Block Model
                $folderBlockModels = \common\models\FolderBlock::find()->andWhere(['folder_id' => $folderModel->id])->all();
                foreach ($folderBlockModels as $folderBlockModel) {
                    $folderBlockModelId[] = $folderBlockModel->block_id;
                }

                //Folder Printing Method Model
                $folderPrintingMethodModels = \common\models\FolderPrintingMethod::find()->andWhere(['folder_id' => $folderModel->id])->all();
                foreach ($folderPrintingMethodModels as $folderPrintingMethodModel) {
                    $folderPrintingMethodModelId[] = $folderPrintingMethodModel->printing_method_id;
                }
            } else {
                $folderModel = new Folder();
            }
        }

        /* ========= Scroll/Tube Model =========== */
        $scrollPaperModelId          = [];
        $scrollDieModelId            = [];
        $scrollBlockModelId          = [];
        $scrollPrintingMethodModelId = [];
        if (empty($scrollModel)) {
            $scrollModel = Scroll::find()->andWhere(['card_id' => [$id]])->one();
            if ( ! empty($scrollModel)) {
                //Tube Model
                $tubeModel = Tube::find()->andWhere(['scroll_id' => $scrollModel->id])->one();

                //Scroll Paper Model
                $scrollPaperModels = \common\models\ScrollPaper::find()->andWhere(['scroll_id' => $scrollModel->id])->all();
                foreach ($scrollPaperModels as $scrollPaperModel) {
                    $scrollPaperModelId[] = $scrollPaperModel->paper_id;
                }
                //Scroll Die Model
                $scrollDieModels = \common\models\ScrollDie::find()->andWhere(['scroll_id' => $scrollModel->id])->all();
                foreach ($scrollDieModels as $scrollDieModel) {
                    $scrollDieModelId[] = $scrollDieModel->die_id;
                }
                //Scroll Block Model
                $scrollBlockModels = \common\models\ScrollBlock::find()->andWhere(['scroll_id' => $scrollModel->id])->all();
                foreach ($scrollBlockModels as $scrollBlockModel) {
                    $scrollBlockModelId[] = $scrollBlockModel->block_id;
                }

                //Scroll Printing Method Model
                $scrollPrintingMethodModels = \common\models\ScrollPrintingMethod::find()->andWhere(['scroll_id' => $scrollModel->id])->all();
                foreach ($scrollPrintingMethodModels as $scrollPrintingMethodModel) {
                    $scrollPrintingMethodModelId[] = $scrollPrintingMethodModel->printing_method_id;
                }
            } else {
                $scrollModel = new Scroll();
                $tubeModel   = new Tube();
            }
        }

        /* ========= Box Model =========== */
        $boxPaperModelId          = [];
        $boxDieModelId            = [];
        $boxBlockModelId          = [];
        $boxPrintingMethodModelId = [];
        if (empty($boxModel)) {
            $boxModel = Box::find()->andWhere(['card_id' => [$id]])->one();
            if ( ! empty($boxModel)) {
                //Box Paper Model
                $boxPaperModels = \common\models\BoxPaper::find()->andWhere(['box_id' => $boxModel->id])->all();
                foreach ($boxPaperModels as $boxPaperModel) {
                    $boxPaperModelId[] = $boxPaperModel->paper_id;
                }
                //Box Die Model
                $boxDieModels = \common\models\BoxDie::find()->andWhere(['box_id' => $boxModel->id])->all();
                foreach ($boxDieModels as $boxDieModel) {
                    $boxDieModelId[] = $boxDieModel->die_id;
                }
                //Box Block Model
                $boxBlockModels = \common\models\BoxBlock::find()->andWhere(['box_id' => $boxModel->id])->all();
                foreach ($boxBlockModels as $boxBlockModel) {
                    $boxBlockModelId[] = $boxBlockModel->block_id;
                }

                //Box Printing Method Model
                $boxPrintingMethodModels = \common\models\BoxPrintingMethod::find()->andWhere(['box_id' => $boxModel->id])->all();
                foreach ($boxPrintingMethodModels as $boxPrintingMethodModel) {
                    $boxPrintingMethodModelId[] = $boxPrintingMethodModel->printing_method_id;
                }
            } else {
                $boxModel = new Box();
            }
        }

        /* ========= Insert Model =========== */
        $insertPaperModelId          = [];
        $insertDieModelId            = [];
        $insertBlockModelId          = [];
        $insertPrintingMethodModelId = [];
        if (empty($insertModels)) {
            $insertModels = Insert::find()->andWhere(['card_id' => $id])->all();
            if ( ! empty($insertModels)) {
                foreach ($insertModels as $key => $insertModel) {
                    //Insert Paper Model
                    $insertPaperModels = \common\models\InsertPaper::find()->andWhere(['insert_id' => $insertModel->id])->all();
                    foreach ($insertPaperModels as $insertPaperModel) {
                        $insertPaperModelId[$key][] = $insertPaperModel->paper_id;
                    }
                    //Insert Die Model
                    $insertDieModels = \common\models\InsertDie::find()->andWhere(['insert_id' => $insertModel->id])->all();
                    foreach ($insertDieModels as $insertDieModel) {
                        $insertDieModelId[$key][] = $insertDieModel->die_id;
                    }
                    //Insert Block Model
                    $insertBlockModels = \common\models\InsertBlock::find()->andWhere(['insert_id' => $insertModel->id])->all();
                    foreach ($insertBlockModels as $insertBlockModel) {
                        $insertBlockModelId[$key][] = $insertBlockModel->block_id;
                    }

                    //Insert Printing Method Model
                    $insertPrintingMethodModels = \common\models\InsertPrintingMethod::find()->andWhere(['insert_id' => $insertModel->id])->all();
                    foreach ($insertPrintingMethodModels as $insertPrintingMethodModel) {
                        $insertPrintingMethodModelId[$key][] = $insertPrintingMethodModel->printing_method_id;
                    }
                }
            } else {
                $insertModels = [new Insert()];
            }
        }

        /* ========= Additions Model =========== */
        if (empty($additionsModel)) {
            $additionsModel = Additions::find()->andWhere(['card_id' => $id])->one();
            if (empty($additionsModel)) {
                $additionsModel = new Additions();
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $envelopeDetails  = Yii::$app->request->post('Envelope'); //Get Envelope Section Data.
            $folderDetails    = Yii::$app->request->post('Folder'); //Get Folder Section Data.
            $scrollDetails    = Yii::$app->request->post('Scroll'); //Get Scroll Section Data.
            $boxDetails       = Yii::$app->request->post('Box'); //Get Box Section Data.
            $tubeDetails      = Yii::$app->request->post('Tube'); //Get Tube Section Data.
            $insertDetails    = Yii::$app->request->post('Insert'); //Get Insert Section Data.
            $additionsDetails = Yii::$app->request->post('Additions'); //Get Additions Section Data.
            $cardThemes       = Yii::$app->request->post('Card')['card_theme']; //Get CardTheme.

            /*             * ======================= Check Scroll Theme Exist or Not =============== * */
            $cardThemeExist = false; //Set a Flag for Scroll Theme.
            if ( ! empty($cardThemes)) {
                $themeArray  = array ();
                $themeModels = Theme::findAll(['id' => $cardThemes]);
                if ( ! empty($themeModels)) {
                    foreach ($themeModels as $themeModel) {
                        $themeArray[] = $themeModel->name;
                    }
                }
                if (in_array(Card::THEME_EXIST, $themeArray)) {
                    $cardThemeExist = true;
                }
            }

            $model->slug = preg_replace('/-+/', '-', str_replace(' ', '-', strtolower($model->name)));
            /*             * ======================= Card Theme Section =============== * */
            if ( ! empty($cardThemes)) {
                $cardThemeoldIDs     = ArrayHelper::map($cardThemeModels, 'theme_id', 'theme_id');
                $cardThemeDeletedIDs = array_diff($cardThemeoldIDs, array_filter(ArrayHelper::map($cardThemes, 'theme_id', 'theme_id')));
            } else {
                $cardThemeDeletedIDs = $cardThemeModels;
            }

            // validate all models
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->name = Card::find()->doCapitalize(strtolower($model->name));
                    $model->save();
                    $data        = ['id' => $model->id];

                    //CardTheme.
                    if ( ! empty($cardThemeDeletedIDs)) {
                        CardTheme::deleteAll(['theme_id' => $cardThemeDeletedIDs, 'card_id' => $id]);
                        foreach ($cardThemes as $cardTheme) {
                            $cardThemeModel           = new CardTheme();
                            $cardThemeModel->card_id  = $model->id;
                            $cardThemeModel->theme_id = $cardTheme;
                            $cardThemeModel->save();
                        }
                    }

                    //Envelope Details
                    if ( ! empty($envelopeModel->id)) {
                        Envelope::deleteAll(['id' => $envelopeModel->id, 'card_id' => $id]);
                        $envelopeModel = new Envelope(); //New Envelope Model Create.
                    }
                    CommonComponent::Envelope($envelopeModel, $envelopeDetails, $data);

                    //Folder Details
                    if ( ! empty($folderModel->id)) {
                        Folder::deleteAll(['id' => $folderModel->id, 'card_id' => $id]);
                        $folderModel = new Folder(); //New Folder Model Create.
                    }
                    CommonComponent::Folder($folderModel, $folderDetails, $data);

                    //Scroll/Tube Details
                    if ( ! empty($scrollModel->id)) {
                        Scroll::deleteAll(['id' => $scrollModel->id, 'card_id' => $id]);
                        $scrollModel = new Scroll(); //New Scroll Model Create.
                        $tubeModel   = new Tube(); //New Tube Model Create.
                    }
                    if ($cardThemeExist) {
                        CommonComponent::ScrollTube($scrollModel, $scrollDetails, $tubeModel, $tubeDetails, $data);
                    }

                    //Box Details
                    if ( ! empty($boxModel->id)) {
                        Box::deleteAll(['id' => $boxModel->id, 'card_id' => $id]);
                        $boxModel = new Box(); //New Box Model Create.
                    }
                    if ($cardThemeExist) {
                        CommonComponent::Box($boxModel, $boxDetails, $data);
                    }

                    //Insert Details
                    if ( ! empty($insertModels)) {
                        foreach ($insertModels as $insertModel) {
                            if ( ! empty($insertModel->id)) {
                                Insert::deleteAll(['id' => $insertModel->id, 'card_id' => $id]);
                            }
                        }
                        $insertModels = [new Insert()]; //New Insert Model Create.
                    }
                    CommonComponent::Insert($insertModels, $insertDetails, $data);

                    //Additions Details
                    if ( ! empty($additionsModel->id)) {
                        Additions::deleteAll(['id' => $additionsModel->id, 'card_id' => $id]);
                        $additionsModel = new Additions(); //New Additions Model Create.
                    }
                    CommonComponent::Additions($additionsModel, $additionsDetails, $data);

                    /* ## Update CardInfo ## */
                    if ( ! empty($model->id)) {
                        \common\models\CardInfo::updateCardInfo($id);
                    }

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Card Update Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
                    'papers' => $papers,
                    'themes' => $themes,
                    'dies' => $dies,
                    'colors' => $colors,
                    'paperTypes' => $paperTypes,
                    'blocks' => $blocks,
                    'printingMethods' => $printingMethods,
                    'envelopeModel' => ! empty($envelopeModel) ? $envelopeModel : new Envelope(),
                    'folderModel' => ! empty($folderModel) ? $folderModel : new Folder(),
                    'scrollModel' => ! empty($scrollModel) ? $scrollModel : new Scroll(),
                    'boxModel' => ! empty($boxModel) ? $boxModel : new Box(),
                    'tubeModel' => ! empty($tubeModel) ? $tubeModel : new Tube(),
                    'insertModels' => ! empty($insertModels) ? $insertModels : [new Insert()],
                    'tassles' => $tassles,
                    'ribbons' => $ribbons,
                    'rhinestones' => $rhinestones,
                    'boxTypes' => $boxTypes,
                    'additionsModel' => ! empty($additionsModel) ? $additionsModel : new Additions(),
                    'cardThemeModelId' => $cardThemeModelId,
                    'envelopePaperModelId' => $envelopePaperModelId,
                    'envelopeDieModelId' => $envelopeDieModelId,
                    'envelopeBlockModelId' => $envelopeBlockModelId,
                    'envelopePrintingMethodModelId' => $envelopePrintingMethodModelId,
                    'folderPaperModelId' => $folderPaperModelId,
                    'folderDieModelId' => $folderDieModelId,
                    'folderBlockModelId' => $folderBlockModelId,
                    'folderPrintingMethodModelId' => $folderPrintingMethodModelId,
                    'scrollPaperModelId' => $scrollPaperModelId,
                    'scrollDieModelId' => $scrollDieModelId,
                    'scrollBlockModelId' => $scrollBlockModelId,
                    'scrollPrintingMethodModelId' => $scrollPrintingMethodModelId,
                    'boxPaperModelId' => $boxPaperModelId,
                    'boxDieModelId' => $boxDieModelId,
                    'boxBlockModelId' => $boxBlockModelId,
                    'boxPrintingMethodModelId' => $boxPrintingMethodModelId,
                    'insertPaperModelId' => $insertPaperModelId,
                    'insertDieModelId' => $insertDieModelId,
                    'insertBlockModelId' => $insertBlockModelId,
                    'insertPrintingMethodModelId' => $insertPrintingMethodModelId,
        ]);
    }

    /**
     * GET All DataInfo of The Select Card PaperCode in Card.
     * @param type $card_paper
     * @return type
     */
    public function actionCardPaper ($card_paper)
    {
        $paperInfo = Paper::find()->getPaperInfo($card_paper);
        return Json::encode($paperInfo);
    }

    /**
     * GET All DataInfo of The Select CardTheme in Card.
     * @param type $card_theme
     * @return type
     */
    public function actionCardTheme ()
    {
        if ( ! empty(Yii::$app->request->post('card_theme'))) {
            $themeArray  = array ();
            $themeModels = Theme::findAll(['id' => Yii::$app->request->post('card_theme')]);
            foreach ($themeModels as $themeModel) {
                $themeArray[] = $themeModel->name;
            }
            return Json::encode($themeArray);
        }
    }

    /**
     * Deletes an existing Card model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == Card::STATUS_DELETE) ? Card::STATUS_ACTIVE : Card::STATUS_DELETE;
        $model->save(false);
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Card model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Card the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = Card::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Get Card List
     * @return type
     */
    public function actionList ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $cards                       = Card::find()->getList();
        $cards                       += [0 => 'Select Card'];
        return $cards;
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        //$searchModel = new CardSearch();
        //$results     = $searchModel->dataTable($searchParams);
        $searchModel = new \backend\models\CardInfoSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {
            //Getting FolderPaper Colors.
            $folderJsonArray  = json_decode($result['folder'], TRUE);
            $folderPaperArray = array ();
            if ( ! empty($folderJsonArray)) {
                foreach ($folderJsonArray as $folderArray) {
                    $folderPaperArray[] = $folderArray['paper_color'];
                }
            }
            $folderPaperColors = implode(', ', $folderPaperArray);

            //Getting ScrollPaper Colors.
            $scrollJsonArray  = json_decode($result['scroll'], TRUE);
            $scrollPaperArray = array ();
            if ( ! empty($scrollJsonArray)) {
                foreach ($scrollJsonArray as $scrollArray) {
                    $scrollPaperArray[] = $scrollArray['paper_color'];
                }
            }
            $scrollPaperColors = implode(', ', $scrollPaperArray);

            $view   = Html::getViewButton(['card/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['card/update', 'id' => $result['id']]);

            if ($result['status'] == Card::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . Card::getConstantList('STATUS_', Card::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . Card::getConstantList('STATUS_', Card::className())[$result['status']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['card/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == Card::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['name'],
                ! empty($folderPaperColors) ? $folderPaperColors : ( ! empty($scrollPaperColors) ? $scrollPaperColors : ''),
                $result['length'],
                $result['width'],
                $result['theme'],
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
