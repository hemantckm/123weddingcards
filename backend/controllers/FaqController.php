<?php

namespace backend\controllers;

use Yii;
use common\models\FaqType;
use common\models\Faq;
use backend\models\FaqSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use backend\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * FaqController implements the CRUD actions for Faq model.
 */
class FaqController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Faq models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new FaqSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new Faq();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\Faq::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single Faq model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        $model                   = $this->findModel($id);
        $feature_images          = array ();
        $time                    = '?r=' . time();
        $feature_images['small'] = FaqType::NO_IMAGE_SMALL . $time;
        $feature_images['full']  = FaqType::NO_IMAGE_FULL . $time;
        if ( ! empty($model->feature_image)) {
            $originalFilePath = '/' . Faq::MAIN_DIRECTORY . Faq::ORIGINAL_FILEPATH . $model->feature_image;
            $thumbFilePath    = '/' . Faq::MAIN_DIRECTORY . Faq::THUMBNAIL_FILEPATH . $model->feature_image;
            if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                $feature_images['small'] = $thumbFilePath . $time;
                $feature_images['full']  = $originalFilePath . $time;
            }
        }
        $feature_image = Html::a(Html::img(Yii::getAlias('@web') . $feature_images['small'], ['class' => 'img-thumbnail', 'style' => 'margin-bottom:10px']), Yii::getAlias('@web') . $feature_images['full'], ['data-fancybox' => 'gallery']);

        //FAQ FAQTypes.
        $faqFaqTypes      = array ();
        $faqFaqTypeModels = \common\models\FaqFaqType::find()->andWhere(['faq_id' => [$id]])->all();
        if ( ! empty($faqFaqTypeModels)) {
            foreach ($faqFaqTypeModels as $faqFaqTypeModel) {
                $faqTypes      = FaqType::find()->andWhere(['id' => $faqFaqTypeModel->faq_type_id])->one();
                $faqFaqTypes[] = $faqTypes->name;
            }
        }

        return $this->render('view', [
                    'model' => $model,
                    'faqFaqTypes' => $faqFaqTypes,
                    'feature_image' => $feature_image,
        ]);
    }

    /**
     * Creates a new Faq model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model    = new Faq();
        $faqTypes = FaqType::find()->getList(); //Get the list of Faq Type.

        if ($model->load(Yii::$app->request->post())) {
            $faqFaqTypes = Yii::$app->request->post('Faq')['faq_faq_type']; //Get Faq FaqTypes.

            /**
             * FAQ Feature Image Upload.
             */
            if ($model->feature_image = UploadedFile::getInstance($model, 'feature_image')) {
                $model->feature_image = $model->upload();
            }

            /* check if any other FAQ set default or not. */
            if ($model->default) {
                $faqModel = Faq::find()->andWhere(['default' => Faq::DEFAULT_ACTIVE])->one();
                if ( ! empty($faqModel)) {
                    Faq::updateAll(['default' => Faq::DEFAULT_INACTIVE], ['id' => $faqModel->id]);
                }
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->heading = ucwords(strtolower($model->heading));
                    $model->save();

                    //Faq FaqType.
                    if ( ! empty($faqFaqTypes)) {
                        foreach ($faqFaqTypes as $faqFaqType) {
                            $faqFaqTypeModel              = new \common\models\FaqFaqType();
                            $faqFaqTypeModel->faq_id      = $model->id;
                            $faqFaqTypeModel->faq_type_id = $faqFaqType;
                            $faqFaqTypeModel->save();
                        }
                    }

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Faq Create Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
                    'faqTypes' => $faqTypes,
        ]);
    }

    /**
     * Updates an existing Faq model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model    = $this->findModel($id);
        $faqTypes = FaqType::find()->getList(); //Get the list of Faq Type.

        /* ========= Faq FaqType =========== */
        $faqFaqTypeModelId = [];
        if (empty($faqFaqTypeModels)) {
            /* ======== Faq FaqType Model ======== */
            $faqFaqTypeModels = \common\models\FaqFaqType::find()->getFaqFaqType($id);
            foreach ($faqFaqTypeModels as $faqFaqTypeModel) {
                /* ======== Fetch FaqType ID From Faq FaqType Model ======== */
                $faqFaqTypeModelId[] = $faqFaqTypeModel['faq_type_id'];
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $faqFaqTypes = Yii::$app->request->post('Faq')['faq_faq_type']; //Get Faq FaqTypes.
            /*             * ======================= Faq FaqType Section =============== * */
            if ( ! empty($faqFaqTypes)) {
                $faqFaqTypeoldIDs     = ArrayHelper::map($faqFaqTypeModels, 'faq_type_id', 'faq_type_id');
                $faqFaqTypeDeletedIDs = array_diff($faqFaqTypeoldIDs, array_filter(ArrayHelper::map($faqFaqTypes, 'faq_type_id', 'faq_type_id')));
            } else {
                $faqFaqTypeDeletedIDs = $faqFaqTypeModels;
            }

            /**
             * FAQ Feature Image Upload.
             */
            $model->feature_image = UploadedFile::getInstance($model, "feature_image");
            $oldFeatureImage      = $model->getOldAttribute('feature_image');
            if (empty($model->feature_image)) {
                $model->feature_image = $oldFeatureImage;
            } else {
                if ( ! empty($oldFeatureImage)) {
                    $originalFilePath = '/' . Faq::MAIN_DIRECTORY . Faq::ORIGINAL_FILEPATH . $oldFeatureImage;
                    $thumbFilePath    = '/' . Faq::MAIN_DIRECTORY . Faq::THUMBNAIL_FILEPATH . $oldFeatureImage;
                    if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                        unlink(Yii::$app->basePath . '/web' . $originalFilePath);
                        unlink(Yii::$app->basePath . '/web' . $thumbFilePath);
                    }
                }
                $model->feature_image = $model->upload();
            }

            /* check if any other FAQ set default or not. */
            if ($model->default) {
                $faqModel = Faq::find()->andWhere(['default' => Faq::DEFAULT_ACTIVE])->one();
                if ( ! empty($faqModel)) {
                    Faq::updateAll(['default' => Faq::DEFAULT_INACTIVE], ['id' => $faqModel->id]);
                }
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->heading = ucwords(strtolower($model->heading));
                    $model->save();

                    //Faq FaqType.
                    if ( ! empty($faqFaqTypeDeletedIDs)) {
                        \common\models\FaqFaqType::deleteAll(['faq_type_id' => $faqFaqTypeDeletedIDs, 'faq_id' => $id]);
                        foreach ($faqFaqTypes as $faqFaqType) {
                            $faqFaqTypeModel              = new \common\models\FaqFaqType();
                            $faqFaqTypeModel->faq_id      = $model->id;
                            $faqFaqTypeModel->faq_type_id = $faqFaqType;
                            $faqFaqTypeModel->save();
                        }
                    }

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Faq Update Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
                    'faqTypes' => $faqTypes,
                    'faqFaqTypeModelId' => $faqFaqTypeModelId,
        ]);
    }

    /**
     * Deletes an existing Faq model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == Faq::STATUS_DELETE) ? Faq::STATUS_ACTIVE : Faq::STATUS_DELETE;
        $model->save();
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Faq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faq the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = Faq::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Get FAQ List
     * @return type
     */
    public function actionList ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $faqs                        = Faq::find()->getList();
        $faqs                        += [0 => 'Select FAQ'];
        return $faqs;
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        $searchModel = new FaqSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {

            $view   = Html::getViewButton(['faq/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['faq/update', 'id' => $result['id']]);

            /**
             * FAQ Feature Image Show.
             */
            $feature_images          = array ();
            $time                    = '?r=' . time();
            $feature_images['small'] = FaqType::NO_IMAGE_SMALL . $time;
            $feature_images['full']  = FaqType::NO_IMAGE_FULL . $time;
            if ( ! empty($result['feature_image'])) {
                $originalFilePath = '/' . Faq::MAIN_DIRECTORY . Faq::ORIGINAL_FILEPATH . $result['feature_image'];
                $thumbFilePath    = '/' . Faq::MAIN_DIRECTORY . Faq::THUMBNAIL_FILEPATH . $result['feature_image'];
                if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                    $feature_images['small'] = $thumbFilePath . $time;
                    $feature_images['full']  = $originalFilePath . $time;
                }
            }
            $feature_image = Html::a(Html::img(Yii::getAlias('@web') . $feature_images['small'], ['class' => 'img-thumbnail']), Yii::getAlias('@web') . $feature_images['full'], ['data-fancybox' => 'gallery']);

            /**
             * FAQ Status.
             */
            if ($result['status'] == Faq::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . Faq::getConstantList('STATUS_', Faq::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . Faq::getConstantList('STATUS_', Faq::className())[$result['status']] . '</span>';
            }

            /**
             * FAQ Default.
             */
            if ($result['default'] == Faq::DEFAULT_ACTIVE) {
                $default = '<span class="label label-success">' . Faq::getConstantList('DEFAULT_', Faq::className())[$result['default']] . '</span>';
            } else {
                $default = '<span class="label label-danger">' . Faq::getConstantList('DEFAULT_', Faq::className())[$result['default']] . '</span>';
            }

            /**
             * FAQ FAQTypes.
             */
            $faqFaqTypes      = array ();
            $faqFaqTypeModels = \common\models\FaqFaqType::find()->andWhere(['faq_id' => [$result['id']]])->all();
            if ( ! empty($faqFaqTypeModels)) {
                foreach ($faqFaqTypeModels as $faqFaqTypeModel) {
                    $faqTypes      = FaqType::find()->andWhere(['id' => $faqFaqTypeModel->faq_type_id])->one();
                    $faqFaqTypes[] = $faqTypes->name;
                }
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['faq/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == Faq::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['heading'],
                ! empty($faqFaqTypes) ? implode(', ', $faqFaqTypes) : '',
                $feature_image,
                $default,
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
