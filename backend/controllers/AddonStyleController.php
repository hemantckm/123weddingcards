<?php

namespace backend\controllers;

use Yii;
use common\models\AddonStyle;
use backend\models\AddonStyleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use backend\helpers\Html;

/**
 * AddonStyleController implements the CRUD actions for AddonStyle model.
 */
class AddonStyleController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AddonStyle models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new AddonStyleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new AddonStyle();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\AddonStyle::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single AddonStyle model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AddonStyle model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model      = new AddonStyle();
        $addonTypes = \common\models\AddonType::find()->getList(); //Get AddonType List.

        if ($model->load(Yii::$app->request->post())) {
            // validate all models
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->name = ucwords($model->name);
                    $model->save();
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Addon Style Create Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
                    'addonTypes' => $addonTypes,
        ]);
    }

    /**
     * Updates an existing AddonStyle model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model      = $this->findModel($id);
        $addonTypes = \common\models\AddonType::find()->getList(); //Get AddonType List.

        if ($model->load(Yii::$app->request->post())) {
            // validate all models
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->name = ucwords($model->name);
                    $model->save();
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Addon Style Update Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
                    'addonTypes' => $addonTypes,
        ]);
    }

    /**
     * Deletes an existing AddonStyle model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == AddonStyle::STATUS_DELETE) ? AddonStyle::STATUS_ACTIVE : AddonStyle::STATUS_DELETE;
        $model->save();
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the AddonStyle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AddonStyle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = AddonStyle::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Get AddonStyle List
     * @return type
     */
    public function actionList ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $addonStyles                 = AddonStyle::find()->getList();
        $addonStyles                 += [0 => 'Select Addon Style'];
        return $addonStyles;
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        $searchModel = new AddonStyleSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {

            $view   = Html::getViewButton(['addon-style/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['addon-style/update', 'id' => $result['id']]);

            if ($result['status'] == AddonStyle::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . AddonStyle::getConstantList('STATUS_', AddonStyle::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . AddonStyle::getConstantList('STATUS_', AddonStyle::className())[$result['status']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['addon-style/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == AddonStyle::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['name'],
                $result['addon_type_name'],
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
