<?php

namespace backend\controllers;

use Yii;
use common\models\Banner;
use backend\models\BannerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use backend\helpers\Html;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new Banner();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\Banner::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single Banner model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        $model           = $this->findModel($id);
        $images          = array ();
        $time            = '?r=' . time();
        $images['small'] = \common\models\FaqType::NO_IMAGE_SMALL . $time;
        $images['full']  = \common\models\FaqType::NO_IMAGE_FULL . $time;
        if ( ! empty($model->image)) {
            $originalFilePath = '/' . Banner::MAIN_DIRECTORY . Banner::ORIGINAL_FILEPATH . $model->image;
            $thumbFilePath    = '/' . Banner::MAIN_DIRECTORY . Banner::THUMBNAIL_FILEPATH . $model->image;
            if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                $images['small'] = $thumbFilePath . $time;
                $images['full']  = $originalFilePath . $time;
            }
        }
        $image = Html::a(Html::img(Yii::getAlias('@web') . $images['small'], ['class' => 'img-thumbnail', 'style' => 'margin-bottom:10px']), Yii::getAlias('@web') . $images['full'], ['data-fancybox' => 'gallery']);


        return $this->render('view', [
                    'model' => $model,
                    'image' => $image,
        ]);
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model = new Banner();

        if ($model->load(Yii::$app->request->post())) {
            /**
             * Banner Feature Image Upload.
             */
            if ($model->image = UploadedFile::getInstance($model, 'image')) {
                $model->image = $model->upload();
            }

            /* check if any other Banner set default or not. */
            if ($model->default) {
                $bannerModel = Banner::find()->andWhere(['default' => Banner::DEFAULT_ACTIVE])->one();
                if ( ! empty($bannerModel)) {
                    Banner::updateAll(['default' => Banner::DEFAULT_INACTIVE], ['id' => $bannerModel->id]);
                }
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->heading = ucwords(strtolower($model->heading));
                    $model->save();

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Banner Create Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            /**
             * Banner Image Upload.
             */
            $model->image = UploadedFile::getInstance($model, "image");
            $oldImage     = $model->getOldAttribute('image');
            if (empty($model->image)) {
                $model->image = $oldImage;
            } else {
                if ( ! empty($oldImage)) {
                    $originalFilePath = '/' . Banner::MAIN_DIRECTORY . Banner::ORIGINAL_FILEPATH . $oldImage;
                    $thumbFilePath    = '/' . Banner::MAIN_DIRECTORY . Banner::THUMBNAIL_FILEPATH . $oldImage;
                    if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                        unlink(Yii::$app->basePath . '/web' . $originalFilePath);
                        unlink(Yii::$app->basePath . '/web' . $thumbFilePath);
                    }
                }
                $model->image = $model->upload();
            }

            /* check if any other Banner set default or not. */
            if ($model->default) {
                $bannerModel = Banner::find()->andWhere(['default' => Banner::DEFAULT_ACTIVE])->one();
                if ( ! empty($bannerModel)) {
                    Banner::updateAll(['default' => Banner::DEFAULT_INACTIVE], ['id' => $bannerModel->id]);
                }
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->heading = ucwords(strtolower($model->heading));
                    $model->save();

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Banner Update Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == Banner::STATUS_DELETE) ? Banner::STATUS_ACTIVE : Banner::STATUS_DELETE;
        $model->save();
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        $searchModel = new BannerSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {

            $view   = Html::getViewButton(['banner/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['banner/update', 'id' => $result['id']]);

            /**
             * Banner Feature Image Show.
             */
            $images          = array ();
            $time            = '?r=' . time();
            $images['small'] = \common\models\FaqType::NO_IMAGE_SMALL . $time;
            $images['full']  = \common\models\FaqType::NO_IMAGE_FULL . $time;
            if ( ! empty($result['image'])) {
                $originalFilePath = '/' . Banner::MAIN_DIRECTORY . Banner::ORIGINAL_FILEPATH . $result['image'];
                $thumbFilePath    = '/' . Banner::MAIN_DIRECTORY . Banner::THUMBNAIL_FILEPATH . $result['image'];
                if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                    $images['small'] = $thumbFilePath . $time;
                    $images['full']  = $originalFilePath . $time;
                }
            }
            $image = Html::a(Html::img(Yii::getAlias('@web') . $images['small'], ['class' => 'img-thumbnail']), Yii::getAlias('@web') . $images['full'], ['data-fancybox' => 'gallery']);

            /**
             * Banner Status.
             */
            if ($result['status'] == Banner::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . Banner::getConstantList('STATUS_', Banner::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . Banner::getConstantList('STATUS_', Banner::className())[$result['status']] . '</span>';
            }

            /**
             * Banner Default.
             */
            if ($result['default'] == Banner::DEFAULT_ACTIVE) {
                $default = '<span class="label label-success">' . Banner::getConstantList('DEFAULT_', Banner::className())[$result['default']] . '</span>';
            } else {
                $default = '<span class="label label-danger">' . Banner::getConstantList('DEFAULT_', Banner::className())[$result['default']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['banner/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == Banner::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['heading'],
                $image,
                $default,
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
