<?php

namespace backend\controllers;

use Yii;

ini_set('memory_limit', '-1');

class DataImporterController extends \yii\web\Controller
{

    public function actionIndex ()
    {
        //$this->actionWebsite(); //WebSite Data Import.
        //$this->actionTheme(); //Theme Data Import.
        //$this->actionPaperType(); //PaperType Data Import.
        //$this->actionPrintingMethod(); //PrintingMethod Data Import.
        //$this->actionDieCut(); //DieCut Data Import.
        //$this->actionBlock(); //Block Data Import.
        //$this->actionTassle(); //Tassle Data Import.
        //$this->actionRibbon(); //Ribbon Data Import.
        //$this->actionRhinestone(); //Rhinestone Data Import.
        //$this->actionColor(); //Color Data Import.
        //$this->actionPaper(); //Paper Data Import.
        $this->actionBoxType(); //BoxType Data Import.
    }

    /**
     * Website Data Importer.
     */
    public function actionWebsite ()
    {
        $obj     = new \common\models\Website();
        $mapping = [
            'id' => 'website_id',
            'name' => 'short_name',
            'url' => 'website_name',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstWebsiteInfo.csv', $obj, $mapping);
    }

    /**
     * Theme Data Importer.
     */
    public function actionTheme ()
    {
        $obj     = new \common\models\Theme();
        $mapping = [
            'id' => 'theme_id',
            'name' => 'theme_name',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstThemeInfo.csv', $obj, $mapping);
    }

    /**
     * PaperType Data Importer.
     */
    public function actionPaperType ()
    {
        $obj     = new \common\models\PaperType();
        $mapping = [
            'id' => 'paper_type_id',
            'name' => 'paper_type_name',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstPaperTypeInfo.csv', $obj, $mapping);
    }

    /**
     * PrintingMethod Data Importer.
     */
    public function actionPrintingMethod ()
    {
        $obj     = new \common\models\PrintingMethod();
        $mapping = [
            'id' => 'process_id',
            'name' => 'process_name',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstProcessInfo.csv', $obj, $mapping);
    }

    /**
     * DieCut Data Importer.
     */
    public function actionDieCut ()
    {
        $obj     = new \common\models\DieCut();
        $mapping = [
            'id' => 'die_id',
            'code' => 'die_code',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstDieCutInfo.csv', $obj, $mapping);
    }

    /**
     * Block Data Importer.
     */
    public function actionBlock ()
    {
        $obj     = new \common\models\Block();
        $mapping = [
            'id' => 'block_id',
            'code' => 'block_code',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstBlockInfo.csv', $obj, $mapping);
    }

    /**
     * Tassle Data Importer.
     */
    public function actionTassle ()
    {
        $obj     = new \common\models\Tassle();
        $mapping = [
            'id' => 'tassle_id',
            'code' => 'tassle_code',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstTassleInfo.csv', $obj, $mapping);
    }

    /**
     * Ribbon Data Importer.
     */
    public function actionRibbon ()
    {
        $obj     = new \common\models\Ribbon();
        $mapping = [
            'id' => 'ribbon_id',
            'code' => 'ribbon_code',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstRibbonInfo.csv', $obj, $mapping);
    }

    /**
     * Rhinestone Data Importer.
     */
    public function actionRhinestone ()
    {
        $obj     = new \common\models\Rhinestone();
        $mapping = [
            'id' => 'rhinestone_id',
            'code' => 'rhinestone_code',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstRhinestoneInfo.csv', $obj, $mapping);
    }

    /**
     * Color Data Importer.
     */
    public function actionColor ()
    {
        $obj     = new \common\models\Color();
        $mapping = [
            'id' => 'material_color_id',
            'name' => 'material_color_name',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstMaterialColorInfo.csv', $obj, $mapping);
    }

    /**
     * Paper Data Importer.
     */
    public function actionPaper ()
    {
        $obj     = new \common\models\Paper();
        $mapping = [
            'id' => 'paper_id',
            'code' => 'paper_code',
            'paper_type_id' => 'paper_type_id',
            'color_id' => 'material_color_id',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstPaperInfo.csv', $obj, $mapping);
    }

    /**
     * BoxType Data Importer.
     */
    public function actionBoxType ()
    {
        $obj     = new \common\models\BoxType();
        $mapping = [
            'id' => 'boxtype_id',
            'name' => 'boxtype_name',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstBoxTypeInfo.csv', $obj, $mapping);
    }

    /**
     * Common Function
     * @param type $file
     * @param type $modelObj
     * @param type $mapping
     */
    public static function migrateData ($file, $modelObj, $mapping)
    {
        $filePath    = Yii::getAlias('@backend') . "/runtime/database/" . $file;
        $handle      = fopen($filePath, 'r');
        $columns     = [];
        $dateTime    = ['dob', 'dow', 'dateJoining', 'dateRelieving', 'DepenDOB'];
        $transaction = \Yii::$app->db->beginTransaction();

        try {
            while (($row = fgetcsv($handle, 1000, ',')) !== false)
            {

                if (empty($columns)) {
                    $columns = self::getColumnsFromTitle($row);
                    continue;
                }
                $model = clone $modelObj;
                foreach ($mapping as $key => $value) {
                    $fieldVal = $row[$columns[$value]] == 'true' ? 1 : ($row[$columns[$value]] == 'false' ? 0 : $row[$columns[$value]]);

                    //Set Timestamp Format.
                    if (in_array($value, $dateTime) && ! empty($row[$columns[$value]])) {
                        $fieldVal = strtotime($row[$columns[$value]]);
                    }

                    //Set Model Files with values.
                    $model->$key = $fieldVal;
                }

                //Its use for Theme, Printing Method, Paper Type,Box Type
                $model->name = ucwords(strtolower(preg_replace('/[^a-zA-Z0-9_]/s', ' ', $model->name)));

                //Its use for Ribbon,Rhinestone,Paper
                //$model->code = strtoupper($model->code);
                //Its use for Color
                //$model->name = ucwords(strtolower($model->name));
                //echo "<pre>" . print_r($model, TRUE) . "</pre>";
                //die;
                //Save Table Data.
                if ( ! $model->save()) {
                    echo $file . "\n";
                    echo "<pre>";
                    print_r($row);
                    print_r($model->getErrors());
                    die;
                }
            }

            $transaction->commit();
        } catch (\Exception $e) {
            echo $e->getMessage();
            $transaction->rollBack();
        }
    }

    /**
     * Getting Columns From Title.
     * @param array $titleRow
     * @return type
     */
    public static function getColumnsFromTitle (array $titleRow)
    {
        $columns = [];
        foreach ($titleRow as $key => $value) {
            if ( ! empty($value)) {
                $columns[trim($value)] = $key;
            }
        }
        return $columns;
    }

}
