<?php

namespace backend\controllers;

use Yii;

ini_set('memory_limit', '-1');

class CardImporterController extends \yii\web\Controller
{

    public function actionIndex ()
    {
        //$this->actionCard(); //Card Data Import
        //$this->actionCardTheme(); //CardTheme Data Import
        //$this->actionEnvelope(); //Envelope Data Import
        //$this->actionEnvelopePaper(); //EnvelopePaper Data Import
        //$this->actionEnvelopeDie(); //EnvelopeDie Data Import
        //$this->actionEnvelopeBlock(); //EnvelopeBlock Data Import
        //$this->actionEnvelopePrintingMethod(); //EnvelopePrintingMethod Data Import
        //$this->actionFolder(); //Folder Data Import
        //$this->actionFolderPaper(); //FolderPape Data Import
        //$this->actionFolderDie(); //FolderDie Data Import
        //$this->actionFolderBlock(); //FolderBlock Data Import
        //$this->actionFolderPrintingMethod(); //FolderPrintingMethod Data Import
        //$this->actionTube(); //Tube Data Import
        //$this->actionBox(); //Box Data Import
        //$this->actionBoxBlock(); //BoxBlock Data Import
        //$this->actionBoxPaper(); //BoxPaper Data Import
        //$this->actionBoxDie(); //BoxDie Data Import
        //$this->actionBoxPrintingMethod(); //BoxPrintingMethod Data Import
        //$this->actionAdditions(); //Additions Data Import
        //$this->actionInsert(); //Insert Data Import
        //$this->actionInsertPaper(); //InsertPaper Data Import
        //$this->actionInsertBlock(); //InsertBlock Data Import
        //$this->actionInsertDie(); //InsertDie Data Import
        //$this->actionInsertPrintingMethod(); //InsertPrintingMethod Data Import
    }

    /**
     * Card Data Importer.
     */
    public function actionCard ()
    {
        $obj     = new \common\models\Card();
        $mapping = [
            'id' => 'card_id',
            'name' => 'card_code',
            //'code' => 'folder_paper_color_id',
            'code' => 'search_folder_paper_color_id',
            'sample_price' => 'sample_price',
            'bulk_price' => 'bulk_price',
            'print_charge' => 'printing_charges',
            'length' => 'card_length',
            'width' => 'card_width',
            'thickness' => 'card_thickness',
            'weight' => 'card_weight',
            'created_at' => 'date_created',
            'updated_at' => 'date_modified',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * CardTheme Data Importer.
     */
    public function actionCardTheme ()
    {
        $obj     = new \common\models\CardTheme();
        $mapping = [
            'card_id' => 'card_id',
            'theme_id' => 'theme_id',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * Envelope Data Importer.
     */
    public function actionEnvelope ()
    {
        $obj     = new \common\models\Envelope();
        $mapping = [
            'card_id' => 'card_id',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * EnvelopePaper Data Importer.
     */
    public function actionEnvelopePaper ()
    {
        $obj     = new \common\models\EnvelopePaper();
        $mapping = [
            'envelope_id' => 'card_id',
            'paper_id' => 'env_paper_id',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * EnvelopeDie Data Importer.
     */
    public function actionEnvelopeDie ()
    {
        $obj     = new \common\models\EnvelopeDie();
        $mapping = [
            'envelope_id' => 'card_id',
            'die_id' => 'env_die_id',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * EnvelopeBlock Data Importer.
     */
    public function actionEnvelopeBlock ()
    {
        $obj     = new \common\models\EnvelopeBlock();
        $mapping = [
            'envelope_id' => 'card_id',
            'block_id' => 'env_block_id',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * EnvelopePrintingMethod Data Importer.
     */
    public function actionEnvelopePrintingMethod ()
    {
        $obj     = new \common\models\EnvelopePrintingMethod();
        $mapping = [
            'envelope_id' => 'card_id',
            'printing_method_id' => 'env_process_id',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * Folder Data Importer.
     */
    public function actionFolder ()
    {
        $obj     = new \common\models\Folder();
        $mapping = [
            'card_id' => 'card_id',
            'status' => 'card_type',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * FolderPaper Data Importer.
     */
    public function actionFolderPaper ()
    {
        $obj     = new \common\models\FolderPaper();
        $mapping = [
            'folder_id' => 'card_id',
            'paper_id' => 'folder_paper_id',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * FolderDie Data Importer.
     */
    public function actionFolderDie ()
    {
        $obj     = new \common\models\FolderDie();
        $mapping = [
            'folder_id' => 'card_id',
            'die_id' => 'folder_die_id',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * FolderBlock Data Importer.
     */
    public function actionFolderBlock ()
    {
        $obj     = new \common\models\FolderBlock();
        $mapping = [
            'folder_id' => 'card_id',
            'block_id' => 'folder_block_id',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * FolderPrintingMethod Data Importer.
     */
    public function actionFolderPrintingMethod ()
    {
        $obj     = new \common\models\FolderPrintingMethod();
        $mapping = [
            'folder_id' => 'card_id',
            'printing_method_id' => 'folder_process_id',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * Scroll Tube Data Importer.
     */
    public function actionTube ()
    {
        $obj     = new \common\models\Tube();
        $mapping = [
            'scroll_id' => 'card_id',
            'length' => 'scroll_tube_length',
            'weight' => 'scroll_tube_weight',
            'diameter' => 'scroll_tube_diameter',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * Boxes Data Importer.
     */
    public function actionBox ()
    {
        $obj     = new \common\models\Box();
        $mapping = [
            'id' => 'scroll_box_id',
            'card_id' => 'card_id',
            'box_type_id' => 'box_type_id',
            'length' => 'box_length',
            'width' => 'box_width',
            'thickness' => 'box_thickness',
        ];
        self::migrateData('dbo_MstBoxInfo.csv', $obj, $mapping);
    }

    /**
     * BoxBlock Data Importer.
     */
    public function actionBoxBlock ()
    {
        $obj     = new \common\models\BoxBlock();
        $mapping = [
            'box_id' => 'card_id',
            'block_id' => 'box_block_id',
        ];
        self::migrateData('dbo_MstBoxInfo.csv', $obj, $mapping);
    }

    /**
     * BoxPaper Data Importer.
     */
    public function actionBoxPaper ()
    {
        $obj     = new \common\models\BoxPaper();
        $mapping = [
            'box_id' => 'card_id',
            'paper_id' => 'box_paper_id',
        ];
        self::migrateData('dbo_MstBoxInfo.csv', $obj, $mapping);
    }

    /**
     * BoxDie Data Importer.
     */
    public function actionBoxDie ()
    {
        $obj     = new \common\models\BoxDie();
        $mapping = [
            'box_id' => 'card_id',
            'die_id' => 'box_die_id',
        ];
        self::migrateData('dbo_MstBoxInfo.csv', $obj, $mapping);
    }

    /**
     * BoxPrintingMethod Data Importer.
     */
    public function actionBoxPrintingMethod ()
    {
        $obj     = new \common\models\BoxPrintingMethod();
        $mapping = [
            'box_id' => 'card_id',
            'printing_method_id' => 'box_process_id',
        ];
        self::migrateData('dbo_MstBoxInfo.csv', $obj, $mapping);
    }

    /**
     * Insert Data Importer.
     */
    public function actionInsert ()
    {
        $obj     = new \common\models\Insert();
        $mapping = [
            'id' => 'insert_id',
            'card_id' => 'card_id',
        ];
        self::migrateData('dbo_MstInsertInfo.csv', $obj, $mapping);
    }

    /**
     * InsertPaper Data Importer.
     */
    public function actionInsertPaper ()
    {
        $obj     = new \common\models\InsertPaper();
        $mapping = [
            'insert_id' => 'card_id',
            'paper_id' => 'insert_paper_id',
        ];
        self::migrateData('dbo_MstInsertInfo.csv', $obj, $mapping);
    }

    /**
     * InsertBlock Data Importer.
     */
    public function actionInsertBlock ()
    {
        $obj     = new \common\models\InsertBlock();
        $mapping = [
            'insert_id' => 'card_id',
            'block_id' => 'insert_block_id',
        ];
        self::migrateData('dbo_MstInsertInfo.csv', $obj, $mapping);
    }

    /**
     * InsertDie Data Importer.
     */
    public function actionInsertDie ()
    {
        $obj     = new \common\models\InsertDie();
        $mapping = [
            'insert_id' => 'card_id',
            'die_id' => 'insert_die_id',
        ];
        self::migrateData('dbo_MstInsertInfo.csv', $obj, $mapping);
    }

    /**
     * InsertPrintingMethod Data Importer.
     */
    public function actionInsertPrintingMethod ()
    {
        $obj     = new \common\models\InsertPrintingMethod();
        $mapping = [
            'insert_id' => 'card_id',
            'printing_method_id' => 'insert_process_id',
        ];
        self::migrateData('dbo_MstInsertInfo.csv', $obj, $mapping);
    }

    /**
     * Additions Data Importer.
     */
    public function actionAdditions ()
    {
        $obj     = new \common\models\Additions();
        $mapping = [
            'card_id' => 'card_id',
            'tassle_id' => 'folder_tassle_id',
            'ribbon_id' => 'folder_ribbon_id',
            'rhinestone_id' => 'folder_rhinestone_id',
            'extra_insert_weight' => 'extra_insert_weight',
            'extra_insert_price' => 'extra_insert_price',
        ];
        self::migrateData('dbo_MstCardInfo.csv', $obj, $mapping);
    }

    /**
     * Common Function
     * @param type $file
     * @param type $modelObj
     * @param type $mapping
     */
    public static function migrateData ($file, $modelObj, $mapping)
    {
        $filePath     = Yii::getAlias('@backend') . "/runtime/database/" . $file;
        $handle       = fopen($filePath, 'r');
        $columns      = [];
        $cardCodeName = [
            8255 => 'Vintage Peacock',
            8231 => 'Ornamental Boteh',
            8258 => 'Delicate Impression',
            8260 => 'Sensibly Simple',
            8261 => 'Contemporary Vegan',
            8263 => 'Chic Blossom',
            8264 => 'Dashing vines',
            801 => 'Modish Indian',
            803 => 'Floral Sketch',
            804 => 'Pinecone Allure',
            8219 => 'Bloom Forest',
            8259 => 'Wispy Greenery',
            805 => 'Loves Ribbon',
            808 => 'Indian Galm',
            809 => 'Midsummer Romance',
            810 => 'Floral Touch',
            820 => 'Mandala Love',
            826 => 'Rustic Charm',
            829 => 'Glitter Glam',
            830 => 'Lase Grace',
            833 => 'Crimson Dust',
            835 => 'Winter Dreams',
            836 => 'Enchanted Onyx',
            838 => 'Touch of velvet',
            839 => 'Ribbon Winsome Florid',
            841 => 'Arabesque Grace',
            842 => 'Raven Crown',
            822 => 'Country Blossoms',
            8234 => 'Ravishing District',
            8254 => 'Timeless Delight',
            8211 => 'Vintage Flirt',
            8262 => 'Laser Pocket',
            882 => 'Musical October',
            896 => 'Icey Snow',
            877 => 'Winter Lust',
            887 => 'Blossoms March',
            5010 => 'Pocket Friendly Scroll',
            5008 => 'Box Scroll',
            5009 => 'Royal Scroll',
            5006 => 'Vintage Scroll',
            5012 => 'Elegant Scroll',
            5015 => 'Indian Scroll',
            5003 => 'Cinderella Scroll',
            5014 => 'Glossy Scroll',
            5005 => 'Roll Scroll',
            5001 => 'Modern Scroll',
            872 => 'Winter Snow Rain',
            846 => 'Snow Bunny',
            852 => 'Years Cheers',
            852 => 'Years Cheers',
            853 => 'Christmas Ornaments',
            855 => 'Smiling Doorway',
            857 => 'Merry Christmas',
            858 => 'Joyful Spruce',
            859 => 'Winter Warm',
            862 => 'Cherry Christmas',
            865 => 'Musical Joy',
            869 => 'Festive Faces',
            875 => 'Christmas Stars',
            871 => 'Sentiments',
        ];
        $transaction  = \Yii::$app->db->beginTransaction();
        try {
            while (($row = fgetcsv($handle, 10000, ',')) !== false)
            {
                if (empty($columns)) {
                    $columns = DataImporterController::getColumnsFromTitle($row);
                    continue;
                }

                $model = clone $modelObj;
                foreach ($mapping as $key => $value) {
                    $fieldVal    = $row[$columns[$value]] == 'TRUE' ? 1 : ($row[$columns[$value]] == 'FALSE' ? 0 : $row[$columns[$value]]);
                    $model->$key = $fieldVal;
                }

                if ( ! empty($model->theme_id) && $model->theme_id != 'null') {
                    $data   = rtrim($model->theme_id, ',');
                    $newKey = 'theme_id';
                } else if ( ! empty($model->paper_id) && $model->paper_id != 'null') {
                    $data   = rtrim($model->paper_id, ',');
                    $newKey = 'paper_id';
                } else if ( ! empty($model->die_id) && $model->die_id != 'null') {
                    $data   = rtrim($model->die_id, ',');
                    $newKey = 'die_id';
                } else if ( ! empty($model->block_id) && $model->block_id != 'null') {
                    $data   = rtrim($model->block_id, ',');
                    $newKey = 'block_id';
                } else if ( ! empty($model->printing_method_id) && $model->printing_method_id != 'null') {
                    $data   = rtrim($model->printing_method_id, ',');
                    $newKey = 'printing_method_id';
                }

                //Card Name Modified.
                /* $cardColorArray = explode(',', $model->code);
                  $cardColor      = \common\models\Color::findOne(['id' => $cardColorArray[0]]);
                  $model->code    = $model->name;
                  if ( ! empty($model->name)) {
                  $cardNameArray = explode('-', $model->name);
                  if ( ! empty($cardNameArray)) {
                  if ( ! empty($cardNameArray[1])) {
                  $cardNameArray[1] = filter_var($cardNameArray[1], FILTER_SANITIZE_NUMBER_INT);
                  if (array_key_exists($cardNameArray[1], $cardCodeName)) {
                  $model->name = $cardCodeName[$cardNameArray[1]] . ' - ' . $cardColor->name;
                  }
                  } else {
                  $model->name = str_replace("_", " ", $model->name);
                  }
                  } else {
                  //TODO
                  }
                  }
                  $model->card_theme = 1;
                  $model->name       = \common\models\Card::find()->doCapitalize($model->name); //Create Card Name.
                  $model->slug       = preg_replace('/-+/', '-', str_replace(' ', '-', strtolower($model->name))); //Create Card Slug.
                 */
                //For Envelope.
//                if ( ! empty($model->envelope_id)) {
//                    $envelopeModel = \common\models\Envelope::findOne(['card_id' => $model->envelope_id]);
//                    if ( ! empty($envelopeModel)) {
//                        $model->envelope_id = $envelopeModel->id;
//                    }
//                }
//                
                //For Tube.
//                if ( ! empty($model->scroll_id)) {
//                    $scrollModel = \common\models\Scroll::findOne(['card_id' => $model->scroll_id]);
//                    if ( ! empty($scrollModel)) {
//                        $model->scroll_id = $scrollModel->id;
//                    }
//                }
                //For Folder/Scroll.
                /* if ( ! empty($model->folder_id)) {
                  $folderModel = \common\models\Folder::findOne(['card_id' => $model->folder_id]);
                  if ( ! empty($folderModel)) {
                  $model->folder_id = $folderModel->id;
                  } else {
                  $scrollModel      = \common\models\Scroll::findOne(['card_id' => $model->folder_id]);
                  $model            = clone new \common\models\ScrollBlock();
                  $model->scroll_id = $scrollModel->id;
                  foreach ($mapping as $key => $value) {
                  if ($key == 'folder_id') {
                  $key = 'scroll_id';
                  }
                  $fieldVal    = $row[$columns[$value]] == 'TRUE' ? 1 : ($row[$columns[$value]] == 'FALSE' ? 0 : $row[$columns[$value]]);
                  $model->$key = ! empty($key) && ($key == 'scroll_id') ? $scrollModel->id : $fieldVal;
                  }
                  }
                  } */
                //For Folder/Scroll Define.
                /* if ( ! empty($model)) {
                  //For Scroll
                  if ($model->status == 2) {
                  $model = clone new \common\models\Scroll();
                  foreach ($mapping as $key => $value) {
                  $fieldVal    = $row[$columns[$value]] == 'TRUE' ? 1 : ($row[$columns[$value]] == 'FALSE' ? 0 : $row[$columns[$value]]);
                  $model->$key = $fieldVal;
                  }
                  }
                  }
                  $model->status = 1; */

                //$model->tassle_id     = ! empty($model->tassle_id) ? $model->tassle_id : NULL;
                //$model->ribbon_id     = ! empty($model->ribbon_id) ? $model->ribbon_id : NULL;
                //$model->rhinestone_id = ! empty($model->rhinestone_id) ? $model->rhinestone_id : NULL;
                //For Box
                /* if ( ! empty($model)) {
                  if ( ! empty($model->box_id)) {
                  $boxModel = \common\models\Box::findOne(['card_id' => $model->box_id]);
                  if ( ! empty($boxModel)) {
                  $model->box_id = $boxModel->id;
                  if ( ! empty($data)) {
                  $values = explode(',', $data);
                  foreach ($values as $value) {
                  if ((int) $value > 0) {
                  $cardThemeModel          = clone new $modelObj;
                  $cardThemeModel->$newKey = (int) $value;
                  $cardThemeModel->box_id  = $model->box_id;
                  $cardThemeModel->save();
                  }
                  }
                  }
                  }
                  }
                  } */

                //For Insert
                if ( ! empty($model)) {
                    if ( ! empty($model->insert_id)) {
                        $insertModel = \common\models\Insert::findOne(['card_id' => $model->insert_id]);
                        if ( ! empty($insertModel)) {
                            $model->insert_id = $insertModel->id;
                            if ( ! empty($data)) {
                                $values = explode(',', $data);
                                foreach ($values as $value) {
                                    if ((int) $value > 0) {
                                        $cardThemeModel            = clone new $modelObj;
                                        $cardThemeModel->$newKey   = (int) $value;
                                        $cardThemeModel->insert_id = $model->insert_id;
                                        $cardThemeModel->save();
                                    }
                                }
                            }
                        }
                    }
                }


                //For Insert Check Card ID Exist or Not into Card Model.
                /* if ( ! empty($model)) {
                  $cardModel = \common\models\Card::findOne(['id' => $model->card_id]);
                  if ( ! empty($cardModel)) {
                  if ( ! $model->save()) {
                  echo $file . "\n";
                  echo "<pre>";
                  print_r($row);
                  print_r($model->getErrors());
                  die;
                  }
                  }
                  } */

                /* if ( ! empty($data)) {
                  $values = explode(',', $data);
                  foreach ($values as $value) {
                  if ((int) $value > 0) {
                  $cardThemeModel            = clone new $modelObj;
                  $cardThemeModel->$newKey   = (int) $value;
                  $cardThemeModel->folder_id = $model->folder_id;
                  $cardThemeModel->save();
                  }
                  }
                  } else {
                  if ( ! $model->save()) {
                  echo $file . "\n";
                  echo "<pre>";
                  print_r($row);
                  print_r($model->getErrors());
                  die;
                  }
                  } */
            }
            //die;
            //## Update CardInfo ##
//            if ( ! empty($model->card_id)) {
//                \common\models\CardInfo::updateCardInfo($model->card_id);
//            }
            $transaction->commit();
        } catch (\Exception $e) {
            echo $e->getMessage();
            $transaction->rollBack();
        }
    }

}
