<?php

namespace backend\controllers;

use Yii;

ini_set('memory_limit', '-1');

class AddonImporterController extends \yii\web\Controller
{

    public function actionIndex ()
    {
        //$this->actionAddonType(); //AddonType Data Import.
        //$this->actionAddonStyle(); //AddonStyle Data Import.
        //$this->actionAddon(); //Addon Data Import.
        //$this->actionAddonEnvelope(); //AddonEnvelope Data Import
        //$this->actionAddonEnvelopePaper(); //AddonEnvelopePaper Data Import
        //$this->actionAddonEnvelopeDie(); //AddonEnvelopeDie Data Import
        //$this->actionAddonEnvelopeBlock(); //AddonEnvelopeBlock Data Import
        //$this->actionAddonEnvelopePrintingMethod(); //AddonEnvelopePrintingMethod Data Import
        //$this->actionAddonFolder(); //AddonFolder Data Import
        //$this->actionAddonFolderPaper(); //AddonFolderPaper Data Import
        //$this->actionAddonFolderDie(); //AddonFolderDie Data Import
        //$this->actionAddonFolderBlock(); //AddonFolderBlock Data Import
        //$this->actionAddonFolderPrintingMethod(); //AddonFolderPrintingMethod Data Import
    }

    /**
     * AddonType Data Importer.
     */
    public function actionAddonType ()
    {
        $obj     = new \common\models\AddonType();
        $mapping = [
            'id' => 'addon_id',
            'code' => 'addon_code',
            'name' => 'addon_name',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstAddonTypeInfo.csv', $obj, $mapping);
    }

    /**
     * AddonStyle Data Importer.
     */
    public function actionAddonStyle ()
    {
        $obj     = new \common\models\AddonStyle();
        $mapping = [
            'id' => 'addontype_id',
            'addon_type_id' => 'addon_id',
            'name' => 'addontype_name',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstAddonStyleInfo.csv', $obj, $mapping);
    }

    /**
     * Addon Data Importer.
     */
    public function actionAddon ()
    {
        $obj     = new \common\models\Addon();
        $mapping = [
            'id' => 'addoncard_id',
            'code' => 'addon_code',
            'card_id' => 'card_code',
            'addon_type_id' => 'addon_id',
            'addon_style_id' => 'addontype_id',
            'price' => 'sale_price',
            'length' => 'card_length',
            'width' => 'card_width',
            'weight' => 'card_weight',
            'status' => 'is_active'
        ];
        self::migrateData('dbo_MstAddonInfo.csv', $obj, $mapping);
    }

    /**
     * AddonEnvelope Data Importer.
     */
    public function actionAddonEnvelope ()
    {
        $obj     = new \common\models\AddonEnvelope();
        $mapping = [
            'addon_id' => 'addon_code',
        ];
        self::migrateData('dbo_MstAddonInfo.csv', $obj, $mapping);
    }

    /**
     * AddonEnvelopePaper Data Importer.
     */
    public function actionAddonEnvelopePaper ()
    {
        $obj     = new \common\models\AddonEnvelopePaper();
        $mapping = [
            'addon_envelope_id' => 'addon_code',
            'paper_id' => 'env_paper_id',
        ];
        self::migrateData('dbo_MstAddonInfo.csv', $obj, $mapping);
    }

    /**
     * AddonEnvelopeDie Data Importer.
     */
    public function actionAddonEnvelopeDie ()
    {
        $obj     = new \common\models\AddonEnvelopeDie();
        $mapping = [
            'addon_envelope_id' => 'addon_code',
            'die_id' => 'env_die_id',
        ];
        self::migrateData('dbo_MstAddonInfo.csv', $obj, $mapping);
    }

    /**
     * AddonEnvelopeBlock Data Importer.
     */
    public function actionAddonEnvelopeBlock ()
    {
        $obj     = new \common\models\AddonEnvelopeBlock();
        $mapping = [
            'addon_envelope_id' => 'addon_code',
            'block_id' => 'env_block_id',
        ];
        self::migrateData('dbo_MstAddonInfo.csv', $obj, $mapping);
    }

    /**
     * AddonEnvelopePrintingMethod Data Importer.
     */
    public function actionAddonEnvelopePrintingMethod ()
    {
        $obj     = new \common\models\AddonEnvelopePrintingMethod();
        $mapping = [
            'addon_envelope_id' => 'addon_code',
            'printing_method_id' => 'env_process_id',
        ];
        self::migrateData('dbo_MstAddonInfo.csv', $obj, $mapping);
    }

    /**
     * AddonFolder Data Importer.
     */
    public function actionAddonFolder ()
    {
        $obj     = new \common\models\AddonFolder();
        $mapping = [
            'addon_id' => 'addon_code',
        ];
        self::migrateData('dbo_MstAddonInfo.csv', $obj, $mapping);
    }

    /**
     * AddonFolderPaper Data Importer.
     */
    public function actionAddonFolderPaper ()
    {
        $obj     = new \common\models\AddonFolderPaper();
        $mapping = [
            'addon_folder_id' => 'addon_code',
            'paper_id' => 'folder_paper_id',
        ];
        self::migrateData('dbo_MstAddonInfo.csv', $obj, $mapping);
    }

    /**
     * AddonFolderDie Data Importer.
     */
    public function actionAddonFolderDie ()
    {
        $obj     = new \common\models\AddonFolderDie();
        $mapping = [
            'addon_folder_id' => 'addon_code',
            'die_id' => 'folder_die_id',
        ];
        self::migrateData('dbo_MstAddonInfo.csv', $obj, $mapping);
    }

    /**
     * AddonFolderBlock Data Importer.
     */
    public function actionAddonFolderBlock ()
    {
        $obj     = new \common\models\AddonFolderBlock();
        $mapping = [
            'addon_folder_id' => 'addon_code',
            'block_id' => 'folder_block_id',
        ];
        self::migrateData('dbo_MstAddonInfo.csv', $obj, $mapping);
    }

    /**
     * AddonFolderPrintingMethod Data Importer.
     */
    public function actionAddonFolderPrintingMethod ()
    {
        $obj     = new \common\models\AddonFolderPrintingMethod();
        $mapping = [
            'addon_folder_id' => 'addon_code',
            'printing_method_id' => 'folder_process_id',
        ];
        self::migrateData('dbo_MstAddonInfo.csv', $obj, $mapping);
    }

    /**
     * Common Function
     * @param type $file
     * @param type $modelObj
     * @param type $mapping
     */
    public static function migrateData ($file, $modelObj, $mapping)
    {
        $filePath    = Yii::getAlias('@backend') . "/runtime/database/" . $file;
        $handle      = fopen($filePath, 'r');
        $columns     = [];
        $transaction = \Yii::$app->db->beginTransaction();

        try {
            while (($row = fgetcsv($handle, 10000, ',')) !== false)
            {
                if (empty($columns)) {
                    $columns = DataImporterController::getColumnsFromTitle($row);
                    continue;
                }
                $model = clone $modelObj;
                foreach ($mapping as $key => $value) {
                    $fieldVal    = $row[$columns[$value]] == 'true' ? 1 : ($row[$columns[$value]] == 'false' ? 0 : $row[$columns[$value]]);
                    $model->$key = $fieldVal;
                }

                $data = '';
                if ( ! empty($model->paper_id) && $model->paper_id != 'null') {
                    $data   = rtrim($model->paper_id, ',');
                    $newKey = 'paper_id';
                } else if ( ! empty($model->die_id) && $model->die_id != 'null') {
                    $data   = rtrim($model->die_id, ',');
                    $newKey = 'die_id';
                } else if ( ! empty($model->block_id) && $model->block_id != 'null') {
                    $data   = rtrim($model->block_id, ',');
                    $newKey = 'block_id';
                } else if ( ! empty($model->printing_method_id) && $model->printing_method_id != 'null') {
                    $data   = rtrim($model->printing_method_id, ',');
                    $newKey = 'printing_method_id';
                }

                //Currect CardCode,AddonCode and CardId.
                /* if ( ! empty($model)) {
                  $cardModels = \common\models\Card::find()->andWhere(['like', 'code', $model->card_id])->all();
                  if ( ! empty($cardModels)) {
                  foreach ($cardModels as $cardModel) {
                  if (in_array($model->addon_type_id, [1, 2, 9])) {
                  $model->old_code       = $model->code;
                  $model->card_id        = $cardModel->id;
                  $model->code           = preg_replace('/-+/', '-', str_replace(' ', '-', $model->addonType->code . ' ' . $cardModel->name));
                  $model->addon_style_id = ! empty($model->addon_style_id) ? $model->addon_style_id : NULL;
                  //echo "<pre>" . print_r($model->code .' '.$model->old_code, TRUE) . "</pre>";
                  //Save Table Data.
                  if ( ! $model->save()) {
                  echo $file . "\n";
                  echo "<pre>";
                  print_r($row);
                  print_r($model->getErrors());
                  die;
                  }
                  }
                  }
                  }
                  } */

                //For AddonEnvelope/Folder Check Addon Exist Or Not.
                /* $addonModel = \common\models\Addon::find()->andWhere(['old_code' => $model->addon_id])->one();
                  if ( ! empty($addonModel)) {
                  $model->addon_id = $addonModel->id;
                  //Save Table Data.
                  if ( ! $model->save()) {
                  echo $file . "\n";
                  echo "<pre>";
                  print_r($row);
                  print_r($model->getErrors());
                  die;
                  }
                  } */

                //For AddonEnvelopePaper/Die/Block/Printing Method Addon Exist Or Not.
                /* $addonModel = \common\models\Addon::find()->andWhere(['old_code' => $model->addon_envelope_id])->one();
                  if ( ! empty($addonModel)) {
                  $addonEnvelopeModel = \common\models\AddonEnvelope::find()->andWhere(['addon_id' => $addonModel->id])->one();
                  if ( ! empty($addonEnvelopeModel)) {
                  $model->addon_envelope_id = $addonEnvelopeModel->id;
                  //Save Table Data.
                  if ( ! empty($data)) {
                  $values = explode(',', $data);
                  if ( ! empty($values)) {
                  foreach ($values as $value) {
                  if ( ! empty($value) && ((int) $value > 0)) {
                  $addonNewModel                    = clone new $modelObj;
                  $addonNewModel->$newKey           = (int) $value;
                  $addonNewModel->addon_envelope_id = $model->addon_envelope_id;
                  $addonNewModel->save();
                  }
                  }
                  }
                  }
                  }
                  } */

                //For AddonFolderPaper/Die/Block/Printing Method Addon Exist Or Not.
                $addonModel = \common\models\Addon::find()->andWhere(['old_code' => $model->addon_folder_id])->one();
                if ( ! empty($addonModel)) {
                    $addonFolderModel = \common\models\AddonFolder::find()->andWhere(['addon_id' => $addonModel->id])->one();
                    if ( ! empty($addonFolderModel)) {
                        $model->addon_folder_id = $addonFolderModel->id;
                        //Save Table Data.
                        if ( ! empty($data)) {
                            $values = explode(',', $data);
                            if ( ! empty($values)) {
                                foreach ($values as $value) {
                                    if ( ! empty($value) && ((int) $value > 0)) {
                                        $addonNewModel                  = clone new $modelObj;
                                        $addonNewModel->$newKey         = (int) $value;
                                        $addonNewModel->addon_folder_id = $model->addon_folder_id;
                                        $addonNewModel->save();
                                    }
                                }
                            }
                        }
                    }
                }


                //Save Table Data.
                /* if ( ! $model->save()) {
                  echo $file . "\n";
                  echo "<pre>";
                  print_r($row);
                  print_r($model->getErrors());
                  die;
                  } */
            }
            //die;
            $transaction->commit();
        } catch (\Exception $e) {
            echo $e->getMessage();
            $transaction->rollBack();
        }
    }

}
