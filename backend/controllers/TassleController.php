<?php

namespace backend\controllers;

use Yii;
use common\models\Tassle;
use backend\models\TassleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use backend\helpers\Html;

/**
 * TassleController implements the CRUD actions for Tassle model.
 */
class TassleController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tassle models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new TassleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new Tassle();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\Tassle::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single Tassle model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tassle model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model = new Tassle();

        if ($model->load(Yii::$app->request->post())) {
            $model->code = strtoupper($model->code);
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Tassle Create Successfully!");
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tassle model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->code = strtoupper($model->code);
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Tassle Update Successfully!");
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tassle model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == Tassle::STATUS_DELETE) ? Tassle::STATUS_ACTIVE : Tassle::STATUS_DELETE;
        $model->save();
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Tassle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tassle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = Tassle::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Get Tassle List
     * @return type
     */
    public function actionList ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $tassles                     = Tassle::find()->getList();
        $tassles                     += [0 => 'Select Tassle Code'];
        return $tassles;
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        $searchModel = new TassleSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {

            $view   = Html::getViewButton(['tassle/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['tassle/update', 'id' => $result['id']]);

            if ($result['status'] == Tassle::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . Tassle::getConstantList('STATUS_', Tassle::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . Tassle::getConstantList('STATUS_', Tassle::className())[$result['status']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['tassle/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == Tassle::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['code'],
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
