<?php

namespace backend\controllers;

use Yii;
use common\models\Logo;
use backend\models\LogoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use backend\helpers\Html;

/**
 * LogoController implements the CRUD actions for Logo model.
 */
class LogoController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Logo models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new LogoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new Logo();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\Logo::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single Logo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        $model           = $this->findModel($id);
        $images          = array ();
        $time            = '?r=' . time();
        $images['small'] = \common\models\FaqType::NO_IMAGE_SMALL . $time;
        $images['full']  = \common\models\FaqType::NO_IMAGE_FULL . $time;
        if ( ! empty($model->image)) {
            $originalFilePath = '/' . Logo::MAIN_DIRECTORY . Logo::ORIGINAL_FILEPATH . $model->image;
            $thumbFilePath    = '/' . Logo::MAIN_DIRECTORY . Logo::THUMBNAIL_FILEPATH . $model->image;
            if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                $images['small'] = $thumbFilePath . $time;
                $images['full']  = $originalFilePath . $time;
            }
        }
        $image = Html::a(Html::img(Yii::getAlias('@web') . $images['small'], ['class' => 'img-thumbnail', 'style' => 'margin-bottom:10px']), Yii::getAlias('@web') . $images['full'], ['data-fancybox' => 'gallery']);


        return $this->render('view', [
                    'model' => $model,
                    'image' => $image,
        ]);
    }

    /**
     * Creates a new Logo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model = new Logo();

        if ($model->load(Yii::$app->request->post())) {
            /**
             * Logo Feature Image Upload.
             */
            if ($model->image = UploadedFile::getInstance($model, 'image')) {
                $model->image = $model->upload();
            }

            /* check if any other Logo set default or not. */
            if ($model->default) {
                $logoModel = Logo::find()->andWhere(['default' => Logo::DEFAULT_ACTIVE])->one();
                if ( ! empty($logoModel)) {
                    Logo::updateAll(['default' => Logo::DEFAULT_INACTIVE], ['id' => $logoModel->id]);
                }
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->heading = ucwords(strtolower($model->heading));
                    $model->save();

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Logo Create Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Logo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            /**
             * Logo Image Upload.
             */
            $model->image = UploadedFile::getInstance($model, "image");
            $oldImage     = $model->getOldAttribute('image');
            if (empty($model->image)) {
                $model->image = $oldImage;
            } else {
                if ( ! empty($oldImage)) {
                    $originalFilePath = '/' . Logo::MAIN_DIRECTORY . Logo::ORIGINAL_FILEPATH . $oldImage;
                    $thumbFilePath    = '/' . Logo::MAIN_DIRECTORY . Logo::THUMBNAIL_FILEPATH . $oldImage;
                    if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                        unlink(Yii::$app->basePath . '/web' . $originalFilePath);
                        unlink(Yii::$app->basePath . '/web' . $thumbFilePath);
                    }
                }
                $model->image = $model->upload();
            }

            /* check if any other Logo set default or not. */
            if ($model->default) {
                $logoModel = Logo::find()->andWhere(['default' => Logo::DEFAULT_ACTIVE])->one();
                if ( ! empty($logoModel)) {
                    Logo::updateAll(['default' => Logo::DEFAULT_INACTIVE], ['id' => $logoModel->id]);
                }
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->heading = ucwords(strtolower($model->heading));
                    $model->save();

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Logo Update Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Logo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == Logo::STATUS_DELETE) ? Logo::STATUS_ACTIVE : Logo::STATUS_DELETE;
        $model->save();
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Logo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Logo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = Logo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        $searchModel = new LogoSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {

            $view   = Html::getViewButton(['logo/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['logo/update', 'id' => $result['id']]);

            /**
             * Logo Feature Image Show.
             */
            $images          = array ();
            $time            = '?r=' . time();
            $images['small'] = \common\models\FaqType::NO_IMAGE_SMALL . $time;
            $images['full']  = \common\models\FaqType::NO_IMAGE_FULL . $time;
            if ( ! empty($result['image'])) {
                $originalFilePath = '/' . Logo::MAIN_DIRECTORY . Logo::ORIGINAL_FILEPATH . $result['image'];
                $thumbFilePath    = '/' . Logo::MAIN_DIRECTORY . Logo::THUMBNAIL_FILEPATH . $result['image'];
                if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                    $images['small'] = $thumbFilePath . $time;
                    $images['full']  = $originalFilePath . $time;
                }
            }
            $image = Html::a(Html::img(Yii::getAlias('@web') . $images['small'], ['class' => 'img-thumbnail']), Yii::getAlias('@web') . $images['full'], ['data-fancybox' => 'gallery']);

            /**
             * Logo Status.
             */
            if ($result['status'] == Logo::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . Logo::getConstantList('STATUS_', Logo::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . Logo::getConstantList('STATUS_', Logo::className())[$result['status']] . '</span>';
            }

            /**
             * Logo Default.
             */
            if ($result['default'] == Logo::DEFAULT_ACTIVE) {
                $default = '<span class="label label-success">' . Logo::getConstantList('DEFAULT_', Logo::className())[$result['default']] . '</span>';
            } else {
                $default = '<span class="label label-danger">' . Logo::getConstantList('DEFAULT_', Logo::className())[$result['default']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['logo/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == Logo::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['heading'],
                $image,
                $default,
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
