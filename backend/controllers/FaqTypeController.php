<?php

namespace backend\controllers;

use Yii;
use common\models\FaqType;
use backend\models\FaqTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use backend\helpers\Html;

/**
 * FaqTypeController implements the CRUD actions for FaqType model.
 */
class FaqTypeController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FaqType models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $searchModel  = new FaqTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model        = new FaqType();

        if ( ! empty(Yii::$app->request->post('checkbox_id'))) {
            if ($model->load(Yii::$app->request->post())) {
                $moduleIds = Yii::$app->request->post('checkbox_id');
                \common\models\FaqType::updateAll(['status' => $model->status], ['id' => $moduleIds]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single FaqType model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView ($id)
    {
        $model          = $this->findModel($id);
        $icons          = array ();
        $time           = '?r=' . time();
        $icons['small'] = FaqType::NO_IMAGE_SMALL . $time;
        $icons['full']  = FaqType::NO_IMAGE_FULL . $time;
        if ( ! empty($model->icon)) {
            $originalFilePath = '/' . FaqType::MAIN_DIRECTORY . FaqType::ORIGINAL_FILEPATH . $model->icon;
            $thumbFilePath    = '/' . FaqType::MAIN_DIRECTORY . FaqType::THUMBNAIL_FILEPATH . $model->icon;
            if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                $icons['small'] = $thumbFilePath . $time;
                $icons['full']  = $originalFilePath . $time;
            }
        }
        $icon = Html::a(Html::img(Yii::getAlias('@web') . $icons['small'], ['class' => 'img-thumbnail', 'style' => 'margin-bottom:10px']), Yii::getAlias('@web') . $icons['full'], ['data-fancybox' => 'gallery']);

        return $this->render('view', [
                    'model' => $model,
                    'icon' => $icon,
        ]);
    }

    /**
     * Creates a new FaqType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model = new FaqType();

        if ($model->load(Yii::$app->request->post())) {
            /**
             * FAQ Type Icon Image Upload.
             */
            if ($model->icon = UploadedFile::getInstance($model, 'icon')) {
                $model->icon = $model->upload();
            }
            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->name = ucwords(strtolower($model->name));
                    $model->save();
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Faq Type Create Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing FaqType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate ($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            /**
             * FAQ Type Icon Image Upload.
             */
            $model->icon = UploadedFile::getInstance($model, "icon");
            $oldIcon     = $model->getOldAttribute('icon');
            if (empty($model->icon)) {
                $model->icon = $oldIcon;
            } else {
                if ( ! empty($oldIcon)) {
                    $originalFilePath = '/' . FaqType::MAIN_DIRECTORY . FaqType::ORIGINAL_FILEPATH . $oldIcon;
                    $thumbFilePath    = '/' . FaqType::MAIN_DIRECTORY . FaqType::THUMBNAIL_FILEPATH . $oldIcon;
                    if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                        unlink(Yii::$app->basePath . '/web' . $originalFilePath);
                        unlink(Yii::$app->basePath . '/web' . $thumbFilePath);
                    }
                }
                $model->icon = $model->upload();
            }

            // validate all models  
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->name = ucwords(strtolower($model->name));
                    $model->save();
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Faq Type Update Successfully!");
                    return $this->redirect(['index']);
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FaqType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete ($id)
    {
        $model         = $this->findModel($id);
        $model->status = ($model->status == FaqType::STATUS_DELETE) ? FaqType::STATUS_ACTIVE : FaqType::STATUS_DELETE;
        $model->save();
        return $this->redirect(['index']);

        //$this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the FaqType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FaqType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        if (($model = FaqType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Get Faq Type List
     * @return type
     */
    public function actionList ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $faqTypes                    = FaqType::find()->getList();
        $faqTypes                    += [0 => 'Select FAQ Type'];
        return $faqTypes;
    }

    /**
     * DataTable Search
     * @return type
     */
    public function actionDatatable ()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data                        = [];
        $requestParams               = Yii::$app->request->post();

        $searchParams = [
            'offset' => ! empty($requestParams['start']) ? $requestParams['start'] : '',
            'limit' => ! empty($requestParams['length']) ? $requestParams['length'] : '',
            'search' => ! empty($requestParams['search']['value']) ? $requestParams['search']['value'] : '',
            'orderField' => ! empty($requestParams['order'][0]['column']) ? $requestParams['order'][0]['column'] : '',
            'orderBy' => ! empty($requestParams['order'][0]['dir']) ? \common\models\CommonModel::orderBy[$requestParams['order'][0]['dir']] : ''
        ];

        $searchModel = new FaqTypeSearch();
        $results     = $searchModel->dataTable($searchParams);

        foreach ($results['records'] as $result) {

            $view   = Html::getViewButton(['faq-type/view', 'id' => $result['id']]);
            $update = Html::getEditButton(['faq-type/update', 'id' => $result['id']]);

            /**
             * FAQ Type Image Show.
             */
            $icons          = array ();
            $time           = '?r=' . time();
            $icons['small'] = FaqType::NO_IMAGE_SMALL . $time;
            $icons['full']  = FaqType::NO_IMAGE_FULL . $time;
            if ( ! empty($result['icon'])) {
                $originalFilePath = '/' . FaqType::MAIN_DIRECTORY . FaqType::ORIGINAL_FILEPATH . $result['icon'];
                $thumbFilePath    = '/' . FaqType::MAIN_DIRECTORY . FaqType::THUMBNAIL_FILEPATH . $result['icon'];
                if (file_exists(Yii::$app->basePath . '/web' . $originalFilePath) && file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
                    $icons['small'] = $thumbFilePath . $time;
                    $icons['full']  = $originalFilePath . $time;
                }
            }
            $icon = Html::a(Html::img(Yii::getAlias('@web') . $icons['small'], ['class' => 'img-thumbnail']), Yii::getAlias('@web') . $icons['full'], ['data-fancybox' => 'gallery']);

            /**
             * FAQ Type Status.
             */
            if ($result['status'] == FaqType::STATUS_ACTIVE) {
                $status = '<span class="label label-success">' . FaqType::getConstantList('STATUS_', FaqType::className())[$result['status']] . '</span>';
            } else {
                $status = '<span class="label label-danger">' . FaqType::getConstantList('STATUS_', FaqType::className())[$result['status']] . '</span>';
            }

            $deleteButton = '';
            if (Yii::$app->user->identity->master_admin) {
                $deleteButton = Html::getDeleteButton(['faq-type/delete', 'id' => $result['id']]);
            }

            if ($result['status'] == FaqType::STATUS_DELETE) {
                $update       = $deleteButton = '';
            }

            $data[] = [
                $result['id'],
                $result['name'],
                $icon,
                $status,
                $view .
                ' ' . $update .
                ' ' . $deleteButton
            ];
        }
        return ["draw" => $requestParams['draw'],
            "recordsTotal" => $results['recordsTotal'],
            "recordsFiltered" => $results['recordsFiltered'],
            'data' => $data];
    }

}
