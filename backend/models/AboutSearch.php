<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\About;

/**
 * AboutSearch represents the model behind the search form of `common\models\About`.
 */
class AboutSearch extends About
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'default', 'status', 'created_at', 'updated_at'], 'integer'],
            [['heading1', 'description1', 'heading2', 'description2', 'image'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = About::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'default' => $this->default,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'heading1', $this->heading1])
                ->andFilterWhere(['like', 'description1', $this->description1])
                ->andFilterWhere(['like', 'heading2', $this->heading2])
                ->andFilterWhere(['like', 'description2', $this->description2])
                ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['a.id', 'a.heading1', 'a.heading2', 'a.image', 'a.default', 'a.status'];
        // Fields which are required
        $fields      = ['a.id', 'a.heading1', 'a.heading2', 'a.image', 'a.default', 'a.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('about a');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'a.id', $params['search']])
                    ->orFilterWhere(['like', 'a.heading1', $params['search']])
                    ->orFilterWhere(['like', 'a.heading2', $params['search']])
                    ->orderBy(['a.id' => SORT_DESC]);
        } else {
            $query->orderBy(['a.id' => SORT_DESC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'a.status', About::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = About::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = About::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = About::STATUS_DELETE; // Delete
            }
            $query->orFilterWhere(['like', 'a.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => About::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
