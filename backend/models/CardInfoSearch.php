<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CardInfo;

/**
 * CardInfoSearch represents the model behind the search form of `common\models\CardInfo`.
 */
class CardInfoSearch extends CardInfo
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['card_id', 'status'], 'integer'],
            [['theme', 'envelope', 'folder', 'scroll', 'box', 'insert', 'additions'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = CardInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'card_id' => $this->card_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'theme', $this->theme])
                ->andFilterWhere(['like', 'envelope', $this->envelope])
                ->andFilterWhere(['like', 'folder', $this->folder])
                ->andFilterWhere(['like', 'scroll', $this->scroll])
                ->andFilterWhere(['like', 'box', $this->box])
                ->andFilterWhere(['like', 'insert', $this->insert])
                ->andFilterWhere(['like', 'additions', $this->additions]);

        return $dataProvider;
    }

    /**
     * DataTable Search.
     * @param type $params
     * @param type $page
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = $fields      = [
            'c.id',
            'c.name',
            'ci.theme',
            'ci.folder',
            'ci.scroll',
            'c.length',
            'c.width',
            'c.status',
        ];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('card_info ci')
                ->innerJoin('card c', 'c.id = ci.card_id');
        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'c.id', $params['search']])
                    ->orFilterWhere(['like', 'c.name', $params['search']])
                    ->orFilterWhere(['like', 'ci.theme', $params['search']])
                    ->orFilterWhere(['like', 'ci.folder', $params['search']])
                    ->orFilterWhere(['like', 'ci.scroll', $params['search']])
                    ->orderBy(['ci.card_id' => SORT_DESC]);
        } else {
            $query->orderBy(['ci.card_id' => SORT_DESC]);
        }

        if ( ! \Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'ci.status', \common\models\Card::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = \common\models\Card::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = \common\models\Card::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = \common\models\Card::STATUS_DELETE; /// Delete
            }
            $query->orFilterWhere(['like', 'ci.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);

        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }

        return [
            'recordsTotal' => CardInfo::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
