<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SocialMedia;

/**
 * SocialMediaSearch represents the model behind the search form of `common\models\SocialMedia`.
 */
class SocialMediaSearch extends SocialMedia
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'link', 'data'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = SocialMedia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'link', $this->link])
                ->andFilterWhere(['like', 'data', $this->data]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['sm.id', 'sm.name', 'sm.link', 'sm.data', 'sm.status'];
        // Fields which are required
        $fields      = ['sm.id', 'sm.name', 'sm.link', 'sm.data', 'sm.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('social_media sm');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'sm.id', $params['search']])
                    ->orFilterWhere(['like', 'sm.name', $params['search']])
                    ->orFilterWhere(['like', 'sm.link', $params['search']])
                    ->orderBy(['sm.name' => SORT_ASC]);
        } else {
            $query->orderBy(['sm.name' => SORT_ASC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'sm.status', SocialMedia::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = SocialMedia::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = SocialMedia::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = SocialMedia::STATUS_DELETE; /// Delete
            }
            $query->orFilterWhere(['like', 'sm.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => SocialMedia::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
