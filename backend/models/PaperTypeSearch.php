<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaperType;

/**
 * PaperTypeSearch represents the model behind the search form of `common\models\PaperType`.
 */
class PaperTypeSearch extends PaperType
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = PaperType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['pt.id', 'pt.name', 'pt.status'];
        // Fields which are required
        $fields      = ['pt.id', 'pt.name', 'pt.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('paper_type pt');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'pt.id', $params['search']])
                    ->orFilterWhere(['like', 'pt.name', $params['search']])
                    ->orderBy(['pt.name' => SORT_ASC]);
        } else {
            $query->orderBy(['pt.name' => SORT_ASC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'pt.status', PaperType::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = PaperType::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = PaperType::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = PaperType::STATUS_DELETE; /// Delete
            }
            $query->orFilterWhere(['like', 'pt.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => PaperType::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
