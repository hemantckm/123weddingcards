<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PrintingMethod;

/**
 * PrintingMethodSearch represents the model behind the search form of `common\models\PrintingMethod`.
 */
class PrintingMethodSearch extends PrintingMethod
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = PrintingMethod::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['pm.id', 'pm.name', 'pm.status'];
        // Fields which are required
        $fields      = ['pm.id', 'pm.name', 'pm.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('printing_method pm');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'pm.id', $params['search']])
                    ->orFilterWhere(['like', 'pm.name', $params['search']])
                    ->orderBy(['pm.name' => SORT_ASC]);
        } else {
            $query->orderBy(['pm.name' => SORT_ASC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'pm.status', PrintingMethod::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = PrintingMethod::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = PrintingMethod::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = PrintingMethod::STATUS_DELETE; /// Delete
            }
            $query->orFilterWhere(['like', 'pm.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => PrintingMethod::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
