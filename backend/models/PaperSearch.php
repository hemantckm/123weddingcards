<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Paper;

/**
 * PaperSearch represents the model behind the search form of `common\models\Paper`.
 */
class PaperSearch extends Paper
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'paper_type_id', 'color_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['code'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = Paper::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'paper_type_id' => $this->paper_type_id,
            'color_id' => $this->color_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['p.id', 'p.code', 'pt.name paper_type_name', 'c.name color_name', 'p.status'];
        // Fields which are required
        $fields      = ['p.id', 'p.code', 'pt.name paper_type_name', 'c.name color_name', 'p.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('paper p')
                ->innerJoin('paper_type pt', 'pt.id = p.paper_type_id')
                ->innerJoin('color c', 'c.id = p.color_id');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'p.id', $params['search']])
                    ->orFilterWhere(['like', 'p.code', $params['search']])
                    ->orFilterWhere(['like', 'pt.name', $params['search']])
                    ->orFilterWhere(['like', 'c.name', $params['search']])
                    ->orderBy(['p.id' => SORT_DESC]);
        } else {
            $query->orderBy(['p.id' => SORT_DESC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'p.status', Paper::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = Paper::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = Paper::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = Paper::STATUS_DELETE; /// Delete
            }
            $query->orFilterWhere(['like', 'p.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => Paper::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
