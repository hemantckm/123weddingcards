<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AddonStyle;

/**
 * AddonStyleSearch represents the model behind the search form of `common\models\AddonStyle`.
 */
class AddonStyleSearch extends AddonStyle
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'addon_type_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = AddonStyle::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'addon_type_id' => $this->addon_type_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['ast.id', 'ast.name', 'aty.name addon_type_name', 'ast.status'];
        // Fields which are required
        $fields      = ['ast.id', 'ast.name', 'aty.name addon_type_name', 'ast.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('addon_style ast')
                ->innerJoin('addon_type aty', 'aty.id = ast.addon_type_id');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'ast.id', $params['search']])
                    ->orFilterWhere(['like', 'ast.name', $params['search']])
                    ->orFilterWhere(['like', 'aty.name', $params['search']])
                    ->orderBy(['ast.id' => SORT_DESC]);
        } else {
            $query->orderBy(['ast.id' => SORT_DESC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'ast.status', AddonStyle::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = AddonStyle::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = AddonStyle::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = AddonStyle::STATUS_DELETE; /// Delete
            }
            $query->orFilterWhere(['like', 'ast.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => AddonStyle::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
