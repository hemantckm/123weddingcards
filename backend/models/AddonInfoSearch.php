<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AddonInfo;

/**
 * AddonInfoSearch represents the model behind the search form of `common\models\AddonInfo`.
 */
class AddonInfoSearch extends AddonInfo
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['addon_id', 'card_id', 'status'], 'integer'],
            [['addon_type', 'addon_style', 'envelope', 'folder'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = AddonInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'addon_id' => $this->addon_id,
            'card_id' => $this->card_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'addon_type', $this->addon_type])
                ->andFilterWhere(['like', 'addon_style', $this->addon_style])
                ->andFilterWhere(['like', 'envelope', $this->envelope])
                ->andFilterWhere(['like', 'folder', $this->folder]);

        return $dataProvider;
    }

    /**
     * DataTable Search.
     * @param type $params
     * @param type $page
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = $fields      = [
            'a.id',
            'a.code',
            'a.price',
            'a.length',
            'a.width',
            'a.weight',
            'a.status',
            'c.name',
            'ai.addon_type',
            'ai.addon_style',
            'ai.envelope',
            'ai.folder',
        ];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('addon_info ai')
                ->innerJoin('addon a', 'a.id = ai.addon_id')
                ->innerJoin('card c', 'c.id = a.card_id');
        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'a.code', $params['search']])
                    ->orFilterWhere(['like', 'a.price', $params['search']])
                    ->orFilterWhere(['like', 'a.length', $params['search']])
                    ->orFilterWhere(['like', 'a.width', $params['search']])
                    ->orFilterWhere(['like', 'a.weight', $params['search']])
                    ->orFilterWhere(['like', 'c.name', $params['search']])
                    ->orFilterWhere(['like', 'ai.envelope', $params['search']])
                    ->orFilterWhere(['like', 'ai.folder', $params['search']])
                    ->orFilterWhere(['like', 'ai.addon_type', $params['search']])
                    ->orFilterWhere(['like', 'ai.addon_style', $params['search']])
                    ->orderBy(['a.id' => SORT_DESC]);
        } else {
            $query->orderBy(['a.id' => SORT_DESC]);
        }

        if ( ! \Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'a.status', \common\models\Addon::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = \common\models\Addon::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = \common\models\Addon::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = \common\models\Addon::STATUS_DELETE; /// Delete
            }
            $query->orFilterWhere(['like', 'a.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);

        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }

        return [
            'recordsTotal' => AddonInfo::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
