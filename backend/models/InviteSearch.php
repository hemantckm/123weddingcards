<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Invite;

/**
 * InviteSearch represents the model behind the search form of `common\models\Invite`.
 */
class InviteSearch extends Invite
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'default', 'status', 'created_at', 'updated_at'], 'integer'],
            [['heading', 'description', 'image', 'link', 'button_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = Invite::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'default' => $this->default,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'heading', $this->heading])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'image', $this->image])
                ->andFilterWhere(['like', 'link', $this->link])
                ->andFilterWhere(['like', 'button_name', $this->button_name]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['i.id', 'i.heading', 'i.image', 'i.default', 'i.status'];
        // Fields which are required
        $fields      = ['i.id', 'i.heading', 'i.image', 'i.default', 'i.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('invite i');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'i.id', $params['search']])
                    ->orFilterWhere(['like', 'i.heading', $params['search']])
                    ->orderBy(['i.id' => SORT_DESC]);
        } else {
            $query->orderBy(['i.id' => SORT_DESC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'i.status', Invite::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = Invite::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = Invite::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = Invite::STATUS_DELETE; // Delete
            }
            $query->orFilterWhere(['like', 'i.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => Invite::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
