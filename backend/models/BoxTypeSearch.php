<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BoxType;

/**
 * BoxTypeSearch represents the model behind the search form of `common\models\BoxType`.
 */
class BoxTypeSearch extends BoxType
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = BoxType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['bt.id', 'bt.name', 'bt.status'];
        // Fields which are required
        $fields      = ['bt.id', 'bt.name', 'bt.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('box_type bt');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'bt.id', $params['search']])
                    ->orFilterWhere(['like', 'bt.name', $params['search']])
                    ->orderBy(['bt.name' => SORT_ASC]);
        } else {
            $query->orderBy(['bt.name' => SORT_ASC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'bt.status', BoxType::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = BoxType::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = BoxType::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = BoxType::STATUS_DELETE; /// Delete
            }
            $query->orFilterWhere(['like', 'bt.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => BoxType::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
