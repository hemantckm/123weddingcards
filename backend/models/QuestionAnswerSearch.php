<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\QuestionAnswer;

/**
 * QuestionAnswerSearch represents the model behind the search form of `common\models\QuestionAnswer`.
 */
class QuestionAnswerSearch extends QuestionAnswer
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['question', 'answer'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = QuestionAnswer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'question', $this->question])
                ->andFilterWhere(['like', 'answer', $this->answer]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['qa.id', 'qa.question', 'qa.answer', 'qa.status'];
        // Fields which are required
        $fields      = ['qa.id', 'qa.question', 'qa.answer', 'qa.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('question_answer qa');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'qa.id', $params['search']])
                    ->orFilterWhere(['like', 'qa.question', $params['search']])
                    ->orFilterWhere(['like', 'qa.answer', $params['search']])
                    ->orderBy(['qa.id' => SORT_DESC]);
        } else {
            $query->orderBy(['qa.id' => SORT_DESC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'qa.status', QuestionAnswer::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = QuestionAnswer::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = QuestionAnswer::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = QuestionAnswer::STATUS_DELETE; // Delete
            }
            $query->orFilterWhere(['like', 'qa.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => QuestionAnswer::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
