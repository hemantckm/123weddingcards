<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\QuestionAnswerFaq;

/**
 * QuestionAnswerFaqSearch represents the model behind the search form of `common\models\QuestionAnswerFaq`.
 */
class QuestionAnswerFaqSearch extends QuestionAnswerFaq
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'question_answer_id', 'faq_id', 'faq_type_id', 'status', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = QuestionAnswerFaq::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'question_answer_id' => $this->question_answer_id,
            'faq_id' => $this->faq_id,
            'faq_type_id' => $this->faq_type_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['qaf.id', 'qa.question', 'f.heading', 'fy.name', 'qaf.status'];
        // Fields which are required
        $fields      = ['qaf.id', 'qa.question', 'f.heading', 'fy.name', 'qaf.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('question_answer_faq qaf')
                ->innerJoin('question_answer qa', 'qa.id = qaf.question_answer_id')
                ->innerJoin('faq f', 'f.id = qaf.faq_id')
                ->innerJoin('faq_type fy', 'fy.id = qaf.faq_type_id');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'qa.question', $params['search']])
                    ->orFilterWhere(['like', 'f.heading', $params['search']])
                    ->orFilterWhere(['like', 'fy.name', $params['search']])
                    ->orderBy(['qaf.id' => SORT_DESC]);
        } else {
            $query->orderBy(['qaf.id' => SORT_DESC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'qaf.status', QuestionAnswerFaq::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = QuestionAnswerFaq::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = QuestionAnswerFaq::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = QuestionAnswerFaq::STATUS_DELETE; // Delete
            }
            $query->orFilterWhere(['like', 'qaf.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => QuestionAnswerFaq::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
