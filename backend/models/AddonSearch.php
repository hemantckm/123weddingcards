<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Addon;
use common\models\Card;

/**
 * AddonSearch represents the model behind the search form of `common\models\Addon`.
 */
class AddonSearch extends Addon
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'card_id', 'addon_type_id', 'addon_style_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['price', 'length', 'width', 'weight'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = Addon::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'card_id' => $this->card_id,
            'addon_type_id' => $this->addon_type_id,
            'addon_style_id' => $this->addon_style_id,
            'price' => $this->price,
            'length' => $this->length,
            'width' => $this->width,
            'weight' => $this->weight,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['c.id', 'c.name', 'c.status'];
        // Fields which are required
        $fields      = ['c.id', 'c.name', 'c.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('card c');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'c.id', $params['search']])
                    ->orFilterWhere(['like', 'c.name', $params['search']])
                    ->orderBy(['c.id' => SORT_DESC]);
        } else {
            $query->orderBy(['c.id' => SORT_DESC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'c.status', Card::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = Card::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = Card::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = Card::STATUS_DELETE; /// Delete
            }
            $query->orFilterWhere(['like', 'c.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => Card::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
