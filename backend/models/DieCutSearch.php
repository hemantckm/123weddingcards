<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DieCut;

/**
 * DieCutSearch represents the model behind the search form of `common\models\DieCut`.
 */
class DieCutSearch extends DieCut
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['code'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = DieCut::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['dc.id', 'dc.code', 'dc.status'];
        // Fields which are required
        $fields      = ['dc.id', 'dc.code', 'dc.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('die_cut dc');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'dc.id', $params['search']])
                    ->orFilterWhere(['like', 'dc.code', $params['search']])
                    ->orderBy(['dc.id' => SORT_DESC]);
        } else {
            $query->orderBy(['dc.id' => SORT_DESC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'dc.status', DieCut::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = DieCut::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = DieCut::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = DieCut::STATUS_DELETE; /// Delete
            }
            $query->orFilterWhere(['like', 'dc.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => DieCut::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
