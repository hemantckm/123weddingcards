<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ContactUs;

/**
 * ContactUsSearch represents the model behind the search form of `common\models\ContactUs`.
 */
class ContactUsSearch extends ContactUs
{

    /**
     * {@inheritdoc}
     */
    public function rules ()
    {
        return [
            [['id', 'default', 'status', 'created_at', 'updated_at'], 'integer'],
            [['heading', 'feature_image'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios ()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search ($params)
    {
        $query = ContactUs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'default' => $this->default,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'heading', $this->heading])
                ->andFilterWhere(['like', 'feature_image', $this->feature_image]);

        return $dataProvider;
    }

    /**
     * DataTable Search
     * @param type $params
     * @return type
     */
    public function dataTable ($params)
    {
        // Fields order should be same as we are using on listing
        $orderFileds = ['cu.id', 'cu.heading', 'cu.feature_image', 'cu.default', 'cu.status'];
        // Fields which are required
        $fields      = ['cu.id', 'cu.heading', 'cu.feature_image', 'cu.default', 'cu.status'];
        $query       = (new \yii\db\Query())
                ->select($fields)
                ->from('contact_us cu');

        if (strlen($params['search']) > 1) {
            $query->orFilterWhere(['like', 'cu.id', $params['search']])
                    ->orFilterWhere(['like', 'cu.heading', $params['search']])
                    ->orderBy(['cu.id' => SORT_DESC]);
        } else {
            $query->orderBy(['cu.id' => SORT_DESC]);
        }

        if ( ! Yii::$app->user->identity->master_admin) {
            $query->andWhere(['<>', 'cu.status', ContactUs::STATUS_DELETE]);
        }

        // Filter active and inactive records
        if (strtolower($params['search'] == 'inactive') || strtolower($params['search'] == 'active') || strtolower($params['search'] == 'delete')) {
            $status = ContactUs::STATUS_ACTIVE; // Active
            if (strtolower($params['search'] == 'inactive')) {
                $status = ContactUs::STATUS_INACTIVE; // Inactive
            }
            if (strtolower($params['search'] == 'delete')) {
                $status = ContactUs::STATUS_DELETE; // Delete
            }
            $query->orFilterWhere(['like', 'cu.status', $status]);
        }

        $query->limit($params['limit']);
        $query->offset($params['offset']);
        // Order field
        if ( ! empty($orderFileds[$params['orderField']])) {
            $query->orderBy([$orderFileds[$params['orderField']] => $params['orderBy']]);
        }
        return [
            'recordsTotal' => ContactUs::find()->count(),
            'recordsFiltered' => $query->count(),
            'records' => $query->all(),
        ];
    }

}
