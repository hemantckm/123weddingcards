<?php

use backend\helpers\Html;
use common\models\Addon;

/* @var $this yii\web\View */
/* @var $model common\models\Addon */

$this->title                   = 'View Addon: ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Addons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->code;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != Addon::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="addon-view">
    <div class="addon-form box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Specifications</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- Addon Detail Section -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <?= Html::label('Card Name', 'card-name', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', $model->card->name); ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('addon_type_id'), 'addon-addon_type_id', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        ?>
                                        <?= Html::tag('p',  ! empty($addonInfo->addon_type) ? $addonInfo->addon_type : ''); ?>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('addon_style_id'), 'addon-addon_style_id', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        ?>
                                        <?= Html::tag('p',  ! empty($addonInfo->addon_style) ? $addonInfo->addon_style : ''); ?>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('price'), 'addon-price', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', $model->price); ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('length'), 'addon-length', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', $model->length); ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('width'), 'addon-width', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', $model->width); ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('weight'), 'addon-weight', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', $model->weight); ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('status'), 'addon-status', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', Addon::getConstantList('STATUS_', Addon::className())[$model->status]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ASSEMBLY Section -->
        <div class="row">
            <!-- Envelope Section -->
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Envelope</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($addonEnvelopeModel->getAttributeLabel('addon_envelope_paper'), 'addonenvelope-addon_envelope_paper', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        $addonEnvelopeJson = json_decode($addonInfo->envelope, TRUE);
                                        if ( ! empty($addonEnvelopeJson)) {
                                            foreach ($addonEnvelopeJson as $addonEnvelope) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($addonEnvelope['paper']) ? $addonEnvelope['paper'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($addonEnvelopeModel->getAttributeLabel('addon_envelope_die'), 'addonenvelope-addon_envelope_die', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        $addonEnvelopeJson = json_decode($addonInfo->envelope, TRUE);
                                        if ( ! empty($addonEnvelopeJson)) {
                                            foreach ($addonEnvelopeJson as $addonEnvelope) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($addonEnvelope['diecut']) ? $addonEnvelope['diecut'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($addonEnvelopeModel->getAttributeLabel('paper_color'), 'addonenvelope-paper_color', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        $addonEnvelopeJson = json_decode($addonInfo->envelope, TRUE);
                                        if ( ! empty($addonEnvelopeJson)) {
                                            foreach ($addonEnvelopeJson as $addonEnvelope) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($addonEnvelope['paper_color']) ? $addonEnvelope['paper_color'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($addonEnvelopeModel->getAttributeLabel('addon_envelope_block'), 'addonenvelope-addon_envelope_block', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        $addonEnvelopeJson = json_decode($addonInfo->envelope, TRUE);
                                        if ( ! empty($addonEnvelopeJson)) {
                                            foreach ($addonEnvelopeJson as $addonEnvelope) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($addonEnvelope['block']) ? $addonEnvelope['block'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($addonEnvelopeModel->getAttributeLabel('paper_type'), 'addonenvelope-paper_type', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        $addonEnvelopeJson = json_decode($addonInfo->envelope, TRUE);
                                        if ( ! empty($addonEnvelopeJson)) {
                                            foreach ($addonEnvelopeJson as $addonEnvelope) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($addonEnvelope['paper_type']) ? $addonEnvelope['paper_type'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($addonEnvelopeModel->getAttributeLabel('addon_envelope_printing_method'), 'addonenvelope-addon_envelope_printing_method', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        $addonEnvelopeJson = json_decode($addonInfo->envelope, TRUE);
                                        if ( ! empty($addonEnvelopeJson)) {
                                            foreach ($addonEnvelopeJson as $addonEnvelope) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($addonEnvelope['printing_method']) ? $addonEnvelope['printing_method'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Folder Section -->
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Folder</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($addonFolderModel->getAttributeLabel('addon_folder_paper'), 'addonfolder-addon_folder_paper', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        $addonFolderJson = json_decode($addonInfo->folder, TRUE);
                                        if ( ! empty($addonFolderJson)) {
                                            foreach ($addonFolderJson as $addonFolder) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($addonFolder['paper']) ? $addonFolder['paper'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($addonFolderModel->getAttributeLabel('addon_folder_die'), 'addonfolder-addon_folder_die', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        $addonFolderJson = json_decode($addonInfo->folder, TRUE);
                                        if ( ! empty($addonFolderJson)) {
                                            foreach ($addonFolderJson as $addonFolder) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($addonFolder['diecut']) ? $addonFolder['diecut'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($addonFolderModel->getAttributeLabel('paper_color'), 'addonfolder-paper_color', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        $addonFolderJson = json_decode($addonInfo->folder, TRUE);
                                        if ( ! empty($addonFolderJson)) {
                                            foreach ($addonFolderJson as $addonFolder) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($addonFolder['paper_color']) ? $addonFolder['paper_color'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($addonFolderModel->getAttributeLabel('addon_folder_block'), 'addonfolder-addon_folder_block', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        $addonFolderJson = json_decode($addonInfo->folder, TRUE);
                                        if ( ! empty($addonFolderJson)) {
                                            foreach ($addonFolderJson as $addonFolder) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($addonFolder['block']) ? $addonFolder['block'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($addonFolderModel->getAttributeLabel('paper_type'), 'addonfolder-paper_type', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        $addonFolderJson = json_decode($addonInfo->folder, TRUE);
                                        if ( ! empty($addonFolderJson)) {
                                            foreach ($addonFolderJson as $addonFolder) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($addonFolder['paper_type']) ? $addonFolder['paper_type'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($addonFolderModel->getAttributeLabel('addon_folder_printing_method'), 'addonfolder-addon_folder_printing_method', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($addonsInfo)) {
                                    foreach ($addonsInfo as $addonInfo) {
                                        $addonFolderJson = json_decode($addonInfo->folder, TRUE);
                                        if ( ! empty($addonFolderJson)) {
                                            foreach ($addonFolderJson as $addonFolder) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($addonFolder['printing_method']) ? $addonFolder['printing_method'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
