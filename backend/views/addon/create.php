<?php
/* @var $this yii\web\View */
/* @var $model common\models\Addon */

$this->title                   = 'Create Addon';
$this->params['breadcrumbs'][] = ['label' => 'Addons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="addon-create">
    <?=
    $this->render('_form', [
        'model' => $model,
        'addonEnvelopeModel' => $addonEnvelopeModel,
        'addonFolderModel' => $addonFolderModel,
        'addonTypes' => $addonTypes,
        'addonStyles' => $addonStyles,
        'cards' => $cards,
        'papers' => $papers,
        'dies' => $dies,
        'colors' => $colors,
        'paperTypes' => $paperTypes,
        'blocks' => $blocks,
        'printingMethods' => $printingMethods,
    ])
    ?>
</div>
