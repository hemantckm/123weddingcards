<?php

use backend\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use common\models\Addon;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Addon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="addon-form box-body">
    <?php $form = ActiveForm::begin(['id' => 'addon-dynamic-form', 'options' => ['autocomplete' => 'off']]); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Specifications</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- Addon Detail Section -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'card_id')->widget(Select2::classname(), [
                                'data' => $cards,
                                'options' => ['data-ajaxUrl' => Url::to(['card/list']), 'placeholder' => 'Select Card'],
                            ])->label(Html::customLabel($model->getAttributeLabel('card_id'), ['card/create']));
                            ?>
                        </div>
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'addon_type_id')->dropDownList($addonTypes, [
                                'prompt' => 'Select Addon Type',
                                'data-ajaxUrl' => Url::to(['addon-type/list']),
                            ])->label(Html::customLabel($model->getAttributeLabel('addon_type_id'), ['addon-type/create']));
                            ?>
                        </div>
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'addon_style_id')->widget(DepDrop::classname(), [
                                'data' => [$model->addon_style_id => $model->addon_style_id],
                                'options' => ['data-ajaxUrl' => Url::to(['addon-style/list'])],
                                'pluginOptions' => [
                                    'depends' => ['addon-addon_type_id'],
                                    'placeholder' => 'Select Addon Style',
                                    'url' => Url::to(['addon/addon-style']),
                                    'initialize' => true
                                ]
                            ])->label(Html::customLabel($model->getAttributeLabel('addon_style_id'), ['addon-style/create']));
                            ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'price')->textInput(['maxlength' => true, 'placeholder' => 'Enter Price']) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'length')->textInput(['maxlength' => true, 'placeholder' => 'Enter Length']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'width')->textInput(['maxlength' => true, 'placeholder' => 'Enter Width']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'weight')->textInput(['maxlength' => true, 'placeholder' => 'Enter Weight']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'status')->dropDownList(Addon::getConstantList('STATUS_', Addon::className())) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ASSEMBLY Section -->
    <div class="row">
        <!-- Envelope Section -->

        <?=
        $this->render('/partials/-addon-envelope', [
            'form' => $form,
            'papers' => $papers,
            'dies' => $dies,
            'colors' => $colors,
            'paperTypes' => $paperTypes,
            'blocks' => $blocks,
            'printingMethods' => $printingMethods,
            'addonEnvelopeModel' => $addonEnvelopeModel,
            'addonEnvelopePaperModelId' => ! empty($addonEnvelopePaperModelId) ? $addonEnvelopePaperModelId : array (),
            'addonEnvelopeDieModelId' => ! empty($addonEnvelopeDieModelId) ? $addonEnvelopeDieModelId : array (),
            'addonEnvelopeBlockModelId' => ! empty($addonEnvelopeBlockModelId) ? $addonEnvelopeBlockModelId : array (),
            'addonEnvelopePrintingMethodModelId' => ! empty($addonEnvelopePrintingMethodModelId) ? $addonEnvelopePrintingMethodModelId : array (),
        ]);
        ?>

        <!-- Folder Section -->
        <?=
        $this->render('/partials/-addon-folder', [
            'form' => $form,
            'papers' => $papers,
            'dies' => $dies,
            'colors' => $colors,
            'paperTypes' => $paperTypes,
            'blocks' => $blocks,
            'printingMethods' => $printingMethods,
            'addonFolderModel' => $addonFolderModel,
            'addonFolderPaperModelId' => ! empty($addonFolderPaperModelId) ? $addonFolderPaperModelId : array (),
            'addonFolderDieModelId' => ! empty($addonFolderDieModelId) ? $addonFolderDieModelId : array (),
            'addonFolderBlockModelId' => ! empty($addonFolderBlockModelId) ? $addonFolderBlockModelId : array (),
            'addonFolderPrintingMethodModelId' => ! empty($addonFolderPrintingMethodModelId) ? $addonFolderPrintingMethodModelId : array (),
        ]);
        ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerCssFile(
        '@web/css/custom.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]
);
$this->registerJsFile(
        '@web/js/refresh.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
        '@web/js/addon.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
