<?php
/* @var $this yii\web\View */
/* @var $model common\models\Addon */

$this->title                   = 'Update Addon: ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Addons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="addon-update">
    <?=
    $this->render('_form', [
        'model' => $model,
        'addonEnvelopeModel' => $addonEnvelopeModel,
        'addonFolderModel' => $addonFolderModel,
        'addonTypes' => $addonTypes,
        'addonStyles' => $addonStyles,
        'cards' => $cards,
        'papers' => $papers,
        'dies' => $dies,
        'colors' => $colors,
        'paperTypes' => $paperTypes,
        'blocks' => $blocks,
        'printingMethods' => $printingMethods,
        'addonEnvelopePaperModelId' => $addonEnvelopePaperModelId,
        'addonEnvelopeDieModelId' => $addonEnvelopeDieModelId,
        'addonEnvelopeBlockModelId' => $addonEnvelopeBlockModelId,
        'addonEnvelopePrintingMethodModelId' => $addonEnvelopePrintingMethodModelId,
        'addonFolderPaperModelId' => $addonFolderPaperModelId,
        'addonFolderDieModelId' => $addonFolderDieModelId,
        'addonFolderBlockModelId' => $addonFolderBlockModelId,
        'addonFolderPrintingMethodModelId' => $addonFolderPrintingMethodModelId,
    ])
    ?>
</div>
