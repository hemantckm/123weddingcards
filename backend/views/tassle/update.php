<?php
/* @var $this yii\web\View */
/* @var $model common\models\Tassle */

$this->title                   = 'Update Tassle: ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Tassles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tassle-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
