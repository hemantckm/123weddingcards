<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Tassle;

/* @var $this yii\web\View */
/* @var $model common\models\Tassle */

$this->title                   = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Tassles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != Tassle::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="tassle-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            [
                'attribute' => 'status',
                'value' => Tassle::getConstantList('STATUS_', Tassle::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
