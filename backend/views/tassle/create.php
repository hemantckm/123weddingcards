<?php
/* @var $this yii\web\View */
/* @var $model common\models\Tassle */

$this->title                   = 'Create Tassle';
$this->params['breadcrumbs'][] = ['label' => 'Tassles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tassle-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
