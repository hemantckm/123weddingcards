<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Theme;

/* @var $this yii\web\View */
/* @var $model common\models\Theme */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Themes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != Theme::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="theme-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'status',
                'value' => Theme::getConstantList('STATUS_', Theme::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
