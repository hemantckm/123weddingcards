<?php
/* @var $this yii\web\View */
/* @var $model common\models\PaperType */

$this->title                   = 'Create Paper Type';
$this->params['breadcrumbs'][] = ['label' => 'Paper Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paper-type-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
