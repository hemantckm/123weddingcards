<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Vendor;

/* @var $this yii\web\View */
/* @var $model common\models\Vendor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vendor-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Enter Vendor Heading']) ?>
        </div>
        <div class="col-md-4">  
            <?= $form->field($model, "image")->fileInput(['maxlength' => true, 'placeholder' => 'Select Vendor Image', 'accept' => 'image/*', 'class' => 'form-control']) ?> 
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList(Vendor::getConstantList('STATUS_', Vendor::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
