<?php

use backend\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\QuestionAnswerFaq;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\QuestionAnswerFaq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="question-answer-faq-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
    <div class="row">
        <div class="col-md-4">
            <?=
            $form->field($model, 'question_answer_id')->widget(Select2::classname(), [
                'data' => $questions,
                'options' => ['data-ajaxUrl' => Url::to(['question-answer/list']), 'placeholder' => 'Select Question'],
            ])->label(Html::customLabel($model->getAttributeLabel('question_answer_id'), ['question-answer/create']));
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'faq_id')->widget(Select2::classname(), [
                'data' => $faqs,
                'options' => ['data-ajaxUrl' => Url::to(['faq/list']), 'placeholder' => 'Select FAQ'],
            ])->label(Html::customLabel($model->getAttributeLabel('faq_id'), ['faq/create']));
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'faq_type_id')->widget(DepDrop::classname(), [
                'data' => [$model->faq_type_id => $model->faq_type_id],
                'options' => ['data-ajaxUrl' => Url::to(['faq-type/list'])],
                'pluginOptions' => [
                    'depends' => ['questionanswerfaq-faq_id'],
                    'placeholder' => 'Select FAQ Type',
                    'url' => Url::to(['question-answer-faq/faq-type']),
                    'initialize' => true
                ]
            ])->label(Html::customLabel($model->getAttributeLabel('faq_type_id'), ['faq-type/create']));
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList(QuestionAnswerFaq::getConstantList('STATUS_', QuestionAnswerFaq::className())) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerCssFile(
        '@web/css/custom.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]
);
$this->registerJsFile(
        '@web/js/refresh.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>

