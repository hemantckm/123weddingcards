<?php
/* @var $this yii\web\View */
/* @var $model common\models\QuestionAnswerFaq */

$this->title                   = 'Update Question Answer Faq: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Question Answer Faqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Question Answer Faq: ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="question-answer-faq-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
        'questions' => $questions,
        'faqs' => $faqs,
    ])
    ?>
</div>
