<?php
/* @var $this yii\web\View */
/* @var $model common\models\QuestionAnswerFaq */

$this->title                   = 'Create Question Answer Faq';
$this->params['breadcrumbs'][] = ['label' => 'Question Answer Faqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-answer-faq-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
        'questions' => $questions,
        'faqs' => $faqs,
    ])
    ?>
</div>
