<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\QuestionAnswerFaq;

/* @var $this yii\web\View */
/* @var $model common\models\QuestionAnswerFaq */

$this->title                   = 'View Question Answer Faq: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Question Answer Faqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != QuestionAnswerFaq::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="question-answer-faq-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'question_answer_id',
                'value' => $model->questionAnswer->question,
            ],
            [
                'attribute' => 'faq_id',
                'value' => $model->faq->heading,
            ],
            [
                'attribute' => 'faq_type_id',
                'value' => $model->faqType->name,
            ],
            [
                'attribute' => 'status',
                'value' => QuestionAnswerFaq::getConstantList('STATUS_', QuestionAnswerFaq::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
