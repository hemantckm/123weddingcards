<?php
/* @var $this yii\web\View */
/* @var $model common\models\AddonType */

$this->title                   = 'Update Addon Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Addon Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="addon-type-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
