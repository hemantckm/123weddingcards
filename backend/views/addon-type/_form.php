<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\AddonType;

/* @var $this yii\web\View */
/* @var $model common\models\AddonType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="addon-type-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Enter Addon Type Name']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'placeholder' => 'Enter Addon Type Code']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList(AddonType::getConstantList('STATUS_', AddonType::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>