<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\AddonType;

/* @var $this yii\web\View */
/* @var $model common\models\AddonType */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Addon Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != AddonType::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="addon-type-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'status',
                'value' => AddonType::getConstantList('STATUS_', AddonType::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
