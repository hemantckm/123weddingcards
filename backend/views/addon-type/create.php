<?php
/* @var $this yii\web\View */
/* @var $model common\models\AddonType */

$this->title                   = 'Create Addon Type';
$this->params['breadcrumbs'][] = ['label' => 'Addon Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="addon-type-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
