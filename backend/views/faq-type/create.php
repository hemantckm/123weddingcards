<?php
/* @var $this yii\web\View */
/* @var $model common\models\FaqType */

$this->title                   = 'Create Faq Type';
$this->params['breadcrumbs'][] = ['label' => 'Faq Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-type-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
