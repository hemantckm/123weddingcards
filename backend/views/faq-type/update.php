<?php
/* @var $this yii\web\View */
/* @var $model common\models\FaqType */

$this->title                   = 'Update Faq Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Faq Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="faq-type-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
