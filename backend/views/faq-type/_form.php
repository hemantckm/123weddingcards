<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\FaqType;

/* @var $this yii\web\View */
/* @var $model common\models\FaqType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-type-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Enter FAQ Type Name']) ?>
        </div>
        <div class="col-md-4">  
            <?= $form->field($model, "icon")->fileInput(['maxlength' => true, 'placeholder' => 'Select FAQ Type Icon', 'accept' => 'image/*', 'class' => 'form-control']) ?> 
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList(FaqType::getConstantList('STATUS_', FaqType::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
