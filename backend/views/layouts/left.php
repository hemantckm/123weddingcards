<?php

use backend\helpers\MenuHelper;
use yii\helpers\Html;
?>
<aside class="main-sidebar">
    <?php
    /**
     * User Profile Picture.
     */
    $userImages          = array ();
    $time                = '?r=' . time();
    $userImages['small'] = \common\models\User::NO_IMAGE_SMALL . $time;
    if ( ! empty(Yii::$app->user->identity->image)) {
        $thumbFilePath = '/' . \common\models\User::MAIN_DIRECTORY . \common\models\User::THUMBNAIL_FILEPATH . Yii::$app->user->identity->image;
        if (file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
            $userImages['small'] = $thumbFilePath . $time;
        }
    }
    ?>
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?= Html::img(Yii::getAlias('@web') . $userImages['small'], ['alt' => 'user-image', 'class' => 'user-image', 'style' => ['height' => '45px']]); ?>
            </div>
            <div class="pull-left info">
                <p><?php
                    if ( ! empty(Yii::$app->user->identity)) {
                        echo ucfirst(Yii::$app->user->identity->username);
                    }
                    ?>
                </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <?=
        dmstr\widgets\Menu::widget([
            'options' => ['class' => 'sidebar-menu'],
            'items' => MenuHelper::getAssignedMenu(Yii::$app->user->id)
        ]);
        ?>
    </section>
</aside>
