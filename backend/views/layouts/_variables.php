<?php

use yii\helpers\Url;
?>
<script>
    var siteUrls = {
        base_url: "<?= Url::base(true); ?>"
    };

    var cardUrls = {
        cardPapers: "<?= Url::to(['card/card-paper']) ?>",
        cardThemes: "<?= Url::to(['card/card-theme']) ?>",
    };

    var addonUrls = {
        addonPapers: "<?= Url::to(['addon/addon-paper']) ?>",
    };
</script>