<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<!-- loading  Div Please don't remove -->
<div id="loader" style="display:none"></div> 
<header class="main-header">
    <?php
    /**
     * User Profile Picture.
     */
    $userImages          = array ();
    $time                = '?r=' . time();
    $userImages['small'] = \common\models\User::NO_IMAGE_SMALL . $time;
    if ( ! empty(Yii::$app->user->identity->image)) {
        $thumbFilePath = '/' . \common\models\User::MAIN_DIRECTORY . \common\models\User::THUMBNAIL_FILEPATH . Yii::$app->user->identity->image;
        if (file_exists(Yii::$app->basePath . '/web' . $thumbFilePath)) {
            $userImages['small'] = $thumbFilePath . $time;
        }
    }
    ?>
    <?= Html::a('<span class="logo-mini">' . Yii::$app->name . '</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?= Html::img(Yii::getAlias('@web') . $userImages['small'], ['alt' => 'user-image', 'class' => 'user-image']); ?>
                        <span class="hidden-xs"><?php
                            if ( ! empty(Yii::$app->user->identity)) {
                                echo ucfirst(Yii::$app->user->identity->username);
                            }
                            ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header"> 
                            <?= Html::img(Yii::getAlias('@web') . $userImages['small'], ['alt' => 'user-image', 'class' => 'img-circle']); ?>
                            <p>
                                <?php
                                if ( ! empty(Yii::$app->user->identity)) {
                                    echo ucfirst(Yii::$app->user->identity->username);
                                }
                                ?> - (<?php
                                if ( ! empty(Yii::$app->user->identity)) {
                                    echo Yii::$app->user->identity->email;
                                }
                                ?>)
                                <small>Member since  <b> <?php
                                        if ( ! empty(Yii::$app->user->identity)) {
                                            echo ucfirst(date('D M j Y', Yii::$app->user->identity->created_at));
                                        }
                                        ?></b></small>
                            </p>
                        </li>  
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left"></div>
                            <div class="pull-left">
                                <?= Html::a('Profile', ['/user/profile'], ['class' => 'btn btn-default btn-flat']) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a('Sign out', ['/site/logout'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
