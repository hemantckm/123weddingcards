<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

if (Yii::$app->controller->action->id === 'login') {
    /**
     * Do not use this code in your template. Remove it. 
     * Instead, use the code  $this->layout = '//main-login'; in your controller.
     */
    echo $this->render('main-login', ['content' => $content]);
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
        //backend\assets\AdminLtePluginAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }
    dmstr\web\AdminLteAsset::register($this);
    rmrevin\yii\ionicon\AssetBundle::register($this); //ionicon Font Use.
    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset; ?>"/>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <?= Html::csrfMetaTags(); ?>
            <title><?= Html::encode($this->title); ?></title>
            <?php
            $this->registerCssFile(
                    '@web/css/jquery.dataTables.min.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]
            );
            $this->registerJsFile(
                    '@web/js/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
            );
            $this->registerJsFile(
                    '@web/js/dataTables.js', ['depends' => [\yii\web\JqueryAsset::className()]]
            );
            ?>
            <?php $this->head(); ?>
            <?= $this->render('_variables') ?>
        </head>
        <?php
        $sidebar = '';
        if (in_array(Yii::$app->controller->action->controller->id, ['product-image', 'product-movement'])) {
            $sidebar = 'sidebar-collapse';
        }
        ?>
        <body class="hold-transition <?= \dmstr\helpers\AdminLteHelper::skinClass() ?> sidebar-mini <?= $sidebar; ?>  ">
            <?php $this->beginBody() ?>
            <div class="wrapper">
                <?= $this->render('header.php', ['directoryAsset' => $directoryAsset]); ?>
                <?= $this->render('left.php', ['directoryAsset' => $directoryAsset]); ?>
                <?= $this->render('content.php', ['content' => $content, 'directoryAsset' => $directoryAsset]); ?>
            </div>
            <?php $this->endBody(); ?>
        </body>
    </html>
    <?php $this->endPage(); ?>
<?php } ?>  