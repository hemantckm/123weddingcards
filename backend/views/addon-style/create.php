<?php
/* @var $this yii\web\View */
/* @var $model common\models\AddonStyle */

$this->title                   = 'Create Addon Style';
$this->params['breadcrumbs'][] = ['label' => 'Addon Styles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="addon-style-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
        'addonTypes' => $addonTypes,
    ])
    ?>
</div>
