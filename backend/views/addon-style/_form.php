<?php

use backend\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\AddonStyle;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\AddonStyle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="addon-style-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Enter Addon Type Name']) ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'addon_type_id')->widget(Select2::classname(), [
                'data' => $addonTypes,
                'options' => ['data-ajaxUrl' => Url::to(['addon-type/list']), 'placeholder' => 'Select Addon Type'],
            ])->label(Html::customLabel('Paper Type', ['addon-type/create']));
            ?> 
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList(AddonStyle::getConstantList('STATUS_', AddonStyle::className())) ?>
        </div>

    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerCssFile(
        '@web/css/custom.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]
);
$this->registerJsFile(
        '@web/js/refresh.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>