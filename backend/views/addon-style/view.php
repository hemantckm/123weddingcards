<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\AddonStyle;

/* @var $this yii\web\View */
/* @var $model common\models\AddonStyle */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Addon Styles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != AddonStyle::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="addon-style-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'addon_type_id',
                'value' => $model->addonType->name,
                'label' => $model->getAttributeLabel('addon_type_id'),
            ],
            [
                'attribute' => 'status',
                'value' => AddonStyle::getConstantList('STATUS_', AddonStyle::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
