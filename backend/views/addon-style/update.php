<?php
/* @var $this yii\web\View */
/* @var $model common\models\AddonStyle */

$this->title                   = 'Update Addon Style: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Addon Styles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="addon-style-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
        'addonTypes' => $addonTypes,
    ])
    ?>
</div>
