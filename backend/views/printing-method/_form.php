<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\PrintingMethod;

/* @var $this yii\web\View */
/* @var $model common\models\PrintingMethod */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="printing-method-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Enter Printing Name']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList(PrintingMethod::getConstantList('STATUS_', PrintingMethod::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
