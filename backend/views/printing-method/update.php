<?php
/* @var $this yii\web\View */
/* @var $model common\models\PrintingMethod */

$this->title                   = 'Update Printing Method: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Printing Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printing-method-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
