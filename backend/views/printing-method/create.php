<?php
/* @var $this yii\web\View */
/* @var $model common\models\PrintingMethod */

$this->title                   = 'Create Printing Method';
$this->params['breadcrumbs'][] = ['label' => 'Printing Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printing-method-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
