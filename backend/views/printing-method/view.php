<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\PrintingMethod;

/* @var $this yii\web\View */
/* @var $model common\models\PrintingMethod */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Printing Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != PrintingMethod::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="printing-method-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'status',
                'value' => PrintingMethod::getConstantList('STATUS_', PrintingMethod::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
