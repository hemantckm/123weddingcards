<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\SocialMedia;

/* @var $this yii\web\View */
/* @var $model common\models\SocialMedia */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Social Media', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != SocialMedia::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="social-media-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'link',
            [
                'attribute' => 'data',
                'format' => 'raw',
                'value' => '<i class="fa fa-'.$model->data.' fa-lg"></i>',
                'label' => $model->getAttributeLabel('data'),
            ],
            [
                'attribute' => 'status',
                'value' => SocialMedia::getConstantList('STATUS_', SocialMedia::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
