<?php
/* @var $this yii\web\View */
/* @var $model common\models\SocialMedia */

$this->title                   = 'Create Social Media';
$this->params['breadcrumbs'][] = ['label' => 'Social Media', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-media-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
