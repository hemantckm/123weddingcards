<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\SocialMedia;

/* @var $this yii\web\View */
/* @var $model common\models\SocialMedia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="social-media-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Enter Social Name']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'link')->textInput(['maxlength' => true, 'placeholder' => 'Enter Social Link Address']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'data')->textInput(['maxlength' => true, 'placeholder' => 'Enter Data Link Icon']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList(SocialMedia::getConstantList('STATUS_', SocialMedia::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
