<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\SocialMedia;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SocialMediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Social Media';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-media-index box box-body box-primary">
    <div class="row">
        <div class="col-xs-12 table-responsive"> 
            <p>
                <?= Html::a('Create Social Media', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="social-media-form box-body">
                <?php $form                          = ActiveForm::begin(['id' => 'action-form', 'options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data']]); ?>
                <div class="row">
                    <div class="col-md-2">
                        <?= $form->field($model, 'status')->dropDownList(SocialMedia::getConstantList('STATUS_', SocialMedia::className()))->label(false); ?> 
                    </div>
                    <div class="col-md-2">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']); ?>
                    </div>
                </div>
                <table id="example" class="display table table-striped table-bordered hover compact" data-ajaxUrl="<?= \yii\helpers\Url::to(['datatable']); ?> " style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align: center;"><input class="dt-body-center" name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
                            <th><?= $model->getAttributeLabel('name'); ?></th>
                            <th><?= $model->getAttributeLabel('data'); ?></th>
                            <th><?= $model->getAttributeLabel('link'); ?></th>
                            <th><?= $model->getAttributeLabel('status'); ?></th>
                            <th class="noSort" width="11%">Action</th>
                        </tr>
                    </thead>
                </table>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
