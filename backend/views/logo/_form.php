<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Logo;

/* @var $this yii\web\View */
/* @var $model common\models\Logo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logo-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'heading')->textInput(['maxlength' => true, 'placeholder' => 'Enter Logo Heading']) ?>
        </div>
        <div class="col-md-3">  
            <?= $form->field($model, "image")->fileInput(['maxlength' => true, 'placeholder' => 'Select Logo Image', 'accept' => 'image/*', 'class' => 'form-control']) ?> 
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'default')->dropDownList(Logo::getConstantList('DEFAULT_', Logo::className())) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList(Logo::getConstantList('STATUS_', Logo::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
