<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Logo;

/* @var $this yii\web\View */
/* @var $model common\models\Logo */

$this->title                   = $model->heading;
$this->params['breadcrumbs'][] = ['label' => 'Logos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != Logo::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="logo-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'heading',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => $image,
                'label' => $model->getAttributeLabel('image'),
            ],
            [
                'attribute' => 'default',
                'value' => Logo::getConstantList('DEFAULT_', Logo::className())[$model->default],
                'label' => $model->getAttributeLabel('default'),
            ],
            [
                'attribute' => 'status',
                'value' => Logo::getConstantList('STATUS_', Logo::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
<?php
$this->registerJsFile(
        '@web/js/jquery.fancybox.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerCssFile(
        '@web/css/jquery.fancybox.min.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]
);
?>
