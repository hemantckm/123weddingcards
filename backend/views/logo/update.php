<?php
/* @var $this yii\web\View */
/* @var $model common\models\Logo */

$this->title                   = 'Update Logo: ' . $model->heading;
$this->params['breadcrumbs'][] = ['label' => 'Logos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->heading, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="logo-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
