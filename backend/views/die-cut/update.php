<?php
/* @var $this yii\web\View */
/* @var $model common\models\DieCut */

$this->title                   = 'Update Die Cut: ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Die Cuts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="die-cut-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
