<?php
/* @var $this yii\web\View */
/* @var $model common\models\DieCut */

$this->title                   = 'Create Die Cut';
$this->params['breadcrumbs'][] = ['label' => 'Die Cuts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="die-cut-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
