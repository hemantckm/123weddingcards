<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\DieCut;

/* @var $this yii\web\View */
/* @var $model common\models\DieCut */

$this->title                   = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Die Cuts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != DieCut::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="die-cut-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            [
                'attribute' => 'status',
                'value' => DieCut::getConstantList('STATUS_', DieCut::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
