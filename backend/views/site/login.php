<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title                   = 'Login';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile("@web/css/AdminLTE.min.css");
?>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <?=
            Html::img('@web/images/logo.png', [
                'alt' => Yii::$app->name,
                'title' => Yii::$app->name
            ]);
            ?>
        </div> 
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <?php $form                          = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>
            <div style="color:#999;margin:1em 0">
                If you forgot your password you can <?= Html::a('reset it', ['user/request-password-reset']) ?>.
                For new user you can <?= Html::a('signup', ['user/signup']) ?>.
            </div>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div> 
            <?php ActiveForm::end(); ?>
        </div> 
    </div>  
</body>