<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\BoxType;

/* @var $this yii\web\View */
/* @var $model common\models\BoxType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-type-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Enter Type Name']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList(BoxType::getConstantList('STATUS_', BoxType::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
