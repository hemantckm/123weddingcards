<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\BoxType;

/* @var $this yii\web\View */
/* @var $model common\models\BoxType */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Box Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != BoxType::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="box-type-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'status',
                'value' => BoxType::getConstantList('STATUS_', BoxType::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
