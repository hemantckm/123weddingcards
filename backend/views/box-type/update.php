<?php
/* @var $this yii\web\View */
/* @var $model common\models\BoxType */

$this->title                   = 'Update Box Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Box Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box-type-update box box-primary">  
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
