<?php
/* @var $this yii\web\View */
/* @var $model common\models\BoxType */

$this->title                   = 'Create Box Type';
$this->params['breadcrumbs'][] = ['label' => 'Box Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box-type-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
