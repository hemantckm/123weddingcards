<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CardInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Card Infos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-info-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Card Info', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'card_id',
            'theme:ntext',
            'envelope:ntext',
            'folder:ntext',
            'scroll:ntext',
            //'box:ntext',
            //'insert:ntext',
            //'additions:ntext',
            //'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
