<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CardInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'card_id')->textInput() ?>

    <?= $form->field($model, 'theme')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'envelope')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'folder')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'scroll')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'box')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'insert')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'additions')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
