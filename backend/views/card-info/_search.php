<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CardInfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card-info-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'card_id') ?>

    <?= $form->field($model, 'theme') ?>

    <?= $form->field($model, 'envelope') ?>

    <?= $form->field($model, 'folder') ?>

    <?= $form->field($model, 'scroll') ?>

    <?php // echo $form->field($model, 'box') ?>

    <?php // echo $form->field($model, 'insert') ?>

    <?php // echo $form->field($model, 'additions') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
