<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CardInfo */

$this->title = 'Update Card Info: ' . $model->card_id;
$this->params['breadcrumbs'][] = ['label' => 'Card Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->card_id, 'url' => ['view', 'id' => $model->card_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="card-info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
