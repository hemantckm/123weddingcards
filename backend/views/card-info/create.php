<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CardInfo */

$this->title = 'Create Card Info';
$this->params['breadcrumbs'][] = ['label' => 'Card Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
