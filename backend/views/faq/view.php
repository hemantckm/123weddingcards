<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Faq;

/* @var $this yii\web\View */
/* @var $model common\models\Faq */

$this->title                   = $model->heading;
$this->params['breadcrumbs'][] = ['label' => 'Faqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != Faq::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="faq-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'heading',
            [
                'attribute' => 'faq_faq_type',
                'value' => ! empty($faqFaqTypes) ? implode(', ', $faqFaqTypes) : '',
                'label' => $model->getAttributeLabel('faq_faq_type'),
            ],
            [
                'attribute' => 'feature_image',
                'format' => 'raw',
                'value' => $feature_image,
                'label' => $model->getAttributeLabel('feature_image'),
            ],
            [
                'attribute' => 'default',
                'value' => Faq::getConstantList('DEFAULT_', Faq::className())[$model->default],
                'label' => $model->getAttributeLabel('default'),
            ],
            [
                'attribute' => 'status',
                'value' => Faq::getConstantList('STATUS_', Faq::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
<?php
$this->registerJsFile(
        '@web/js/jquery.fancybox.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerCssFile(
        '@web/css/jquery.fancybox.min.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]
);
?>

