<?php

use backend\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Faq;
use kartik\select2\Select2;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Faq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-form box-body">
    <?php $form                      = ActiveForm::begin(['options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'heading')->textInput(['maxlength' => true, 'placeholder' => 'Enter FAQ Heading']) ?>
        </div>
        <div class="col-md-3">
            <?php
            $faq_faq_type_groups_array = array ();
            if ( ! empty($faqFaqTypeModelId)) {
                foreach ($faqFaqTypeModelId as $faqFaqTypeId) {
                    $faq_faq_type_groups_array[$faqFaqTypeId] = array ("selected" => true);
                }
            }
            ?>
            <?=
            $form->field($model, 'faq_faq_type')->widget(Select2::classname(), [
                'data' => $faqTypes,
                'options' => ['data-ajaxUrl' => Url::to(['faq-type/list']), 'placeholder' => 'Select FAQ Type', 'multiple' => true, 'options' => $faq_faq_type_groups_array],
            ])->label(Html::customLabel($model->getAttributeLabel('faq_faq_type'), ['faq-type/create']));
            ?> 
        </div>
        <div class="col-md-3">  
            <?= $form->field($model, "feature_image")->fileInput(['maxlength' => true, 'placeholder' => 'Select FAQ Feature', 'accept' => 'image/*', 'class' => 'form-control']) ?> 
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'default')->dropDownList(Faq::getConstantList('DEFAULT_', Faq::className())) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList(Faq::getConstantList('STATUS_', Faq::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerCssFile(
        '@web/css/custom.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]
);
$this->registerJsFile(
        '@web/js/refresh.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
