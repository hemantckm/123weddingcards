<?php
/* @var $this yii\web\View */
/* @var $model common\models\Faq */

$this->title                   = 'Create Faq';
$this->params['breadcrumbs'][] = ['label' => 'Faqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
        'faqTypes' => $faqTypes,
    ])
    ?>
</div>
