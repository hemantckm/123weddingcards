<?php
/* @var $this yii\web\View */
/* @var $model common\models\Faq */

$this->title                   = 'Update Faq: ' . $model->heading;
$this->params['breadcrumbs'][] = ['label' => 'Faqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->heading, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="faq-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
        'faqTypes' => $faqTypes,
        'faqFaqTypeModelId' => $faqFaqTypeModelId,
    ])
    ?>
</div>
