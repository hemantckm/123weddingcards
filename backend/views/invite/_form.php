<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Invite;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Invite */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invite-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'heading')->textInput(['maxlength' => true, 'placeholder' => 'Enter Invite Heading']) ?>
        </div>
        <div class="col-md-6">
            <?=
            $form->field($model, "description")->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'standard',
                'clientOptions' => [
                    'height' => 100,
                    'replaceDivs' => false,
                    'allowedContent' => true,
                ]
            ])
            ?>
        </div>
        <div class="col-md-3">  
            <?= $form->field($model, "image")->fileInput(['maxlength' => true, 'placeholder' => 'Select Invite Image', 'accept' => 'image/*', 'class' => 'form-control']) ?> 
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">  
            <?= $form->field($model, 'button_name')->textInput(['maxlength' => true, 'placeholder' => 'Enter Button Name']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'link')->textInput(['maxlength' => true, 'placeholder' => 'Enter Button Link Url']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'default')->dropDownList(Invite::getConstantList('DEFAULT_', Invite::className())) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList(Invite::getConstantList('STATUS_', Invite::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
