<?php
/* @var $this yii\web\View */
/* @var $model common\models\Invite */

$this->title                   = 'Update Invite: ' . $model->heading;
$this->params['breadcrumbs'][] = ['label' => 'Invites', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->heading, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="invite-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
