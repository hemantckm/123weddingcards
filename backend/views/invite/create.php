<?php
/* @var $this yii\web\View */
/* @var $model common\models\Invite */

$this->title                   = 'Create Invite';
$this->params['breadcrumbs'][] = ['label' => 'Invites', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invite-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
