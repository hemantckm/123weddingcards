<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Website;

/* @var $this yii\web\View */
/* @var $model common\models\Website */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="website-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Enter Website Name']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'url')->textInput(['maxlength' => true, 'placeholder' => 'Enter Website Url']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList(Website::getConstantList('STATUS_', Website::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
