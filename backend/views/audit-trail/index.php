<?php

use common\models\AuditTrail;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AuditTrailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Audit Trails';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-trail-index box box-body table-responsive box-primary">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'label' => 'User',
                'attribute' => 'user_id',
                'filter' => Html::activeDropDownList($searchModel, 'user_id', AuditTrail::userList(), ['class' => 'form-control', 'prompt' => 'Select User']),
                'value' => function ($model, $key, $index) {
                    return AuditTrail::userList()[$model->user_id];
                },
            ],
            [
                'label' => 'Action',
                'attribute' => 'action',
                'filter' => Html::activeDropDownList($searchModel, 'action', AuditTrail::getConstantList('ACTION_', AuditTrail::className()), ['class' => 'form-control', 'prompt' => 'Action']),
                'value' => function ($model, $key, $index) {
                    return AuditTrail::getConstantList('ACTION_', AuditTrail::className())[$model->action];
                },
            ],
            [
                'label' => 'Table',
                'attribute' => 'model',
                'filter' => Html::activeDropDownList($searchModel, 'model', AuditTrail::modelList(), ['class' => 'form-control', 'prompt' => 'Select Table']),
                'value' => function ($model, $key, $index) {
                    return AuditTrail::modelList()[$model->model];
                },
            ],
            [
                'attribute' => 'field',
                'value' => function ($model) {
                    return $model->field;
                },
            ],
            [
                'attribute' => 'old_value',
                'value' => function ($model) {
                    return substr($model->old_value, 0, 30);
                },
            ],
            [
                'attribute' => 'new_value',
                'value' => function ($model) {
                    return substr($model->new_value, 0, 25);
                },
            ],
            [
                'label' => 'Date-Time',
                'value' => function ($model, $key, $index) {
                    if ($model->stamp) {
                        return date("d M,Y h:i:s", strtotime($model->stamp));
                    }
                },
                'attribute' => 'stamp',
                'format' => 'raw',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'stamp',
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-M-yyyy'
                    ]
                ]),
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(
                                        '<span class="glyphicon glyphicon-eye-open"></span>', ['audit-trail/view', 'id' => $model->id], ['title' => Yii::t('app', 'View Audit Trails'), 'class' => 'btn btn-info margin-r-5']
                        );
                    },
                ],
            ],
        ],
    ]);
    ?>
</div>
