<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AuditTrail */

$this->title                   = \common\models\User::findIdentity($model->user_id)->username;
$this->params['breadcrumbs'][] = ['label' => 'Audit Trails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="audit-trail-view box box-body box-primary">

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            array (
                'label' => 'User',
                'value' => \common\models\User::findIdentity($model->user_id)->username
            ),
            //'id',
            'old_value:ntext',
            'new_value:ntext',
            'action',
            'model',
            'field',
            'stamp:date',
        //'model_id',
        ],
    ])
    ?>

</div>
