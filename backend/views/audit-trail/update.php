<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AuditTrail */

$this->title                   = 'Update Audit Trail: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Audit Trails', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="audit-trail-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
