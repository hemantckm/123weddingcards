<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Paper;

/* @var $this yii\web\View */
/* @var $model common\models\Paper */

$this->title                   = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Papers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != Paper::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="paper-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            [
                'attribute' => 'paper_type_id',
                'value' => $model->paperType->name,
            ],
            [
                'attribute' => 'color_id',
                'value' => $model->color->name,
            ],
            [
                'attribute' => 'status',
                'value' => Paper::getConstantList('STATUS_', Paper::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
