<?php

use backend\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\Paper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Paper */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="loader" style="display:none"></div>
<div class="paper-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'placeholder' => 'Enter Paper Code']); ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'paper_type_id')->widget(Select2::classname(), [
                'data' => $paperTypes,
                'options' => ['data-ajaxUrl' => Url::to(['paper-type/list']), 'placeholder' => 'Select Paper Type'],
            ])->label(Html::customLabel($model->getAttributeLabel('paper_type_id'), ['paper-type/create']));
            ?> 
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'color_id')->widget(Select2::classname(), [
                'data' => $colors,
                'options' => ['data-ajaxUrl' => Url::to(['color/list']), 'placeholder' => 'Select Paper Color'],
            ])->label(Html::customLabel($model->getAttributeLabel('color_id'), ['color/create']));
            ?> 
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList(Paper::getConstantList('STATUS_', Paper::className())); ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerCssFile(
        '@web/css/custom.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]
);
$this->registerJsFile(
        '@web/js/refresh.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
