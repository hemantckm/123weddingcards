<?php
/* @var $this yii\web\View */
/* @var $model common\models\Paper */

$this->title                   = 'Update Paper: ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Papers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paper-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
        'paperTypes' => $paperTypes,
        'colors' => $colors,
    ])
    ?>
</div>
