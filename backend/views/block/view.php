<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Block;

/* @var $this yii\web\View */
/* @var $model common\models\Block */

$this->title                   = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != Block::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="block-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            [
                'attribute' => 'status',
                'value' => Block::getConstantList('STATUS_', Block::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
