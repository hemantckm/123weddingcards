<?php
/* @var $this yii\web\View */
/* @var $model common\models\Ribbon */

$this->title                   = 'Create Ribbon';
$this->params['breadcrumbs'][] = ['label' => 'Ribbons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ribbon-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
