<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Ribbon;

/* @var $this yii\web\View */
/* @var $model common\models\Ribbon */

$this->title                   = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Ribbons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != Ribbon::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="ribbon-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            [
                'attribute' => 'status',
                'value' => Ribbon::getConstantList('STATUS_', Ribbon::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
