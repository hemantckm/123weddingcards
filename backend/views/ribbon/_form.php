<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Ribbon;

/* @var $this yii\web\View */
/* @var $model common\models\Ribbon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ribbon-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'placeholder' => 'Enter Ribbon Code']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList(Ribbon::getConstantList('STATUS_', Ribbon::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
