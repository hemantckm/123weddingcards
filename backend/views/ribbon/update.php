<?php
/* @var $this yii\web\View */
/* @var $model common\models\Ribbon */

$this->title                   = 'Update Ribbon: ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Ribbons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ribbon-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
