<?php
/* @var $this yii\web\View */
/* @var $model common\models\Card */

$this->title                   = 'Create Card';
$this->params['breadcrumbs'][] = ['label' => 'Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-create">
    <?=
    $this->render('_form', [
        'model' => $model,
        'themes' => $themes,
        'papers' => $papers,
        'dies' => $dies,
        'colors' => $colors,
        'paperTypes' => $paperTypes,
        'blocks' => $blocks,
        'printingMethods' => $printingMethods,
        'envelopeModel' => $envelopeModel,
        'folderModel' => $folderModel,
        'scrollModel' => $scrollModel,
        'boxModel' => $boxModel,
        'tubeModel' => $tubeModel,
        'insertModels' => $insertModels,
        'tassles' => $tassles,
        'ribbons' => $ribbons,
        'rhinestones' => $rhinestones,
        'boxTypes' => $boxTypes,
        'additionsModel' => $additionsModel,
    ])
    ?>
</div>
