<?php

use backend\helpers\Html;
use common\models\Card;

/* @var $this yii\web\View */
/* @var $model common\models\Card */

$this->title                   = 'View Card: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != Card::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>

<div class="card-view">
    <div class="card-view box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Specifications</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- Card Detail Section -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('name'), 'card-name', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', $model->name); ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('sample_price'), 'card-sample_price', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', $model->sample_price); ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('bulk_price'), 'card-bulk_price', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', $model->bulk_price); ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('print_charge'), 'card-print_charge', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', $model->print_charge); ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('length'), 'card-length', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', $model->length); ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('width'), 'card-width', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', $model->width); ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('weight'), 'card-weight', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', $model->weight); ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('card_theme'), 'card-card_theme', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        ?>
                                        <?= Html::tag('p', $cardInfo->theme); ?> 
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <?= Html::label($model->getAttributeLabel('status'), 'card-status', ['class' => 'control-label']) ?>
                                <?= Html::tag('p', Card::getConstantList('STATUS_', Card::className())[$model->status]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ASSEMBLY Section -->
        <div class="row">
            <!-- Envelope Section -->
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Envelope</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($envelopeModel->getAttributeLabel('envelope_paper'), 'envelope-envelope_paper', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $envelopeJson = json_decode($cardInfo->envelope, TRUE);
                                        if ( ! empty($envelopeJson)) {
                                            foreach ($envelopeJson as $envelopeData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($envelopeData['paper']) ? $envelopeData['paper'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($envelopeModel->getAttributeLabel('envelope_die'), 'envelope-envelope_die', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $envelopeJson = json_decode($cardInfo->envelope, TRUE);
                                        if ( ! empty($envelopeJson)) {
                                            foreach ($envelopeJson as $envelopeData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($envelopeData['diecut']) ? $envelopeData['diecut'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($envelopeModel->getAttributeLabel('paper_color'), 'envelope-paper_color', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $envelopeJson = json_decode($cardInfo->envelope, TRUE);
                                        if ( ! empty($envelopeJson)) {
                                            foreach ($envelopeJson as $envelopeData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($envelopeData['paper_color']) ? $envelopeData['paper_color'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($envelopeModel->getAttributeLabel('envelope_block'), 'envelope-envelope_block', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $envelopeJson = json_decode($cardInfo->envelope, TRUE);
                                        if ( ! empty($envelopeJson)) {
                                            foreach ($envelopeJson as $envelopeData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($envelopeData['block']) ? $envelopeData['block'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($envelopeModel->getAttributeLabel('paper_type'), 'envelope-paper_type', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $envelopeJson = json_decode($cardInfo->envelope, TRUE);
                                        if ( ! empty($envelopeJson)) {
                                            foreach ($envelopeJson as $envelopeData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($envelopeData['paper_type']) ? $envelopeData['paper_type'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($envelopeModel->getAttributeLabel('envelope_printing_method'), 'envelope-envelope_printing_method', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $envelopeJson = json_decode($cardInfo->envelope, TRUE);
                                        if ( ! empty($envelopeJson)) {
                                            foreach ($envelopeJson as $envelopeData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($envelopeData['printing_method']) ? $envelopeData['printing_method'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Folder Section -->
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Folder / Invite</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($folderModel->getAttributeLabel('folder_paper'), 'folder-folder_paper', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $folderJson = json_decode($cardInfo->folder, TRUE);
                                        if ( ! empty($folderJson)) {
                                            foreach ($folderJson as $folderData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($folderData['paper']) ? $folderData['paper'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($folderModel->getAttributeLabel('folder_die'), 'folder-folder_die', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $folderJson = json_decode($cardInfo->folder, TRUE);
                                        if ( ! empty($folderJson)) {
                                            foreach ($folderJson as $folderData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($folderData['diecut']) ? $folderData['diecut'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($folderModel->getAttributeLabel('paper_color'), 'folder-paper_color', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $folderJson = json_decode($cardInfo->folder, TRUE);
                                        if ( ! empty($folderJson)) {
                                            foreach ($folderJson as $folderData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($folderData['paper_color']) ? $folderData['paper_color'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($folderModel->getAttributeLabel('folder_block'), 'folder-folder_block', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $folderJson = json_decode($cardInfo->folder, TRUE);
                                        if ( ! empty($folderJson)) {
                                            foreach ($folderJson as $folderData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($folderData['block']) ? $folderData['block'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($folderModel->getAttributeLabel('paper_type'), 'folder-paper_type', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $folderJson = json_decode($cardInfo->folder, TRUE);
                                        if ( ! empty($folderJson)) {
                                            foreach ($folderJson as $folderData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($folderData['paper_type']) ? $folderData['paper_type'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($folderModel->getAttributeLabel('folder_printing_method'), 'folder-folder_printing_method', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $folderJson = json_decode($cardInfo->folder, TRUE);
                                        if ( ! empty($folderJson)) {
                                            foreach ($folderJson as $folderData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($folderData['printing_method']) ? $folderData['printing_method'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scroll/Tube/Box Sections -->
        <div class="row">
            <!-- Scroll/Tube Sections -->
            <div class="col-md-6">
                <!-- Scroll Section -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Scroll</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($scrollModel->getAttributeLabel('scroll_paper'), 'scroll-scroll_paper', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $scrollJson = json_decode($cardInfo->scroll, TRUE);
                                        if ( ! empty($scrollJson)) {
                                            foreach ($scrollJson as $scrollData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($scrollData['paper']) ? $scrollData['paper'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($scrollModel->getAttributeLabel('scroll_die'), 'scroll-scroll_die', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $scrollJson = json_decode($cardInfo->scroll, TRUE);
                                        if ( ! empty($scrollJson)) {
                                            foreach ($scrollJson as $scrollData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($scrollData['diecut']) ? $scrollData['diecut'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($scrollModel->getAttributeLabel('paper_color'), 'scroll-paper_color', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $scrollJson = json_decode($cardInfo->scroll, TRUE);
                                        if ( ! empty($scrollJson)) {
                                            foreach ($scrollJson as $scrollData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($scrollData['paper_color']) ? $scrollData['paper_color'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($scrollModel->getAttributeLabel('scroll_block'), 'scroll-scroll_block', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $scrollJson = json_decode($cardInfo->scroll, TRUE);
                                        if ( ! empty($scrollJson)) {
                                            foreach ($scrollJson as $scrollData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($scrollData['block']) ? $scrollData['block'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($scrollModel->getAttributeLabel('paper_type'), 'scroll-paper_type', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $scrollJson = json_decode($cardInfo->scroll, TRUE);
                                        if ( ! empty($scrollJson)) {
                                            foreach ($scrollJson as $scrollData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($scrollData['paper_type']) ? $scrollData['paper_type'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($scrollModel->getAttributeLabel('scroll_printing_method'), 'scroll-scroll_printing_method', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $scrollJson = json_decode($cardInfo->scroll, TRUE);
                                        if ( ! empty($scrollJson)) {
                                            foreach ($scrollJson as $scrollData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($scrollData['printing_method']) ? $scrollData['printing_method'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Tube Section -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tube</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <?= Html::label($tubeModel->getAttributeLabel('length'), 'tube-length', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $tubeJson = json_decode($cardInfo->scroll, TRUE);
                                        if ( ! empty($tubeJson)) {
                                            foreach ($tubeJson as $tubeData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($tubeData['length']) ? $tubeData['length'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-4">
                                <?= Html::label($tubeModel->getAttributeLabel('weight'), 'tube-weight', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $tubeJson = json_decode($cardInfo->scroll, TRUE);
                                        if ( ! empty($tubeJson)) {
                                            foreach ($tubeJson as $tubeData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($tubeData['weight']) ? $tubeData['weight'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-4">
                                <?= Html::label($tubeModel->getAttributeLabel('diameter'), 'tube-diameter', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $tubeJson = json_decode($cardInfo->scroll, TRUE);
                                        if ( ! empty($tubeJson)) {
                                            foreach ($tubeJson as $tubeData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($tubeData['diameter']) ? $tubeData['diameter'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Box Section -->
            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Box</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($boxModel->getAttributeLabel('box_paper'), 'box-box_paper', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $boxJson = json_decode($cardInfo->box, TRUE);
                                        if ( ! empty($boxJson)) {
                                            foreach ($boxJson as $boxData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($boxData['paper']) ? $boxData['paper'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($boxModel->getAttributeLabel('box_die'), 'box-box_die', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $boxJson = json_decode($cardInfo->box, TRUE);
                                        if ( ! empty($boxJson)) {
                                            foreach ($boxJson as $boxData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($boxData['diecut']) ? $boxData['diecut'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($boxModel->getAttributeLabel('paper_color'), 'box-paper_color', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $boxJson = json_decode($cardInfo->box, TRUE);
                                        if ( ! empty($boxJson)) {
                                            foreach ($boxJson as $boxData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($boxData['paper_color']) ? $boxData['paper_color'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($boxModel->getAttributeLabel('box_block'), 'box-box_block', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $boxJson = json_decode($cardInfo->box, TRUE);
                                        if ( ! empty($boxJson)) {
                                            foreach ($boxJson as $boxData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($boxData['block']) ? $boxData['block'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($boxModel->getAttributeLabel('paper_type'), 'box-paper_type', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $boxJson = json_decode($cardInfo->box, TRUE);
                                        if ( ! empty($boxJson)) {
                                            foreach ($boxJson as $boxData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($boxData['paper_type']) ? $boxData['paper_type'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($boxModel->getAttributeLabel('box_printing_method'), 'box-box_printing_method', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $boxJson = json_decode($cardInfo->box, TRUE);
                                        if ( ! empty($boxJson)) {
                                            foreach ($boxJson as $boxData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($boxData['printing_method']) ? $boxData['printing_method'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($boxModel->getAttributeLabel('box_type_id'), 'box-box_type_id', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $boxJson = json_decode($cardInfo->box, TRUE);
                                        if ( ! empty($boxJson)) {
                                            foreach ($boxJson as $boxData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($boxData['box_type']) ? $boxData['box_type'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($boxModel->getAttributeLabel('length'), 'box-length', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $boxJson = json_decode($cardInfo->box, TRUE);
                                        if ( ! empty($boxJson)) {
                                            foreach ($boxJson as $boxData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($boxData['length']) ? $boxData['length'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($boxModel->getAttributeLabel('width'), 'box-width', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $boxJson = json_decode($cardInfo->box, TRUE);
                                        if ( ! empty($boxJson)) {
                                            foreach ($boxJson as $boxData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($boxData['width']) ? $boxData['width'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($boxModel->getAttributeLabel('thickness'), 'box-thickness', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $boxJson = json_decode($cardInfo->box, TRUE);
                                        if ( ! empty($boxJson)) {
                                            foreach ($boxJson as $boxData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($boxData['thickness']) ? $boxData['thickness'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Inserts Section -->
        <?php
        $insertArrays = array ();
        if ( ! empty($cardsInfo)) {
            foreach ($cardsInfo as $cardInfo) {
                $insertArrays[] = json_decode($cardInfo->insert, TRUE);
            }
        }
        if (count($insertArrays) > 0) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Inserts</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool pull-left" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body cardInsert-container-items">
                            <?php
                            if ( ! empty($cardsInfo)) {
                                foreach ($cardsInfo as $cardInfo) {
                                    $insertJsons = json_decode($cardInfo->insert, TRUE);
                                    if ( ! empty($insertJsons)) {
                                        $i = 1;
                                        foreach ($insertJsons as $index => $insertJson) {
                                            ?>
                                            <div class="cardInsert-item">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title-insert">Insert: <?= $i ++; ?></h4>
                                                    <div class="clearfix"></div>
                                                </div> 

                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <?= Html::label($insertModel->getAttributeLabel('insert_paper'), 'insert-' . ($i - count($insertJsons)) . '-insert_paper', ['class' => 'control-label']) ?>
                                                        <?php
                                                        if ( ! empty($insertJson)) {
                                                            ?>
                                                            <?= Html::tag('p',  ! empty($insertJson['paper']) ? $insertJson['paper'] : ''); ?>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <?= Html::label($insertModel->getAttributeLabel('paper_color'), 'insert-' . ($i - count($insertJsons)) . '-paper_color', ['class' => 'control-label']) ?>
                                                        <?php
                                                        if ( ! empty($insertJson)) {
                                                            ?>
                                                            <?= Html::tag('p',  ! empty($insertJson['paper_color']) ? $insertJson['paper_color'] : ''); ?>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <?= Html::label($insertModel->getAttributeLabel('paper_type'), 'insert-' . ($i - count($insertJsons)) . '-paper_type', ['class' => 'control-label']) ?>
                                                        <?php
                                                        if ( ! empty($insertJson)) {
                                                            ?>
                                                            <?= Html::tag('p',  ! empty($insertJson['paper_type']) ? $insertJson['paper_type'] : ''); ?>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <?= Html::label($insertModel->getAttributeLabel('insert_die'), 'insert-' . ($i - count($insertJsons)) . '-insert_die', ['class' => 'control-label']) ?>
                                                        <?php
                                                        if ( ! empty($insertJson)) {
                                                            ?>
                                                            <?= Html::tag('p',  ! empty($insertJson['diecut']) ? $insertJson['diecut'] : ''); ?>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <?= Html::label($insertModel->getAttributeLabel('insert_block'), 'insert-' . ($i - count($insertJsons)) . '-insert_block', ['class' => 'control-label']) ?>
                                                        <?php
                                                        if ( ! empty($insertJson)) {
                                                            ?>
                                                            <?= Html::tag('p',  ! empty($insertJson['block']) ? $insertJson['block'] : ''); ?>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <?= Html::label($insertModel->getAttributeLabel('insert_printing_method'), 'insert-' . ($i - count($insertJsons)) . '-insert_printing_method', ['class' => 'control-label']) ?>
                                                        <?php
                                                        if ( ! empty($insertJson)) {
                                                            ?>
                                                            <?= Html::tag('p',  ! empty($insertJson['printing_method']) ? $insertJson['printing_method'] : ''); ?>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <!-- Additions Section -->
        <div class="row">
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Additions</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($additionsModel->getAttributeLabel('extra_insert_weight'), 'additions-extra_insert_weight', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $additionsJson = json_decode($cardInfo->additions, TRUE);
                                        if ( ! empty($additionsJson)) {
                                            foreach ($additionsJson as $additionsData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($additionsData['extra_insert_weight']) ? $additionsData['extra_insert_weight'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($additionsModel->getAttributeLabel('extra_insert_price'), 'additions-extra_insert_price', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $additionsJson = json_decode($cardInfo->additions, TRUE);
                                        if ( ! empty($additionsJson)) {
                                            foreach ($additionsJson as $additionsData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($additionsData['extra_insert_price']) ? $additionsData['extra_insert_price'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($additionsModel->getAttributeLabel('tassle_id'), 'additions-tassle_id', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $additionsJson = json_decode($cardInfo->additions, TRUE);
                                        if ( ! empty($additionsJson)) {
                                            foreach ($additionsJson as $additionsData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($additionsData['tassle']) ? $additionsData['tassle'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($additionsModel->getAttributeLabel('ribbon_id'), 'additions-ribbon_id', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $additionsJson = json_decode($cardInfo->additions, TRUE);
                                        if ( ! empty($additionsJson)) {
                                            foreach ($additionsJson as $additionsData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($additionsData['ribbon']) ? $additionsData['ribbon'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Additions</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($additionsModel->getAttributeLabel('rhinestone_id'), 'additions-rhinestone_id', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $additionsJson = json_decode($cardInfo->additions, TRUE);
                                        if ( ! empty($additionsJson)) {
                                            foreach ($additionsJson as $additionsData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($additionsData['rhinestone']) ? $additionsData['rhinestone'] : ''); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($additionsModel->getAttributeLabel('photocard'), 'additions-photocard', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $additionsJson = json_decode($cardInfo->additions, TRUE);
                                        if ( ! empty($additionsJson)) {
                                            foreach ($additionsJson as $additionsData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($additionsData['photocard']) ? $additionsModel::PHOTOCARD[$additionsData['photocard']] : $additionsModel::PHOTOCARD[0]); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <?= Html::label($additionsModel->getAttributeLabel('linear'), 'additions-linear', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $additionsJson = json_decode($cardInfo->additions, TRUE);
                                        if ( ! empty($additionsJson)) {
                                            foreach ($additionsJson as $additionsData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($additionsData['linear']) ? $additionsModel::LINEAR[$additionsData['linear']] : $additionsModel::LINEAR[0]); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= Html::label($additionsModel->getAttributeLabel('velvet'), 'additions-velvet', ['class' => 'control-label']) ?>
                                <?php
                                if ( ! empty($cardsInfo)) {
                                    foreach ($cardsInfo as $cardInfo) {
                                        $additionsJson = json_decode($cardInfo->additions, TRUE);
                                        if ( ! empty($additionsJson)) {
                                            foreach ($additionsJson as $additionsData) {
                                                ?>
                                                <?= Html::tag('p',  ! empty($additionsData['velvet']) ? $additionsModel::VELVET[$additionsData['velvet']] : $additionsModel::VELVET[0]); ?>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

