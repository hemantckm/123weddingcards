<?php
/* @var $this yii\web\View */
/* @var $model common\models\Card */

$this->title                   = 'Update Card: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="card-update">
    <?=
    $this->render('_form', [
        'model' => $model,
        'papers' => $papers,
        'themes' => $themes,
        'dies' => $dies,
        'colors' => $colors,
        'paperTypes' => $paperTypes,
        'blocks' => $blocks,
        'printingMethods' => $printingMethods,
        'envelopeModel' => $envelopeModel,
        'folderModel' => $folderModel,
        'scrollModel' => $scrollModel,
        'boxModel' => $boxModel,
        'tubeModel' => $tubeModel,
        'insertModels' => $insertModels,
        'tassles' => $tassles,
        'ribbons' => $ribbons,
        'rhinestones' => $rhinestones,
        'boxTypes' => $boxTypes,
        'additionsModel' => $additionsModel,
        'cardThemeModelId' => $cardThemeModelId,
        'envelopePaperModelId' => $envelopePaperModelId,
        'envelopeDieModelId' => $envelopeDieModelId,
        'envelopeBlockModelId' => $envelopeBlockModelId,
        'envelopePrintingMethodModelId' => $envelopePrintingMethodModelId,
        'folderPaperModelId' => $folderPaperModelId,
        'folderDieModelId' => $folderDieModelId,
        'folderBlockModelId' => $folderBlockModelId,
        'folderPrintingMethodModelId' => $folderPrintingMethodModelId,
        'scrollPaperModelId' => $scrollPaperModelId,
        'scrollDieModelId' => $scrollDieModelId,
        'scrollBlockModelId' => $scrollBlockModelId,
        'scrollPrintingMethodModelId' => $scrollPrintingMethodModelId,
        'boxPaperModelId' => $boxPaperModelId,
        'boxDieModelId' => $boxDieModelId,
        'boxBlockModelId' => $boxBlockModelId,
        'boxPrintingMethodModelId' => $boxPrintingMethodModelId,
        'insertPaperModelId' => $insertPaperModelId,
        'insertDieModelId' => $insertDieModelId,
        'insertBlockModelId' => $insertBlockModelId,
        'insertPrintingMethodModelId' => $insertPrintingMethodModelId,
    ])
    ?>
</div>
