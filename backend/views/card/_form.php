<?php

use backend\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\Card;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Card */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="loader" style="display:none"></div>
<div class="card-form box-body">
    <?php $form                    = ActiveForm::begin(['id' => 'card-dynamic-form', 'options' => ['autocomplete' => 'off']]); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Specifications</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- Card Detail Section -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Enter Card Name']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'sample_price')->textInput(['maxlength' => true, 'placeholder' => 'Enter Sample Price']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'bulk_price')->textInput(['maxlength' => true, 'placeholder' => 'Enter Bulk Price']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'print_charge')->textInput(['maxlength' => true, 'placeholder' => 'Enter Printing Charges']) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'length')->textInput(['maxlength' => true, 'placeholder' => 'Enter Length']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'width')->textInput(['maxlength' => true, 'placeholder' => 'Enter Width']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'weight')->textInput(['maxlength' => true, 'placeholder' => 'Enter Weight']) ?>
                        </div>
                        <div class="col-md-3">
                            <?php
                            $card_theme_groups_array = array ();
                            if ( ! empty($cardThemeModelId)) {
                                foreach ($cardThemeModelId as $cardThemeId) {
                                    $card_theme_groups_array[$cardThemeId] = array ("selected" => true);
                                }
                            }
                            ?>
                            <?=
                            $form->field($model, 'card_theme')->widget(Select2::classname(), [
                                'data' => $themes,
                                'options' => ['placeholder' => 'Select Card Theme', 'data-ajaxUrl' => Url::to(['theme/list']), 'multiple' => true, 'options' => $card_theme_groups_array],
                            ])->label(Html::customLabel($model->getAttributeLabel('card_theme'), ['theme/create']));
                            ?> 
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'status')->dropDownList(Card::getConstantList('STATUS_', Card::className()), ['prompt' => 'Select Availability']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ASSEMBLY Section -->
    <div class="row">
        <!-- Envelope Section -->
        <?=
        $this->render('/partials/-card-envelope', [
            'form' => $form,
            'papers' => $papers,
            'dies' => $dies,
            'colors' => $colors,
            'paperTypes' => $paperTypes,
            'blocks' => $blocks,
            'printingMethods' => $printingMethods,
            'envelopeModel' => $envelopeModel,
            'envelopePaperModelId' => ! empty($envelopePaperModelId) ? $envelopePaperModelId : array (),
            'envelopeDieModelId' => ! empty($envelopeDieModelId) ? $envelopeDieModelId : array (),
            'envelopeBlockModelId' => ! empty($envelopeBlockModelId) ? $envelopeBlockModelId : array (),
            'envelopePrintingMethodModelId' => ! empty($envelopePrintingMethodModelId) ? $envelopePrintingMethodModelId : array (),
        ]);
        ?>

        <!-- Folder Section -->
        <?=
        $this->render('/partials/-card-folder', [
            'form' => $form,
            'papers' => $papers,
            'dies' => $dies,
            'colors' => $colors,
            'paperTypes' => $paperTypes,
            'blocks' => $blocks,
            'printingMethods' => $printingMethods,
            'folderModel' => $folderModel,
            'folderPaperModelId' => ! empty($folderPaperModelId) ? $folderPaperModelId : array (),
            'folderDieModelId' => ! empty($folderDieModelId) ? $folderDieModelId : array (),
            'folderBlockModelId' => ! empty($folderBlockModelId) ? $folderBlockModelId : array (),
            'folderPrintingMethodModelId' => ! empty($folderPrintingMethodModelId) ? $folderPrintingMethodModelId : array (),
        ]);
        ?>
    </div>

    <!-- Scroll, Box & Tube Sections -->
    <div class="scrollSections">
        <div class="row">
            <!-- Scroll Section -->
            <?=
            $this->render('/partials/-card-scroll', [
                'form' => $form,
                'papers' => $papers,
                'dies' => $dies,
                'colors' => $colors,
                'paperTypes' => $paperTypes,
                'blocks' => $blocks,
                'printingMethods' => $printingMethods,
                'tubeModel' => $tubeModel, //Tube Model
                'scrollModel' => $scrollModel,
                'scrollPaperModelId' => ! empty($scrollPaperModelId) ? $scrollPaperModelId : array (),
                'scrollDieModelId' => ! empty($scrollDieModelId) ? $scrollDieModelId : array (),
                'scrollBlockModelId' => ! empty($scrollBlockModelId) ? $scrollBlockModelId : array (),
                'scrollPrintingMethodModelId' => ! empty($scrollPrintingMethodModelId) ? $scrollPrintingMethodModelId : array (),
            ]);
            ?>

            <!-- Box Section -->
            <?=
            $this->render('/partials/-card-box', [
                'form' => $form,
                'papers' => $papers,
                'dies' => $dies,
                'colors' => $colors,
                'paperTypes' => $paperTypes,
                'blocks' => $blocks,
                'printingMethods' => $printingMethods,
                'boxTypes' => $boxTypes,
                'boxModel' => $boxModel,
                'boxPaperModelId' => ! empty($boxPaperModelId) ? $boxPaperModelId : array (),
                'boxDieModelId' => ! empty($boxDieModelId) ? $boxDieModelId : array (),
                'boxBlockModelId' => ! empty($boxBlockModelId) ? $boxBlockModelId : array (),
                'boxPrintingMethodModelId' => ! empty($boxPrintingMethodModelId) ? $boxPrintingMethodModelId : array (),
            ]);
            ?>
        </div>
    </div>

    <!-- Inserts Section -->
    <div class="row">
        <?=
        $this->render('/partials/-card-insert', [
            'form' => $form,
            'papers' => $papers,
            'dies' => $dies,
            'colors' => $colors,
            'paperTypes' => $paperTypes,
            'blocks' => $blocks,
            'printingMethods' => $printingMethods,
            'insertModels' => $insertModels,
            'insertPaperModelId' => ! empty($insertPaperModelId) ? $insertPaperModelId : array (),
            'insertDieModelId' => ! empty($insertDieModelId) ? $insertDieModelId : array (),
            'insertBlockModelId' => ! empty($insertBlockModelId) ? $insertBlockModelId : array (),
            'insertPrintingMethodModelId' => ! empty($insertPrintingMethodModelId) ? $insertPrintingMethodModelId : array (),
        ]);
        ?> 
    </div>

    <!-- Additions Section -->
    <div class="row">
        <?=
        $this->render('/partials/-card-additions', [
            'form' => $form,
            'tassles' => $tassles,
            'ribbons' => $ribbons,
            'rhinestones' => $rhinestones,
            'additionsModel' => $additionsModel,
        ]);
        ?> 
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerCssFile(
        '@web/css/custom.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]
);
$this->registerJsFile(
        '@web/js/refresh.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
        '@web/js/card.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
