<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Rhinestone;

/* @var $this yii\web\View */
/* @var $model common\models\Rhinestone */

$this->title                   = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Rhinestones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != Rhinestone::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="rhinestone-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            [
                'attribute' => 'status',
                'value' => Rhinestone::getConstantList('STATUS_', Rhinestone::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
