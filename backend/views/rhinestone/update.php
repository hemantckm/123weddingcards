<?php
/* @var $this yii\web\View */
/* @var $model common\models\Rhinestone */

$this->title                   = 'Update Rhinestone: ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Rhinestones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rhinestone-update box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
