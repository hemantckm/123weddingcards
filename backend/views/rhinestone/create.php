<?php
/* @var $this yii\web\View */
/* @var $model common\models\Rhinestone */

$this->title                   = 'Create Rhinestone';
$this->params['breadcrumbs'][] = ['label' => 'Rhinestones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rhinestone-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
