<?php
/* @var $this yii\web\View */
/* @var $model common\models\Banner */

$this->title                   = 'Create Banner';
$this->params['breadcrumbs'][] = ['label' => 'Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
