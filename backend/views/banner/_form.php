<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Banner;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'heading')->textInput(['maxlength' => true, 'placeholder' => 'Enter Banner Heading']) ?>
        </div>
        <div class="col-md-9">
            <?=
            $form->field($model, "description")->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'standard',
                'clientOptions' => [
                    'height' => 100,
                    'replaceDivs' => false,
                    'allowedContent' => true,
                ]
            ])
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">  
            <?= $form->field($model, "image")->fileInput(['maxlength' => true, 'placeholder' => 'Select Banner Image', 'accept' => 'image/*', 'class' => 'form-control']) ?> 
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'default')->dropDownList(Banner::getConstantList('DEFAULT_', Banner::className())) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList(Banner::getConstantList('STATUS_', Banner::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
