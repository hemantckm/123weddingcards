<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Banner;

/* @var $this yii\web\View */
/* @var $model common\models\Banner */

$this->title                   = $model->heading;
$this->params['breadcrumbs'][] = ['label' => 'Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != Banner::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="banner-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'heading',
            'description:ntext',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => $image,
                'label' => $model->getAttributeLabel('image'),
            ],
            [
                'attribute' => 'default',
                'value' => Banner::getConstantList('DEFAULT_', Banner::className())[$model->default],
                'label' => $model->getAttributeLabel('default'),
            ],
            [
                'attribute' => 'status',
                'value' => Banner::getConstantList('STATUS_', Banner::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
<?php
$this->registerJsFile(
        '@web/js/jquery.fancybox.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerCssFile(
        '@web/css/jquery.fancybox.min.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]
);
?>
