<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ContactUs;

/* @var $this yii\web\View */
/* @var $model common\models\ContactUs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-us-form box-body">
    <?php $form = ActiveForm::begin(['id' => 'contact-us-form', 'options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Specifications</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- Contact-US Section -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'heading')->textInput(['maxlength' => true, 'placeholder' => 'Enter Contact-Us Heading']) ?>
                        </div>
                        <div class="col-md-3">  
                            <?= $form->field($model, "feature_image")->fileInput(['maxlength' => true, 'placeholder' => 'Select Contact-Us Feature', 'accept' => 'image/*', 'class' => 'form-control']) ?> 
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'default')->dropDownList(ContactUs::getConstantList('DEFAULT_', ContactUs::className())) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'status')->dropDownList(ContactUs::getConstantList('STATUS_', ContactUs::className())) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Contact-Us Details Section-->
    <div class="row">
        <?=
        $this->render('/partials/-contact-us-details', [
            'form' => $form,
            'contactUsDetailModels' => $contactUsDetailModels,
        ]);
        ?> 
    </div>

    <!-- Contact-Us TC Section-->
    <div class="row">
        <?=
        $this->render('/partials/-contact-us-tc', [
            'form' => $form,
            'contactUsTcModel' => $contactUsTcModel,
        ]);
        ?> 
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerJsFile(
        '@web/js/contact-us.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
