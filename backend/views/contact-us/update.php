<?php
/* @var $this yii\web\View */
/* @var $model common\models\ContactUs */

$this->title                   = 'Update Contact Us: ' . $model->heading;
$this->params['breadcrumbs'][] = ['label' => 'Contact uses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->heading, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contact-us-update">
    <?=
    $this->render('_form', [
        'model' => $model,
        'contactUsDetailModels' => $contactUsDetailModels,
        'contactUsTcModel' => $contactUsTcModel,
    ])
    ?>
</div>
