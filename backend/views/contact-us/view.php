<?php

use yii\helpers\Html;
use common\models\ContactUs;

/* @var $this yii\web\View */
/* @var $model common\models\ContactUs */

$this->title                   = $model->heading;
$this->params['breadcrumbs'][] = ['label' => 'Contact-Us', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != ContactUs::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>

<?php if ( ! empty($contactUsDatas)) { ?>
    <div class="contact-us-view">
        <?php
        foreach ($contactUsDatas as $contactUsData) {
            ?>
            <div class="contact-us-form box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Specifications</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- Contact-US Section -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= Html::label($model->getAttributeLabel('heading'), 'contactus-heading', ['class' => 'control-label']) ?>
                                        <?= Html::tag('p', $contactUsData['contact_us_heading']); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?= Html::label($model->getAttributeLabel('feature_image'), 'contactus-feature_image', ['class' => 'control-label']) ?>
                                        <?= Html::tag('p', $contactUsData['contact_us_feature_image']['gallery']); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?= Html::label($model->getAttributeLabel('default'), 'contactus-default', ['class' => 'control-label']) ?>
                                        <?= Html::tag('p', ContactUs::getConstantList('DEFAULT_', ContactUs::className())[$contactUsData['contact_us_default']]); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <?= Html::label($model->getAttributeLabel('status'), 'contactus-status', ['class' => 'control-label']) ?>
                                        <?= Html::tag('p', ContactUs::getConstantList('STATUS_', ContactUs::className())[$contactUsData['contact_us_status']]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Contact-Us Details Section-->
                <?php if ( ! empty($contactUsData['contact_us_detail'])) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Contact-US Details</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool pull-left" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body contactUsDetails-container-items">

                                    <?php
                                    $i = 1;
                                    foreach ($contactUsData['contact_us_detail'] as $index => $contactUsDetails) {
                                        ?>
                                        <div class="contactUsDetails-item">
                                            <div class="panel-heading">
                                                <span class="panel-title-insert">Detail:<?= $i ++; ?> </span>
                                                <div class="clearfix"></div>
                                            </div> 

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <?= Html::label($contactUsDetailModel->getAttributeLabel('name'), "contactusdetails-$index-name", ['class' => 'control-label']) ?>
                                                    <?= Html::tag('p', $contactUsDetails['name']); ?>
                                                </div>
                                                <div class="col-md-3">
                                                    <?= Html::label($contactUsDetailModel->getAttributeLabel('icon'), "contactusdetails-$index-icon", ['class' => 'control-label']) ?>
                                                    <?= Html::tag('p', $feature_image = Html::a(Html::img(Yii::getAlias('@web') . $contactUsDetails['icon']['small'], ['class' => 'img-thumbnail', 'style' => 'margin-bottom:10px']), Yii::getAlias('@web') . $contactUsDetails['icon']['full'], ['data-fancybox' => 'gallery'])); ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?= Html::label($contactUsDetailModel->getAttributeLabel('description'), "contactusdetails-$index-description", ['class' => 'control-label']) ?>
                                                    <?= Html::tag('p', $contactUsDetails['description']); ?>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <!-- Contact-Us TC Section-->
                <?php if ( ! empty($contactUsData['contact_us_tc'])) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Terms & Policies</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <?= Html::label($contactUsTcModel->getAttributeLabel('heading'), "contactustc-heading", ['class' => 'control-label']) ?>
                                            <?= Html::tag('p', $contactUsData['contact_us_tc']['heading']); ?>
                                        </div>
                                        <div class="col-md-8">
                                            <?= Html::label($contactUsTcModel->getAttributeLabel('description'), "contactustc-description", ['class' => 'control-label']) ?>
                                            <?= Html::tag('p', $contactUsData['contact_us_tc']['description']); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
<?php } ?>
<?php
$this->registerJsFile(
        '@web/js/jquery.fancybox.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerCssFile(
        '@web/css/jquery.fancybox.min.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]
);
?>
