<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\About;

/* @var $this yii\web\View */
/* @var $model common\models\About */

$this->title                   = $model->heading1;
$this->params['breadcrumbs'][] = ['label' => 'Abouts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != About::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="about-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'heading1',
            'description1:ntext',
            'heading2',
            'description2:ntext',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => $image,
                'label' => $model->getAttributeLabel('image'),
            ],
            [
                'attribute' => 'default',
                'value' => About::getConstantList('DEFAULT_', About::className())[$model->default],
                'label' => $model->getAttributeLabel('default'),
            ],
            [
                'attribute' => 'status',
                'value' => About::getConstantList('STATUS_', About::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
<?php
$this->registerJsFile(
        '@web/js/jquery.fancybox.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerCssFile(
        '@web/css/jquery.fancybox.min.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]
);
?>

