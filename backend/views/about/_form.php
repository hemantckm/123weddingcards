<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\About;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\About */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="about-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'heading1')->textInput(['maxlength' => true, 'placeholder' => 'Enter First Heading']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'heading2')->textInput(['maxlength' => true, 'placeholder' => 'Enter Second Heading']) ?>
        </div>
        <div class="col-md-3">  
            <?= $form->field($model, "image")->fileInput(['maxlength' => true, 'placeholder' => 'Select About Image', 'accept' => 'image/*', 'class' => 'form-control']) ?> 
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'default')->dropDownList(About::getConstantList('DEFAULT_', About::className())) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <?=
            $form->field($model, "description1")->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'standard',
                'clientOptions' => [
                    'height' => 100,
                    'replaceDivs' => false,
                    'allowedContent' => true,
                ]
            ])
            ?>
        </div>
        <div class="col-md-5">
            <?=
            $form->field($model, "description2")->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'standard',
                'clientOptions' => [
                    'height' => 100,
                    'replaceDivs' => false,
                    'allowedContent' => true,
                ]
            ])
            ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'status')->dropDownList(About::getConstantList('STATUS_', About::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
