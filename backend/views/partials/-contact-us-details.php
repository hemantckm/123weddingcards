<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use backend\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\ckeditor\CKEditor;
?>

<!-- Contact-US Details Section -->
<?php
DynamicFormWidget::begin([
    'widgetContainer' => 'contactUsDetailsForm_dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
    'widgetBody' => '.contactUsDetails-container-items', // required: css class selector
    'widgetItem' => '.contactUsDetails-item', // required: css class
    'limit' => 4, // the maximum times, an element can be cloned (default 999)
    'min' => 1, // 0 or 1 (default 1)
    'insertButton' => '.add-contactUsDetails-item', // css class
    'deleteButton' => '.remove-contactUsDetails-item', // css class
    'model' => $contactUsDetailModels[0],
    'formId' => $form->options['id'],
    'formFields' => [
        'name',
        'icon',
        'description',
    ],
]);
?>
<div class="col-md-12">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Contact-US Details</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool pull-left" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="pull-right add-contactUsDetails-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Details</button>
            </div>
        </div>
        <div class="box-body contactUsDetails-container-items">
            <?php foreach ($contactUsDetailModels as $index => $contactUsDetailModel): ?>
                <div class="contactUsDetails-item">
                    <div class="panel-heading">
                        <span class="panel-title-insert">Detail: <?= ($index + 1) ?></span>
                        <button type="button" class="pull-right remove-contactUsDetails-item btn btn-danger btn-xs"><i class="fa fa-minus"></i> Delete</button>
                        <div class="clearfix"></div>
                    </div> 
                    <?php
                    // necessary for update action.
                    if ( ! $contactUsDetailModel->isNewRecord) {
                        echo Html::activeHiddenInput($contactUsDetailModel, "[{$index}]id");
                    }
                    ?> 
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($contactUsDetailModel, "[{$index}]name")->textInput(['maxlength' => true, 'placeholder' => 'Enter Contact-Us Detail Name']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($contactUsDetailModel, "[{$index}]icon")->fileInput(['maxlength' => true, 'placeholder' => 'Select Contact-Us Detail Icon', 'accept' => 'image/*', 'class' => 'form-control']) ?>  
                        </div>
                        <div class="col-md-6">
                            <?=
                            $form->field($contactUsDetailModel, "[{$index}]description")->widget(CKEditor::className(), [
                                'options' => ['rows' => 2],
                                'preset' => 'standard',
                                'clientOptions' => [
                                    'height' => 100,
                                    'replaceDivs' => false,
                                    'allowedContent' => true,
                                ]
                            ])
                            ?>
                            <?php /* = $form->field($contactUsDetailModel, "[{$index}]description")->textarea(['maxlength' => true, 'placeholder' => 'Enter Contact-Us Detail Description', 'rows' => '5']) */ ?> 
                        </div>
                    </div>
                    <hr>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php DynamicFormWidget::end(); ?>
