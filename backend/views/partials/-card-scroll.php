<?php

use kartik\select2\Select2;
use yii\helpers\Url;
use backend\helpers\Html;
?>
<div class="col-md-6">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Scroll</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?php
                    $scroll_paper_groups_array = array ();
                    if ( ! empty($scrollPaperModelId)) {
                        foreach ($scrollPaperModelId as $scrollPaperId) {
                            $scroll_paper_groups_array[$scrollPaperId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($scrollModel, 'scroll_paper')->widget(Select2::classname(), [
                        'data' => $papers,
                        'options' => ['data-ajaxUrl' => Url::to(['paper/list']), 'placeholder' => 'Select Paper Code', 'multiple' => true, 'options' => $scroll_paper_groups_array],
                    ])->label(Html::customLabel($scrollModel->getAttributeLabel('scroll_paper'), ['paper/create']));
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $scroll_die_groups_array = array ();
                    if ( ! empty($scrollDieModelId)) {
                        foreach ($scrollDieModelId as $scrollDieId) {
                            $scroll_die_groups_array[$scrollDieId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($scrollModel, 'scroll_die')->widget(Select2::classname(), [
                        'data' => $dies,
                        'options' => ['data-ajaxUrl' => Url::to(['die-cut/list']), 'placeholder' => 'Select Die Code', 'multiple' => true, 'options' => $scroll_die_groups_array],
                    ])->label(Html::customLabel($scrollModel->getAttributeLabel('scroll_die'), ['die-cut/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($scrollModel, 'paper_color')->widget(Select2::classname(), [
                        'data' => $colors,
                        'options' => ['placeholder' => 'Select Paper Color', 'multiple' => true, 'disabled' => true],
                    ]);
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $scroll_block_groups_array = array ();
                    if ( ! empty($scrollBlockModelId)) {
                        foreach ($scrollBlockModelId as $scrollBlockId) {
                            $scroll_block_groups_array[$scrollBlockId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($scrollModel, 'scroll_block')->widget(Select2::classname(), [
                        'data' => $blocks,
                        'options' => ['data-ajaxUrl' => Url::to(['block/list']), 'placeholder' => 'Select Block Code', 'multiple' => true, 'options' => $scroll_block_groups_array],
                    ])->label(Html::customLabel($scrollModel->getAttributeLabel('scroll_block'), ['block/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($scrollModel, 'paper_type')->widget(Select2::classname(), [
                        'data' => $paperTypes,
                        'options' => ['placeholder' => 'Select Paper Type', 'multiple' => true, 'disabled' => true],
                    ]);
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $scroll_printing_method_groups_array = array ();
                    if ( ! empty($scrollPrintingMethodModelId)) {
                        foreach ($scrollPrintingMethodModelId as $scrollPrintingMethodId) {
                            $scroll_printing_method_groups_array[$scrollPrintingMethodId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($scrollModel, 'scroll_printing_method')->widget(Select2::classname(), [
                        'data' => $printingMethods,
                        'options' => ['data-ajaxUrl' => Url::to(['printing-method/list']), 'placeholder' => 'Select Printing Method', 'multiple' => true, 'options' => $scroll_printing_method_groups_array],
                    ])->label(Html::customLabel($scrollModel->getAttributeLabel('scroll_printing_method'), ['printing-method/create']));
                    ?> 
                </div>
            </div>
        </div>
    </div>

    <!-- Tube Section -->
    <?=
    $this->render('/partials/-card-tube', [
        'form' => $form,
        'tubeModel' => $tubeModel
    ]);
    ?>
</div>
