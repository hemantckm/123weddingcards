<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use kartik\select2\Select2;
use yii\helpers\Url;
use backend\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;
?>

<!-- Insert Section -->
<?php
DynamicFormWidget::begin([
    'widgetContainer' => 'cardInsertForm_dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
    'widgetBody' => '.cardInsert-container-items', // required: css class selector
    'widgetItem' => '.cardInsert-item', // required: css class
    'limit' => 4, // the maximum times, an element can be cloned (default 999)
    'min' => 1, // 0 or 1 (default 1)
    'insertButton' => '.add-cardInsert-item', // css class
    'deleteButton' => '.remove-cardInsert-item', // css class
    'model' => $insertModels[0],
    'formId' => $form->options['id'],
    'formFields' => [
        'paper_id',
        'paper_color',
        'paper_type',
        'printing_method',
        'die_id',
        'block_id',
    ],
]);
?>
<div class="col-md-12">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Inserts</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool pull-left" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="pull-right add-cardInsert-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Insert</button>
            </div>
        </div>
        <div class="box-body cardInsert-container-items">
            <?php foreach ($insertModels as $index => $insertModel): ?>
                <div class="cardInsert-item">
                    <div class="panel-heading">
                        <span class="panel-title-insert">Insert: <?= ($index + 1) ?></span>
                        <button type="button" class="pull-right remove-cardInsert-item btn btn-danger btn-xs"><i class="fa fa-minus"></i> Delete</button>
                        <div class="clearfix"></div>
                    </div> 
                    <?php
                    // necessary for update action.
                    if ( ! $insertModel->isNewRecord) {
                        echo Html::activeHiddenInput($insertModel, "[{$index}]id");
                    }
                    ?> 
                    <div class="row">
                        <div class="col-md-3">
                            <?php
                            $insert_paper_groups_array = array ();
                            if ( ! empty($insertPaperModelId[$index])) {
                                foreach ($insertPaperModelId[$index] as $insertPaperId) {
                                    $insert_paper_groups_array[$insertPaperId] = array ("selected" => true);
                                }
                            }
                            ?>
                            <?=
                            $form->field($insertModel, "[{$index}]insert_paper")->widget(Select2::classname(), [
                                'data' => $papers,
                                'options' => ['data-ajaxUrl' => Url::to(['paper/list']), 'placeholder' => 'Select Paper Code', 'class' => 'insert_paper_code', 'multiple' => true, 'options' => $insert_paper_groups_array],
                            ])->label(Html::customLabel($insertModel->getAttributeLabel('insert_paper'), ['paper/create']));
                            ?>
                        </div>
                        <div class="col-md-3">
                            <?=
                            $form->field($insertModel, "[{$index}]paper_color")->widget(Select2::classname(), [
                                'data' => $colors,
                                'options' => ['placeholder' => 'Select Paper Color', 'multiple' => true, 'disabled' => true],
                            ]);
                            ?> 
                        </div>
                        <div class="col-md-3">
                            <?=
                            $form->field($insertModel, "[{$index}]paper_type")->widget(Select2::classname(), [
                                'data' => $paperTypes,
                                'options' => ['placeholder' => 'Select Paper Type', 'multiple' => true, 'disabled' => true],
                            ]);
                            ?> 
                        </div>
                        <div class="col-md-3">
                            <?php
                            $insert_die_groups_array = array ();
                            if ( ! empty($insertDieModelId[$index])) {
                                foreach ($insertDieModelId[$index] as $insertDieId) {
                                    $insert_die_groups_array[$insertDieId] = array ("selected" => true);
                                }
                            }
                            ?>
                            <?=
                            $form->field($insertModel, "[{$index}]insert_die")->widget(Select2::classname(), [
                                'data' => $dies,
                                'options' => ['data-ajaxUrl' => Url::to(['die-cut/list']), 'placeholder' => 'Select Die Code', 'multiple' => true, 'options' => $insert_die_groups_array],
                            ])->label(Html::customLabel($insertModel->getAttributeLabel('insert_die'), ['die-cut/create']));
                            ?> 
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <?php
                            $insert_block_groups_array = array ();
                            if ( ! empty($insertBlockModelId[$index])) {
                                foreach ($insertBlockModelId[$index] as $insertBlockId) {
                                    $insert_block_groups_array[$insertBlockId] = array ("selected" => true);
                                }
                            }
                            ?>
                            <?=
                            $form->field($insertModel, "[{$index}]insert_block")->widget(Select2::classname(), [
                                'data' => $blocks,
                                'options' => ['data-ajaxUrl' => Url::to(['block/list']), 'placeholder' => 'Select Block Code', 'multiple' => true, 'options' => $insert_block_groups_array],
                            ])->label(Html::customLabel($insertModel->getAttributeLabel('insert_block'), ['block/create']));
                            ?> 
                        </div>
                        <div class="col-md-3">
                            <?php
                            $insert_printing_method_groups_array = array ();
                            if ( ! empty($insertPrintingMethodModelId[$index])) {
                                foreach ($insertPrintingMethodModelId[$index] as $insertPrintingMethodId) {
                                    $insert_printing_method_groups_array[$insertPrintingMethodId] = array ("selected" => true);
                                }
                            }
                            ?>
                            <?=
                            $form->field($insertModel, "[{$index}]insert_printing_method")->widget(Select2::classname(), [
                                'data' => $printingMethods,
                                'options' => ['data-ajaxUrl' => Url::to(['printing-method/list']), 'placeholder' => 'Select Printing Method', 'multiple' => true, 'options' => $insert_printing_method_groups_array],
                            ])->label(Html::customLabel($insertModel->getAttributeLabel('insert_printing_method'), ['printing-method/create']));
                            ?> 
                        </div>
                    </div>
                    <hr>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php DynamicFormWidget::end(); ?>

