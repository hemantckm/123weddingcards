<?php

use kartik\select2\Select2;
use yii\helpers\Url;
use backend\helpers\Html;
?>
<div class="col-md-6">
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Envelope</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?php
                    $addonEnvelope_paper_groups_array = array ();
                    if ( ! empty($addonEnvelopePaperModelId)) {
                        foreach ($addonEnvelopePaperModelId as $addonEnvelopePaperId) {
                            $addonEnvelope_paper_groups_array[$addonEnvelopePaperId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($addonEnvelopeModel, 'addon_envelope_paper')->widget(Select2::classname(), [
                        'data' => $papers,
                        'options' => ['data-ajaxUrl' => Url::to(['paper/list']), 'placeholder' => 'Select Paper Code', 'multiple' => true, 'options' => $addonEnvelope_paper_groups_array],
                    ])->label(Html::customLabel($addonEnvelopeModel->getAttributeLabel('addon_envelope_paper'), ['paper/create']));
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $addonEnvelope_die_groups_array = array ();
                    if ( ! empty($addonEnvelopeDieModelId)) {
                        foreach ($addonEnvelopeDieModelId as $addonEnvelopeDieId) {
                            $addonEnvelope_die_groups_array[$addonEnvelopeDieId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($addonEnvelopeModel, 'addon_envelope_die')->widget(Select2::classname(), [
                        'data' => $dies,
                        'options' => ['data-ajaxUrl' => Url::to(['die-cut/list']), 'placeholder' => 'Select Die Code', 'multiple' => true, 'options' => $addonEnvelope_die_groups_array],
                    ])->label(Html::customLabel($addonEnvelopeModel->getAttributeLabel('addon_envelope_die'), ['die-cut/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($addonEnvelopeModel, 'paper_color')->widget(Select2::classname(), [
                        'data' => $colors,
                        'options' => ['placeholder' => 'Select Paper Color', 'multiple' => true, 'disabled' => true],
                    ]);
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $addonEnvelope_block_groups_array = array ();
                    if ( ! empty($addonEnvelopeBlockModelId)) {
                        foreach ($addonEnvelopeBlockModelId as $addonEnvelopeBlockId) {
                            $addonEnvelope_block_groups_array[$addonEnvelopeBlockId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($addonEnvelopeModel, 'addon_envelope_block')->widget(Select2::classname(), [
                        'data' => $blocks,
                        'options' => ['data-ajaxUrl' => Url::to(['block/list']), 'placeholder' => 'Select Block Code', 'multiple' => true, 'options' => $addonEnvelope_block_groups_array],
                    ])->label(Html::customLabel($addonEnvelopeModel->getAttributeLabel('addon_envelope_block'), ['block/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($addonEnvelopeModel, 'paper_type')->widget(Select2::classname(), [
                        'data' => $paperTypes,
                        'options' => ['placeholder' => 'Select Paper Type', 'multiple' => true, 'disabled' => true],
                    ]);
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $addonEnvelope_printing_method_groups_array = array ();
                    if ( ! empty($addonEnvelopePrintingMethodModelId)) {
                        foreach ($addonEnvelopePrintingMethodModelId as $addonEnvelopePrintingMethodId) {
                            $addonEnvelope_printing_method_groups_array[$addonEnvelopePrintingMethodId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($addonEnvelopeModel, 'addon_envelope_printing_method')->widget(Select2::classname(), [
                        'data' => $printingMethods,
                        'options' => ['data-ajaxUrl' => Url::to(['printing-method/list']), 'placeholder' => 'Select Printing Method', 'multiple' => true, 'options' => $addonEnvelope_printing_method_groups_array],
                    ])->label(Html::customLabel($addonEnvelopeModel->getAttributeLabel('addon_envelope_printing_method'), ['printing-method/create']));
                    ?> 
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerCssFile(
        '@web/css/bootstrap-toggle.min.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]
);
$this->registerJsFile(
        '@web/js/bootstrap-toggle.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>