<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use kartik\select2\Select2;
use yii\helpers\Url;
use backend\helpers\Html;
use common\models\Additions;
?>
<div class="col-md-6">
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Additions</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($additionsModel, 'extra_insert_weight')->textInput(['maxlength' => true, 'placeholder' => "Enter Extra Insert Weight"]); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($additionsModel, 'extra_insert_price')->textInput(['maxlength' => true, 'placeholder' => "Enter Extra Insert Price"]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($additionsModel, 'tassle_id')->widget(Select2::classname(), [
                        'data' => $tassles,
                        'options' => ['data-ajaxUrl' => Url::to(['tassle/list']), 'placeholder' => 'Select Tassle Code'],
                    ])->label(Html::customLabel($additionsModel->getAttributeLabel('tassle_id'), ['tassle/create']));
                    ?> 
                </div>
                <div class="col-md-6">
                    <?=
                    $form->field($additionsModel, 'ribbon_id')->widget(Select2::classname(), [
                        'data' => $ribbons,
                        'options' => ['data-ajaxUrl' => Url::to(['ribbon/list']), 'placeholder' => 'Select Ribbon Code'],
                    ])->label(Html::customLabel($additionsModel->getAttributeLabel('ribbon_id'), ['ribbon/create']));
                    ?> 
                </div>
            </div>

        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Additions</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($additionsModel, 'rhinestone_id')->widget(Select2::classname(), [
                        'data' => $rhinestones,
                        'options' => ['data-ajaxUrl' => Url::to(['rhinestone/list']), 'placeholder' => 'Select Rhinestone Code'],
                    ])->label(Html::customLabel($additionsModel->getAttributeLabel('rhinestone_id'), ['rhinestone/create']));
                    ?> 
                </div>
                <div class="col-md-6">
                    <?= $form->field($additionsModel, 'photocard')->dropDownList(Additions::PHOTOCARD, ['prompt' => 'Select Photocard']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($additionsModel, 'linear')->dropDownList(Additions::LINEAR, ['prompt' => 'Select Liner']) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($additionsModel, 'velvet')->dropDownList(Additions::VELVET, ['prompt' => 'Select Velvet']) ?>
                </div>
            </div>
        </div>
    </div>
</div>


