<?php

use kartik\select2\Select2;
use yii\helpers\Url;
use backend\helpers\Html;
?>
<div class="col-md-6">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Box</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?php
                    $box_paper_groups_array = array ();
                    if ( ! empty($boxPaperModelId)) {
                        foreach ($boxPaperModelId as $boxPaperId) {
                            $box_paper_groups_array[$boxPaperId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($boxModel, 'box_paper')->widget(Select2::classname(), [
                        'data' => $papers,
                        'options' => ['data-ajaxUrl' => Url::to(['paper/list']), 'placeholder' => 'Select Paper Code', 'multiple' => true, 'options' => $box_paper_groups_array],
                    ])->label(Html::customLabel($boxModel->getAttributeLabel('box_paper'), ['paper/create']));
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $box_die_groups_array = array ();
                    if ( ! empty($boxDieModelId)) {
                        foreach ($boxDieModelId as $boxDieId) {
                            $box_die_groups_array[$boxDieId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($boxModel, 'box_die')->widget(Select2::classname(), [
                        'data' => $dies,
                        'options' => ['data-ajaxUrl' => Url::to(['die-cut/list']), 'placeholder' => 'Select Die Code', 'multiple' => true, 'options' => $box_die_groups_array],
                    ])->label(Html::customLabel($boxModel->getAttributeLabel('box_die'), ['die-cut/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($boxModel, 'paper_color')->widget(Select2::classname(), [
                        'data' => $colors,
                        'options' => ['placeholder' => 'Select Paper Color', 'multiple' => true, 'disabled' => true],
                    ]);
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $box_block_groups_array = array ();
                    if ( ! empty($boxBlockModelId)) {
                        foreach ($boxBlockModelId as $boxBlockId) {
                            $box_block_groups_array[$boxBlockId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($boxModel, 'box_block')->widget(Select2::classname(), [
                        'data' => $blocks,
                        'options' => ['data-ajaxUrl' => Url::to(['block/list']), 'placeholder' => 'Select Block Code', 'multiple' => true, 'options' => $box_block_groups_array],
                    ])->label(Html::customLabel($boxModel->getAttributeLabel('box_block'), ['block/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($boxModel, 'paper_type')->widget(Select2::classname(), [
                        'data' => $paperTypes,
                        'options' => ['placeholder' => 'Select Paper Type', 'multiple' => true, 'disabled' => true],
                    ]);
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $box_printing_method_groups_array = array ();
                    if ( ! empty($boxPrintingMethodModelId)) {
                        foreach ($boxPrintingMethodModelId as $boxPrintingMethodId) {
                            $box_printing_method_groups_array[$boxPrintingMethodId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($boxModel, 'box_printing_method')->widget(Select2::classname(), [
                        'data' => $printingMethods,
                        'options' => ['data-ajaxUrl' => Url::to(['printing-method/list']), 'placeholder' => 'Select Printing Method', 'multiple' => true, 'options' => $box_printing_method_groups_array],
                    ])->label(Html::customLabel($boxModel->getAttributeLabel('box_printing_method'), ['printing-method/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($boxModel, 'box_type_id')->widget(Select2::classname(), [
                        'data' => $boxTypes,
                        'options' => ['data-ajaxUrl' => Url::to(['box-type/list']), 'placeholder' => 'Select Box Type'],
                    ])->label(Html::customLabel($boxModel->getAttributeLabel('box_type_id'), ['box-type/create']));
                    ?> 
                </div>
                <div class="col-md-6">
                    <?= $form->field($boxModel, 'length')->textInput(['maxlength' => true, 'placeholder' => "Enter length"]); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($boxModel, 'width')->textInput(['maxlength' => true, 'placeholder' => "Enter Width"]); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($boxModel, 'thickness')->textInput(['maxlength' => true, 'placeholder' => "Enter Thickness"]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
