<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use dosamigos\ckeditor\CKEditor;
?>
<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Terms & Policies</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($contactUsTcModel, "heading")->textInput(['maxlength' => true, 'placeholder' => 'Enter Terms & Condition Heading']) ?>
                </div>
                <div class="col-md-8">
                    <?=
                    $form->field($contactUsTcModel, "description")->widget(CKEditor::className(), [
                        'options' => ['rows' => 2],
                        'preset' => 'standard',
                        'clientOptions' => [
                            'height' => 100,
                            'replaceDivs' => false,
                            'allowedContent' => true,
                        ]
                    ])
                    ?>
                    <?php /* = $form->field($contactUsTcModel, "description")->textarea(['maxlength' => true, 'placeholder' => 'Enter Terms & Condition Description', 'rows' => '5']) */ ?> 
                </div>
            </div>
        </div>
    </div>
</div>
