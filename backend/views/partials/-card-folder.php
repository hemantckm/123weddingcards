<?php

use kartik\select2\Select2;
use yii\helpers\Url;
use backend\helpers\Html;
?>
<div class="col-md-6">
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Folder / Invite</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?php
                    $folder_paper_groups_array = array ();
                    if ( ! empty($folderPaperModelId)) {
                        foreach ($folderPaperModelId as $folderPaperId) {
                            $folder_paper_groups_array[$folderPaperId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($folderModel, 'folder_paper')->widget(Select2::classname(), [
                        'data' => $papers,
                        'options' => ['data-ajaxUrl' => Url::to(['paper/list']), 'placeholder' => 'Select Paper Code', 'multiple' => true, 'options' => $folder_paper_groups_array],
                    ])->label(Html::customLabel($folderModel->getAttributeLabel('folder_paper'), ['paper/create']));
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $folder_die_groups_array = array ();
                    if ( ! empty($folderDieModelId)) {
                        foreach ($folderDieModelId as $folderDieId) {
                            $folder_die_groups_array[$folderDieId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($folderModel, 'folder_die')->widget(Select2::classname(), [
                        'data' => $dies,
                        'options' => ['data-ajaxUrl' => Url::to(['die-cut/list']), 'placeholder' => 'Select Die Code', 'multiple' => true, 'options' => $folder_die_groups_array],
                    ])->label(Html::customLabel($folderModel->getAttributeLabel('folder_die'), ['die-cut/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($folderModel, 'paper_color')->widget(Select2::classname(), [
                        'data' => $colors,
                        'options' => ['placeholder' => 'Select Paper Color', 'multiple' => true, 'disabled' => true],
                    ]);
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $folder_block_groups_array = array ();
                    if ( ! empty($folderBlockModelId)) {
                        foreach ($folderBlockModelId as $folderBlockId) {
                            $folder_block_groups_array[$folderBlockId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($folderModel, 'folder_block')->widget(Select2::classname(), [
                        'data' => $blocks,
                        'options' => ['data-ajaxUrl' => Url::to(['block/list']), 'placeholder' => 'Select Block Code', 'multiple' => true, 'options' => $folder_block_groups_array],
                    ])->label(Html::customLabel($folderModel->getAttributeLabel('folder_block'), ['block/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($folderModel, 'paper_type')->widget(Select2::classname(), [
                        'data' => $paperTypes,
                        'options' => ['placeholder' => 'Select Paper Type', 'multiple' => true, 'disabled' => true],
                    ]);
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $folder_printing_method_groups_array = array ();
                    if ( ! empty($folderPrintingMethodModelId)) {
                        foreach ($folderPrintingMethodModelId as $folderPrintingMethodId) {
                            $folder_printing_method_groups_array[$folderPrintingMethodId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($folderModel, 'folder_printing_method')->widget(Select2::classname(), [
                        'data' => $printingMethods,
                        'options' => ['data-ajaxUrl' => Url::to(['printing-method/list']), 'placeholder' => 'Select Printing Method', 'multiple' => true, 'options' => $folder_printing_method_groups_array],
                    ])->label(Html::customLabel($folderModel->getAttributeLabel('folder_printing_method'), ['printing-method/create']));
                    ?> 
                </div>
            </div>
        </div>
    </div>
</div>