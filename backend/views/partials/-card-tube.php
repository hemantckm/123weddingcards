<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Tube</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($tubeModel, 'length')->textInput(['maxlength' => true, 'placeholder' => "Enter Length"]); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($tubeModel, 'weight')->textInput(['maxlength' => true, 'placeholder' => "Enter Weight"]); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($tubeModel, 'diameter')->textInput(['maxlength' => true, 'placeholder' => "Enter Diameter"]); ?>
            </div>
        </div>
    </div>
</div>