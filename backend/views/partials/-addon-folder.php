<?php

use kartik\select2\Select2;
use yii\helpers\Url;
use backend\helpers\Html;
?>
<div class="col-md-6">
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Folder</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?php
                    $addonFolder_paper_groups_array = array ();
                    if ( ! empty($addonFolderPaperModelId)) {
                        foreach ($addonFolderPaperModelId as $addonFolderPaperId) {
                            $addonFolder_paper_groups_array[$addonFolderPaperId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($addonFolderModel, 'addon_folder_paper')->widget(Select2::classname(), [
                        'data' => $papers,
                        'options' => ['data-ajaxUrl' => Url::to(['paper/list']), 'placeholder' => 'Select Paper Code', 'multiple' => true, 'options' => $addonFolder_paper_groups_array],
                    ])->label(Html::customLabel($addonFolderModel->getAttributeLabel('addon_folder_paper'), ['paper/create']));
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $addonFolder_die_groups_array = array ();
                    if ( ! empty($addonFolderDieModelId)) {
                        foreach ($addonFolderDieModelId as $addonFolderDieId) {
                            $addonFolder_die_groups_array[$addonFolderDieId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($addonFolderModel, 'addon_folder_die')->widget(Select2::classname(), [
                        'data' => $dies,
                        'options' => ['data-ajaxUrl' => Url::to(['die-cut/list']), 'placeholder' => 'Select Die Code', 'multiple' => true, 'options' => $addonFolder_die_groups_array],
                    ])->label(Html::customLabel($addonFolderModel->getAttributeLabel('addon_folder_die'), ['die-cut/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($addonFolderModel, 'paper_color')->widget(Select2::classname(), [
                        'data' => $colors,
                        'options' => ['placeholder' => 'Select Paper Color', 'multiple' => true, 'disabled' => true],
                    ]);
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $addonFolder_block_groups_array = array ();
                    if ( ! empty($addonFolderBlockModelId)) {
                        foreach ($addonFolderBlockModelId as $addonFolderBlockId) {
                            $addonFolder_block_groups_array[$addonFolderBlockId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($addonFolderModel, 'addon_folder_block')->widget(Select2::classname(), [
                        'data' => $blocks,
                        'options' => ['data-ajaxUrl' => Url::to(['block/list']), 'placeholder' => 'Select Block Code', 'multiple' => true, 'options' => $addonFolder_block_groups_array],
                    ])->label(Html::customLabel($addonFolderModel->getAttributeLabel('addon_folder_block'), ['block/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($addonFolderModel, 'paper_type')->widget(Select2::classname(), [
                        'data' => $paperTypes,
                        'options' => ['placeholder' => 'Select Paper Type', 'multiple' => true, 'disabled' => true],
                    ]);
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $addonFolder_printing_method_groups_array = array ();
                    if ( ! empty($addonFolderPrintingMethodModelId)) {
                        foreach ($addonFolderPrintingMethodModelId as $addonFolderPrintingMethodId) {
                            $addonFolder_printing_method_groups_array[$addonFolderPrintingMethodId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($addonFolderModel, 'addon_folder_printing_method')->widget(Select2::classname(), [
                        'data' => $printingMethods,
                        'options' => ['data-ajaxUrl' => Url::to(['printing-method/list']), 'placeholder' => 'Select Printing Method', 'multiple' => true, 'options' => $addonFolder_printing_method_groups_array],
                    ])->label(Html::customLabel($addonFolderModel->getAttributeLabel('addon_folder_printing_method'), ['printing-method/create']));
                    ?> 
                </div>
            </div>
        </div>
    </div>
</div>