<?php

use kartik\select2\Select2;
use yii\helpers\Url;
use backend\helpers\Html;
?>
<div class="col-md-6">
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Envelope</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?php
                    $envelope_paper_groups_array = array ();
                    if ( ! empty($envelopePaperModelId)) {
                        foreach ($envelopePaperModelId as $envelopePaperId) {
                            $envelope_paper_groups_array[$envelopePaperId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($envelopeModel, 'envelope_paper')->widget(Select2::classname(), [
                        'data' => $papers,
                        'options' => ['data-ajaxUrl' => Url::to(['paper/list']), 'placeholder' => 'Select Paper Code', 'multiple' => true, 'options' => $envelope_paper_groups_array],
                    ])->label(Html::customLabel($envelopeModel->getAttributeLabel('envelope_paper'), ['paper/create']));
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $envelope_die_groups_array = array ();
                    if ( ! empty($envelopeDieModelId)) {
                        foreach ($envelopeDieModelId as $envelopeDieId) {
                            $envelope_die_groups_array[$envelopeDieId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($envelopeModel, 'envelope_die')->widget(Select2::classname(), [
                        'data' => $dies,
                        'options' => ['data-ajaxUrl' => Url::to(['die-cut/list']), 'placeholder' => 'Select Die Code', 'multiple' => true, 'options' => $envelope_die_groups_array],
                    ])->label(Html::customLabel($envelopeModel->getAttributeLabel('envelope_die'), ['die-cut/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($envelopeModel, 'paper_color')->widget(Select2::classname(), [
                        'data' => $colors,
                        'options' => ['placeholder' => 'Select Paper Color', 'multiple' => true, 'disabled' => true],
                    ]);
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $envelope_block_groups_array = array ();
                    if ( ! empty($envelopeBlockModelId)) {
                        foreach ($envelopeBlockModelId as $envelopeBlockId) {
                            $envelope_block_groups_array[$envelopeBlockId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($envelopeModel, 'envelope_block')->widget(Select2::classname(), [
                        'data' => $blocks,
                        'options' => ['data-ajaxUrl' => Url::to(['block/list']), 'placeholder' => 'Select Block Code', 'multiple' => true, 'options' => $envelope_block_groups_array],
                    ])->label(Html::customLabel($envelopeModel->getAttributeLabel('envelope_block'), ['block/create']));
                    ?> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($envelopeModel, 'paper_type')->widget(Select2::classname(), [
                        'data' => $paperTypes,
                        'options' => ['placeholder' => 'Select Paper Type', 'multiple' => true, 'disabled' => true],
                    ]);
                    ?> 
                </div>
                <div class="col-md-6">
                    <?php
                    $envelope_printing_method_groups_array = array ();
                    if ( ! empty($envelopePrintingMethodModelId)) {
                        foreach ($envelopePrintingMethodModelId as $envelopePrintingMethodId) {
                            $envelope_printing_method_groups_array[$envelopePrintingMethodId] = array ("selected" => true);
                        }
                    }
                    ?>
                    <?=
                    $form->field($envelopeModel, 'envelope_printing_method')->widget(Select2::classname(), [
                        'data' => $printingMethods,
                        'options' => ['data-ajaxUrl' => Url::to(['printing-method/list']), 'placeholder' => 'Select Printing Method', 'multiple' => true, 'options' => $envelope_printing_method_groups_array],
                    ])->label(Html::customLabel($envelopeModel->getAttributeLabel('envelope_printing_method'), ['printing-method/create']));
                    ?> 
                </div>
            </div>
        </div>

        <?php if ($envelopeModel->isNewRecord) { ?>
            <!---- Copy Content Sections ---->
            <div class="box-header with-border"></div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <?=
                        Html::label('copy content for other sections');
                        ?>
                    </div>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <?=
                            Html::checkbox('card_envelope_info', false, [
                                'id' => 'card_envelope_info',
                                'data-toggle' => 'toggle',
                                'data-on' => 'Yes',
                                'data-off' => 'No',
                                'data-size' => 'mini',
                                'data-onstyle' => 'success',
                                'data-offstyle' => 'danger',
                                'disabled' => true,
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php
$this->registerCssFile(
        '@web/css/bootstrap-toggle.min.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]
);
$this->registerJsFile(
        '@web/js/bootstrap-toggle.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>