<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title                   = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="changepassword box box-primary">
    <div class="box-header with-border">
        <?php
        $form                          = ActiveForm::begin([
                    'id' => 'changepassword-form',
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-3\">
                        {input}</div>\n<div class=\"col-lg-5\">
                        {error}</div>",
                        'labelOptions' => ['class' => 'col-lg-2 control-label'],
                    ],
        ]);
        ?>

        <?=
        $form->field($model, 'oldpassword', ['inputOptions' => [
                'placeholder' => 'Old Password', 'class' => 'form-control'
    ]])->passwordInput()
        ?>

        <?=
        $form->field($model, 'newpassword', ['inputOptions' => [
                'placeholder' => 'New Password', 'class' => 'form-control'
    ]])->passwordInput()
        ?>

        <?=
        $form->field($model, 'repeatnewpassword', ['inputOptions' => [
                'placeholder' => 'Repeat New Password', 'class' => 'form-control'
    ]])->passwordInput()
        ?>

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-11">
                <?=
                Html::submitButton('Change password', [
                    'class' => 'btn btn-primary'
                ])
                ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
