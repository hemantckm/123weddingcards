<?php

use yii\helpers\Html;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->title                   = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?> 
<div class="changepassword  box box-body box-primary">
    <div class="box-header with-border"> 
        <div class="row">
            <div class="col-md-4">  </div>
            <div class="col-md-3">  
                <div class="box-body box-profile">

                    <?= Html::a(Html::img(Yii::getAlias('@web') . $userImages['small'], ['alt' => 'User profile picture', 'class' => 'profile-user-img img-responsive img-circle']), Yii::getAlias('@web') . $userImages['full'], ['data-fancybox' => 'gallery']) ?>

                    <h3 class="profile-username text-center"><?php
                        if ( ! empty(Yii::$app->user->identity)) {
                            echo ucfirst(Yii::$app->user->identity->username);
                        }
                        ?></h3>
                    <p class="text-muted text-center">
                        <?php
                        if ( ! empty(Yii::$app->user->identity)) {
                            echo User::getConstantList('MASTER_ADMIN_', User::className())[Yii::$app->user->identity->master_admin];
                        }
                        ?>
                    </p>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>E-Mail</b> <a class="pull-right"><?php
                                if ( ! empty(Yii::$app->user->identity)) {
                                    echo Yii::$app->user->identity->email;
                                }
                                ?></a>
                        </li> 
                    </ul>
                    <?= Html::a('Change Password', ['/user/change-password'], ['data-method' => 'post', 'class' => 'btn btn-primary btn-block']) ?> 
                    <?= Html::a('Update Profile', ['/user/update', 'id' => Yii::$app->user->identity->id], ['class' => 'btn btn-success btn-block']) ?>
                </div> 
            </div> 
        </div>
        <!-- /.row -->  
    </div>
</div>
<?php
$this->registerJsFile(
        '@web/js/jquery.fancybox.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerCssFile(
        '@web/css/jquery.fancybox.min.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]
);
?>
