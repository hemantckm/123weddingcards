<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
$readOnly = true;
if ($model->isNewRecord) {
    $readOnly = false;
}
?>

<div class="user-form box-body"> 
    <?php $form = ActiveForm::begin(['id' => 'user-form', 'options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data']]); ?>
    <div class="box-body">  
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'readonly' => $readOnly, 'placeholder' => "Enter Username"]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => "Enter E-mail"]) ?>
            </div> 
            <div class="col-md-6">
                <?= $form->field($model, "image")->fileInput(['class' => 'form-control', 'accept' => 'image/*', 'maxlength' => true]) ?> 
            </div> 
        </div>
        <div class="row">
            <?php if ($model->isNewRecord || Yii::$app->user->identity->master_admin == User::MASTER_ADMIN_SUPER) { ?>
                <div class="col-md-3">
                    <?= $form->field($model, 'password')->passwordInput(['placeholder' => "Enter Password"]); ?> 
                </div> 
                <div class="col-md-3">
                    <?= $form->field($model, 'retypePassword')->passwordInput(['placeholder' => "Enter Confirm Password"])->label("Confirm Password"); ?>
                </div> 
            <?php } ?>
            <div class="col-md-3">
                <?= $form->field($model, 'status')->dropDownList(User::getConstantList('STATUS_', User::className())) ?>
            </div>
            <div class="col-md-3"></div>
            <!--
            <div class="col-md-3">
            <?= $form->field($model, 'master_admin')->dropDownList(User::getConstantList('MASTER_ADMIN', User::className())) ?>
            </div>-->

            <!-- Show Image -->
            <?php if ( ! empty($userImages)) { ?>
                <div class="col-md-3">
                    <?= Html::a(Html::img(Yii::getAlias('@web') . $userImages['small'], ['class' => 'img-thumbnail']), Yii::getAlias('@web') . $userImages['full'], ['data-fancybox' => 'gallery']); ?>
                </div> 
            <?php } ?>

        </div>
    </div> 
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJsFile(
        '@web/js/jquery.fancybox.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerCssFile(
        '@web/css/jquery.fancybox.min.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]
);
?>
