<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-body table-responsive box-primary"> 
    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p> 

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'email:email',
            [
                'label' => 'Master Admin',
                'attribute' => 'master_admin',
                'filter' => Html::activeDropDownList($searchModel, 'master_admin', User::getConstantList('MASTER_ADMIN_', User::className()), ['class' => 'form-control', 'prompt' => 'Master Admin']),
                'value' => function ($model, $key, $index) {
                    return User::getConstantList('MASTER_ADMIN_', User::className())[$model->master_admin];
                },
            ],
            [
                'label' => 'Status',
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'status', User::getConstantList('STATUS_', User::className()), ['class' => 'form-control', 'prompt' => 'Status']),
                'value' => function ($model, $key, $index) {
                    if ($model->status == User::STATUS_ACTIVE) {
                        return '<span class="label label-success">' . User::getConstantList('STATUS_', User::className())[$model->status] . '</span>';
                    } else {
                        return '<span class="label label-danger">' . User::getConstantList('STATUS_', User::className())[$model->status] . '</span>';
                    }
                },
            ],
           // 'created_at:date',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:11%;'],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(
                                        '<span class="glyphicon glyphicon-eye-open"></span>', ['user/view', 'id' => $model->id], ['title' => Yii::t('app', 'View User'), 'class' => 'btn btn-info margin-r-5']
                        );
                    },
                    'update' => function ($url, $model) {
                        return Html::a(
                                        '<span class="glyphicon glyphicon-pencil"></span>', ['user/update', 'id' => $model->id], ['title' => Yii::t('app', 'Update User'), 'class' => 'btn btn-primary margin-r-5']
                        );
                    },
                    'delete' => function ($url, $model) {
                        if (\Yii::$app->user->identity->master_admin) {
                            return Html::a(
                                            '<span class="glyphicon glyphicon-trash"></span>', ['user/delete', 'id' => $model->id], ['title' => Yii::t('app', 'Delete User'),
                                        'aria-label' => 'Delete',
                                        'data-pjax' => '0',
                                        'data-confirm' => "Are you sure you want to delete this item?",
                                        'data-method' => "post",
                                        'class' => 'btn btn-danger margin-r-5']
                            );
                        }
                    },
                ],
            ],
        ],
    ]);
    ?>


</div>
