<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title                   = ucfirst($model->username);
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<p>
    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
</p>
<div class="user-view box box-body box-primary">  
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'email:email',
            [
                'attribute' => 'image',
                'format' => ['raw'],
                'value' => Html::a(Html::img(Yii::getAlias('@web') . $userImages['small'], ['class' => 'img-thumbnail']), Yii::getAlias('@web') . $userImages['full'], ['data-fancybox' => 'gallery']),
                'label' => 'Image',
            ],
            [
                'attribute' => 'master_admin',
                'value' => User::getConstantList('MASTER_ADMIN_', User::className())[$model->master_admin],
                'label' => 'Master Admin',
            ],
            [
                'attribute' => 'status',
                'value' => User::getConstantList('STATUS_', User::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>

</div>

<?php
$this->registerJsFile(
        '@web/js/jquery.fancybox.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerCssFile(
        '@web/css/jquery.fancybox.min.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]
);
?>
