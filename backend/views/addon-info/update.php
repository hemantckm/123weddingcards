<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AddonInfo */

$this->title = 'Update Addon Info: ' . $model->addon_id;
$this->params['breadcrumbs'][] = ['label' => 'Addon Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->addon_id, 'url' => ['view', 'id' => $model->addon_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="addon-info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
