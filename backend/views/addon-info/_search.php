<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\AddonInfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="addon-info-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'addon_id') ?>

    <?= $form->field($model, 'card_id') ?>

    <?= $form->field($model, 'addon_type') ?>

    <?= $form->field($model, 'addon_style') ?>

    <?= $form->field($model, 'envelope') ?>

    <?php // echo $form->field($model, 'folder') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
