<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AddonInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="addon-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'addon_id')->textInput() ?>

    <?= $form->field($model, 'card_id')->textInput() ?>

    <?= $form->field($model, 'addon_type')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'addon_style')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'envelope')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'folder')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
