<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AddonInfo */

$this->title = 'Create Addon Info';
$this->params['breadcrumbs'][] = ['label' => 'Addon Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="addon-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
