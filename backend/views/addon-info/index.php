<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AddonInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Addon Infos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="addon-info-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Addon Info', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'addon_id',
            'card_id',
            'addon_type:ntext',
            'addon_style:ntext',
            'envelope:ntext',
            //'folder:ntext',
            //'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
