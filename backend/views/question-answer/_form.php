<?php

use backend\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\QuestionAnswer;

/* @var $this yii\web\View */
/* @var $model common\models\QuestionAnswer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="question-answer-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'question')->textInput(['maxlength' => true, 'placeholder' => 'Enter Question']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'answer')->textarea(['maxlength' => true, 'placeholder' => 'Enter Answer', 'rows' => '5']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList(QuestionAnswer::getConstantList('STATUS_', QuestionAnswer::className())) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerCssFile(
        '@web/css/custom.css', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]
);
$this->registerJsFile(
        '@web/js/refresh.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>