<?php
/* @var $this yii\web\View */
/* @var $model common\models\QuestionAnswer */

$this->title                   = 'Create Question Answer';
$this->params['breadcrumbs'][] = ['label' => 'Question Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-answer-create box box-primary">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
