<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\QuestionAnswer;

/* @var $this yii\web\View */
/* @var $model common\models\QuestionAnswer */

$this->title                   = $model->question;
$this->params['breadcrumbs'][] = ['label' => 'Question Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php if ($model->status != QuestionAnswer::STATUS_DELETE) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php } ?>
<div class="question-answer-view box box-body box-primary">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'question',
            'answer',
            [
                'attribute' => 'status',
                'value' => QuestionAnswer::getConstantList('STATUS_', QuestionAnswer::className())[$model->status],
                'label' => $model->getAttributeLabel('status'),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>
</div>
