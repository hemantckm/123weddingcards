<?php

use floor12\backup\models\BackupType;
use \yii\web\Request;

$params = array_merge(
        require __DIR__ . '/../../common/config/params.php', require __DIR__ . '/../../common/config/params-local.php', require __DIR__ . '/params.php', require __DIR__ . '/params-local.php'
);

//$baseUrl = str_replace('/backend/web', '', (new Request)->getBaseUrl());
$baseUrl = (new Request)->getBaseUrl();
return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'name' => '123WeddingCards', //Site Name
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'left-menu'
        ],
        'backup' => [
            'class' => 'floor12\backup\Module',
            'administratorRoleName' => '@',
            'configs' => [
                [
                    'id' => 'main_db',
                    'type' => BackupType::DB,
                    'title' => 'Main database',
                    'connection' => 'db',
                    'limit' => 0
                ],
//                [
//                    'id' => 'main_storage',
//                    'type' => BackupType::FILES,
//                    'title' => 'TMP folder',
//                    'path' => '@app/tmp',
//                    'limit' => 0
//                ]
            ]
        ]
    ],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-blue',
                ],
            ],
        ],
        'formatter' => [
            'dateFormat' => 'php:d M, Y',
            'datetimeFormat' => 'php:d M, Y h:i:s a',
            'thousandSeparator' => ',',
            'currencyCode' => 'INR',
            'defaultTimeZone' => 'Asia/Calcutta'
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
            'enableCsrfValidation' => false,
            'baseUrl' => $baseUrl,
        //'baseUrl' => '/123weddingcards/admin', // localhost/123weddingcards/admin
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'authTimeout' => 60 * 60 * 12, // 12 Hours
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'PHPBACKSESSID',
            'timeout' => 60 * 60 * 12
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'authManager' => [
            'class' => 'mdm\admin\components\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'rules' => [
                '' => 'site/index',
                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
            /* '<controller:\w+>/<id:\d+>' => '<controller>/view',
              '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
              '<controller:\w+>/<action:\w+>' => '<controller>/<action>', */
            ],
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
            //'user/*',
            //'admin/*',
            'some-controller/some-action',
        // The actions listed here will be allowed to everyone including guests.
        // So, 'admin/*' should not appear here in the production, of course.
        // But in the earlier stages of your development, you may probably want to
        // add a lot of actions here until you finally completed setting up rbac,
        // otherwise you may not even take a first step.
        ]
    ],
    'params' => $params,
];
